//
//  Account.swift
//  SplayGameSDK
//
//  Created by Nguyen Nang on 6/10/21.
//  Copyright © 2021 splay. All rights reserved.
//

import Foundation

@objc public class Account: NSObject {
    @objc public var accountID: String = ""
    @objc public var accountName: String = ""
    @objc public var accessToken: String = ""
    
    
    public override init() {
        
    }
    
    public init(accountID: String, accountName: String, accessToken: String) {
        self.accountID      = accountID
        self.accountName    = accountName
        self.accessToken    = accessToken
    }
    
    
    public init(data: [String : Any]) {
        self.accountID      = data[KEY_USER_ID] as? String ?? ""
        self.accountName    = data[KEY_USERNAME] as? String ?? ""
        self.accessToken    = data[KEY_ACCESS_TOKEN] as? String ?? ""
    }
}

