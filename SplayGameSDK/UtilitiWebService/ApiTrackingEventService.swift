//
//  ApiTrackingEventService.swift
//  SplayGameSDK
//
//  Created by Nguyen Nang on 8/6/20.
//  Copyright © 2020 splay. All rights reserved.
//

import Foundation
import UIKit
import WebKit
import SystemConfiguration.CaptiveNetwork
import CoreTelephony
import AdSupport
import AppsFlyerLib
import CleanroomLogger

typealias LiteAPIsCompletion = (Any?, Error?, ResultCode) -> Void

class ApiTrackingEventService: NSObject {
    

    static var shareSingletonVar: ApiTrackingEventService?

    class func shareSingleton() -> ApiTrackingEventService? {
        let lockQueue = DispatchQueue(label: "self")
        lockQueue.sync {
            if shareSingletonVar == nil {
                shareSingletonVar = ApiTrackingEventService()
            }
        }
        
        return shareSingletonVar
    }
    
    func checkNetworkCompletion(_ completion: LiteAPIsCompletion?) -> Bool {
        
        guard let networkStatus =  Reachability.forInternetConnection()?.currentReachabilityStatus() else { return false }
        guard networkStatus == NotReachable else { return true }
        if let completion = completion {
            let error = NSError(domain: "NetworkUnAvaiable", code: Int(ERROR_CODE_NETWORK_UNAVAIABLE), userInfo: nil)
            completion(nil, error, ResultCodeNetWorkError)
        }
        return false
    }
    

    func getUrlBaseConfigApp() -> String {

        if let typeConfig = Bundle.main.object(forInfoDictionaryKey: "Sandbox") as? Bool, typeConfig {
            return "http://sandbox.graph.mobiplay.vn"
        } else {
//            return "https://graph.mobiplay.vn"
            guard let config = AppData.instance.config, config.apiRelease.count > 0,
                  let _ = URL(string: config.apiRelease) else { return "https://graph.mobiplay.vn" }
            return config.apiRelease
        }
    }
    

    
    func requestWithUrlString(_ urlString: String, mothod: MethodType, params: Any?, completion: LiteAPIsCompletion?) {
        //        DVLog(@"paramater = %@, url = %@, method = %d", params,urlString,method);
        
        Log.message(.info, message: "paramater = \(String(describing: params)) | url = \(urlString) | method = \(String(describing: method))")
        
        guard checkNetworkCompletion(completion) else {

            Log.message(.error, message: "______mat mang roi nhe!")
            Utility.showAlertWithMessage(LanguageSetting.get("MessagerNetworkResult", alter: nil), viewController: nil)
            return
        }
        
        let manager = AFHTTPSessionManager()
        manager.requestSerializer.timeoutInterval = 25
        manager.responseSerializer = AFHTTPResponseSerializer()
        
        switch mothod {
        case MethodTypePOST:
            manager.post(urlString, parameters: params, progress: nil, success: { (operation: URLSessionDataTask, responseObject: Any?) in
                
                Log.message(.info, message: "data responseObject = \(String(describing: responseObject))")
                
                
                guard let completion = completion else { return }
                
                if let response = responseObject as? [String: Any] {
                    completion(response, nil, ResultCodeSuccess)
                    return
                }
                
                do {
                    guard let data = responseObject as? Data,
                        let response = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String : Any] else {
                            
                            completion(responseObject, nil, ResultCodeDataError)
                            return
                    }
                    
                    
                    Log.message(.info, message: "data result = \(response)")
                    let resultCode = response[KEY_ERROR_CODE] as? Int ?? 0
                    completion(response, nil, ResultCode(rawValue: Int32(resultCode)))
                    
                    
                } catch {
                    completion(responseObject, nil, RequestCodeUnknowError)
                }
              
                
            }, failure: { (operation: URLSessionDataTask?, error: Error) in
                Log.message(.error, message: "POST fails with error \(error)")
                
                guard let completion = completion else { return }
                
                
                let userinfo1 = (error as NSError).userInfo
                Log.message(.error, message: "data userinfo1 = \(userinfo1)")
                completion(userinfo1,error,RequestCodeUnknowError)
            })
            
        default:
            manager.get(urlString, parameters: params, progress: nil, success: { (operation: URLSessionDataTask, responseObject: Any?) in
                
                
                
                Log.message(.info, message: "data responseObject = \(String(describing: responseObject))")
                
                guard let completion = completion else { return }
                
                if let response = responseObject as? [String: Any] {
                    completion(response, nil, ResultCodeSuccess)
                    return
                }

                do {
                    guard let data = responseObject as? Data,
                        let response = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String : Any] else {
                            
                            completion(responseObject, nil, ResultCodeDataError)
                            return
                    }
                    let resultCode = response[KEY_ERROR_CODE] as? Int ?? 0
                    completion(response, nil, ResultCode(rawValue: Int32(resultCode)))
                } catch {
                    completion(responseObject, nil, RequestCodeUnknowError)
                }
                
            }, failure: { (operation: URLSessionDataTask?, error: Error?) in
                        

                Log.message(.error, message: "Error: \(String(describing: error))")
                guard let completion = completion else { return }
                
                guard let errorNew = error else {
                    completion(nil, nil, RequestCodeUnknowError)
                    return
                }
                
                let userinfo1 = (errorNew as NSError).userInfo
                Log.message(.error, message: "data userinfo1 = \(userinfo1)")
                completion(userinfo1,errorNew,ResultCodeDataError)
                
                
                if let innerError = userinfo1["NSUnderlyingError"] as? Error {
                    
                    let innerUserInfo = (innerError as NSError).userInfo
                    
                    guard let object = innerUserInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] as? Data else { return }
                    guard let error1 = String(data: object, encoding: .utf8) else { return }
                    Log.message(.error, message: "Error is : \(error1)")
                    
                } else {
                    
                    guard let value = userinfo1[AFNetworkingOperationFailingURLResponseDataErrorKey] as? Data else { return }
                    guard let error1 = String(data: value, encoding: .utf8) else { return }
                    Log.message(.error, message: "\(error1)")
                }
                
            })
        }
    }
    
    
    
    func sendEventApiTracking(withOrignalUrl orignalUrl: String? = nil,
                                            eventName: String?,
                                            eventParams: [String : Any]? = nil,
                                            httpReferrer: String? = nil,
                                            googlePlayerReferrer: String? = nil,
                                            deepLink: String? = nil,
                                            completion: LiteAPIsCompletion? = nil) {
        
       
   
        let userAgent   = WKWebView(frame: .zero).customUserAgent ?? ""
        let url         = String(format: "%@%@", Utility.getUrlApiTracking(), API_TRACKING_EVENT)
        
        let userDefault = UserDefaults.standard
        let userName    = userDefault.object(forKey: KEY_SAVE_USERNAME) as? String ?? ""
        let userId      = userDefault.object(forKey: KEY_SAVE_USERID) as? String ?? ""
        let agencyIDApp = userDefault.object(forKey: KEY_AGENCY_APP) as? String ?? ""
        let apiKeyApp   = userDefault.object(forKey: KEY_API_CONFIG_APP) as? String ?? ""
        let osVersion   = UIDevice.current.systemVersion
        let ipAddress   = Utility.externalIPAddress()
        let carrierName = CTTelephonyNetworkInfo().subscriberCellularProvider?.carrierName ?? ""
        let deviceOs    = "IOS"
        let idFA        = ASIdentifierManager.shared().advertisingIdentifier.uuidString
        let appsFlyerUID = AppsFlyerLib.shared().getAppsFlyerUID()
        let bundleIdentifier = userDefault.object(forKey: KEY_APPID) as? String ?? ""
        let wifiStatus  = Utility.checkIfUsingWifi() ? "true" : "false"
        
        do {
            
            let eventData   = try JSONSerialization.data(withJSONObject: eventParams ?? [:], options: .fragmentsAllowed)
            let eventValue  = String(data: eventData, encoding: .utf8)

            let parameters: [String : Any] = ["accountId": userId,
                                              "accountName":userName,
                                              "platform": deviceOs,
                                              "deviceType": UIDevice.modelName,
                                              "osVersion": osVersion,
                                              "userAgent": userAgent,
                                              "clientIp": ipAddress,
                                              "wifi": wifiStatus,
                                              "operatorName": "",
                                              "carrier": carrierName,
                                              "iDFA": idFA,
                                              "api_key": apiKeyApp,
                                              "agency_id": agencyIDApp,
                                              "orignalUrl": orignalUrl ?? "",
                                              "httpReferrer": httpReferrer ?? "",
                                              "googlePlayerReferrer": googlePlayerReferrer ?? "",
                                              "deepLink": deepLink ?? "",
                                              "eventName": eventName ?? "",
                                              "eventValue": eventValue ?? "",
                                              "appflyersId": appsFlyerUID,
                                              "packageName": bundleIdentifier]
            
            
            let jsonData = try JSONSerialization.data(withJSONObject: parameters, options: .fragmentsAllowed)
            let jsonString = String(data: jsonData, encoding: .utf8)
            
            let manager = AFURLSessionManager(sessionConfiguration: .default)
            var req = AFJSONRequestSerializer().request(withMethod: "POST", urlString: url, parameters: nil, error: nil) as URLRequest
            req.setValue("application/json", forHTTPHeaderField: "Content-Type")
            req.setValue(apiKeyApp, forHTTPHeaderField: "ApiKey")
            req.httpBody = jsonString?.data(using: .utf8)
            
            
            Log.message(.info, message: "UPLOAD params = \(parameters)")
            let task = manager.dataTask(with: req) { (response, responseObject, error) -> Void in
                       
                       if error == nil {
                           
                           Log.message(.info, message: "Reply JSON: \(String(describing: responseObject))")
                           guard let completion = completion else { return }
                           
                           if let response = responseObject as? [String: Any] {
                               let resultCode = response[KEY_ERROR_CODE] as? Int ?? 0
                               completion(response, nil, ResultCode(rawValue: Int32(resultCode)))
                               return
                           }
                           
                           do {
                               
                               guard let data = responseObject as? Data,
                                   let response = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String : Any] else {
                                       
                                       completion(responseObject, nil, ResultCodeDataError)
                                       return
                               }
                               
                               Log.message(.info, message: "data result = \(response)")
                               
                               let resultCode = response[KEY_ERROR_CODE] as? Int ?? 0
                               completion(response, nil, ResultCode(rawValue: Int32(resultCode)))
                               
                           } catch {
                               completion(nil, error, RequestCodeUnknowError)
                           }
                           
                           
                           
                       } else {
                           
                           Log.message(.error, message: "Error: \(String(describing: error)), \(response), \(String(describing: responseObject))")
                           guard let completion = completion else { return }
                           
                           guard let errorNew = error else {
                               completion(nil, nil, RequestCodeUnknowError)
                               return
                           }
                           
                           let userinfo1 = (errorNew as NSError).userInfo
                           Log.message(.error, message: "data userinfo1 = \(userinfo1)")
                           completion(userinfo1,errorNew,ResultCodeDataError)
                           
                           
                           if let innerError = userinfo1["NSUnderlyingError"] as? Error {
                               
                               let innerUserInfo = (innerError as NSError).userInfo
                               
                               guard let object = innerUserInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] as? Data else { return }
                               guard let error1 = String(data: object, encoding: .utf8) else { return }
                               Log.message(.error, message: "Error is : \(error1)")
                               
                           } else {
                               
                               guard let value = userinfo1[AFNetworkingOperationFailingURLResponseDataErrorKey] as? Data else { return }
                               guard let error1 = String(data: value, encoding: .utf8) else { return }
                               Log.message(.error, message: "\(error1)")
                           }
                       }
                       
                   }
                   
                   task.resume()
            
        } catch {
            
            Log.message(.error, message: "Error Try sendEventApiTracking: \(error)")
            
            guard let completion = completion else { return }
            completion(nil, error, RequestCodeUnknowError)
        }
    }
}
