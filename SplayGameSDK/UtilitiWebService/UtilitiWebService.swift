//
//  UtilitiWebService.swift
//  SplayGameSDK
//
//  Created by Nguyen Nang on 9/3/20.
//  Copyright © 2020 splay. All rights reserved.
//

import Foundation
import UserNotifications
import CleanroomLogger
import WebKit
import SystemConfiguration.CaptiveNetwork
import CoreTelephony
import AdSupport
import AppsFlyerLib
import Firebase
import FBSDKCoreKit
import AdBrixRM


class UtilitieWebService: NSObject {
    
    
    var backUpUrl: String {
        
        guard let config = AppData.instance.config, config.apiBackup.count > 0,
              let _ = URL(string: config.apiBackup) else { return "https://graph.vtcmobile.com" }
        return config.apiBackup
    }
    
    
    static var shareSingletonVar: UtilitieWebService?
    
    class func shareSingleton() -> UtilitieWebService? {
        let lockQueue = DispatchQueue(label: "self")
        lockQueue.sync {
            if shareSingletonVar == nil {
                shareSingletonVar = UtilitieWebService()
            }
        }
        
        return shareSingletonVar
    }
    
    func getImageFromPathWithStringName(_ stringName: String) -> UIImage? {
        
        guard let path = Bundle.main.path(forResource: stringName, ofType: "txt") else { return nil }
        return UIImage(contentsOfFile: path)
    }
    
    func getImageFromStringBase(_ stringBase: String) -> UIImage? {
        
        guard let url = URL(string: stringBase) else { return nil }
        
        do {
            let decodedImageData = try Data(contentsOf: url)
            return UIImage(data: decodedImageData)
            
        } catch {
            return nil
        }
    }
    
    func getUrlBaseConfigApp() -> String {
        
    
        if let typeConfig = Bundle.main.object(forInfoDictionaryKey: "Sandbox") as? Bool, typeConfig {
            return "http://sandbox.graph.mobiplay.vn"
        } else {
            
            guard let config = AppData.instance.config, config.apiRelease.count > 0,
                  let _ = URL(string: config.apiRelease) else { return "https://graph.mobiplay.vn" }
            return config.apiRelease
            
        }
    }

    
    func dkPushNotification() {
        
        UNUserNotificationCenter
            .current()
            .requestAuthorization(options:[.badge,
                                           .alert,
                                           .sound])
            { (granted, error) in
                
                if error == nil {
                    
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                    
                    AdBrixRM.getInstance.setPushEnable(toPushEnable: true)
                    
                    Log.message(.info, message: "Push registration success - iOS 10")
                    
                } else {
                    Log.message(.error, message: "Push registration FAILED - iOS 10")
                    Log.message(.error, message: String(format: "ERROR: %@", error!.localizedDescription))
                    AdBrixRM.getInstance.setPushEnable(toPushEnable: false)
                    
                }
            }
    }
    
    func checkNetworkCompletion(_ completion: LiteAPIsCompletion?) -> Bool {
        
        guard let networkStatus =  Reachability.forInternetConnection()?.currentReachabilityStatus() else { return false }
        guard networkStatus == NotReachable else { return true }
        if let completion = completion {
            let error = NSError(domain: "NetworkUnAvaiable", code: Int(ERROR_CODE_NETWORK_UNAVAIABLE), userInfo: nil)
            completion(nil, error, ResultCodeNetWorkError)
        }
        return false
    }
    
    func checkNetWork() -> Bool {
        
        guard let networkStatus =  Reachability.forInternetConnection()?.currentReachabilityStatus() else { return false }
        guard networkStatus == NotReachable else { return true }
        return false
    }
    
    
    func requestWithUrlStringInvite(_ urlString: String, method: MethodType, params: Any?, completion: LiteAPIsCompletion?) {
        
        guard checkNetworkCompletion(completion) else {
            Log.message(.error, message: "______mat mang roi nhe!")
            Utility.showAlertWithMessage(LanguageSetting.get("MessagerNetworkResult", alter: nil), viewController: nil)
            return
        }
        
        let manager = AFHTTPSessionManager()
        manager.requestSerializer.timeoutInterval = 25
        manager.responseSerializer = AFHTTPResponseSerializer()
        manager.requestSerializer  = AFJSONRequestSerializer()
        manager.post(urlString,
                     parameters: params,
                     progress: nil,
                     success: { (operation: URLSessionDataTask, responseObject: Any?) in
                        
                        Log.message(.info, message: "data responseObject = \(String(describing: responseObject))")
                        
                        guard let completion = completion else { return }
                        
                        if let response = responseObject as? [String: Any] {
                            completion(response, nil, ResultCodeSuccess)
                            return
                        }
                        
                        do {
                            guard let data = responseObject as? Data,
                                  let response = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String : Any] else {
                                completion(responseObject, nil, RequestCodeUnknowError)
                                return
                            }
                            
                            Log.message(.info, message: "data result = \(response)")
                            let resultCode = response[KEY_ERROR_CODE] as? Int ?? 0
                            completion(response, nil, ResultCode(rawValue: Int32(resultCode)))
                            
                        } catch {
                            completion(responseObject, error, RequestCodeUnknowError)
                        }
                     }, failure: { (operation: URLSessionDataTask?, error: Error) in
                        
                        Log.message(.error, message: "POST fails with error \(error)")
                        let userinfo1 = (error as NSError).userInfo
                        Log.message(.error, message: "data userinfo1 = \(userinfo1)")
                        
                        guard let completion = completion else { return }
                        completion(userinfo1, error, RequestCodeUnknowError)
                     })
    }
    
    func requestWithUrlString(_ urlString: String, method: MethodType, params: Any?, completion: LiteAPIsCompletion?) {
        
        Log.message(.info, message: "Paramater: \(String(describing: params)) | URL: \(urlString) | Method: \(method)")
        
        guard checkNetworkCompletion(completion) else {
            Log.message(.error, message: "______mat mang roi nhe!")
            Utility.showAlertWithMessage(LanguageSetting.get("MessagerNetworkResult", alter: nil), viewController: nil)
            return
        }
        
        let manager = AFHTTPSessionManager()
        manager.requestSerializer.timeoutInterval = 25
        manager.responseSerializer = AFHTTPResponseSerializer()
        
        switch method {
        case MethodTypePOST:
            manager.post(urlString,
                         parameters: params,
                         progress: nil,
                         success: { (operation: URLSessionDataTask, responseObject: Any?) in
                            
                            Log.message(.info, message: "data responseObject = \(String(describing: responseObject))")
                            guard let completion = completion else { return }
                            
                            if let response = responseObject as? [String: Any] {
                                completion(response, nil, ResultCodeSuccess)
                                return
                            }
                            
                            do {
                                guard let data = responseObject as? Data,
                                      let response = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String : Any] else {
                                    
                                    completion(responseObject, nil, RequestCodeUnknowError)
                                    return
                                }
                                
                                Log.message(.info, message: "data result = \(response)")
                                
                                let resultCode = response[KEY_ERROR_CODE] as? Int ?? 0
                                completion(response, nil, ResultCode(rawValue: Int32(resultCode)))
                                
                            } catch {
                                completion(responseObject, error, RequestCodeUnknowError)
                            }
                            
                            
                         }, failure: { (operation: URLSessionDataTask?, error: Error) in
                            
                            
                            Log.message(.error, message: "data error1 = \(error)")
                            
                            
                            
                            let userinfo1 = (error as NSError).userInfo
                            Log.message(.error, message: "data userinfo1 = \(userinfo1)")
                            guard let completion = completion else { return }
                            completion(userinfo1, error, RequestCodeUnknowError)
                            
                         })
            
        default:
            manager.get(urlString, parameters: params, progress: nil, success: { (operation: URLSessionDataTask, responseObject: Any?) in
                
                Log.message(.info, message: "data responseObject = \(String(describing: responseObject))")
                guard let completion = completion else { return }
                
                if let response = responseObject as? [String: Any] {
                    completion(response, nil, ResultCodeSuccess)
                    return
                }
                
                do {
                    
                    guard let data = responseObject as? Data,
                          let response = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String : Any] else {
                        
                        completion(responseObject, nil, RequestCodeUnknowError)
                        return
                    }
                    
                    Log.message(.info, message: "data result = \(response)")
                    let resultCode = response[KEY_ERROR_CODE] as? Int ?? 0
                    completion(response, nil, ResultCode(rawValue: Int32(resultCode)))
                } catch {
                    completion(responseObject, error, RequestCodeUnknowError)
                }
                
            }, failure: { (operation: URLSessionDataTask?, error: Error) in
                
                Log.message(.error, message: "data error1 = \(error)")
                
                
                
                let userinfo1 = (error as NSError).userInfo
                Log.message(.error, message: "data userinfo1 = \(userinfo1)")
                guard let completion = completion else { return }
                completion(userinfo1,error,RequestCodeUnknowError)
                
                
                if let innerError = userinfo1["NSUnderlyingError"] as? Error {
                    
                    let innerUserInfo = (innerError as NSError).userInfo
                    
                    guard let object = innerUserInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] as? Data else { return }
                    guard let error = String(data: object, encoding: .utf8) else { return }
                    Log.message(.error, message: "Error is : \(error)")
                    
                } else {
                    
                    guard let value = userinfo1[AFNetworkingOperationFailingURLResponseDataErrorKey] as? Data else { return }
                    guard let error = String(data: value, encoding: .utf8) else { return }
                    Log.message(.error, message: "\(error)")
                }
                
            })
        }
    }
    
    func requestItunesInappWithUrlString(_ urlString: String, method: MethodType, params: Any?, completion: LiteAPIsCompletion?) {
        
        
        do {
            
            let jsonData = try JSONSerialization.data(withJSONObject: params as Any, options: [])
            
            guard let jsonString = String(data: jsonData, encoding: .utf8) else { return }
            
            var req = AFJSONRequestSerializer().request(withMethod: "POST", urlString: urlString, parameters: nil, error: nil) as URLRequest
            req.timeoutInterval = (UserDefaults.standard.value(forKey: "timeoutInterval") as? NSNumber)?.doubleValue ?? 25
            req.setValue("application/json", forHTTPHeaderField: "Content-Type")
            req.setValue("application/json", forHTTPHeaderField: "Accept")
            req.httpBody = jsonString.data(using: .utf8)
            
            let manager = AFURLSessionManager(sessionConfiguration: URLSessionConfiguration.default)
            
            let task = manager.dataTask(with: req) { (response, responseObject, error) -> Void in
                
                if error == nil {
                    
                    Log.message(.info, message: "Reply JSON: \(String(describing: responseObject))")
                    guard let completion = completion else { return }
                    
                    if let response = responseObject as? [String: Any] {
                        let resultCode = response[KEY_ERROR_CODE] as? Int ?? 0
                        completion(response, nil, ResultCode(rawValue: Int32(resultCode)))
                        return
                    }
                    
                    do {
                        
                        guard let data = responseObject as? Data,
                              let response = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String : Any] else {
                            
                            completion(responseObject, nil, ResultCodeDataError)
                            return
                        }
                        
                        Log.message(.info, message: "data result = \(response)")
                        
                        let resultCode = response[KEY_ERROR_CODE] as? Int ?? 0
                        completion(response, nil, ResultCode(rawValue: Int32(resultCode)))
                        
                    } catch {
                        completion(nil, error, RequestCodeUnknowError)
                    }
                    
                    
                    
                } else {
                    
                    Log.message(.error, message: "Error: \(String(describing: error)), \(response), \(String(describing: responseObject))")
                    guard let completion = completion else { return }
                    
                    guard let errorNew = error else {
                        completion(nil, nil, RequestCodeUnknowError)
                        return
                    }
                    
                    let userinfo1 = (errorNew as NSError).userInfo
                    Log.message(.error, message: "data userinfo1 = \(userinfo1)")
                    completion(userinfo1,errorNew,ResultCodeDataError)
                    
                    
                    if let innerError = userinfo1["NSUnderlyingError"] as? Error {
                        
                        let innerUserInfo = (innerError as NSError).userInfo
                        
                        guard let object = innerUserInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] as? Data else { return }
                        guard let error1 = String(data: object, encoding: .utf8) else { return }
                        Log.message(.error, message: "Error is : \(error1)")
                        
                    } else {
                        
                        guard let value = userinfo1[AFNetworkingOperationFailingURLResponseDataErrorKey] as? Data else { return }
                        guard let error1 = String(data: value, encoding: .utf8) else { return }
                        Log.message(.error, message: "\(error1)")
                    }
                }
                
            }
            
            task.resume()
            
        } catch {
            
            guard let completion = completion else { return }
            completion(nil, error, RequestCodeUnknowError)
        }
        
    }
    
    
    func getConfigDefaultApp(completion: LiteAPIsCompletion?) {
        
        let adId            = ASIdentifierManager.shared().advertisingIdentifier
        
        let parameters      = ["tracking_id": adId.uuidString]
        
        
        requestWithApi(API_APP_CONFIG, parameters: parameters, completion: completion)
    }
    
    
    func getAppStoreId(_ completion: LiteAPIsCompletion?) {
        
        let bundleIdentifier = Bundle.main.bundleIdentifier ?? ""
        let itunesUrl        = "http://itunes.apple.com/lookup?bundleId="
        let url              = String(format: "%@%@", itunesUrl, bundleIdentifier)
        requestWithUrlString(url, method: MethodTypePOST, params: nil, completion: { (results, error, resultCode) in
            
            guard let completion = completion else { return }
            completion(results, error, resultCode)
        })
    }
    
    func getProductInfo(_ completion: LiteAPIsCompletion?) {
        

        let accessToken = UserDefaults.standard.string(forKey: KEY_SAVE_USER_TOKEN)
        
        let parameters: [String : Any]      = ["access_token": accessToken ?? "",
                                               "channel": "ITUNES"]
        
        requestWithApi(API_GET_PRODUCT_INFO, parameters: parameters, completion: completion)
    }
    
    
    
    func gettttk(withAccName accName: String, completion: LiteAPIsCompletion?) {

        requestWithApi(API_APP_GET_INFOACC_BY_NAME, parameters: ["username": accName], completion: completion)
    }
    
    
    func gettttk(withAccessToken accessToken: String, completion: LiteAPIsCompletion?) {

        requestWithApi(API_APP_GET_INFOACC, parameters: ["access_token": accessToken], completion: completion)
    }
    
    
    func dn(withTentk tentk: String, mk: String, completion: LiteAPIsCompletion?) {
        
        
        let clienVersion    = Bundle.main.object(forInfoDictionaryKey: "FBundleShortVersionString")
        let deviceOsVersion = String(format: "%@ %@", UIDevice.current.name, UIDevice.current.systemVersion)
        
        let parameters: [String : Any]      = ["username":tentk,
                                               "password":mk,
                                               "client_version":clienVersion ?? "",
                                               "device_os_version":deviceOsVersion ]
        
        requestWithApi(API_APP_LOGIN, parameters: parameters, completion: completion)
    }
    
    
    
    func dnnhanh(completion: LiteAPIsCompletion?) {
        

        let deviceOsVersion = String(format: "%@ %@", UIDevice.current.name, UIDevice.current.systemVersion)
        
        let parameters: [String : Any]      = ["device_os_version": deviceOsVersion]
        
        requestWithApi(API_APP_FAST_LOGIN, parameters: parameters, completion: completion)
    }
    
    
    
    func dktk(withTentk tentk: String, mk: String, completion: LiteAPIsCompletion?) {
        
        
        let clienVersion    = Bundle.main.object(forInfoDictionaryKey: "FBundleShortVersionString")
        let deviceOsVersion = String(format: "%@ %@", UIDevice.current.name, UIDevice.current.systemVersion)
        
        let parameters: [String : Any]      = ["username":tentk,
                                               "password":mk,
                                               "client_version":clienVersion ?? "",
                                               "device_os_version":deviceOsVersion ]
        
        requestWithApi(API_APP_REGISTER, parameters: parameters, completion: completion)
    }
    
    
    
    func dktkmobile(withNumberPhone numberPhone: String?, mk: String?, codeCoutry: String?, completion: LiteAPIsCompletion?) {
        

        let clienVersion    = Bundle.main.object(forInfoDictionaryKey: "FBundleShortVersionString")
        let deviceOsVersion = String(format: "%@ %@", UIDevice.current.name, UIDevice.current.systemVersion)
        
        let parameters: [String : Any]       = ["password":mk ?? "",
                                                "mobile": numberPhone ?? "",
                                                "code_coutry": codeCoutry ?? "",
                                                "carrier":"vms",
                                                "client_version": clienVersion ?? "",
                                                "device_os_version": deviceOsVersion]
        
        requestWithApi(API_APP_REGISTER_MOBILE, parameters: parameters, completion: completion)
    }
    
    
    
    
    func activeDkac(byNumberPhone numberPhone: String, mk: String, otpResult: String, completion: LiteAPIsCompletion?) {
        

        let clienVersion    = Bundle.main.object(forInfoDictionaryKey: "FBundleShortVersionString")
        let deviceOsVersion = String(format: "%@ %@", UIDevice.current.name, UIDevice.current.systemVersion)
        
        let parameters: [String : Any]      = ["password":mk,
                                               "mobile":numberPhone,
                                               "otp":otpResult,
                                               "carrier":"vms",
                                               "client_version":clienVersion ?? "",
                                               "device_os_version":deviceOsVersion ]
        
        requestWithApi(API_APP_REGISTER_MOBILE_ACTIVE, parameters: parameters, completion: completion)
    }
    
    
    
    func dxtk(completion: LiteAPIsCompletion?) {

        let parameters      = ["user_id": UserDefaults.standard.string(forKey: KEY_SAVE_USERID) ?? ""]
        
        requestWithApi(API_APP_LOGOUT, parameters: parameters, completion: completion)
    }
    
    
    func checkDevice(completion: LiteAPIsCompletion?) {

        requestWithApi(API_APP_CHECK_DEVICE, completion: completion)
    }
    
    
    func authenFacebook(withFacebookID facebookID: String, fEmail: String, facebookName: String, facebookGender: Int, facebookToken: String, completion: LiteAPIsCompletion?) {
        

        let deviceOsVersion = String(format: "%@ %@", UIDevice.current.name, UIDevice.current.systemVersion)
        
        let parameters: [String : Any]      = ["facebookId":facebookID,
                                               "email":fEmail,
                                               "facebookName":facebookName,
                                               "facebookGender": "\(facebookGender)",
                                               "facebookToken":facebookToken,
                                               "device_os_version":deviceOsVersion ]
        
        requestWithApi(API_APP_AUTHEN_FACEBOOK, parameters: parameters, completion: completion)
        
    }
    
    
    func authenApple(withAppleID appleID: String?, email: String?, fullName: String?, completion: LiteAPIsCompletion?) {

        let deviceOsVersion = String(format: "%@ %@", UIDevice.current.name, UIDevice.current.systemVersion)
        
        let parameters: [String : Any]      = ["appleId": appleID ?? "",
                                               "email": email ?? "",
                                               "fullName": fullName ?? "",
                                               "device_os_version":deviceOsVersion]

        requestWithApi(API_APP_AUTHEN_APPLEID, parameters: parameters, completion: completion)
    }

    func knFacebook(withFacebookID facebookID: String?, accessToken: String?, fEmail: String?, facebookName: String?, fGender faceBookGender: Int, fToken facebookToken: String, completion: LiteAPIsCompletion?) {
        
        let parameters: [String : Any]      = ["access_token":accessToken ?? "",
                                               "facebookId":facebookID ?? "",
                                               "facebookEmail":fEmail ?? "",
                                               "facebookName":facebookName ?? "",
                                               "facebookGender":"\(faceBookGender)",
                                               "facebookToken":facebookToken]
        
        requestWithApi(API_APP_CONNEC_FACEBOOK, parameters: parameters, completion: completion)

    }
    
    
    func authenGoogle(withGoogleID googleID: String?, gEmail: String?, googleName: String?, gGender: Int, completion: LiteAPIsCompletion?) {
        
        let deviceOsVersion = String(format: "%@ %@", UIDevice.current.name, UIDevice.current.systemVersion)
        
        
        let parameters: [String : Any]      = ["googleId": googleID ?? "",
                                               "googleEmail": gEmail ?? "",
                                               "googleName": googleName ?? "",
                                               "googleGender": "\(gGender)",
                                               "device_os_version": deviceOsVersion]
        
        requestWithApi(API_APP_AUTHEN_GOOGLE, parameters: parameters, completion: completion)
    }

    
    func knGoogle(withGoogleID googleID: String?, accessToken: String?, gEmail: String?, gName googleName: String?, gGender googleGender: Int, completion: LiteAPIsCompletion?) {
        
        
        let parameters: [String : Any]      = ["access_token": accessToken ?? "",
                                               "googleId": googleID ?? "",
                                               "googleEmail": gEmail ?? "",
                                               "googleName": googleName ?? "",
                                               "googleGender": "\(googleGender)"]
        
        requestWithApi(API_APP_CONNEC_GOOGLE, parameters: parameters, completion: completion)
    }
    
    
    /*
    func sendDataInAppPurchaseToSever(withAccessToken accessToken: String?, receiptData: String?, partnerInfo: String?, completion: LiteAPIsCompletion?) {
        

        let deviceID: String    = UIDevice.current.identifierForVendor?.uuidString ?? ""
  
        let parameters: [String : Any]      = ["device_id": deviceID,
                                               "access_token": accessToken ?? "",
                                               "receipt_data": receiptData ?? "",
                                               "partner_info": partnerInfo ?? ""]
        
        requestWithApi(API_APP_MTT, parameters: parameters, completion: completion)
    }
    */
    
    func sendDataInAppPurchaseToSever(withAccessToken accessToken: String?, receiptData: String?, partnerInfo: String?, completion: LiteAPIsCompletion?) {
        
        let url: String         = String(format: "%@/%@", getUrlBaseConfigApp(), API_APP_MTT)
        let deviceOs: String    = "IOS"
        let apiKeyApp: String   = UserDefaults.standard.string(forKey: KEY_API_CONFIG_APP) ?? ""
        let deviceID: String    = UIDevice.current.identifierForVendor?.uuidString ?? ""
        let agencyIDApp: String = UserDefaults.standard.string(forKey: KEY_AGENCY_APP) ?? ""
        
        let parameters: [String : Any]      = ["device_os": deviceOs,
                                               "api_key": apiKeyApp,
                                               "agency_id":agencyIDApp,
                                               "device_id": deviceID,
                                               "access_token": (accessToken ?? ""),
                                               "receipt_data": (receiptData ?? ""),
                                               "partner_info": (partnerInfo ?? "")]
        
        
        
        Log.message(.info, message: "__________params send data to server \(parameters)")
        
        requestItunesInappWithUrlString(url, method: MethodTypePOST, params: parameters, completion: { (results, error, resultCode) in
            
            if error == nil {
                
                if let completion = completion { completion(results, nil, resultCode) }
            } else {
                
                let backupUrl = String(format: "%@/%@", self.backUpUrl, API_APP_MTT)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + Double(Int64(3.0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) {
                    
                    self.requestItunesInappWithUrlString(backupUrl, method: MethodTypePOST, params: parameters, completion: { (resultsBackUp, errorBackUp, resultCodeBackUp) in
                        
                        if errorBackUp == nil {
                            if let completion = completion { completion(resultsBackUp, nil, resultCodeBackUp) }
                            
                        } else {
                            if let completion = completion { completion(nil, errorBackUp, resultCodeBackUp) }
                        }
                    })
                }
            }
            
        })
    }
    
    
    
    func forgotmkstepOne(withUsername userName: String?, step: Int, hiddenNumber: String?, completion: LiteAPIsCompletion?) {
        
        let parameters: [String : Any]      = ["username": userName ?? "",
                                               "step": "\(step)",
                                               "hidemobile": hiddenNumber ?? ""]
        
        requestWithApi(API_APP_FORGOT_BY_MOBILE_B1, parameters: parameters, completion: completion)
    }
    
    
    func forgotmkstepTwo(withUsername userName: String?, step: Int, hiddenNumber: String?, otp: String, completion: LiteAPIsCompletion?) {
        
        let parameters: [String : Any]      = ["username": userName ?? "",
                                               "step": "\(step)",
                                               "hidemobile": hiddenNumber ?? "",
                                               "otp":otp]
        
        
        requestWithApi(API_APP_FORGOT_BY_MOBILE_B1, parameters: parameters, completion: completion)
    }
    
    func forgotmkstepThree(withUsername userName: String?, step: Int, hiddenNumber: String?, otp: String, newPassword: String, completion: LiteAPIsCompletion?) {
        
        
        let parameters: [String : Any]      = ["username": userName ?? "",
                                               "step": "\(step)",
                                               "hidemobile": hiddenNumber ?? "",
                                               "otp": otp,
                                               "new_password": newPassword]
        
        requestWithApi(API_APP_FORGOT_BY_MOBILE_B1, parameters: parameters, completion: completion)
    }

    func forgotmkByEmail(withUserName userName: String?, completion: LiteAPIsCompletion?) {
        
        requestWithApi(API_APP_FORGOT_BY_EMAIL, parameters: ["username": userName ?? ""], completion: completion)
    }
    
    func getInfomationSupport(withCompletion completion: LiteAPIsCompletion?) {
        
        requestWithApi(API_APP_GET_SUPPORT, completion: completion)
    }

    
    func updateProfileFastAc(withUserId userID: String?, useName userName: String?, passWord: String?, email: String, completion: LiteAPIsCompletion?) {

        let deviceOsVersion = String(format: "%@ %@", UIDevice.current.name, UIDevice.current.systemVersion)
        
        let parameters: [String : Any]      = ["email":email,
                                               "user_id":userID ?? "",
                                               "username":userName ?? "",
                                               "password":passWord ?? "",
                                               "device_os_version":deviceOsVersion]
        
        requestWithApi(API_APP_UPDATE_PROFILE, parameters: parameters, completion: completion)
    }
    
    
    func getListTran(withAccessToken accessToken: String, apiKey: String, completion: LiteAPIsCompletion?) {
        let rootURL = Utility.getUrlApiTracking() + "/v1/lpm?api_key="
        let url = String(format: "%@%@&access_token=%@", rootURL, apiKey, accessToken)
        
        self.requestWithUrlString(url, method: MethodTypeGET, params: nil, completion: { (results, error, resultCode) in
            
            if error == nil {
                if let completion = completion { completion(results, nil, resultCode) }
            } else {
                if let completion = completion { completion(nil, error, resultCode) }
            }
        })
    }
    
    func sendStatusTran(withAccessToken accessToken: String, apiKey: String, transactionId: String, completion: LiteAPIsCompletion?) {
        let rootURL =  Utility.getUrlApiTracking() + "/v1/lpm?api_key="
        let url = String(format: "%@%@&access_token=%@&lastTransactionId=%@", rootURL, apiKey, accessToken, transactionId)
        
        self.requestWithUrlString(url, method: MethodTypePOST, params: nil, completion: { (results, error, resultCode) in
            
            if error == nil {
                if let completion = completion { completion(results, nil, resultCode) }
            } else {
                if let completion = completion { completion(nil, error, resultCode) }
            }
        })
    }
    
    
    
    
    
    func getWarningCheatWithCompletion(completion: LiteAPIsCompletion?) {
        
        requestWithApi(API_APP_GET_WARNING_CHEAT, method: MethodTypePOST, completion: completion)
    }
    
    
    
    
    func getListExcludeIdsFromServerCompletion(completion: LiteAPIsCompletion?) {
        
        
        let dictInfo: [String : Any] = Bundle.main.infoDictionary ?? [:]
        let url             = String(format: "%@%@/%@", getUrlBaseConfigApp(), SERVERLIVEGAMEINVITE, GETOLDLISTINVITE)
        let deviceOs        = "IOS"
        let apiKeyApp       = UserDefaults.standard.string(forKey: KEY_API_CONFIG_APP) ?? ""
        let AppID           = dictInfo["FacebookAppID"] as? String ?? ""
        let userID          = UserDefaults.standard.string(forKey: KEY_SAVE_USERID) ?? ""
        let strSign         = String(format: "%@%@%@", apiKeyApp, userID, AppID)
        let sign            = strSign.md5
        
        let parameters: [String : Any]      = ["device_os": deviceOs,
                                               "APIKey":apiKeyApp,
                                               "Sign":sign,
                                               "AppId":AppID,
                                               "AccountId":userID]
        
        Log.message(.info, message: "__________DataExclude\(parameters) url=\(url)")
        
        requestWithUrlString(url, method: MethodTypePOST, params: parameters, completion: { (results, error, resultCode) in
            
            if error == nil {
                
                if let completion = completion { completion(results, nil, resultCode) }
            } else {
                
                let backupUrl = String(format: "%@%@/%@", self.backUpUrl, BACKUP_SERVERLIVEGAMEINVITE, GETOLDLISTINVITE)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + Double(Int64(3.0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) {
                    
                    self.requestWithUrlString(backupUrl, method: MethodTypePOST, params: parameters, completion: { (resultsBackUp, errorBackUp, resultCodeBackUp) in
                        
                        if errorBackUp == nil {
                            if let completion = completion { completion(resultsBackUp, nil, resultCodeBackUp) }
                            
                        } else {
                            if let completion = completion { completion(nil, errorBackUp, resultCodeBackUp) }
                        }
                    })
                }
            }
            
        })
    }
    
    
    
    
    func insertNewListInviteToServer(withRequestID requestID: String?, facebookFriendID: String?, completion: LiteAPIsCompletion?) {
        
        
        let dictInfo: [String : Any] = Bundle.main.infoDictionary ?? [:]
        let url             = String(format: "%@%@/%@", getUrlBaseConfigApp(), SERVERLIVEGAMEINVITE, INSERTLISTNEWINVITE)
        let deviceOs        = "IOS"
        let apiKeyApp       = UserDefaults.standard.string(forKey: KEY_API_CONFIG_APP) ?? ""
        let AppID           = dictInfo["FacebookAppID"] as? String ?? ""
        let userID          = UserDefaults.standard.string(forKey: KEY_SAVE_USERID) ?? ""
        let strSign         = String(format: "%@%@%@%@%@", userID, apiKeyApp, requestID ?? "", facebookFriendID ?? "", AppID)
        let sign            = strSign.md5
        
        let parameters: [String : Any]      = ["device_os": deviceOs,
                                               "APIKey": apiKeyApp,
                                               "Sign": sign,
                                               "AppId": AppID,
                                               "AccountId": userID,
                                               "FacebookRequest": requestID ?? "",
                                               "FacebookSend": facebookFriendID ?? ""]
        
        Log.message(.info, message: "__________insertNewListInvite \(parameters) url=\(url)")
        
        requestWithUrlString(url, method: MethodTypePOST, params: parameters) { (results, error, resultCode) in
            
            guard error != nil else {
                if let completion = completion { completion(results, nil, resultCode) }
                return
            }
            
            let backupUrl = String(format: "%@%@/%@", self.backUpUrl, BACKUP_SERVERLIVEGAMEINVITE, INSERTLISTNEWINVITE)
            DispatchQueue.main.asyncAfter(deadline: .now() + Double(Int64(3.0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) {
                
                self.requestWithUrlString(backupUrl, method: MethodTypePOST, params: parameters) { (resultsBackUp, errorBackUp, resultCodeBackUp) in
                    
                    
                    guard let completion = completion else { return }
                    
                    if errorBackUp == nil {
                        completion(resultsBackUp, nil, resultCodeBackUp)
                    } else {
                        completion(nil, errorBackUp, resultCodeBackUp)
                    }
                }
            }
        }
    }
    
    
    func uploadavatar(withStringImage stringImage: String, accessToken: String, completion: LiteAPIsCompletion?) {
        
        
        let parameters: [String : Any] = ["access_token": accessToken,
                                          "ibase64": stringImage]
        
        requestWithApi(API_APP_UPLOAD_AVATAR, parameters: parameters, completion: completion)
    }
    
    
    func uploadImage(withStringImage stringImage: String, accessToken: String, completion: LiteAPIsCompletion?) {
        
        let parameters: [String : Any] = ["access_token": accessToken,
                                          "ibase64": stringImage]
        
        requestWithApi(API_APP_UPLOAD_IMAGE, parameters: parameters, completion: completion)
    }
    
    
    
    /*
     Request All API
     */
    
    func requestWithApi(_ api: String, method: MethodType = MethodTypePOST, parameters: [String : Any]? = nil, completion: LiteAPIsCompletion?) {
        
        
        let url             = String(format: "%@/%@", getUrlBaseConfigApp(), api)
        let backupUrl       = String(format: "%@/%@", backUpUrl, api)
        
        
        var params: [String : Any] = ["device_os": "IOS",
                                      "device_id": UIDevice.current.identifierForVendor?.uuidString ?? "",
                                      "api_key": UserDefaults.standard.string(forKey: KEY_API_CONFIG_APP) ?? "",
                                      "agency_id": UserDefaults.standard.string(forKey: KEY_AGENCY_APP) ?? ""]
        
        if let parameters = parameters {
            parameters.forEach { params[$0.key] = $0.value }
        }
        
        requestWithUrlString(url, method: method, params: params, completion: { (results, error, resultCode) in
            
            guard error != nil else {
                
                UnitAnalyticHelper.analyticFireBaseEven(api, contentID: AnalyticsParameterSuccess)
                if let completion = completion { completion(results, nil, resultCode) }
                return
                
            }
    
            DispatchQueue.main.asyncAfter(deadline: .now() + Double(Int64(3.0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) {
                
                self.requestWithUrlString(backupUrl, method: method, params: params) { (resultsBackUp, errorBackUp, resultCodeBackUp) in
                    
                    guard let errorBackUp = errorBackUp else {
                        UnitAnalyticHelper.analyticFireBaseEven(api, contentID: AnalyticsParameterSuccess)
                        if let completion = completion { completion(resultsBackUp, nil, resultCodeBackUp) }
                        return
                    }
                    
                    UnitAnalyticHelper.analyticFireBaseEven(api, contentID: "error \(resultCodeBackUp)")
                    if let completion = completion { completion(nil, errorBackUp, resultCodeBackUp) }
                }
            }
        })
    }
    
}
