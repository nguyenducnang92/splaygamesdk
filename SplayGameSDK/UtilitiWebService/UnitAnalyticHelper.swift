//
//  UnitAnalyticHelper.swift
//  SplayGameSDK
//
//  Created by Nguyen Nang on 8/5/20.
//  Copyright © 2020 splay. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FBSDKCoreKit
import FBSDKLoginKit
import FBSDKShareKit
import AppsFlyerLib
import GoogleSignIn
import AdBrixRM


 struct UnitAnalyticHelper {
    
    static func standardizeLog(_ log: String) -> String {
        
        guard log.count >= 300 else { return log }
        return String(NSString(format: "%@", log).trimmingCharacters(in: .whitespacesAndNewlines).prefix(300))
    }

    static func analyticFireBaseEven(_ action: String, contentID: String) {
        
        let paramsFir: [String : Any] = [AnalyticsParameterItemID : contentID,
                                         AnalyticsParameterContentType : action]
        
        let paramsFacebook: [String : Any] = [AppEvents.ParameterName.contentID.rawValue: contentID,
                                              AppEvents.ParameterName.contentType.rawValue: action]
        
        Utility.sendEventToFacebookWith(AppEvents.Name.viewedContent.rawValue, parameters: paramsFacebook)
        Utility.sendEventToFirebaseWith(AnalyticsEventSelectContent, parameters: paramsFir)
        Utility.sendEventToAppsFlyerWith(AnalyticsEventSelectContent, parameters: paramsFir)
        Utility.sendEventToHubJSWith(AnalyticsEventSelectContent, parameters: paramsFir)
        Utility.sendEventToAdBrixWith(AnalyticsEventSelectContent, parameters: paramsFir)
    }

    static func analyticLoginEvent(_ method: String) {

        Utility.sendEventToFacebookWith("fb_login", parameters: ["Method": method])
        Utility.sendEventToFirebaseWith("login", parameters: ["Method": method])
        Utility.sendEventToAppsFlyerWith("login", parameters: ["Method": method])
        Utility.sendEventToAdBrixWith("login", parameters: ["Method": method])
        Utility.sendEventToHubJSWith("login", parameters: ["Method": method])
    }
}
