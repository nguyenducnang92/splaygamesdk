//
//  LanguageSetting.m
//  SDKLite
//
//  Created by Duc Viet on 8/7/18.
//  Copyright © 2018 DucViet. All rights reserved.
//

#import "LanguageSetting.h"

@implementation LanguageSetting
static NSBundle *bundle = nil;

+(void)initialize
{
    NSUserDefaults* defs = [NSUserDefaults standardUserDefaults];
    NSArray* languages = [defs objectForKey:@"AppleLanguages"];
    NSString *current = [languages objectAtIndex:0];
    [self setLanguage:current];
}

/*
 example calls:
 [Language setLanguage:@"it"];
 [Language setLanguage:@"de"];
 */
+(void)setLanguage:(NSString *)l
{
    NSLog(@"preferredLang: %@", l);

    NSString *path = [[NSBundle bundleWithIdentifier:@"splay.SDK"] pathForResource:[NSString stringWithFormat:@"%@",l] ofType:@"lproj" ];
    
    bundle = [NSBundle bundleWithPath:path];
    
}

+(NSString *)get:(NSString *)key alter:(NSString *)alternate
{
    return [bundle localizedStringForKey:key value:alternate table:nil];
}
@end
