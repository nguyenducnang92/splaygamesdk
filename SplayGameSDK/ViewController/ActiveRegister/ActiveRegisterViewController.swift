//
//  ActiveRegisterViewController.swift
//  SplayGameSDK
//
//  Created by Nguyen Nang on 7/13/20.
//  Copyright © 2020 splay. All rights reserved.
//


import UIKit
import GoogleSignIn
import OTPFieldView

class ActiveRegisterViewController: CustomViewController {
    
    //-----------------------
    // MARK: - ENUM
    //-----------------------
    
    enum Size: CGFloat {
        case button = 36, padding10 = 10
    }
    
    
    
    
    //-----------------------
    // MARK: - VAR
    //-----------------------
    
    var strMessager: String = ""
    var strsdt: String = ""
    var strmk: String = ""
    var isGetOtp: Bool = false
    var strOtp: String = ""
    var isHasOTP: Bool = false
    
    
//    var timeTinSeconds: Int = 0
//    var timer: Timer!
    
    //-----------------------
    // MARK: - VIEW
    //-----------------------
    
    
    
    var activeContentView: ActiveRegisterContentView!
    
    
    
    //-----------------------
    // MARK: - LIFE CYCLE
    //-----------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if UIScreen.main.bounds.width > UIScreen.main.bounds.height {
            // Landscape
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                landscapeContentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 320))
                
            } else {
                landscapeContentSizeInPopup = CGSize(width: 336, height: 350)
            }
            
            
        } else {
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                contentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 340))
                
            } else {
                contentSizeInPopup = CGSize(width: 336, height: 350)
            }
        }
        
        setupAllSubviews()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        updateFrame()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
//        guard timer != nil else { return }
//        timer.invalidate()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
    }
    
    func updateFrame() {
        
        btnClose.frame = CGRect(x: view.frame.width - 40,
                                y: 0,
                                width: 40,
                                height: 40)
        
        
        labelTitle.frame = CGRect(x: 0,
                                  y: 0,
                                  width: view.frame.width,
                                  height: 40)
        
        seperator.frame = CGRect(x: 0,
                                 y: labelTitle.frame.height - 1 / UIScreen.main.scale,
                                 width: view.frame.width,
                                 height: 1 / UIScreen.main.scale)
        
        helpView.frame = CGRect(x: 0,
                                y: view.frame.height - 40,
                                width: view.frame.width,
                                height: 40)
        
        activeContentView.frame = CGRect(x: 0,
                                  y: seperator.frame.maxY,
                                  width: view.frame.width,
                                  height: helpView.frame.minY - seperator.frame.maxY)

    }
}


extension ActiveRegisterViewController {
    

    func loadLanguage() {
        
        labelTitle.text = LanguageSetting.get("TitleView", alter: nil)
 
        let mainString = String(format: LanguageSetting.get("activi_register_label_use_phone_send_message", alter: "Bạn vui lòng sử dụng số điện thoại %@ soạn tin nhắn"), strsdt)
        
        activeContentView.lbSendMessage.attributedText = Utility.getStringAttributeWith(mainString,
                                                                                        subStrings: [strsdt],
                                                                                        mainFont: UIFont.systemFont(ofSize: 13),
                                                                                        subFonts: [UIFont.boldSystemFont(ofSize: 14)],
                                                                                        mainColor: .gray,
                                                                                        subColors: [UIColor.Text.blackMediumColor()])
        
        let stringMessage = String(format: LanguageSetting.get("forgot_pass_step_two_send_to", alter: nil), "MID OTP \(strsdt)", "8030")
        let attr = Utility.getStringAttributeWith(stringMessage,
                                                  subStrings: ["MID OTP \(strsdt)", "8030"],
                                                  mainFont: UIFont.systemFont(ofSize: 14),
                                                  subFonts: [UIFont.boldSystemFont(ofSize: 16)],
                                                  mainColor: .white,
                                                  subColors: [.white])
        
        
        activeContentView.btnSendMessage.setAttributedTitle(attr, for: .normal)
        activeContentView.lbMessager.text = LanguageSetting.get("activi_register_label_please_input_code_and_accept", alter: "để nhận mã Xác thực OTP. Vui lòng nhập Mã Xác Thực OTP vào ô bên dưới và bấm nút Xác thực để xác nhận thay đổi.")
        activeContentView.btnConfirm.setTitle(LanguageSetting.get("title_button_confirm", alter: nil), for: .normal)
        
        
//        activeContentView.lbMessager.text = LanguageSetting.get("confirm_otp_register", alter: nil)
//        if timeTinSeconds > 0 {
//            lbGetOTP.text = String(format: LanguageSetting.get("waitting_for_otp", alter: nil), timeTinSeconds)
//
//        }else{
//            lbGetOTP.text = LanguageSetting.get("get_verification_code_again", alter: nil)
//        }
        
    }

    
    /*
    @objc func getOTTCode() {
        btnGetOTP.isUserInteractionEnabled = true
        //        DVLog(@"_______get otp funtion %@  %@",self.strsdt,self.strmk);
        UtilitieWebService
            .shareSingleton()?
            .dktkmobile(withNumberPhone: strsdt,
                        mk: strmk,
                        codeCoutry: "",
                        completion: { (results, error, resultCode) in
                            
                            if resultCode == ResultCodeSuccess {
                                //  DVLog(@"Thong tin dang ky thanh cong %@",results[KEY_DATA]);
                                self.timer.invalidate()
                            }
                            
                            
            })
    }
    
    
    @objc func updateCountSecond() {
        
        timeTinSeconds -= 1
        
        guard timeTinSeconds == 0 else {
            
            lbGetOTP.text = String(format: LanguageSetting.get("waitting_for_otp", alter: nil), timeTinSeconds)
            
            return
        }
        
        
        lbGetOTP.text = LanguageSetting.get("get_verification_code_again", alter: nil)
        timeTinSeconds = 0
        timer.invalidate()
    }
    
    @objc func btnGetOTPClick(_ sender: UIButton) {
        
        if timer == nil {
            
            btnGetOTP.isUserInteractionEnabled = false
            timeTinSeconds = 20
            
            timer = Timer.scheduledTimer(timeInterval: 1.0,
                                         target: self,
                                         selector: #selector(self.updateCountSecond),
                                         userInfo: nil,
                                         repeats: true)
            
        }
        
        self.perform(#selector(self.getOTTCode), with: nil, afterDelay: 20)
        
    }
    */
}

extension ActiveRegisterViewController {
    
    @objc func changeLanguage() {
        loadLanguage()
    }
    
    @objc func orientationChanged(_ sender: Notification) {
        
        let device = UIDevice.current
        
        switch device.orientation {
        case .faceUp:
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                contentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 340))
                
            } else {
                contentSizeInPopup = CGSize(width: 336, height: 350)
            }
            
            
        case .portrait, .portraitUpsideDown:
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                contentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 340))
                
            } else {
                contentSizeInPopup = CGSize(width: 336, height: 350)
            }
            
        case .landscapeLeft, .landscapeRight:
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                landscapeContentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 320))
                
            } else {
                landscapeContentSizeInPopup = CGSize(width: 336, height: 350)
            }
            
            
        default:
            break
        }
        
        updateFrame()
    }
    
    @objc func btnCloseClick(_ sender: UIButton) {
        popupController?.dismiss()
    }
    

    
    
    @objc func btnSendMessageClick(_ sender: UIButton) {
        
        let sms: String = String(format: "sms:%@&body=MID OTP %@", "8030", strsdt)
        
        guard let strURL = sms.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
        guard let url = URL(string: strURL) else { return }
        
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
        
    }
    
    @objc func btnConfirmClick(_ sender: UIButton) {
        
        view.endEditing(true)
        guard strOtp.count > 0 else {
            
            Utility.showAlertWithMessage(LanguageSetting.get("verification_code_emtry", alter: nil), viewController: self)
            return
        }
        
        guard isHasOTP else {
            
            Utility.showAlertWithMessage(LanguageSetting.get("incorrect_verification_code", alter: nil), viewController: self)
            return
        }
        
        HUD.showHUD(onView: self.view)
        UtilitieWebService
            .shareSingleton()?
            .activeDkac(byNumberPhone: strsdt,
                        mk: strmk,
                        otpResult: strOtp,
                        completion: { (results, error, resultCode) in
                            
                            
                            //                            DVLog(@"active thanh cong %@",results[KEY_DATA]);
                            
                            HUD.dismissHUD()
                            guard resultCode == ResultCodeSuccess else {
                                Utility.showAlertFrom(resultCode, results: results, viewController: self)
                                
                                
                                return
                            }

                            Utility.trackEventCompleteRegisterOldOrNew("splay_mobile", results: results, isRegisterPushNotification: false)
                            
                            guard let result = results as? [String : Any] else { return }
                            guard let resultData = result[KEY_DATA] as? [String : Any] else { return }
                            
                            let confirmVC = ConfirmViewController()
                            confirmVC.stracMobile =  resultData[KEY_USERNAME] as? String ?? ""
                            confirmVC.strAcName =  resultData[KEY_USERNAME] as? String ?? ""
                            confirmVC.dictResultLogin = result
                            confirmVC.toScreen = ActiveMobileScreen
                            self.popupController?.push(confirmVC, animated: true)
                            
                            
                            
            })
        
    }
}

extension ActiveRegisterViewController: CustomHelpViewDelegate {
    
    func showViewSupport() {
        
    }
    
    func showSettingLanguage() {
        
    }
}

//-----------------------
// MARK: - SETUP
//-----------------------

extension ActiveRegisterViewController: OTPFieldViewDelegate {
    
    
    func hasEnteredAllOTP(hasEnteredAll hasEntered: Bool) -> Bool {
        print("Has entered all OTP? \(hasEntered)")
        
        isHasOTP = hasEntered
        return false
    }
    
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otp otpString: String) {
        print("OTPString: \(otpString)")
        
        strOtp = otpString
    }
}


//-----------------------
// MARK: - SETUP
//-----------------------

extension ActiveRegisterViewController {
    
    func setupAllSubviews() {
        
        
        
        title = "Đăng nhập"
        view.backgroundColor = UIColor(red: 248/255.0, green: 248/255.0, blue: 248/255.0, alpha: 1.0)
        view.layer.cornerRadius = 8.0
        view.layer.borderColor =  UIColor(red: 13/255.0, green: 183/255.0, blue: 212/255.0, alpha: 1.0).cgColor
        view.layer.borderWidth = 1.5
        view.layer.masksToBounds = true
        
        setupNotification()
        
        
        
        
        helpView.delegate = self
        btnClose.addTarget(self, action: #selector(self.btnCloseClick(_:)), for: .touchUpInside)
        
        
        
        
        activeContentView = setupActiveView()
       
        
        
        
        
        view.addSubview(helpView)
        view.addSubview(activeContentView)
        view.addSubview(seperator)
        view.addSubview(labelTitle)
        view.addSubview(btnClose)
        
        loadLanguage()
        
//        if isGetOtp {
//            getOTTCode()
//        }
    }
    
    func setupNotification() {
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.changeLanguage),
                                               name: Notification.Name(rawValue: CHANGE_LANGUAGE_SETTING),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.orientationChanged(_:)),
                                               name: UIDevice.orientationDidChangeNotification,
                                               object: UIDevice.current)
        
        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
        
    }
    
    
    
    func setupActiveView() -> ActiveRegisterContentView {
        let view = ActiveRegisterContentView()
      
        view.otpStackView.delegate = self
        view.btnConfirm.addTarget(self, action: #selector(self.btnConfirmClick(_:)), for: .touchUpInside)
        view.btnSendMessage.addTarget(self, action: #selector(self.btnSendMessageClick(_:)), for: .touchUpInside)
        return view
    }
    
    
}


