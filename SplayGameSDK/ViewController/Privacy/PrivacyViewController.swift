//
//  PrivacyViewController.swift
//  SplayGameSDK
//
//  Created by Nguyen Nang on 8/4/20.
//  Copyright © 2020 splay. All rights reserved.
//


import UIKit
import WebKit

class PrivacyViewController: CustomViewController {
    
    //-----------------------
    // MARK: - ENUM
    //-----------------------
    
    enum Size: CGFloat {
        case button = 36, padding10 = 10
    }
    
    //-----------------------
    // MARK: - VAR
    //-----------------------
    
    var pageLoad: String?

    //-----------------------
    // MARK: - VIEW
    //-----------------------
    var webView: WKWebView!
    
    
    
    //-----------------------
    // MARK: - LIFE CYCLE
    //-----------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if UIScreen.main.bounds.width > UIScreen.main.bounds.height {
            // Landscape
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                landscapeContentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 320))
                
            } else {
                landscapeContentSizeInPopup = CGSize(width: 336, height: 350)
            }
            
            
        } else {
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                contentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 340))
                
            } else {
                contentSizeInPopup = CGSize(width: 336, height: 350)
            }
        }
        
        setupAllSubviews()
        
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        updateFrame()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        

    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
    }
    
    func updateFrame() {
        
        btnClose.frame = CGRect(x: view.frame.width - 40,
                                y: 0,
                                width: 40,
                                height: 40)
        
        
        labelTitle.frame = CGRect(x: 0,
                                  y: 0,
                                  width: view.frame.width,
                                  height: 40)
        
        seperator.frame = CGRect(x: 0,
                                 y: labelTitle.frame.height - 1 / UIScreen.main.scale,
                                 width: view.frame.width,
                                 height: 1 / UIScreen.main.scale)

        
        webView.frame = CGRect(x: 0,
                                  y: seperator.frame.maxY,
                                  width: view.frame.width,
                                  height: view.frame.height - seperator.frame.maxY)

    }
}






extension PrivacyViewController {
    

    func loadLanguage() {

        if let pageLoad = pageLoad, pageLoad.contains("terms") {
            labelTitle.text = LanguageSetting.get("TitleViewTerms", alter: nil)
            loadContenWebviewWithNamePage(LanguageSetting.get("page_terms", alter: nil))
            
        } else {
            labelTitle.text = LanguageSetting.get("TitleViewPrivacy", alter: nil)
            loadContenWebviewWithNamePage(LanguageSetting.get("page_privacy", alter: nil))
        }
    }
    
    func loadContenWebviewWithNamePage(_ namePage: String) {
//        DVLog(@"__________name page %@",namePage);
        
//        let pageResource = String(format: "Resources.bundle/%@", namePage)
//        guard let url = Bundle.main.url(forResource: pageResource, withExtension: "html") else { return }
        
        
        let pageResource = String(format: "%@", namePage)
        guard let url = Bundle.module.url(forResource: pageResource, withExtension: "html") else { return }
        
        webView.load(URLRequest(url: url))
    }
}

extension PrivacyViewController {
    
    @objc func changeLanguage() {
        loadLanguage()
    }
    
    @objc func orientationChanged(_ sender: Notification) {
        
        let device = UIDevice.current
        
        switch device.orientation {
        case .faceUp:
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                contentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 340))
                
            } else {
                contentSizeInPopup = CGSize(width: 336, height: 350)
            }
            
            
        case .portrait, .portraitUpsideDown:
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                contentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 340))
                
            } else {
                contentSizeInPopup = CGSize(width: 336, height: 350)
            }
            
        case .landscapeLeft, .landscapeRight:
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                landscapeContentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 320))
                
            } else {
                landscapeContentSizeInPopup = CGSize(width: 336, height: 350)
            }
            
            
        default:
            break
        }
        
        updateFrame()
    }
    
    @objc func btnCloseClick(_ sender: UIButton) {
        
//        let dict = ["Key_segment_index" : 0]
//        DispatchQueue.main.async(execute: {
//            NotificationCenter.default.post(name: NSNotification.Name("SegmentIndex"), object: self, userInfo: dict)
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: CHANGE_LANGUAGE_SETTING), object: nil)
//        })

        popupController?.popViewController(animated: true)
        
    }
}


//-----------------------
// MARK: - WEBVIEW DELEGATE
//-----------------------

extension PrivacyViewController: WKNavigationDelegate {
    
}


extension PrivacyViewController: WKUIDelegate {
    
}

//-----------------------
// MARK: - SETUP
//-----------------------

extension PrivacyViewController {
    
    func setupAllSubviews() {
        
        
        
        title = "Đăng nhập"
        view.backgroundColor = UIColor(red: 248/255.0, green: 248/255.0, blue: 248/255.0, alpha: 1.0)
        view.layer.cornerRadius = 8.0
        view.layer.borderColor =  UIColor(red: 13/255.0, green: 183/255.0, blue: 212/255.0, alpha: 1.0).cgColor
        view.layer.borderWidth = 1.5
        view.layer.masksToBounds = true
        
        setupNotification()
        

        btnClose.addTarget(self, action: #selector(self.btnCloseClick(_:)), for: .touchUpInside)
        
        
        
        
        webView = setupWebView()
       

        view.addSubview(webView)
        view.addSubview(seperator)
        view.addSubview(labelTitle)
        view.addSubview(btnClose)
        
        loadLanguage()
        

    }
    
    func setupNotification() {
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.changeLanguage),
                                               name: Notification.Name(rawValue: CHANGE_LANGUAGE_SETTING),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.orientationChanged(_:)),
                                               name: UIDevice.orientationDidChangeNotification,
                                               object: UIDevice.current)
        
        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
        
    }
    
    
    
    func setupWebView() -> WKWebView {
        let webConfiguration = WKWebViewConfiguration()
        
        let web = WKWebView(frame: .zero, configuration: webConfiguration)
        
        web.backgroundColor = UIColor.white
        web.uiDelegate = self
        web.navigationDelegate = self
        web.scrollView.maximumZoomScale = 15 // set as you want.
        web.scrollView.minimumZoomScale = 1 // set as you want.
        web.scrollView.showsHorizontalScrollIndicator = false
 
        web.allowsBackForwardNavigationGestures = true
        if #available(iOS 9.0, *) {
            web.allowsLinkPreview = true
        } else {
            // Fallback on earlier versions
        }
        
        
        return web
    }
    
    
}



