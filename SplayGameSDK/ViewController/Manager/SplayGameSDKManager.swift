//
//  SplayGameSDKManager.swift
//  SplayGameSDK
//
//  Created by Nguyen Nang on 7/1/20.
//  Copyright © 2020 splay. All rights reserved.
//

import UIKit
import Firebase
import FBSDKCoreKit
import FBSDKLoginKit
import FBSDKShareKit
import AppsFlyerLib
import GoogleSignIn
import STPopup
import OneSignal
import UserNotifications
import CleanroomLogger
import STPopup
import SwiftyStoreKit
import AppTrackingTransparency
import AdBrixRM
import AdSupport
import HubjsTracker


public typealias CompletionHandlerResult = (_ completionResult: String?) -> Void
public typealias InviteFriendCompleteHandle = (_ requestID: String?, _ numberPeople: Int) -> Void
public typealias AuthenCompletionHandle = (_ suceess: Bool, _ statusResult: Int, _ message: String?) -> Void
public typealias InAppPurchaseCompletionHandle = (_ suceess: Bool, _ statusResult: Int, _ message: String?) -> Void



//---------------------------------
// MARK: - SDK MANAGER
//---------------------------------

@objc public class SplayGameSDKManager: NSObject {
    
    public var closeButton: Bool = true
    public var completionHandler: ((_ finished: Bool) -> Void)?
    public var completionHandleAvatar: ((_ url: String) -> Void)?
    public var inAppPurchaseCompletion: InAppPurchaseCompletionHandle? = nil
    
    private lazy var facebookShare: FacebookShare? = FacebookShare(parent: self)
    private lazy var adBrixDeeplink: AdbrixDeeplink? = AdbrixDeeplink(parent: self)
    private lazy var adBrixDefferredDeeplink: AdBrixDefferredDeeplink? = AdBrixDefferredDeeplink(parent: self)
    private lazy var adBrixPushLocal: AdBrixPushLocal? = AdBrixPushLocal(parent: self)
    private lazy var adBrixPushRemote: AdBrixPushRemote? = AdBrixPushRemote(parent: self)
    private lazy var appsFlyer: AppsFlyerLog? = AppsFlyerLog(parent: self)
    

    
    var timer: Timer? = nil
    var second: Int = 0
    
    //    @objc(sharedManager)
    //    static let shared = SplayGameSDKManager()
    

    static let shared = SplayGameSDKManager()
    
    @objc public class func sharedManager() -> SplayGameSDKManager {
        // `dispatch_once()` call was converted to a static variable initializer
        return shared
    }
    
    @objc public class func splayGameSDKInit(_ apikeyLive: String?, APIKeySandbox apikeySandbox: String?) {
        
        SplayGameSDKManager.sharedManager().configLogger()
        
        Log.message(.info, message: "                                 ")
        Log.message(.info, message: "---------------------------------")
        Log.message(.info, message: "----------- START APP -----------")
        Log.message(.info, message: "---------------------------------")
        Log.message(.info, message: "                                 ")
        
        
        let agencyIDApp = Bundle.main.infoDictionary?["AgencyID"] as? String ?? ""
        let typeConfig  = Bundle.main.infoDictionary?["Sandbox"] as? Bool ?? false
        let appleAppID  = Bundle.main.infoDictionary?["AppleAppID"] as? String ?? ""
        
        
        let userDefault = UserDefaults.standard
        userDefault.set(typeConfig ? apikeySandbox : apikeyLive, forKey: KEY_API_CONFIG_APP)
        userDefault.set(agencyIDApp, forKey: KEY_AGENCY_APP)
        userDefault.set(appleAppID.contains("id") ? appleAppID : String(format: "id%@", appleAppID), forKey: KEY_APPID)
        userDefault.synchronize()
        
 

        Log.message(.info, message: String(format: "ApikeyLive = %@ | ApikeySandbox = %@ | AgencyIDApp = %@",
                                           apikeyLive ?? "",
                                           apikeySandbox ?? "",
                                           agencyIDApp))
        
        
        SplayGameSDKManager.sharedManager().setupLanguage()
        SplayGameSDKManager.sharedManager().configAdbrix()

        
        guard let apikey = apikeyLive, apikey.count > 0,
              let url = URL(string: "http://c.vtcmobile.com/" + apikey + ".txt"),
              let network = UtilitieWebService.shareSingleton()?.checkNetWork(), network else {
            
            SplayGameSDKManager.sharedManager().getConfigApp()
            return
            
        }

        
        DispatchQueue.main.async {
            
            do {
                guard let data = try String(contentsOf: url).data(using: .utf8),
                      let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [Dictionary<String,Any>] else {
                    
                    SplayGameSDKManager.sharedManager().getConfigApp()
                    return
                }
                
                print(jsonArray) // use the json here
                
                let configs: [ConfigAPI] = jsonArray.map { ConfigAPI(data: $0) }
                
                if let config = configs.first(where: { $0.agencyID == Int(agencyIDApp) }) {
                    
                    /// Lưu lại config
                    AppData.instance.config = config
                    
                } else if let config = configs.first(where: { !$0.isReview  }) {
                    
                    /// Lưu lại config
                    AppData.instance.config = config
                }
                SplayGameSDKManager.sharedManager().getConfigApp()
         
                
            } catch {
                print("Error: \(error.localizedDescription)")
                SplayGameSDKManager.sharedManager().getConfigApp()
            }
        }
    }
}




//---------------------------
// MARK: - CONFIG DELEGATE
//---------------------------

extension SplayGameSDKManager {
    
    // Bổ sung 4/3/2021
    
    @objc public func application(_ application: UIApplication, continueUserActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        
        
  
        guard continueUserActivity.activityType == NSUserActivityTypeBrowsingWeb,
              let incomingURL = continueUserActivity.webpageURL else { return false }
        
        print("DEEPLINK :: UniversialLink was clicked !! incomingURL - \(incomingURL)")
        NSLog("UNIVERSAL LINK OPENN!!!!!!!!!!!!!!!!!")

        AdBrixRM.getInstance.deepLinkOpen(url: incomingURL)
        
        return true
    }

    //Thêm mới
    
    @objc public func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data){
    
        AdBrixRM.getInstance.setRegistrationId(deviceToken: deviceToken)
    }
    
    
    @objc public func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        AdBrixRM.getInstance.userNotificationCenter(center: center, response: response)
        completionHandler()
        
    }
    
    //=============================================================================
    
    
    
    @objc public func application(_ application: UIApplication, openURL url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        Log.message(.info, message: "CONFIG FACEBOOK application openURL: \(url) --- sourceApplication: \(String(describing: sourceApplication))")
        
        
        AppsFlyerLib.shared().handleOpen(url, sourceApplication: sourceApplication, withAnnotation: annotation)
        
        return ApplicationDelegate.shared.application(application,
                                                      open: url,
                                                      sourceApplication: sourceApplication,
                                                      annotation: annotation) || (GIDSignIn.sharedInstance()?.handle(url) ?? false)
        
    }
    
    @objc public func application(_ application: UIApplication, openURL url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        
        Utility.logEventDebug(.openURLOptions)
        
        Log.message(.info, message: "CONFIG FACEBOOK application openURL: \(url) --- options: \(options)")
        
        AdBrixRM.getInstance.deepLinkOpen(url: url)
        
        AppsFlyerLib.shared().handleOpen(url, options: options)
        
        return ApplicationDelegate.shared.application(application, open: url, options: options)
    }
}








//---------------------------
// MARK: - SETUP
//---------------------------

extension SplayGameSDKManager {
    
    
    func setupLanguage() {

        var language = UserDefaults.standard.object(forKey: KEY_SAVE_LANGUAGE_SETTING) as? String ?? ""
        if language.count == 0 { language = NSLocale.preferredLanguages.first ?? "" }
        LanguageSetting.setLanguage(["vi", "vi-VN"].contains(language) ? "vi" : "en")
    }
    
    
    func setupIAP() {
        
        SwiftyStoreKit.completeTransactions(atomically: true) { purchases in
            
            for purchase in purchases {
                switch purchase.transaction.transactionState {
                case .purchased, .restored:
                    let downloads = purchase.transaction.downloads
                    if !downloads.isEmpty {
                        SwiftyStoreKit.start(downloads)
                    } else if purchase.needsFinishTransaction {
                        // Deliver content from server, then:
                        SwiftyStoreKit.finishTransaction(purchase.transaction)
                    }
                    print("\(purchase.transaction.transactionState.debugDescription): \(purchase.productId)")
                case .failed, .purchasing, .deferred:
                    break // do nothing
                @unknown default:
                    break // do nothing
                }
            }
        }
        
        SwiftyStoreKit.updatedDownloadsHandler = { downloads in
            
            // contentURL is not nil if downloadState == .finished
            let contentURLs = downloads.compactMap { $0.contentURL }
            if contentURLs.count == downloads.count {
                print("Saving: \(contentURLs)")
                SwiftyStoreKit.finishTransaction(downloads[0].transaction)
            }
        }
    }
    
    
    
    
    @objc public func configFacebookApplication(_ application: UIApplication, launchOptions: [UIApplication.LaunchOptionsKey : Any]?) {
        
        Log.message(.info, message: "CONFIG FACEBOOK didFinishLaunchingWithOptions: \(String(describing: launchOptions))")
        

        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
  
    }
    
    @objc public func configPushNotificationApp(_ application: UIApplication, launchOptions: [String : Any]) {
        

        OneSignal.setLogLevel(.LL_VERBOSE, visualLevel: .LL_NONE)
        
        guard let oneSignalAppID = Bundle.main.infoDictionary?["OneSignalAppID"] as? String, oneSignalAppID.count > 0 else { return }
        
        
        Log.message(.info, message: "onesignal appid : \(oneSignalAppID)")
        
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false]
        OneSignal.initWithLaunchOptions(launchOptions,
                                        appId: oneSignalAppID,
                                        handleNotificationAction: { _ in
                                            
                                        }, settings: onesignalInitSettings)
        
        
        OneSignal.inFocusDisplayType = .notification
        
        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
        
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            
            AdBrixRM.getInstance.setPushEnable(toPushEnable: accepted)
            guard accepted else { return }
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
            
        })
        
        // Call syncHashedEmail anywhere in your iOS app if you have the user's email.
        // This improves the effectiveness of OneSignal's "best-time" notification scheduling feature.
        // [OneSignal syncHashedEmail:userEmail];
    }
    
    @objc public func configFireBase() {
        
        FirebaseApp.configure()
        Analytics.setUserID(UserDefaults.standard.string(forKey: KEY_SAVE_USERID))
        
//        if let config = AppData.instance.config, config.isReview {
            Analytics.setAnalyticsCollectionEnabled(true)
//        }
        
        
        guard let path = Bundle.main.path(forResource: "GoogleService-Info", ofType: "plist"),
              let plistXML = FileManager.default.contents(atPath: path) else {
            
            Log.message(.error, message: "Error GoogleService-Info")
            return
        }
        
        var propertyListFormat = PropertyListSerialization.PropertyListFormat.xml //Format of the Property List.
        
        do {//convert the data to a dictionary and handle errors.
            guard let dict = try PropertyListSerialization.propertyList(from: plistXML,
                                                                        options: .mutableContainersAndLeaves,
                                                                        format: &propertyListFormat) as? [String: Any] else { return }
            
            // Setup the Sign-In instance.
            GIDSignIn.sharedInstance()?.clientID = dict["CLIENT_ID"] as? String
            
        } catch {
            Log.message(.error, message: "Error reading plist: \(error), format: \(propertyListFormat)")
        }
    }
    
    
    
    @objc public func configAppsFlyer() {
        

        let appsFlyerDevKey = Bundle.main.infoDictionary?["AppsFlyerDevKey"] as? String ?? ""
        let appleAppID = Bundle.main.infoDictionary?["AppleAppID"] as? String ?? ""

        Log.message(.info, message: "key = \(appsFlyerDevKey), appid = \(appleAppID)")
        
        guard appleAppID.count > 0 && appsFlyerDevKey.count > 0 else {
            crashApp(message: "Not found AppsFlyerDevKey and AppleAppID in file Info.Plist")
        }
        
        appsFlyer?.delegate = self
        
        AppsFlyerLib.shared().appsFlyerDevKey = appsFlyerDevKey
        AppsFlyerLib.shared().appleAppID = appleAppID
        AppsFlyerLib.shared().isDebug = true
        AppsFlyerLib.shared().delegate = appsFlyer
   
        
        guard #available(iOS 14, *) else { return }
        
        
        if let showAlertATT = Bundle.main.infoDictionary?["RequiredATT"] as? Bool, !showAlertATT { return }
        
        
        AppsFlyerLib.shared().waitForATTUserAuthorization(timeoutInterval: 60)
        
        
        let adBrix = AdBrixRM.getInstance
        
        DispatchQueue.main.async {
            ATTrackingManager.requestTrackingAuthorization() { status in
                print("Tracking Status: \(status)")
                
                switch status{
                case.authorized:
                    adBrix.startGettingIDFA()
                    Settings.setAdvertiserTrackingEnabled(true)

                default:
                    adBrix.stopGettingIDFA()
                    Settings.setAdvertiserTrackingEnabled(false)
                }
                
            }
        }
            
    }
    
    
    //Call function in didFinishLaunchingWithOptions appdelegate
    @objc public func configAppsFlyerTrackUninstall(_ application: UIApplication, launchOptions: [String : Any]) {
        // AppsFlyer SDK Initialization
        // Register for Push Notifications
        
        UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in
            
            
            AdBrixRM.getInstance.setPushEnable(toPushEnable: granted)
            
            if granted {
                
                DispatchQueue.main.async {
                    application.registerForRemoteNotifications()
                    UIApplication.shared.registerForRemoteNotifications()
                    
                }
            }
        }
    
        
        application.registerForRemoteNotifications()
        application.setMinimumBackgroundFetchInterval(UIApplication.backgroundFetchIntervalMinimum)
        application.applicationIconBadgeNumber = 0
        
    }
    
    
    @objc public func appsFlyerRegisterUninstallWithDeviceToken(_ deviceToken: Data?) {
        AppsFlyerLib.shared().registerUninstall(deviceToken)
        AppsFlyerLib.shared().useUninstallSandbox = false
    }
    
    
    //Call function in applicationDidBecomeActive appdelagate
    @objc public func installEvenFacebookApp() {
        
        //Log the application activation
        AppEvents.singleton.activateApp()
//        AppEvents.activateApp()
    }
    
    //Call function in applicationDidBecomeActive appdelagate
    @objc public func trackingInstallAppWithAppsflyer() {
        // Track Installs, updates & sessions(app opens) (You must include this API to enable tracking)
        AppsFlyerLib.shared().start()
    }
    
    func configAdbrix() {
        
        // AdBrix AppKey / SecretKey setup
        let adBrixAppKey = Bundle.main.infoDictionary?["AdBrixAppKey"] as? String ?? ""
        let adBrixSecretKey = Bundle.main.infoDictionary?["AdBrixSecretKey"] as? String ?? ""
        Log.message(.info, message: "adBrixAppKey: \(adBrixAppKey) | adBrixSecretKey: \(adBrixSecretKey)")
        
        adBrixDeeplink?.delegate = self
        adBrixDefferredDeeplink?.delegate = self
        
        // Create AdBrixRm instance
        let adBrix = AdBrixRM.getInstance
        adBrix.initAdBrix(appKey: adBrixAppKey, secretKey: adBrixSecretKey)
        adBrix.setLogLevel(.ERROR)
        adBrix.setEventUploadCountInterval(.MIN)
        adBrix.setEventUploadTimeInterval(.MIN)
        adBrix.startGettingIDFA()
        adBrix.delegateDeeplink = adBrixDeeplink
        adBrix.delegateDeferredDeeplink = adBrixDefferredDeeplink
        
        // Local Push Delegate
        if let pushLocal = adBrixPushLocal {
            adBrix.setAdBrixRmPushLocalDelegate(delegate: pushLocal)
        }
        
        // Reomte Push Delegate
        if let pushRemote = adBrixPushRemote {
            adBrix.setAdBrixRmPushRemoteDelegate(delegate: pushRemote)
        }
          
         
    }
    

    func configLogger() {
        
        #if DEV // Hiển thị log để debug
        
        FileManager.default.createAppDirectory("Log", skipBackupAttribute: true)
        let path = FileManager.default.getDocumentDirectory().appendingPathComponent("Log")
        Log.enable(configuration: CustomLogConfiguration(minimumSeverity: .verbose, dayToKeep: 7, filePath: path.path))
        
        #endif
    }
}



//---------------------------
// MARK: - ADBRIX
//---------------------------


protocol AdbrixDelegate: class {
    func didReceiveDeeplink(_ deeplink: String)
}

protocol AdBrixDeferredDeeplinkDelegate: class {
    func didReceiveDeferredDeeplink(deeplink: String)
}

protocol AdBrixPushLocalDelegate: class {
    func pushLocalCallback(data: Dictionary<String, Any>?, state: UIApplication.State)
}

protocol AdBrixPushRemoteDelegate: class {
    func pushRemoteCallback(data: Dictionary<String, Any>?, state: UIApplication.State)
}



private class AdbrixDeeplink : NSObject, AdBrixRMDeeplinkDelegate {
    
    private weak var parent: SplayGameSDKManager?
    weak var delegate: AdbrixDelegate?
    
    init(parent: SplayGameSDKManager) {
        super.init()
        self.parent = parent
    }
    
    func didReceiveDeeplink(deeplink: String) {
        delegate?.didReceiveDeeplink(deeplink)
    }
}

private class AdBrixDefferredDeeplink: NSObject, AdBrixRMDeferredDeeplinkDelegate {
    
    private weak var parent: SplayGameSDKManager?
    weak var delegate: AdBrixDeferredDeeplinkDelegate?
    
    init(parent: SplayGameSDKManager) {
        super.init()
        self.parent = parent
    }
    
    func didReceiveDeferredDeeplink(deeplink: String) {
        delegate?.didReceiveDeferredDeeplink(deeplink: deeplink)
    }
}

private class AdBrixPushLocal: NSObject, AdBrixRmPushLocalDelegate {

    private weak var parent: SplayGameSDKManager?
    weak var delegate: AdBrixPushLocalDelegate?
    
    init(parent: SplayGameSDKManager) {
        super.init()
        self.parent = parent
    }
    
    func pushLocalCallback(data: Dictionary<String, Any>?, state: UIApplication.State) {
        delegate?.pushLocalCallback(data: data, state: state)
    }
}


private class AdBrixPushRemote: NSObject, AdBrixRmPushRemoteDelegate {

    private weak var parent: SplayGameSDKManager?
    weak var delegate: AdBrixPushRemoteDelegate?
    
    init(parent: SplayGameSDKManager) {
        super.init()
        self.parent = parent
    }
    
    func pushRemoteCallback(data: Dictionary<String, Any>?, state: UIApplication.State) {
        delegate?.pushRemoteCallback(data: data, state: state)
    }
    

}



//---------------------------
// MARK: - FACEBOOK
//---------------------------

extension SplayGameSDKManager {
    
    func logoutFacebook() {
        NSLog("Close session")
        
        let storage: HTTPCookieStorage = HTTPCookieStorage.shared
        
        guard let cookies = storage.cookies else { return }
        
        for cookie in cookies {
   
            if let domainRange = cookie.domain.range(of: "facebook"), !domainRange.isEmpty {
                storage.deleteCookie(cookie)
            }
        }
    }
    
    
    @objc public func publishFacebookFeedWithLinkShare(_ linkshare: String, handler completionHandler: @escaping (Bool) -> Void) {
        shareFacebook(linkshare: linkshare, handler: completionHandler)
    }
    
    
    @objc public func shareFacebook(content: String? = nil, arrayImage: [UIImage] = [], hashtag: String? = nil, linkshare: String? = nil, handler completionHandler: @escaping (Bool) -> Void) {
        
        self.completionHandler = completionHandler
        
        facebookShare?.delegate = self
        
        guard let parentViewController = UIApplication.shared.keyWindow?.rootViewController else { return }
        
        
        let shareDialog = ShareDialog()
        shareDialog.mode = .native
        shareDialog.fromViewController = parentViewController
        shareDialog.delegate = facebookShare
        
        let contentLink = ShareLinkContent()
        let contentPhoto = SharePhotoContent()


        if let url = URL(string: linkshare ?? "") {
            contentLink.contentURL =  url
            contentPhoto.contentURL = url
        }

        if let hashtag = hashtag {
            
            let newHashTag = hashtag.contains("#") ? hashtag : ("#" + hashtag)
            
            contentLink.hashtag = Hashtag(newHashTag)
            contentPhoto.hashtag = Hashtag(newHashTag)
        }
        
        if let content = content { contentLink.quote = content }

        contentPhoto.photos = arrayImage.map { SharePhoto(image: $0, userGenerated: true) }
        
        if contentPhoto.photos.count > 0 {
            shareDialog.shareContent = contentPhoto
        } else {
            shareDialog.shareContent = contentLink
        }
        
        
        if !shareDialog.canShow {
            UserDefaults.standard.set("WebFacebook", forKey: "WebFacebook")
            UserDefaults.standard.synchronize()
            shareDialog.mode = .feedBrowser
        }
        shareDialog.show()
    }
    
}


extension SplayGameSDKManager: FacebookShareDelegate {
    func finishedShareWith(_ success: Bool) {
        
        guard let completion = completionHandler else { return }
        completion(success)
    }
}


//---------------------------
// MARK: - FACEBOOK SHARE
//---------------------------

protocol FacebookShareDelegate: class {
    func finishedShareWith(_ success: Bool)
}

private class FacebookShare : NSObject, SharingDelegate {
    
    private weak var parent: SplayGameSDKManager?
    weak var delegate: FacebookShareDelegate?
    
    init(parent: SplayGameSDKManager) {
        super.init()
        self.parent = parent
    }
    
    // Implement delegate methods here by accessing the members
    // of the parent through the 'parent' variable
    
    public func sharer(_ sharer: Sharing, didCompleteWithResults results: [String : Any]) {
        Log.message(.info, message: "didCompleteWithResults results = \(results)")
        delegate?.finishedShareWith(true)
    }
    
    public func sharer(_ sharer: Sharing, didFailWithError error: Error) {
        Log.message(.error, message: "didFailWithError error = \(error)")
        delegate?.finishedShareWith(false)
    }
    
    public func sharerDidCancel(_ sharer: Sharing) {
        Log.message(.error, message: "FB share cancel")
        delegate?.finishedShareWith(false)
    }
}


//---------------------------
// MARK: - APPSFLYER DELEGATE
//---------------------------

protocol AppsFlyerDelegate: class {
    func onConversionDataSuccess(_ conversionInfo: [AnyHashable : Any])
}

private class AppsFlyerLog: NSObject, AppsFlyerLibDelegate {
    
    private weak var parent: SplayGameSDKManager?
    weak var delegate: AppsFlyerDelegate?
    
    init(parent: SplayGameSDKManager) {
        super.init()
        self.parent = parent
    }
    
    func onConversionDataSuccess(_ conversionInfo: [AnyHashable : Any]) {
        delegate?.onConversionDataSuccess(conversionInfo)
    }
    
    func onConversionDataFail(_ error: Error) {
        print("\(error)")
    }
    
    // Handle Deeplink
    func onAppOpenAttribution(_ attributionData: [AnyHashable: Any]) {
        //Handle Deep Link Data
        print("onAppOpenAttribution data:")
        for (key, value) in attributionData {
            print(key, ":",value)
        }
    }
    
    func onAppOpenAttributionFailure(_ error: Error) {
        print("\(error)")
    }
}
