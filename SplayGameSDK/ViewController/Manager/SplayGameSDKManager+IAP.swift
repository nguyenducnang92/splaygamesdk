//
//  SplayGameSDKManager+IAP.swift
//  SplayGameSDK
//
//  Created by Nguyen Nang on 1/7/21.
//  Copyright © 2021 splay. All rights reserved.
//

import UIKit
import Firebase
import FBSDKCoreKit
import FBSDKLoginKit
import FBSDKShareKit
import AppsFlyerLib
import GoogleSignIn
import STPopup
import OneSignal
import UserNotifications
import CleanroomLogger
import STPopup
import SwiftyStoreKit



//TODO: NANGND Check
public typealias RestorePurchaseCompletionHandle = (_ suceess: Bool, _ statusResult: Int, _ message: String?,_ products: [String]) -> Void


//---------------------------
// MARK: - PAYMENT
//---------------------------

extension SplayGameSDKManager {
    
    
    @objc func countDownTime() {
        guard let _ = timer else { return }
        second += (second == 0 ? 5 : -1)
        var string: String = LanguageSetting.get("payment_paying_please_waiting_a_moment", alter: nil)
        for _ in 0..<(3 - (second % 3)) {
            string += "."
        }
        string += "\n(\(second)s)"
        HUD.updateMessage(string)
    }
    
    
    
    
    
    
    
    /*
     
     @objc public func buyInAppItemPartnerInfo(_ partnerinfo: String,
     andProductID productID: String,
     CompletionHandle completionHandle: InAppPurchaseCompletionHandle?) {
     
     
     guard !AppData.instance.isPayment else { return }
     
     
     
     Utility.logEventDebug(.iapReceiveProductID, parameters: ["product_id": productID,
     "partner_info" : partnerinfo])
     
     guard partnerinfo.count > 0, productID.count > 0 else {
     Utility.showAlertWithMessage(LanguageSetting.get("invalid_purchase_information", alter: nil), viewController: nil)
     return
     }
     
     AppData.instance.isPayment = true
     
     inAppPurchaseCompletion = completionHandle
     Utility.logEventDebug(.iapSendProductID, parameters: ["product_id": "\(productID)",
     "partner_info" : "\(partnerinfo)"])
     
     
     DispatchQueue.main.async {
     HUD.showHUD(LanguageSetting.get("payment_getting_infomation_item", alter: nil))
     }
     
     
     SwiftyStoreKit.retrieveProductsInfo([productID]) { result in
     
     
     guard let product = result.retrievedProducts.first else {
     
     DispatchQueue.main.async {
     HUD.dismissHUD()
     }
     
     AppData.instance.isPayment = false
     Utility.logEventDebug(.iapReceiveProductNil)
     
     Log.message(.error, message: "Inapp not success!.")
     Utility.showAlertWithMessage(LanguageSetting.get("payment_failed", alter: nil), viewController: nil)
     
     if let completion = completionHandle {
     completion(false, 2, "In-app Purchase items Fail")
     }
     return
     }
     
     self.buyProduct(partnerinfo, skProduct: product, CompletionHandle: completionHandle)
     }
     }
     
     @objc func buyProduct(_ partnerinfo: String,
     skProduct: SKProduct,
     CompletionHandle completionHandle: InAppPurchaseCompletionHandle?) {
     
     
     DispatchQueue.main.async {
     
     self.second = 60
     
     HUD.updateMessage(LanguageSetting.get("payment_paying_please_waiting_a_moment", alter: nil) + "\n(\(self.second)s)")
     
     //            HUD.showMessage(LanguageSetting.get("payment_paying_please_waiting_a_moment", alter: nil) + "\n(\(self.second)s)")
     
     self.timer?.invalidate()
     self.timer = Timer.scheduledTimer(timeInterval: 1.0,
     target: self,
     selector: #selector(self.countDownTime),
     userInfo: nil,
     repeats: true)
     }
     
     
     SwiftyStoreKit.purchaseProduct(skProduct, atomically: false) { result in
     
     AppData.instance.isPayment = false
     self.timer?.invalidate()
     self.timer = nil
     
     
     switch result {
     case .success(let purchase):
     
     let downloads = purchase.transaction.downloads
     if !downloads.isEmpty {
     
     print("start(downloads)")
     
     SwiftyStoreKit.start(downloads)
     }
     // Deliver content from server, then:
     if purchase.needsFinishTransaction {
     
     print("needsFinishTransaction")
     SwiftyStoreKit.finishTransaction(purchase.transaction)
     }
     
     
     print("transaction.transactionState: \(purchase.transaction.transactionState)")
     
     Log.message(.info, message: "transaction.transactionState: \(purchase.transaction.transactionState.rawValue)")
     
     switch purchase.transaction.transactionState {
     case .failed:
     
     HUD.dismissHUD()
     
     Utility.logEventDebug(.iapPaymentAppleFailed, parameters: ["transactionState" : "failed"])
     Utility.showAlertWithMessage(LanguageSetting.get("payment_failed", alter: nil), viewController: nil)
     
     
     Log.message(.error, message: "Inapp not success!.")
     if let completion = completionHandle {
     completion(false, 2, "In-app Purchase items Fail")
     }
     
     case .purchased, .restored:
     
     Utility.logEventDebug(.iapPaymentAppleSuccess, parameters: ["transactionState" : "\(purchase.transaction.transactionState)"])
     
     Log.message(.info, message: "SendDataInappPuschaseToSeverWithPartnerInfo")
     
     self.sendDataInappPuschaseToSeverWithPartnerInfo(partnerinfo,
     transaction: purchase.transaction)
     
     default:
     
     //                    DispatchQueue.main.async {
     HUD.dismissHUD()
     //                    }
     
     Utility.logEventDebug(.iapPaymentAppleDefaultFailed, parameters: ["transactionState" : "\(purchase.transaction.transactionState)"])
     
     Log.message(.error, message: "transaction.transactionState ERROR")
     break
     }
     
     case .error(let error):
     
     print("error: \(error.localizedDescription) | \(error.code.rawValue)")
     
     switch error.code {
     case .cloudServiceNetworkConnectionFailed:
     self.buyProduct(partnerinfo, skProduct: skProduct, CompletionHandle: completionHandle)
     
     default:
     HUD.dismissHUD()
     if let alert = self.alertForPurchaseResult(result) { self.showAlert(alert) }
     }
     
     
     }
     
     
     }
     
     }
     
     
     
     func sendDataInappPuschaseToSeverWithPartnerInfo(_ partnerInfo: String, transaction: PaymentTransaction) {
     
     
     DispatchQueue.main.async {
     HUD.updateMessage(LanguageSetting.get("payment_completing_payment", alter: nil))
     //            HUD.showMessage(LanguageSetting.get("payment_completing_payment", alter: nil))
     }
     
     
     var receipt: String = ""
     
     if let url = Bundle.main.appStoreReceiptURL, FileManager.default.fileExists(atPath: url.path) {
     do {
     receipt = try Data(contentsOf: url).base64EncodedString(options: [])
     } catch {
     Log.message(.error, message: "Error sendDataInappPuschaseToSeverWithPartnerInfo: \(error)")
     }
     }
     
     Log.message(.info, message: "sendDataInAppPurchaseToSever: \(partnerInfo)")
     
     
     Utility.logEventDebug(.iapSendPaymentInfoServer, parameters: ["partnerInfo" : partnerInfo])
     
     UtilitieWebService
     .shareSingleton()?
     .sendDataInAppPurchaseToSever(withAccessToken: UserDefaults.standard.string(forKey: KEY_SAVE_USER_TOKEN),
     receiptData: receipt,
     partnerInfo: partnerInfo) { (results, error, resultCode) in
     
     DispatchQueue.main.async {
     HUD.dismissHUD()
     }
     
     Utility.logEventDebug(.iapReveiceResultServer, parameters: ["result_code" : "\(resultCode)"])
     
     Log.message(.info, message: "RESPONE InAppPurchaseToSever results \(String(describing: results)) | resultCode: \(resultCode)")
     
     var messager: String = ""
     if let result = results as? [String : Any],
     let message = result[KEY_MESSAGE] as? String, message.count > 0 {
     
     messager = message
     Utility.showAlertWithMessage(message, viewController: nil)
     }
     
     
     
     switch resultCode {
     case ResultCodeSuccess:
     
     Utility.logEventDebug(.iapFinishedPaymentSuccess, parameters: ["is_success" : "true"])
     
     Log.message(.info, message: "InAppPurchaseToSever SUCCESS")
     
     
     SwiftyStoreKit.finishTransaction(transaction)
     
     //                    SKPaymentQueue.default().finishTransaction(transaction)
     //                [[SplayGameSDKManager sharedManager] logPurchaseEvent:[NSString stringWithFormat:@"%@",skproduct.price] productId:skproduct.productIdentifier paymentInapp:@"IAP"];
     //                    IAPHelper.shared()?.production = false
     
     //                    IAPShare.sharedHelper()?.iap.production = false
     
     
     if let completion = self.inAppPurchaseCompletion { completion(true, 0, messager) }
     
     default:
     
     
     
     Utility.logEventDebug(.iapFinishedPaymentFailed, parameters: ["is_success" : "failed"])
     
     Log.message(.info, message: "InAppPurchaseToSever FAILED")
     
     if let completion = self.inAppPurchaseCompletion { completion(false, 1, messager) }
     }
     
     }
     }
     
     // swiftlint:disable cyclomatic_complexity
     func alertForPurchaseResult(_ result: PurchaseResult) -> UIAlertController? {
     switch result {
     case .success(let purchase):
     print("Purchase Success: \(purchase.productId)")
     return nil
     case .error(let error):
     print("Purchase Failed: \(error)")
     switch error.code {
     case .unknown: return alertWithTitle("Purchase failed", message: error.localizedDescription)
     case .clientInvalid: // client is not allowed to issue the request, etc.
     return alertWithTitle("Purchase failed", message: "Not allowed to make the payment")
     case .paymentCancelled: // user cancelled the request, etc.
     return nil
     case .paymentInvalid: // purchase identifier was invalid, etc.
     return alertWithTitle("Purchase failed", message: "The purchase identifier was invalid")
     case .paymentNotAllowed: // this device is not allowed to make the payment
     return alertWithTitle("Purchase failed", message: "The device is not allowed to make the payment")
     case .storeProductNotAvailable: // Product is not available in the current storefront
     return alertWithTitle("Purchase failed", message: "The product is not available in the current storefront")
     case .cloudServicePermissionDenied: // user has not allowed access to cloud service information
     return alertWithTitle("Purchase failed", message: "Access to cloud service information is not allowed")
     case .cloudServiceNetworkConnectionFailed: // the device could not connect to the nework
     return alertWithTitle("Purchase failed", message: "Could not connect to the network")
     case .cloudServiceRevoked: // user has revoked permission to use this cloud service
     return alertWithTitle("Purchase failed", message: "Cloud service was revoked")
     default:
     return alertWithTitle("Purchase failed", message: (error as NSError).localizedDescription)
     }
     }
     }
     
     func alertWithTitle(_ title: String, message: String) -> UIAlertController {
     
     let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
     alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
     return alert
     }
     
     func showAlert(_ alert: UIAlertController) {
     guard let vc = UIApplication.shared.keyWindow?.rootViewController else { return }
     
     vc.present(alert, animated: true, completion: nil)
     }
     */
    
    
    
    @objc public func buyInAppItemPartnerInfo(_ partnerinfo: String,
                                              andProductID productID: String,
                                              CompletionHandle completionHandle: InAppPurchaseCompletionHandle?) {
        
        
        guard !AppData.instance.isPayment else { return }
        AppData.instance.isPayment = true
        
        Utility.logEventDebug(.iapReceiveProductID, parameters: ["product_id": productID,
                                                                 "partner_info" : partnerinfo])
        
        guard partnerinfo.count > 0, productID.count > 0 else {
            Utility.showAlertWithMessage(LanguageSetting.get("invalid_purchase_information", alter: nil), viewController: nil)
            AppData.instance.isPayment = false
            return
        }
        
        inAppPurchaseCompletion = completionHandle
        Utility.logEventDebug(.iapSendProductID, parameters: ["product_id": "\(productID)",
                                                              "partner_info" : "\(partnerinfo)"])
        
        
        HUD.showHUD(LanguageSetting.get("payment_getting_infomation_item", alter: nil))
        
        
        IAPShare.sharedHelper()?.iap = IAPHelper(productIdentifiers: NSSet(object: productID) as? Set<AnyHashable>)
        IAPShare.sharedHelper()?.iap.production = true
        IAPShare.sharedHelper()?.iap.requestProducts {(request, response) in
            
            
            Log.message(.info, message: "RequestProducts respones: \(String(describing: response))")
            
            Utility.logEventDebug(.iapReceiveProduct, parameters: ["product" : "\(response?.products.count ?? 0)"])
            
            guard let response = response, let skProduct = response.products.first else {
                
                DispatchQueue.main.async {
                    HUD.dismissHUD()
                }
                
                AppData.instance.isPayment = false
                Utility.logEventDebug(.iapReceiveProductNil)
                
                Log.message(.error, message: "Inapp not success!.")
                Utility.showAlertWithMessage(LanguageSetting.get("payment_failed", alter: nil), viewController: nil)
                
                if let completion = completionHandle {
                    completion(false, 2, "In-app Purchase items Fail")
                }
                return
            }
            
            
            Log.message(.info, message: "produc_________ \(skProduct)")
            self.buyProduct(partnerinfo, skProduct: skProduct, CompletionHandle: completionHandle)
            
        }
    }
    
    
    @objc func buyProduct(_ partnerinfo: String,
                          skProduct: SKProduct,
                          CompletionHandle completionHandle: InAppPurchaseCompletionHandle?) {
        
        
        DispatchQueue.main.async {
            
            self.second = 60
            
            HUD.updateMessage(LanguageSetting.get("payment_paying_please_waiting_a_moment", alter: nil) + "\n(\(self.second)s)")
            
            self.timer?.invalidate()
            self.timer = Timer.scheduledTimer(timeInterval: 1.0,
                                              target: self,
                                              selector: #selector(self.countDownTime),
                                              userInfo: nil,
                                              repeats: true)
        }
        
        
        
        Log.message(.info, message: "product: \(skProduct.localizedTitle) | price: \(skProduct.price)")
        Utility.logEventDebug(.iapSendProduct, parameters: ["product" : skProduct.localizedTitle,
                                                            "price" : "\(skProduct.price)"])
        
        
        IAPShare.sharedHelper()?.iap.buyProduct(skProduct) { transaction in
            
            AppData.instance.isPayment = false
            self.timer?.invalidate()
            self.timer = nil
            
            
            Log.message(.info, message: "RESPONES BUY PRODUCT: \(String(describing: transaction))")
            
            Utility.logEventDebug(.iapReceiveResultApple, parameters: ["transaction" : "\(String(describing: transaction))",
                                                                       "error" : transaction?.error?.localizedDescription ?? ""])
            
            guard let transaction = transaction else {
                
                DispatchQueue.main.async {
                    HUD.dismissHUD()
                }
                
                Utility.logEventDebug(.iapPaymentAppleTransactionNil)
                Utility.showAlertWithMessage(LanguageSetting.get("payment_failed", alter: nil), viewController: nil)
                Log.message(.error, message: "Transaction nil")
                if let completion = completionHandle { completion(false, 2, "In-app Purchase items Fail") }
                return
            }
            
            if let error = transaction.error {
                
                DispatchQueue.main.async {
                    HUD.dismissHUD()
                }
                
                Utility.logEventDebug(.iapPaymentAppleTransactionError, parameters: ["error" : error.localizedDescription])
                
                if let config = AppData.instance.config, config.isReview {
                    Utility.sendEventToFirebaseWith("iap_" + Utility.normalizeString(error.localizedDescription.replacingOccurrences(of: " ", with: "")).prefix(30) , parameters: nil)
                }
                
                
                
                Utility.showAlertWithMessage(error.localizedDescription, viewController: nil)
                
                if let completion = completionHandle { completion(false, 2, "In-app Purchase items Fail") }
                SKPaymentQueue.default().finishTransaction(transaction)
                return
            }
            
            
            Log.message(.info, message: "transaction.transactionState: \(transaction.transactionState.rawValue)")
            
            switch transaction.transactionState {
            case .failed:
                
                DispatchQueue.main.async {
                    HUD.dismissHUD()
                }
                
                Utility.logEventDebug(.iapPaymentAppleFailed, parameters: ["transactionState" : "failed"])
                
                Utility.showAlertWithMessage(LanguageSetting.get("payment_failed", alter: nil), viewController: nil)
                
                
                Log.message(.error, message: "Inapp not success!.")
                if let completion = completionHandle {
                    completion(false, 2, "In-app Purchase items Fail")
                }
                
            case .purchased, .restored:
                
                Utility.logEventDebug(.iapPaymentAppleSuccess, parameters: ["transactionState" : "\(transaction.transactionState)"])
                
                Log.message(.info, message: "SendDataInappPuschaseToSeverWithPartnerInfo")
                
                self.sendDataInappPuschaseToSeverWithPartnerInfo(partnerinfo,
                                                                 transaction: transaction)
                
            default:
                
                DispatchQueue.main.async {
                    HUD.dismissHUD()
                }
                
                Utility.logEventDebug(.iapPaymentAppleDefaultFailed, parameters: ["transactionState" : "\(transaction.transactionState)"])
                
                Log.message(.error, message: "transaction.transactionState ERROR")
                break
            }
        }
    }
    
    
    @objc func sendDataInappPuschaseToSeverWithPartnerInfo(_ partnerInfo: String, transaction: SKPaymentTransaction) {
        
        
        DispatchQueue.main.async {
            HUD.updateMessage(LanguageSetting.get("payment_completing_payment", alter: nil))
        }
        
        
        var receipt: String = ""
        
        if let url = Bundle.main.appStoreReceiptURL, FileManager.default.fileExists(atPath: url.path) {
            do {
                receipt = try Data(contentsOf: url).base64EncodedString(options: [])
            } catch {
                Log.message(.error, message: "Error sendDataInappPuschaseToSeverWithPartnerInfo: \(error)")
            }
        }
        
        
        
        
        
        Log.message(.info, message: "sendDataInAppPurchaseToSever: \(partnerInfo)")
        
        
        Utility.logEventDebug(.iapSendPaymentInfoServer, parameters: ["partnerInfo" : partnerInfo])
        
        UtilitieWebService
            .shareSingleton()?
            .sendDataInAppPurchaseToSever(withAccessToken: UserDefaults.standard.string(forKey: KEY_SAVE_USER_TOKEN),
                                          receiptData: receipt,
                                          partnerInfo: partnerInfo) { (results, error, resultCode) in
                
                DispatchQueue.main.async {
                    HUD.dismissHUD()
                }
                
                Utility.logEventDebug(.iapReveiceResultServer, parameters: ["result_code" : "\(resultCode)"])
                
                Log.message(.info, message: "RESPONE InAppPurchaseToSever results \(String(describing: results)) | resultCode: \(resultCode)")
                
                var messager: String = ""
                if let result = results as? [String : Any],
                   let message = result[KEY_MESSAGE] as? String, message.count > 0 {
                    
                    messager = message
                    Utility.showAlertWithMessage(message, viewController: nil)
                }
                
                
                
                switch resultCode {
                case ResultCodeSuccess:
                    
                    Utility.logEventDebug(.iapFinishedPaymentSuccess, parameters: ["is_success" : "true"])
                    
                    Log.message(.info, message: "InAppPurchaseToSever SUCCESS")
                    
                    
                    
                    
                    SKPaymentQueue.default().finishTransaction(transaction)
                    //                [[SplayGameSDKManager sharedManager] logPurchaseEvent:[NSString stringWithFormat:@"%@",skproduct.price] productId:skproduct.productIdentifier paymentInapp:@"IAP"];
                    //                    IAPHelper.shared()?.production = false
                    
                    
                    
                    if let completion = self.inAppPurchaseCompletion { completion(true, 0, messager) }
                    
                default:
                    
                    
                    
                    Utility.logEventDebug(.iapFinishedPaymentFailed, parameters: ["is_success" : "failed"])
                    
                    Log.message(.info, message: "InAppPurchaseToSever FAILED")
                    
                    if let completion = self.inAppPurchaseCompletion { completion(false, 1, messager) }
                }
                
            }
    }
    
    
    /*
    //TODO: Demo
    @objc public func restorePurchase(CompletionHandle completionHandle: RestorePurchaseCompletionHandle?) {
        
        IAPShare.sharedHelper()?.iap = IAPHelper(productIdentifiers: NSSet(object: "") as? Set<AnyHashable>)
        IAPShare.sharedHelper()?.iap.production = true
        IAPShare.sharedHelper()?.iap.restoreProducts() { (payment, error)  in
            
            guard let payment = payment, error == nil else {
                print("Restore Error: \(String(describing: error))")
                return
            }
                
            /*
            //check with SKPaymentQueue
            
            // number of restore count
            let numberOfTransactions = payment.transactions.count
            for transaction in payment.transactions {
                
                let purchased = transaction.payment.productIdentifier
                
                if purchased == "com.myproductType.product" {
                    //enable the prodcut here
                }
            }
             */
            
            let arrayPurchased = payment.transactions.map { $0.payment.productIdentifier }

            //enable the prodcut here
            
            print("arrayPurchased: \(arrayPurchased)")
            
            if let completionHandle = completionHandle {
                completionHandle(true, 0, "", arrayPurchased)
            }
        }
        
    }
    */
    //TODO: Demo
    
    /*
    
    @objc public func restorePurchase(CompletionHandle completionHandle: RestorePurchaseCompletionHandle?) {
        
        IAPShare.sharedHelper()?.iap = IAPHelper(productIdentifiers: NSSet(object: "") as? Set<AnyHashable>)
        IAPShare.sharedHelper()?.iap.production = true
        IAPShare.sharedHelper()?.iap.restoreProducts() { (payment, error)  in
            
            guard let payment = payment, error == nil else {
                print("Restore Error: \(String(describing: error))")
                
                if let completionHandle = completionHandle {
                    completionHandle(false, 1, "", [])
                }
                return
            }
                
            let arrayPurchased = payment.transactions.map { $0.payment.productIdentifier }

            if let completionHandle = completionHandle {
                completionHandle(true, 0, "", arrayPurchased)
            }
        }
    }
    */
     
    
    
    
    /*
     Tạm bỏ
     */
    
    /*
    @objc public func buyInAppItemNonConsumablePartnerInfo(_ partnerinfo: String,
                                              andProductID productID: String,
                                              CompletionHandle completionHandle: InAppPurchaseCompletionHandle?) {
        
        
        guard !AppData.instance.isPayment else { return }
        AppData.instance.isPayment = true
        
        Utility.logEventDebug(.iapReceiveProductID, parameters: ["product_id": productID,
                                                                 "partner_info" : partnerinfo])
        
        guard partnerinfo.count > 0, productID.count > 0 else {
            Utility.showAlertWithMessage(LanguageSetting.get("invalid_purchase_information", alter: nil), viewController: nil)
            AppData.instance.isPayment = false
            return
        }
        
        inAppPurchaseCompletion = completionHandle
        Utility.logEventDebug(.iapSendProductID, parameters: ["product_id": "\(productID)",
                                                              "partner_info" : "\(partnerinfo)"])
        
        
        HUD.showHUD(LanguageSetting.get("payment_getting_infomation_item", alter: nil))
        
        
        IAPShare.sharedHelper()?.iap = IAPHelper(productIdentifiers: NSSet(object: productID) as? Set<AnyHashable>)
        IAPShare.sharedHelper()?.iap.production = true
        IAPShare.sharedHelper()?.iap.requestProducts {(request, response) in
            
            
            Log.message(.info, message: "RequestProducts respones: \(String(describing: response))")
            
            Utility.logEventDebug(.iapReceiveProduct, parameters: ["product" : "\(response?.products.count ?? 0)"])
            
            guard let response = response, let skProduct = response.products.first else {
                
                DispatchQueue.main.async {
                    HUD.dismissHUD()
                }
                
                AppData.instance.isPayment = false
                Utility.logEventDebug(.iapReceiveProductNil)
                
                Log.message(.error, message: "Inapp not success!.")
                Utility.showAlertWithMessage(LanguageSetting.get("payment_failed", alter: nil), viewController: nil)
                
                if let completion = completionHandle {
                    completion(false, 2, "In-app Purchase items Fail")
                }
                return
            }
            
            
            Log.message(.info, message: "produc_________ \(skProduct)")
            self.buyProduct(partnerinfo, skProduct: skProduct, CompletionHandle: completionHandle)
            
        }
    }
    
    
    @objc func buyNonConsumableProduct(_ partnerinfo: String,
                          skProduct: SKProduct,
                          CompletionHandle completionHandle: InAppPurchaseCompletionHandle?) {
        
        
        DispatchQueue.main.async {
            
            self.second = 60
            
            HUD.updateMessage(LanguageSetting.get("payment_paying_please_waiting_a_moment", alter: nil) + "\n(\(self.second)s)")
            
            self.timer?.invalidate()
            self.timer = Timer.scheduledTimer(timeInterval: 1.0,
                                              target: self,
                                              selector: #selector(self.countDownTime),
                                              userInfo: nil,
                                              repeats: true)
        }
        
        
        
        Log.message(.info, message: "product: \(skProduct.localizedTitle) | price: \(skProduct.price)")
        Utility.logEventDebug(.iapSendProduct, parameters: ["product" : skProduct.localizedTitle,
                                                            "price" : "\(skProduct.price)"])
        
        
        IAPShare.sharedHelper()?.iap.buyProduct(skProduct) { transaction in
            
            AppData.instance.isPayment = false
            self.timer?.invalidate()
            self.timer = nil
            
            
            Log.message(.info, message: "RESPONES BUY PRODUCT: \(String(describing: transaction))")
            
            Utility.logEventDebug(.iapReceiveResultApple, parameters: ["transaction" : "\(String(describing: transaction))",
                                                                       "error" : transaction?.error?.localizedDescription ?? ""])
            
            guard let transaction = transaction else {
                
                DispatchQueue.main.async {
                    HUD.dismissHUD()
                }
                
                Utility.logEventDebug(.iapPaymentAppleTransactionNil)
                Utility.showAlertWithMessage(LanguageSetting.get("payment_failed", alter: nil), viewController: nil)
                Log.message(.error, message: "Transaction nil")
                if let completion = completionHandle { completion(false, 2, "In-app Purchase items Fail") }
                return
            }
            
            if let error = transaction.error {
                
                DispatchQueue.main.async {
                    HUD.dismissHUD()
                }
                
                Utility.logEventDebug(.iapPaymentAppleTransactionError, parameters: ["error" : error.localizedDescription])
                
                if let config = AppData.instance.config, config.isReview {
                    Utility.sendEventToFirebaseWith("iap_" + Utility.normalizeString(error.localizedDescription.replacingOccurrences(of: " ", with: "")).prefix(30) , parameters: nil)
                }
                
                
                
                Utility.showAlertWithMessage(error.localizedDescription, viewController: nil)
                
                if let completion = completionHandle { completion(false, 2, "In-app Purchase items Fail") }
                SKPaymentQueue.default().finishTransaction(transaction)
                return
            }
            
            
            Log.message(.info, message: "transaction.transactionState: \(transaction.transactionState.rawValue)")
            
            switch transaction.transactionState {
            case .failed:
                
                DispatchQueue.main.async {
                    HUD.dismissHUD()
                }
                
                Utility.logEventDebug(.iapPaymentAppleFailed, parameters: ["transactionState" : "failed"])
                
                Utility.showAlertWithMessage(LanguageSetting.get("payment_failed", alter: nil), viewController: nil)
                
                
                Log.message(.error, message: "Inapp not success!.")
                if let completion = completionHandle {
                    completion(false, 2, "In-app Purchase items Fail")
                }
                
            case .purchased, .restored:
                
                Utility.logEventDebug(.iapPaymentAppleSuccess, parameters: ["transactionState" : "\(transaction.transactionState)"])
                
                Log.message(.info, message: "SendDataInappPuschaseToSeverWithPartnerInfo")
                
                
                if let completion = completionHandle {
                    completion(true, 0, "In-app Purchase items Non-Consumable Success")
                }
                
//                self.sendDataInappPuschaseToSeverWithPartnerInfo(partnerinfo,
//                                                                 transaction: transaction)
                
            default:
                
                DispatchQueue.main.async {
                    HUD.dismissHUD()
                }
                
                Utility.logEventDebug(.iapPaymentAppleDefaultFailed, parameters: ["transactionState" : "\(transaction.transactionState)"])
                
                Log.message(.error, message: "transaction.transactionState ERROR")
                break
            }
        }
    }
 */
    
}
