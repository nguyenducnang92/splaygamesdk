//
//  SplayGameSDKManager+Delegate.swift
//  SplayGameSDK
//
//  Created by Nguyen Nang on 3/1/21.
//  Copyright © 2021 splay. All rights reserved.
//

import UIKit
import Firebase
import FBSDKCoreKit
import FBSDKLoginKit
import FBSDKShareKit
import AppsFlyerLib
import GoogleSignIn
import STPopup
import OneSignal
import UserNotifications
import CleanroomLogger
import STPopup
import SwiftyStoreKit
import AppTrackingTransparency
import AdBrixRM
import AdSupport
import GoogleTagManager

//---------------------------
// MARK: - IMAGE PICKER DELEGATE
//---------------------------

extension SplayGameSDKManager: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @objc public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        
        guard let image = info[.originalImage] as? UIImage else { return }
        let myImageData = image.jpegData(compressionQuality: 0.9)
        let avatarWith: Double? = Double(Bundle.main.infoDictionary?["AvatarWidth"] as? String ?? "")
        let avatarHeight: Double? =  Double(Bundle.main.infoDictionary?["AvatarHeight"] as? String ?? "")
        var newAvatarSize = CGSize(width: 200, height: 200)
        
        if let width = avatarWith, let height = avatarHeight {
            newAvatarSize = CGSize(width: CGFloat(width), height: CGFloat(height))
        }
        

        let strEncode = Base64.encode(myImageData)
        
        let userDefault = UserDefaults.standard
        userDefault.set(strEncode, forKey: KEY_DATA_IMAGE_UPLOAD_TO_SERVER)
        userDefault.synchronize()
        
        let accessToken = userDefault.object(forKey: KEY_SAVE_USER_TOKEN) as? String ?? ""
        
        
        picker.dismiss(animated: true) {
            
            MBProgressHUD.showAdded(to: UIApplication.shared.keyWindow?.rootViewController?.view ?? UIView(), animated: true)
            
            UtilitieWebService
                .shareSingleton()?
                .uploadavatar(withStringImage: self.handleImage(image, scaledToSize: newAvatarSize),
                              accessToken: accessToken) {  (results, error, resultCode)  in
                    
                    MBProgressHUD.hide(for: UIApplication.shared.keyWindow?.rootViewController?.view ?? UIView(), animated: false)
                    
                    guard let result = results as? [String : Any] else {
                        
                        //   if let completion = self.completionHandleAvatar {
                        //   completion("Lỗi: \(results) --- \(resultCode)")
                        //   }
                        return
                        
                    }
                    
                    let urlImage = result[KEY_MESSAGE] as? String ?? ""
                    
                    if let completion = self.completionHandleAvatar {
                        completion(urlImage)
                    }
                }
        }
    }
    

    
    
    func handleImage(_ image: UIImage, scaledToSize newSize: CGSize) -> String {
        
        UIGraphicsBeginImageContext(newSize)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        guard let myImageData = newImage?.jpegData(compressionQuality: 0.9) else { return "" }
        return Base64.encode(myImageData) ?? ""
        
    }
}






//---------------------------
// MARK: - ADBRIX
//---------------------------

extension SplayGameSDKManager: AdbrixDelegate {
    
    func didReceiveDeeplink(_ deeplink: String) {
        print("Adbrix deeplink: \(deeplink)")
    }
}


extension SplayGameSDKManager: AdBrixDeferredDeeplinkDelegate {
    public func didReceiveDeferredDeeplink(deeplink: String) {
        print("DeferredDeeplink: \(deeplink)")
    }
}

extension SplayGameSDKManager: AdBrixPushLocalDelegate, AdBrixPushRemoteDelegate {

    public func pushLocalCallback(data: Dictionary<String, Any>?, state: UIApplication.State) {
        print("Local Push Received")
    }

    public func pushRemoteCallback(data: Dictionary<String, Any>?, state: UIApplication.State) {
        print("Remote Push Received")
    }
}

//---------------------------
// MARK: - APPSFLYER
//---------------------------

extension SplayGameSDKManager: AppsFlyerDelegate {
    
    public func onConversionDataSuccess(_ conversionInfo: [AnyHashable : Any]) {
        
        
        print("onConversionDataSuccess data:")
        for (key, value) in conversionInfo {
            print(key, ":", value)
        }

        guard let status = conversionInfo["af_status"] as? String else { return }
        if (status == "Non-organic") {
            if let sourceID = conversionInfo["media_source"],
               let campaign = conversionInfo["campaign"] {
                print("This is a Non-Organic install. Media source: \(sourceID)  Campaign: \(campaign)")
            }
        } else {
            print("This is an organic install.")
        }
        
        if let isFirstLaunch = conversionInfo["is_first_launch"] as? Bool, isFirstLaunch {
            if AppConfig.isUseGTM { sendInstallToFirebase(conversionInfo) }
            print("First Launch")
        } else {
            print("Not First Launch")
        }
        
    }
}
