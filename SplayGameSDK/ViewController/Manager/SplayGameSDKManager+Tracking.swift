//
//  SplayGameSDKManager+Tracking.swift
//  SplayGameSDK
//
//  Created by Nguyen Nang on 1/11/21.
//  Copyright © 2021 splay. All rights reserved.
//

import UIKit
import Firebase
import FBSDKCoreKit
import FBSDKLoginKit
import FBSDKShareKit
import AppsFlyerLib
import GoogleSignIn
import STPopup
import OneSignal
import UserNotifications
import CleanroomLogger
import STPopup
import AdBrixRM

//---------------------------
// MARK: - TRACKING
//---------------------------

extension SplayGameSDKManager {
    

    public func trackEvent(_ type: TrackingType, parameters: [String : Any]? = nil) {
        
        
        if type.sendLimit {
            guard !UserDefaults.standard.bool(forKey: type.key) else { return }
            UserDefaults.standard.set(true, forKey: type.key)
        }
       
        
        
        Utility.sendEventToAllWith(type.eventName, parameters: parameters)
    
        
        switch type {
        case .firstOpen: break
        case .firstLevel:
            Utility.sendEventToFirebaseWith(AnalyticsEventTutorialBegin)
            
        case .firstUpdateResource:
            Utility.sendEventToAppsFlyerWith(AFEventUpdate)
   
        case .completeTutorialEvent:
            Utility.sendEventToFirebaseWith(AnalyticsEventTutorialComplete)
            Utility.sendEventToFacebookWith(AppEvents.Name.completedTutorial.rawValue)
            Utility.sendEventToAppsFlyerWith(AFEventTutorial_completion)

            
        case .completeOverTutorialEvent:    break
        case .completeLoadServerListEvent:  break
        case .completeLoadSdkEvent:         break
        case .completeLoadRoleEvent:        break
        case .completeCreateRoleEvent:      break
        case .completeExtractObb:           break
        case .completeRegistration:
            Utility.sendEventToFirebaseWith(AnalyticsEventSignUp,
                                            parameters: parameters)
            Utility.sendEventToFacebookWith(AppEvents.Name.completedRegistration.rawValue,
                                            parameters: parameters)
            Utility.sendEventToAppsFlyerWith(AFEventCompleteRegistration,
                                             parameters: parameters)
            
        case .completeRegistrationNew:      break
        case .completeRegistrationOld:      break
        }
        

        UserDefaults.standard.synchronize()
    }
    
    
    
    @objc public func trackAchievedLevelEvent(_ level: String) {
        

        Utility.sendEventToFacebookWith(AppEvents.Name.achievedLevel.rawValue,
                                        parameters: [AppEvents.ParameterName.level.rawValue : level])
        
        Utility.sendEventToFirebaseWith(AnalyticsEventLevelUp,
                                        parameters: [AnalyticsParameterLevel : level])
        
        Utility.sendEventToAppsFlyerWith(AFEventLevelAchieved,
                                         parameters: [AFEventParamLevel : level])

        Utility.sendEventToAdBrixWith(AnalyticsEventLevelUp,
                                      parameters: [AnalyticsParameterLevel : level])
        
        Utility.sendEventToHubJSWith(AnalyticsEventLevelUp,
                                     parameters: [AnalyticsParameterLevel : level])
    }
    
    @objc public func trackFirstOpenEvent() {
        trackEvent(.firstOpen)
    }
    
    @objc public func trackAchievedFirstLevelEvent() {
        trackEvent(.firstLevel)
    }
    
    @objc public func trackCompletedFirstUpdateEvent() {
        trackEvent(.firstUpdateResource)
    }
    
    @objc public func trackCompleteTutorialEvent() {
        trackEvent(.completeTutorialEvent)
    }
    
    @objc public func trackCompleteOverTutorialEvent() {
        trackEvent(.completeOverTutorialEvent)
    }
    
    @objc public func trackCompleteLoadServerList() {
        trackEvent(.completeLoadServerListEvent)
    }
    
    @objc public func trackCompleteLoadSDK() {
        trackEvent(.completeLoadSdkEvent)
    }
    
    @objc public func trackCompleteLoadRole() {
        trackEvent(.completeLoadRoleEvent)
    }
    
    @objc public func trackCompleteCreateRole() {
        trackEvent(.completeCreateRoleEvent)
    }
    
    @objc public func trackCompleteExtractObb() {
        trackEvent(.completeExtractObb)
    }
}


//---------------------------
// MARK: - LOG
//---------------------------

extension SplayGameSDKManager {
    
    @objc public func logStartTutorialEvent(_ userId: String, success: Bool) {
        
        let paramsFB: [String : Any] = [AppEvents.ParameterName.contentID.rawValue : userId,
                                        AppEvents.ParameterName.success.rawValue : success ? 1 : 0]
        
        let paramsFir: [String : Any] = [AnalyticsParameterCharacter : userId,
                                         AnalyticsParameterSuccess : success ? 1 : 0]

        
        Utility.sendEventToFacebookWith("fb_mobile_tutorial_start", parameters: paramsFB)
        Utility.sendEventToAppsFlyerWith(AnalyticsEventTutorialBegin, parameters: paramsFir)
        Utility.sendEventToFirebaseWith(AnalyticsEventTutorialBegin, parameters: paramsFir)
        Utility.sendEventToAdBrixWith(AnalyticsEventTutorialBegin, parameters: paramsFir)
        Utility.sendEventToHubJSWith(AnalyticsEventTutorialBegin, parameters: paramsFir)
    }
    
    
    @objc public func logCompletedTutorialEvent(_ userId: String, success: Bool) {
        
        let paramsFB: [String : Any] = [AppEvents.ParameterName.contentID.rawValue : userId,
                                        AppEvents.ParameterName.success.rawValue : success ? 1 : 0]
        
        let paramsFir: [String : Any] = [AnalyticsParameterCharacter : userId,
                                         AnalyticsParameterSuccess : success ? 1 : 0]

        Utility.sendEventToFacebookWith(AppEvents.Name.completedTutorial.rawValue, parameters: paramsFB)
        Utility.sendEventToFirebaseWith(AnalyticsEventTutorialComplete, parameters: paramsFir)
        Utility.sendEventToAppsFlyerWith(AnalyticsEventTutorialComplete, parameters: paramsFir)
        Utility.sendEventToAdBrixWith(AnalyticsEventTutorialComplete, parameters: paramsFir)
        Utility.sendEventToHubJSWith(AnalyticsEventTutorialComplete, parameters: paramsFir)
    }
    
    @objc public func logInitiatedCheckoutEvent(_ productId: String, contentType: String) {
        
        let paramsFB = [AppEvents.ParameterName.contentID.rawValue : productId,
                        AppEvents.ParameterName.contentType.rawValue : contentType,
                        AppEvents.ParameterName.currency.rawValue : "VND"]
        
        let paramsFir = [AnalyticsParameterItemID : productId,
                         AnalyticsParameterItemCategory : contentType,
                         AnalyticsParameterCurrency : "VND"]
        


        Utility.sendEventToFacebookWith(AppEvents.Name.initiatedCheckout.rawValue, parameters: paramsFB)
        Utility.sendEventToFirebaseWith(AnalyticsEventBeginCheckout, parameters: paramsFir)
        Utility.sendEventToAppsFlyerWith(AnalyticsEventBeginCheckout, parameters: paramsFir)
        Utility.sendEventToAdBrixWith(AnalyticsEventBeginCheckout, parameters: paramsFir)
        Utility.sendEventToHubJSWith(AnalyticsEventBeginCheckout, parameters: paramsFir)
        
    }
    
    @objc public func logPurchaseEvent(_ purchaseAmount: String, eventName: String, productId: String, paymentInapp: String) {
        
        //        let paramsFB = [AppEvents.ParameterName.contentID.rawValue : productId,
        //                        AppEvents.ParameterName.contentType.rawValue : paymentInapp]
        //
        //        AppEvents.logPurchase(Double(purchaseAmount), currency: "VND", parameters: paramsFB)
        
        let paramsFir: [String : Any] = [AnalyticsParameterItemID : productId,
                                         AnalyticsParameterItemCategory : paymentInapp,
                                         AnalyticsParameterCurrency : "VND",
                                         AnalyticsParameterValue :  Double(purchaseAmount) as Any]
        
        
        Utility.sendEventToFirebaseWith(AnalyticsEventPurchase, parameters: paramsFir)
        Utility.sendEventToAppsFlyerWith(AnalyticsEventPurchase, parameters: paramsFir)
        Utility.sendEventToAdBrixWith(AnalyticsEventPurchase, parameters: paramsFir)
        Utility.sendEventToHubJSWith(AnalyticsEventPurchase, parameters: paramsFir)
    }
    
    
    @objc public func logStartUpdateResourceEvent(_ userId: String, success: Bool) {
        
        let paramsFB: [String : Any] = [AppEvents.ParameterName.contentID.rawValue : userId,
                                        AppEvents.ParameterName.success.rawValue : success ? 1 : 0]
        
        let paramsFir: [String : Any] = [AnalyticsParameterCharacter : userId,
                                         AnalyticsParameterSuccess : success ? 1 : 0]
        
        Utility.sendEventToFacebookWith("fb_mobile_update_resource_start", parameters: paramsFB)
        Utility.sendEventToFirebaseWith("update_resource_start", parameters: paramsFir)
        Utility.sendEventToAppsFlyerWith("update_resource_start", parameters: paramsFir)
        Utility.sendEventToAdBrixWith("update_resource_start", parameters: paramsFir)
        Utility.sendEventToHubJSWith("update_resource_start", parameters: paramsFir)
    }
    
    
    @objc public func logCompletedUpdateResourceEvent(_ userId: String, success: Bool) {
        
        let paramsFB: [String : Any] = [AppEvents.ParameterName.contentID.rawValue : userId,
                                        AppEvents.ParameterName.success.rawValue : success ? 1 : 0]
        
        let paramsFir: [String : Any] = [AnalyticsParameterCharacter : userId,
                                         AnalyticsParameterSuccess : success ? 1 : 0]
        
        Utility.sendEventToFacebookWith("fb_mobile_update_resource_completed", parameters: paramsFB)
        Utility.sendEventToFirebaseWith("update_resource_complete", parameters: paramsFir)
        Utility.sendEventToAppsFlyerWith("update_resource_complete", parameters: paramsFir)
        Utility.sendEventToAdBrixWith("update_resource_complete", parameters: paramsFir)
        Utility.sendEventToHubJSWith("update_resource_complete", parameters: paramsFir)
    }
    
    
    @objc public func logSpentCreditsEvent(_ contentId: String, virtualCurrencyName: String, totalValue: Double) {
        
        let paramsFB: [String : Any] = [AppEvents.ParameterName.contentID.rawValue : contentId,
                                        AppEvents.ParameterName.contentType.rawValue : virtualCurrencyName]
        
        let paramsFir: [String : Any] = [AnalyticsParameterItemID : contentId,
                                         AnalyticsParameterValue : totalValue,
                                         AnalyticsParameterVirtualCurrencyName : virtualCurrencyName]
        

        AppEvents.logEvent(AppEvents.Name.spentCredits, valueToSum: totalValue, parameters: paramsFB)
        
        Utility.sendEventToFirebaseWith(AnalyticsEventSpendVirtualCurrency, parameters: paramsFir)
        Utility.sendEventToAdBrixWith(AnalyticsEventSpendVirtualCurrency, parameters: paramsFir)
        Utility.sendEventToAdBrixWith(AnalyticsEventSpendVirtualCurrency, parameters: paramsFir)
        Utility.sendEventToHubJSWith(AnalyticsEventSpendVirtualCurrency, parameters: paramsFir)
    }
}
