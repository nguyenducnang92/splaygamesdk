//
//  SplayGameSDKManager+Selector.swift
//  SplayGameSDK
//
//  Created by Nguyen Nang on 5/21/21.
//  Copyright © 2021 splay. All rights reserved.
//

import UIKit
import Firebase
import FBSDKCoreKit
import FBSDKLoginKit
import FBSDKShareKit
import AppsFlyerLib
import GoogleSignIn
import STPopup
import OneSignal
import UserNotifications
import CleanroomLogger
import STPopup
import SwiftyStoreKit
import AppTrackingTransparency
import AdBrixRM
import AdSupport
import GoogleTagManager
import HubjsTracker

//---------------------------
// MARK: - PUBLIC METHOD
//---------------------------

extension SplayGameSDKManager {
    
    
    @objc public func showWelcome() {
        
        TWMessageBarManager.sharedInstance().showMessage(withTitle: LanguageSetting.get("text_welcome", alter: nil),
                                                         description: UserDefaults.standard.object(forKey: KEY_SAVE_USERNAME) as? String ?? "",
                                                         type: TWMessageBarMessageTypeInfo)
    }
    
    
    @objc public func sdkShowLoginForm() {
        
        if UserDefaults.standard.object(forKey: KEY_USERDEFAULT_DATA_CONFIG) == nil { getConfigApp() }
        
        
        guard let _ = UserDefaults.standard.object(forKey: KEY_API_CONFIG_APP),
              let _ = UserDefaults.standard.object(forKey: KEY_AGENCY_APP) else {
            
            Log.message(.error, message: "Apikey or agency not enter in plist. Please check your app")
            return
        }
        
        checkDeviceLogin()
    }
    
    
    @objc public func showFromLoginView() {
        
        Utility.logEventDebug(.showLoginView)
        
        guard let rootVC = UIApplication.shared.keyWindow?.rootViewController else { return }
        let popupViewController = STPopupController(rootViewController: LoginViewController())
        popupViewController.containerView.layer.cornerRadius = 8
        popupViewController.present(in: rootVC)
        popupViewController.hidesCloseButton = self.closeButton
        popupViewController.navigationBarHidden = true
        
        Log.message(.info, message: "Finished khởi tạo LoginViewController")
        Log.message(.info, message: "Khởi tạo LoginViewController")
    }
    
    
    @objc public func showStatus() {
        UtilitieWebService.shareSingleton()?.getConfigDefaultApp { (results, error, resultCode) in
            
            
            guard resultCode == ResultCodeSuccess else { return }
            guard let result = results as? [String : Any] else { return }
            guard let resultData = result[KEY_DATA] as? [String : Any] else { return }
            
            Utility.showAlertWithMessage(resultData[KEY_STATUS_APP] as? String ?? "",
                                         buttonTitle: LanguageSetting.get("title_button_agree", alter: nil),
                                         viewController: nil)
            
        }
    }
    
    
    @objc public func autoLogin() {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            
            Utility.logEventDebug(.autoLogin)
            
            UtilitieWebService.shareSingleton()?.getConfigDefaultApp { (results, error, resultCode) in
                
                guard resultCode == ResultCodeSuccess else {
                    Utility.showAlertFrom(resultCode, results: results, viewController: nil)
                    return
                }
                
                guard let result = results as? [String : Any] else { return }
                
                UserDefaults.standard.set(result[KEY_DATA], forKey: KEY_USERDEFAULT_DATA_CONFIG)
                UserDefaults.standard.synchronize()
                self.checkDeviceLogin()
            }
            
        }
    }
    
    @objc public func sdkLogout() {
        
        
        guard UserDefaults.standard.bool(forKey: KEY_CHECK_ACCOUNT_LOGIN) else {
            
            Utility.showAlertWithMessage("You have not login with account",
                                         alertTitle: NSLocalizedString("Note", comment: ""),
                                         buttonTitle: NSLocalizedString("Ok", comment: ""),
                                         viewController: nil)
            
            return
        }
        
        
        
        UtilitieWebService.shareSingleton()?.dxtk { (results, error, resultCode) in
            
            
            guard resultCode == ResultCodeSuccess else {
                
                Log.message(.info, message: "logout failed with error = \(String(describing: error))")
                return
            }
            
            UIApplication.shared.keyWindow?.subviews
                .filter { $0.isKind(of: ShowInforFastAccView.self) }
                .forEach { $0.removeFromSuperview() }
            
            
            let userDefault = UserDefaults.standard
            userDefault.set(false, forKey: KEY_CHECK_ACCOUNT_LOGIN)
            userDefault.set("", forKey: KEY_SAVE_USERID)
            userDefault.set("", forKey: KEY_SAVE_USERNAME)
            userDefault.set("", forKey: KEY_SAVE_USER_TOKEN)
            userDefault.synchronize()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: ACTION_LOGOUT_USER_SUCCESS),
                                            object: self)
            
            NotificationCenter.default.removeObserver(self,
                                                      name: NSNotification.Name(rawValue: ACTION_LOGOUT_USER_SUCCESS),
                                                      object: nil)
            
            
            LoginManager().logOut()
            GIDSignIn.sharedInstance()?.signOut()
            
            Log.message(.info, message: "logout success with result = \(String(describing: results))")
        }
    }
    
    
    @objc public func reStoreInAppPurchase() {
        
        IAPShare.sharedHelper()?.iap.restoreProducts { (payment, error) in
            Log.message(.info, message: "Retore purchase success")
        }
    }
    
    
    @objc public func getAppStatusWithCompletionHandler(_ completionHandle: CompletionHandlerResult?) {
        
        getDataConfigWithCondition("status") { completionResult in
            
            Log.message(.info, message: "________Data handle \(String(describing: completionResult))")
            
            if let completion = completionHandle {
                completion(completionResult)
            }
        }
    }
    
    @objc public func getAvatarWithCompletionHandler(_ completionHandler: @escaping (String) -> Void) {
        
        self.completionHandleAvatar = completionHandler
        let parentViewController = UIApplication.shared.keyWindow?.rootViewController ?? UIViewController()
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true
        picker.sourceType = .photoLibrary
        picker.modalPresentationStyle = .overCurrentContext
        parentViewController.present(picker, animated: true, completion: nil)
    }
    
    @objc public func uploadAvatarWithCompletionHandler(_ completionHandler: @escaping (String) -> Void) {
        getAvatarWithCompletionHandler(completionHandler)
    }
    
    
    @objc public func uploadImageWithBase64Image(_ imageString: String, completionHandler: @escaping (String) -> Void) {
        
        
        self.completionHandleAvatar = completionHandler
        
        let accessToken = UserDefaults.standard.object(forKey: KEY_SAVE_USER_TOKEN) as? String ?? ""
        MBProgressHUD.showAdded(to: UIApplication.shared.keyWindow?.rootViewController?.view ?? UIView(), animated: true)
        
        UtilitieWebService
            .shareSingleton()?
            .uploadImage(withStringImage: imageString,
                          accessToken: accessToken) {  (results, error, resultCode)  in
                
                MBProgressHUD.hide(for: UIApplication.shared.keyWindow?.rootViewController?.view ?? UIView(), animated: false)
                
                guard let result = results as? [String : Any] else {
                    
                    //   if let completion = self.completionHandleAvatar {
                    //   completion("Lỗi: \(results) --- \(resultCode)")
                    //   }
                    return
                    
                }
                
                let urlImage = result[KEY_MESSAGE] as? String ?? ""
                
                if let completion = self.completionHandleAvatar {
                    completion(urlImage)
                }
            }
        
        
    }
    
    
    @objc public func authenAppleWithAppleID(_ appleID: String,
                                             email: String,
                                             fullName: String,
                                             CompletionHandle completionHandle: AuthenCompletionHandle? = nil) {
        
        
        
        if let view = UIApplication.shared.keyWindow?.rootViewController?.view {
            HUD.showHUD(onView: view)
        }
        
        
        UtilitieWebService.shareSingleton()?.authenApple(withAppleID: appleID,
                                                         email: email,
                                                         fullName: fullName) { (results, error, resultCode) in
            
            HUD.dismissHUD()
            
            
            
            Utility.logEventDebug(.loginApple)
            
            guard resultCode == ResultCodeSuccess else {
                Utility.showAlertFrom(resultCode, results: results, viewController: nil)
                
                if let completion = completionHandle {
                    completion(false, 2, "Authen Fail")
                    
                }
                return
            }
            
            Utility.updateAccountInfoWith(results, loginMethod: .byTkApple)
            UnitAnalyticHelper.analyticLoginEvent(MethodDn.byTkApple.key)
            Utility.trackEventCompleteRegisterOldOrNew(MethodDn.byTkApple.key, results: results)
            
            
            UIApplication.shared.keyWindow?.rootViewController?.dismiss(animated: true, completion: nil)
            self.perform(#selector(self.showWelcome), with: nil, afterDelay: 1.5)
            //            [self.popupController dismiss];
            
            
            if let completion = completionHandle {
                completion(true, 1, "Authen Success")
            }
        }
        
    }
    

    @objc public func logVersionLibSDK() {
        
        print("Facebook     SDK Version: \(FBSDK_VERSION_STRING)")
        print("HubJS        SDK Version: \(HubjsTracker.sdkVersion)")
        print("AdBrix       SDK Version: \(AdBrixRMVersionNumber)")
        print("Firebase     SDK Version: \(FirebaseVersion())")
        print("AppFlyer     SDK Version: \(AppsFlyerLib.shared().getSDKVersion()) ")
        print("OneSignal    SDK Version: \(String(describing: OneSignal.sdk_semantic_version()))")
    }
}


//---------------------------
// MARK: - PRIVATE METHOD
//---------------------------

extension SplayGameSDKManager {
    
    func checkDeviceLogin() {
        
        if let view = UIApplication.shared.keyWindow?.rootViewController?.view {
            HUD.showHUD(onView: view)
        }
        
        UtilitieWebService.shareSingleton()?.checkDevice { (results, error, resultCode) in
            HUD.dismissHUD()

            guard resultCode == ResultCodeSuccess else { self.showFromLoginView(); return }

            if let result = results as? [String : Any] {
                Log.message(.info, message: "Check device dang nhap game \(String(describing: result[KEY_DATA]))")
            }
            
            self.perform(#selector(self.showWelcome), with: nil, afterDelay: 1.5)

            Utility.updateAccountInfoWith(results, sendEvent: false)
            UnitAnalyticHelper.analyticLoginEvent(MethodDn.byTk.key)

            
            

            guard let resultJson = results as? [String : Any],
                  let dictData = resultJson[KEY_DATA] as? [String : Any],
                  let username = dictData[KEY_USERNAME] as? String else { return }
            
            guard let rootVC = UIApplication.shared.keyWindow?.rootViewController,
                  username.contains("_fastlogin_") else { return }
            
            let cntttkFastViewController = STPopupController(rootViewController: AlertUpdateFastAccountViewController())
            cntttkFastViewController.containerView.layer.cornerRadius = 8
            cntttkFastViewController.present(in: rootVC)
            cntttkFastViewController.hidesCloseButton = self.closeButton
            cntttkFastViewController.navigationBarHidden = true
            
        }
        
    }
    
    func getConfigApp() {
        
        UtilitieWebService.shareSingleton()?.getConfigDefaultApp { (results, error, resultCode) in
            
            guard resultCode == ResultCodeSuccess else {
                Utility.showAlertFrom(resultCode, results: results, viewController: nil)
                return
            }
            
            guard let result = results as? [String : Any] else { return }
            UserDefaults.standard.set(result[KEY_DATA], forKey: KEY_USERDEFAULT_DATA_CONFIG)
            UserDefaults.standard.synchronize()
            
        }
    }
    
    
    func getAppStoreID() {
        
        UtilitieWebService.shareSingleton()?.getAppStoreId { (results, error, resultCode) in
            
            let userDefault = UserDefaults.standard
            var appleID     = ""
            
 
            if resultCode == ResultCodeSuccess,
               let result  = results as? [String : Any],
               let topLevelArray = result["results"] as? [Any],
               let dict    = topLevelArray.first as? [String : Any],
               let trackId = dict["trackId"] as? String {
                
                appleID = String(format: "id%@", trackId)
            }
            
            userDefault.set(appleID, forKey: KEY_APPID)
            userDefault.synchronize()
    
            Log.message(.info, message: "AppleID = \(String(describing: appleID))")
        }
    }
    
    
    /*
     
     -(void)getDataConfigWithCondition:(NSString *)condition completionHandle:(CompletionHandlerResult)comletionHandle
     {
     [[UtilitieWebService shareSingleton] getConfigDefaultAppWithCompletion:^(id results, NSError *error, ResultCode resultCode) {
     if (resultCode == ResultCodeSuccess) {
     NSString *_messagerStatus;
     if ([condition isEqualToString:@"rating"]) {
     _messagerStatus = [NSString stringWithFormat:@"%@",results[KEY_DATA][KEY_RATING]];
     }else if ([condition isEqualToString:@"status"]){
     _messagerStatus = [NSString stringWithFormat:@"%@",results[KEY_DATA][KEY_STATUS_APP]];
     }else{
     _messagerStatus = [NSString stringWithFormat:@"%@",results[KEY_DATA][KEY_REDMINED]];
     }
     if (comletionHandle) comletionHandle(_messagerStatus);
     
     UIAlertController * alert = [UIAlertController
     alertControllerWithTitle:NSLocalizedString(@"Thông báo", nil)
     message:_messagerStatus
     preferredStyle:UIAlertControllerStyleAlert];
     UIAlertAction* yesButton = [UIAlertAction
     actionWithTitle:NSLocalizedString(@"Đồng ý", nil)
     style:UIAlertActionStyleDefault
     handler:^(UIAlertAction * action) {
     //Handle your yes please button action here
     }];
     [alert addAction:yesButton];
     [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alert animated:YES completion:nil];
     }else{
     if (comletionHandle) comletionHandle(@"Error");
     }
     }];
     }*/
    
    func getDataConfigWithCondition(_ condition: String, comletionHandle: CompletionHandlerResult?) {
        
        UtilitieWebService.shareSingleton()?.getConfigDefaultApp {(results, error, resultCode) in
            
            guard let result = results as? [String : Any], resultCode == ResultCodeSuccess else {
                
                if let comletion = comletionHandle  {
                    comletion("\(String(describing: error))")
                }
                return
            }
            
            
            var messagerStatus: String
            let resultData = result[KEY_DATA] as? [String : Any] ?? [:]
            
            switch condition {
            case "rating":
                messagerStatus = resultData[KEY_RATING] as? String ?? ""
                
            case "status":
                messagerStatus = resultData[KEY_STATUS_APP] as? String ?? ""
                
            default:
                messagerStatus = resultData[KEY_REDMINED] as? String ?? ""
            }
            
            
            if let comletion = comletionHandle  {
                comletion(messagerStatus)
            }
            
            Utility.showAlertWithMessage(messagerStatus,
                                         buttonTitle: LanguageSetting.get("title_button_agree", alter: nil),
                                         viewController: nil)
            
        }
        
    }
    

    public func sdkLogMessage(_ type: LogType = .info, message: String) {
        Log.message(type == .info ? .info : .error, message: message)
    }
    
    func checkOrientation() {
        
        switch UIApplication.shared.statusBarOrientation {
        case .landscapeLeft:    break
        case .landscapeRight:   break
        case .portrait:         break
        case .portraitUpsideDown:   break
        default: break
        }
    }

    func parseURLParams(_ query: String?) -> [AnyHashable : Any]? {
        let pairs = query?.components(separatedBy: "&")
        var params: [AnyHashable : Any] = [:]
        for pair in pairs ?? [] {
            let kv = pair.components(separatedBy: "=")
            let val = kv[1].removingPercentEncoding
            params[kv[0]] = val ?? ""
        }
        return params
    }
    

    func sendInstallToFirebase(_ installData: [AnyHashable : Any]!){
        
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy hh:mm:ss"
        let _newDate: String = dateFormatter.string(from: date)
        
        if(installData["af_status"] as? String == "Organic"){
            
            Utility.sendEventToFirebaseWith("install", parameters: [
                "install_time": _newDate,
                "media_source": "organic",
                "campaign": "organic"
            ])
            
        } else {
            
            Utility.sendEventToFirebaseWith("install", parameters: [
                "install_time": installData["install_time"] as Any,
                "click_time": installData["click_time"] as Any,
                "media_source": installData["media_source"] as Any,
                "campaign": installData["campaign"] as Any,
                "install_type": installData["af_status"] as Any
            ])
            
        }
        
    }
}

