//
//  ForgotPasswordViewController.swift
//  SplayGameSDK
//
//  Created by Nguyen Nang on 7/14/20.
//  Copyright © 2020 splay. All rights reserved.
//


import UIKit
import GoogleSignIn

class CheckAccountViewController: CustomViewController {
    
    //-----------------------
    // MARK: - ENUM
    //-----------------------
    
    enum Size: CGFloat {
        case button = 36, padding10 = 10
    }
    
    
    
    //-----------------------
    // MARK: - VAR
    //-----------------------

    
    //-----------------------
    // MARK: - VIEW
    //-----------------------
    
    
    
    
    var lbInfoAc: UILabel!
    var btnNext: UIButton!
    var tfInfoAc: DLTextField!

    
    
    
    
    //-----------------------
    // MARK: - LIFE CYCLE
    //-----------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if UIScreen.main.bounds.width > UIScreen.main.bounds.height {
            // Landscape
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                landscapeContentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 320))
                
            } else {
                landscapeContentSizeInPopup = CGSize(width: 336, height: 350)
            }
            
            
        } else {
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                contentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 340))
                
            } else {
                contentSizeInPopup = CGSize(width: 336, height: 350)
            }
        }
        
        setupAllSubviews()
        
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        updateFrame()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
    }
    
    func updateFrame() {
        
        btnClose.frame = CGRect(x: view.frame.width - 40,
                                y: 0,
                                width: 40,
                                height: 40)
        
        
        labelTitle.frame = CGRect(x: 0,
                                  y: 0,
                                  width: view.frame.width,
                                  height: 40)
        
        seperator.frame = CGRect(x: 0,
                                 y: labelTitle.frame.height - 1 / UIScreen.main.scale,
                                 width: view.frame.width,
                                 height: 1 / UIScreen.main.scale)
        
        helpView.frame = CGRect(x: 0,
                                y: view.frame.height - 40,
                                width: view.frame.width,
                                height: 40)
        
//        btnNext.frame = CGRect(x: view.frame.width / 3,
//                                  y: helpView.frame.minY - Size.button.rawValue - Size.padding10.rawValue / 2,
//                                  width: view.frame.width / 3,
//                                  height: Size.button.rawValue)
        
        
        lbInfoAc.frame = CGRect(x:  Size.padding10.rawValue ,
                                y: seperator.frame.maxY + Size.padding10.rawValue * 2,
                                width: view.frame.width - Size.padding10.rawValue * 2,
                                height: Size.button.rawValue)
        
        
        tfInfoAc.frame = CGRect(x: Size.padding10.rawValue,
                                y: lbInfoAc.frame.maxY + Size.padding10.rawValue,
                                width: view.frame.width - Size.padding10.rawValue * 2,
                                height: Size.button.rawValue)
        
        btnNext.frame = CGRect(x: view.frame.width / 3,
                                  y: tfInfoAc.frame.maxY + Size.padding10.rawValue ,
                                  width: view.frame.width / 3,
                                  height: Size.button.rawValue)
        
    }
}






extension CheckAccountViewController {
    

    
    func loadLanguage() {

        labelTitle.text = LanguageSetting.get("TitleForgotPass", alter: nil)
        lbInfoAc.text = LanguageSetting.get("title_info_ac", alter: nil)
        tfInfoAc.attributedPlaceholder =   NSAttributedString(string: LanguageSetting.get("TextFieldLoginAccountName", alter: nil),
                                                                          attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        
        btnNext.setTitle(LanguageSetting.get("TitleButtonNext", alter: nil), for: .normal)
    }


    
    
}

extension CheckAccountViewController {
    
    @objc func changeLanguage() {
        loadLanguage()
    }
    
    @objc func orientationChanged(_ sender: Notification) {
        
        let device = UIDevice.current
        
        switch device.orientation {
        case .faceUp:
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                contentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 340))
                
            } else {
                contentSizeInPopup = CGSize(width: 336, height: 350)
            }
            
            
        case .portrait, .portraitUpsideDown:
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                contentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 340))
                
            } else {
                contentSizeInPopup = CGSize(width: 336, height: 350)
            }
            
        case .landscapeLeft, .landscapeRight:
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                landscapeContentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 320))
                
            } else {
                landscapeContentSizeInPopup = CGSize(width: 336, height: 350)
            }
            
            
        default:
            break
        }
        
        updateFrame()
    }
    
    @objc func btnCloseClick(_ sender: UIButton) {
        popupController?.popViewController(animated: true)
    }
    
   
    
    
    @objc func btnNextClick(_ sender: UIButton) {
        
        view.endEditing(true)
        
        guard let username = tfInfoAc.text, username.count > 0 else {
            showAlertWithMessage(LanguageSetting.get("validate_account_emtry", alter: nil))
            return
        }
        
        HUD.showHUD(onView: self.view)
        UtilitieWebService
            .shareSingleton()?
            .gettttk(withAccName: username,
                     completion: { (results, error, resultCode) in
                        

                        HUD.dismissHUD()
                        guard resultCode == ResultCodeSuccess else {
                            
                            let messagerResult: String
                            if resultCode == ResultCodeNetWorkError {
                                messagerResult = LanguageSetting.get("MessagerNetworkResult", alter: nil)
                                
                            } else if resultCode == RequestCodeUnknowError {
                                messagerResult = LanguageSetting.get("error_connect_server", alter: nil)
                                
                            } else if resultCode == ResultCodeDataEmpty {
                                messagerResult = LanguageSetting.get("result_not_found", alter: nil)
                                
                            } else {
                                messagerResult = LanguageSetting.get("MessagerUnknowError", alter: nil)
                            }
                            
                            self.showAlertWithMessage(messagerResult)
                            return
                        }
                        
                        guard let result = results as? [String : Any] else { return }
                        guard let dictData = result[KEY_DATA] as? [String : Any] else { return }
                        
                        let strMobile   = dictData[KEY_MOBILE] as? String ?? ""
                        let strEmail    = dictData[KEY_EMAIL] as? String ?? ""
                        let strUserName = dictData[KEY_USERNAME] as? String ?? ""
                        
                        if (strEmail.count == 0 || strEmail == "(null)") && (strMobile.count == 0 || strMobile == "(null)") {
                            
                            let phoneEmailViewController = TDAndEmailViewController()
                            self.popupController?.push(phoneEmailViewController, animated: true)
                            
                        } else {

                            let qmkViewController = ForgotPasswordViewController()
                            qmkViewController.strNumberPhone = strMobile
                            qmkViewController.strEmail = strEmail
                            qmkViewController.strUserName = strUserName
                            qmkViewController.dictResults = dictData
                            self.popupController?.push(qmkViewController, animated: true)

                        }
                        
            })
        
    }
}

extension CheckAccountViewController: CustomHelpViewDelegate {
    
    func showViewSupport() {
        
    }
    
    func showSettingLanguage() {
        
    }
}


//----------------------------------
// MARK: - TEXT FIELD DELEGATE
//----------------------------------

extension CheckAccountViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        btnNext.sendActions(for: .touchUpInside)
        return true
    }
}


//-----------------------
// MARK: - SETUP
//-----------------------

extension CheckAccountViewController {
    
    func setupAllSubviews() {
        
        
        
        title = "Đăng nhập"
        view.backgroundColor = UIColor(red: 248/255.0, green: 248/255.0, blue: 248/255.0, alpha: 1.0)
        view.layer.cornerRadius = 8.0
        view.layer.borderColor =  UIColor(red: 13/255.0, green: 183/255.0, blue: 212/255.0, alpha: 1.0).cgColor
        view.layer.borderWidth = 1.5
        view.layer.masksToBounds = true
        
        setupNotification()
        
        
        
        
        helpView.delegate = self
        btnClose.addTarget(self, action: #selector(self.btnCloseClick(_:)), for: .touchUpInside)
        
        
        
        lbInfoAc = setupLabelCustom(LanguageSetting.get("title_info_ac", alter: nil),
                                textColor: .gray,
                                font: UIFont.boldSystemFont(ofSize: 15))
        
        tfInfoAc = setupTextField(LanguageSetting.get("TextFieldLoginAccountName", alter: nil))
      
        
        btnNext = setupButtonCustom(title: LanguageSetting.get("TitleButtonNext", alter: nil),
                                 titleColor: .white)
        btnNext.setBackgroundImage(UtilitieWebService.shareSingleton()?.getImageFromStringBase(dn_bg_bt_li), for: .normal)
        btnNext.addTarget(self, action: #selector(self.btnNextClick(_:)), for: .touchUpInside)
        
        
        
        
        view.addSubview(helpView)
        view.addSubview(lbInfoAc)
        view.addSubview(tfInfoAc)
        view.addSubview(btnNext)
        
        view.addSubview(seperator)
        view.addSubview(labelTitle)
        view.addSubview(btnClose)
        
        loadLanguage()
        
    }
    
    func setupNotification() {
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.changeLanguage),
                                               name: Notification.Name(rawValue: CHANGE_LANGUAGE_SETTING),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.orientationChanged(_:)),
                                               name: UIDevice.orientationDidChangeNotification,
                                               object: UIDevice.current)
        
        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
        
    }
    
    func setupTextField(_ placeHoler: String, textInsets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0),
                        isSecureTextEntry: Bool = false) -> DLTextField {
        let textField = DLTextField()
        textField.placeholder = placeHoler
        textField.layer.cornerRadius = 3
        textField.layer.borderWidth = 0.5
        textField.layer.borderColor = UIColor.lightGray.cgColor
        textField.layer.masksToBounds = true
        textField.textInsets = textInsets
        textField.rightViewMode = isSecureTextEntry ? .always : .never
        textField.isSecureTextEntry = isSecureTextEntry
        textField.font = UIFont.systemFont(ofSize: 14)
        textField.background = UtilitieWebService.shareSingleton()?.getImageFromStringBase(dn_bg_textfile)
        textField.delegate = self
        textField.returnKeyType =  .done
        textField.textColor = UIColor.Text.blackMediumColor()
        return textField
    }
    
    
    

}


