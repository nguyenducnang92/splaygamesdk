//
//  ForgotPasswordViewController.swift
//  SplayGameSDK
//
//  Created by Nguyen Nang on 7/14/20.
//  Copyright © 2020 splay. All rights reserved.
//


import UIKit
import GoogleSignIn
import CleanroomLogger

class ForgotPasswordViewController: CustomViewController {
    
    
    //-----------------------
    // MARK: - VAR
    //-----------------------
    
    var strEmail: String = ""
    var strNumberPhone: String = ""
    var strUserName: String = ""
    var dictResults: [String : Any]?
    
    
    
    var tagButtoneSelect: Int = 0
    
    //-----------------------
    // MARK: - VIEW
    //-----------------------
    
    
    
    var forgotView: ForgotPasswordContentView!
    
    
    
    //-----------------------
    // MARK: - LIFE CYCLE
    //-----------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if UIScreen.main.bounds.width > UIScreen.main.bounds.height {
            // Landscape
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                landscapeContentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 320))
                
            } else {
                landscapeContentSizeInPopup = CGSize(width: 336, height: 350)
            }
            
            
        } else {
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                contentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 340))
                
            } else {
                contentSizeInPopup = CGSize(width: 336, height: 350)
            }
        }
        
        
        
        setupAllSubviews()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        
        
        updateFrame()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
    }
    
    func updateFrame() {
        
        btnClose.frame = CGRect(x: view.frame.width - 40,
                                y: 0,
                                width: 40,
                                height: 40)
        
        
        labelTitle.frame = CGRect(x: 0,
                                  y: 0,
                                  width: view.frame.width,
                                  height: 40)
        
        seperator.frame = CGRect(x: 0,
                                 y: labelTitle.frame.height - 1 / UIScreen.main.scale,
                                 width: view.frame.width,
                                 height: 1 / UIScreen.main.scale)
        
        helpView.frame = CGRect(x: 0,
                                y: view.frame.height - 40,
                                width: view.frame.width,
                                height: 40)
        
        forgotView.frame = CGRect(x: 0,
                                  y: labelTitle.frame.maxY,
                                  width: view.frame.width,
                                  height: helpView.frame.minY - labelTitle.frame.maxY)
        
    }
}






extension ForgotPasswordViewController {
    
    func loadLanguage() {
        
        title = LanguageSetting.get("TitleForgotPassWordStep1", alter: nil)
        
        labelTitle.text = LanguageSetting.get("TitleForgotPassWordStep1", alter: nil)
        
        forgotView.lbTitle.text = LanguageSetting.get("TitleContentForgotPassStep1", alter: nil)
        
        forgotView.btnMobile.setTitle(String(format: "%@ %@", LanguageSetting.get("TitlePhoneForgot", alter: nil), strNumberPhone), for: .normal)
        
        forgotView.lbFourEndNumber.text = LanguageSetting.get("Inputlast4digits", alter: nil)
        forgotView.lbOr.text = LanguageSetting.get("TitleOr", alter: nil)
        forgotView.btnEmail.setTitle(String(format: "Email: %@", strEmail), for: .normal)
        
        forgotView.btnNext.setTitle(LanguageSetting.get("TitleButtonNext", alter: nil), for: .normal)
        forgotView.btnBack.setTitle(LanguageSetting.get("title_button_back", alter: nil), for: .normal)
    }
}

extension ForgotPasswordViewController {
    
    /**
     Sự kiện bàn phím hiện lên
     */
    @objc func keyboardWillShow(_ sender: Foundation.Notification) {
        
        Log.message(.info, message: "Show keyboard")
        
        
        
        guard let userInfo = sender.userInfo else { return }
        guard let value = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        guard UIScreen.main.bounds.width > UIScreen.main.bounds.height  else { return }
        
        
        let maxPosition = (popupController?.containerView.frame.origin.y ?? 0) + forgotView.frame.minY + forgotView.btnNext.frame.maxY
        
        Log.message(.info, message: "XXX: \(maxPosition) --- \(value.cgRectValue.height) --- \(UIScreen.main.bounds) ")
        
        guard UIScreen.main.bounds.height - value.cgRectValue.height < maxPosition else { return }
        
        let scrollHeight: CGFloat = maxPosition - 41 - (UIScreen.main.bounds.height - value.cgRectValue.height)
        
        Log.message(.info, message: "AAAA: \(scrollHeight) --- \(view.frame)")
        
        
        //            var paddingButton: CGFloat = (value.cgRectValue.origin.y - loginView.btnLogin.frame.height - 5) - scrollHeight - loginView.frame.minY
        
        
        var paddingButton: CGFloat = value.cgRectValue.origin.y - forgotView.btnNext.frame.height - 5 - abs(popupController?.containerView.frame.origin.y ?? 0) - forgotView.frame.minY
        
        paddingButton = min(forgotView.btnEmail.frame.maxY, paddingButton)
        
        forgotView.animationWithKeyboard(isShow: true, y: paddingButton)
        
        
    }
    
    /**
     Sự kiện bàn phím ẩn đi
     */
    @objc func keyboardWillHide(_ sender: Foundation.Notification) {
        
        Log.message(.info, message: "keyboardWillHide")
        forgotView.animationWithKeyboard(isShow: false, y: 0)
        
    }
    
    @objc func changeLanguage() {
        loadLanguage()
    }
    
    @objc func orientationChanged(_ sender: Notification) {
        
        let device = UIDevice.current
        
        switch device.orientation {
        case .faceUp:
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                contentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 340))
                
            } else {
                contentSizeInPopup = CGSize(width: 336, height: 350)
            }
            
            
        case .portrait, .portraitUpsideDown:
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                contentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 340))
                
            } else {
                contentSizeInPopup = CGSize(width: 336, height: 350)
            }
            
        case .landscapeLeft, .landscapeRight:
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                landscapeContentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 320))
                
            } else {
                landscapeContentSizeInPopup = CGSize(width: 336, height: 350)
            }
            
            
        default:
            break
        }
        
        updateFrame()
    }
    
    @objc func btnCloseClick(_ sender: UIButton) {
        popupController?.dismiss()
    }
    
    @objc func btnBackClick(_ sender: UIButton) {
        
        popupController?.popViewController(animated: true)
    }
    
    @objc func btnNextClick(_ sender: UIButton) {
        
        view.endEditing(true)
        sender.isUserInteractionEnabled = false
        
        if tagButtoneSelect == 0 {
            
            guard let fourEndNumber = forgotView.tfFourEndNumber.text, fourEndNumber.count > 0 else {
                showAlertWithMessage(LanguageSetting.get("messager_input_3_digits", alter: nil))
                sender.isUserInteractionEnabled = true
                return
            }
            
            HUD.showHUD(onView: self.view)
            UtilitieWebService
                .shareSingleton()?
                .forgotmkstepOne(withUsername: self.strUserName,
                                 step: 1,
                                 hiddenNumber: fourEndNumber,
                                 completion: { (results, error, resultCode) in
                                    
                                    HUD.dismissHUD()
                                    sender.isUserInteractionEnabled = true
                                    
                                    guard resultCode == ResultCodeSuccess else {
                                        Utility.showAlertFrom(resultCode, results: results, viewController: self)
                                        return
                                    }
                                    
                                    let qmkb2ViewController = ForgotPasswordStepTwoViewController()
                                    qmkb2ViewController.dictInfo = self.dictResults
                                    qmkb2ViewController.strFourNumber = fourEndNumber
                                    qmkb2ViewController.strUserName = self.strUserName
                                    qmkb2ViewController.strMessager = (results as? [String : Any])?[KEY_MESSAGE] as? String ?? ""
                                    qmkb2ViewController.strsdt = qmkb2ViewController.strMessager
                                    self.popupController?.push(qmkb2ViewController, animated: true)
                                    
                                 })
            
            
        } else {
            
            
            HUD.showHUD(onView: self.view)
            UtilitieWebService
                .shareSingleton()?
                .forgotmkByEmail(withUserName: strUserName, completion: { (results, error, resultCode) in
                    
                    HUD.dismissHUD()
                    sender.isUserInteractionEnabled = true
                    
                    guard let resuultJson = results as? [String : Any], resultCode == ResultCodeSuccess else {
                        Utility.showAlertFrom(resultCode, results: results, viewController: self)
                        return
                    }
                    
                    Utility.showAlertWithMessage(resuultJson[KEY_MESSAGE] as? String ?? "",
                                                 viewController: self,
                                                 handler: { _ in
                                                    
                                                    self.popupController?.dismiss()
                                                 })
                    
                    
                    
                })
        }
    }
    
    @objc func onRadioButtonValueChanged(_ sender: RadioButton) {
        
        if sender.isSelected {
            print(String(format: "Selected color: %@", sender.titleLabel?.text ?? ""))
            tagButtoneSelect = sender.tag
        }
        
        view.endEditing(true)
    }
    
    @objc func onRadioBtn(_ sender: RadioButton) {
        print(String(format: "Value selected %@", sender.titleLabel?.text ?? ""))
        
        view.endEditing(true)
    }
    
}

extension ForgotPasswordViewController: CustomHelpViewDelegate {
    
    func showViewSupport() {
        
    }
    
    func showSettingLanguage() {
        
    }
}

//--------------------------------
// MARK: - TEXT FIELD DELEGATE
//--------------------------------

extension ForgotPasswordViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        var newLength =  string.count - range.length
        
        if let name = textField.text {
            newLength += name.count
        }
        
        return !(newLength > 3)
    }
}

//-----------------------
// MARK: - SETUP
//-----------------------

extension ForgotPasswordViewController {
    
    func setupAllSubviews() {
        
        
        
        view.backgroundColor = UIColor(red: 248/255.0, green: 248/255.0, blue: 248/255.0, alpha: 1.0)
        view.layer.cornerRadius = 8.0
        view.layer.borderColor =  UIColor(red: 13/255.0, green: 183/255.0, blue: 212/255.0, alpha: 1.0).cgColor
        view.layer.borderWidth = 1.5
        view.layer.masksToBounds = true
        
        setupNotification()
        
        
        
        
        helpView.delegate = self
        btnClose.addTarget(self, action: #selector(self.btnCloseClick(_:)), for: .touchUpInside)
        
        
        
        forgotView       = setupForgotView()
        
        
        view.addSubview(forgotView)
        view.addSubview(helpView)
        
        view.addSubview(seperator)
        view.addSubview(labelTitle)
        view.addSubview(btnClose)
        
        loadLanguage()
        
        if strEmail.count > 0 && strNumberPhone.count > 0 {
            
            forgotView.btnMobile.isSelected = true
            forgotView.btnEmail.isSelected = false
            tagButtoneSelect = 0
            
        } else {
            
            forgotView.lbOr.isHidden = true
            
            if strNumberPhone.count == 0 {
                forgotView.btnMobile.isHidden = true
                forgotView.lbFourEndNumber.isHidden = true
                forgotView.tfFourEndNumber.isHidden = true
                forgotView.btnMobile.isSelected = false
                forgotView.btnEmail.isSelected = true
                tagButtoneSelect = 1
                
            } else {
                
                forgotView.btnEmail.isHidden = true
                forgotView.btnEmail.isSelected = false
                forgotView.btnMobile.isSelected = true
                tagButtoneSelect = 0
            }
            
        }
    }
    
    func setupNotification() {
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.changeLanguage),
                                               name: Notification.Name(rawValue: CHANGE_LANGUAGE_SETTING),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.orientationChanged(_:)),
                                               name: UIDevice.orientationDidChangeNotification,
                                               object: UIDevice.current)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillShow(_:)),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillHide(_:)),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
        
        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
        
    }
    
    
    
    func setupForgotView() -> ForgotPasswordContentView {
        let view = ForgotPasswordContentView()
        view.btnNext.addTarget(self, action: #selector(self.btnNextClick(_:)), for: .touchUpInside)
        view.btnBack.addTarget(self, action: #selector(self.btnBackClick(_:)), for: .touchUpInside)
        
        view.btnEmail.addTarget(self, action: #selector(self.onRadioButtonValueChanged(_:)), for: .valueChanged)
        view.btnMobile.addTarget(self, action: #selector(self.onRadioButtonValueChanged(_:)), for: .valueChanged)
        
        view.tfFourEndNumber.delegate = self
        return view
    }
    
    
    
    func setupButton(title: String? = nil,
                     titleSelected: String? = nil,
                     image: UIImage? = nil,
                     imageSelected: UIImage? = nil,
                     titleColor: UIColor = .white) -> UIButton {
        let button = UIButton()
        button.setTitleColor( titleColor, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 11)
        button.setTitle(title, for: .normal)
        button.setTitle(titleSelected, for: .selected)
        button.setImage(image, for: .normal)
        button.setImage(imageSelected, for: .selected)
        button.clipsToBounds = true
        button.imageView?.contentMode = .scaleAspectFit
        return button
    }
    
    
    
    func setupLabel(_ title: String = "", textColor: UIColor = UIColor.black,
                    font: UIFont = UIFont.systemFont(ofSize: 12),
                    bgColor: UIColor = .clear,
                    alignment: NSTextAlignment = .left) -> UILabel {
        
        let label = UILabel()
        label.text = title
        label.textColor = textColor
        label.backgroundColor = bgColor
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = alignment
        label.font = font
        return label
    }
}


