//
//  ForgotPasswordStepThirdViewController.swift
//  SplayGameSDK
//
//  Created by Nguyen Nang on 7/15/20.
//  Copyright © 2020 splay. All rights reserved.
//

import UIKit
import GoogleSignIn

class ForgotPasswordStepThirdViewController: CustomViewController {
    
    //-----------------------
    // MARK: - ENUM
    //-----------------------
    
    enum Size: CGFloat {
        case button = 36, padding10 = 10
    }
    
    
    
    //-----------------------
    // MARK: - VAR
    //-----------------------
    
    var strStringOtp: String = ""
    var strNumberHidden: String = ""
    var dictInfoStep3: [String : Any]?
    
    
    var contentAlert: String = ""
    
    //-----------------------
    // MARK: - VIEW
    //-----------------------
    
    
    
    
    
    var btnUpdate: UIButton!
    var tfPassword: DLTextField!
    var tfRePassword: DLTextField!
    
    
    
    
    //-----------------------
    // MARK: - LIFE CYCLE
    //-----------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if UIScreen.main.bounds.width > UIScreen.main.bounds.height {
            // Landscape
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                landscapeContentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 320))
                
            } else {
                landscapeContentSizeInPopup = CGSize(width: 336, height: 350)
            }
            
            
        } else {
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                contentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 340))
                
            } else {
                contentSizeInPopup = CGSize(width: 336, height: 350)
            }
        }
        
        setupAllSubviews()
        
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        updateFrame()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
    }
    
    func updateFrame() {
        
        btnClose.frame = CGRect(x: view.frame.width - 40,
                                y: 0,
                                width: 40,
                                height: 40)
        
        
        labelTitle.frame = CGRect(x: 0,
                                  y: 0,
                                  width: view.frame.width,
                                  height: 40)
        
        seperator.frame = CGRect(x: 0,
                                 y: labelTitle.frame.height - 1 / UIScreen.main.scale,
                                 width: view.frame.width,
                                 height: 1 / UIScreen.main.scale)
        
        helpView.frame = CGRect(x: 0,
                                y: view.frame.height - 40,
                                width: view.frame.width,
                                height: 40)
        
        btnUpdate.frame = CGRect(x: view.frame.width / 3,
                                 y: helpView.frame.minY - Size.button.rawValue - Size.padding10.rawValue / 2,
                                 width: view.frame.width / 3,
                                 height: Size.button.rawValue)
        
        
        tfPassword.frame = CGRect(x:  Size.padding10.rawValue ,
                                  y: seperator.frame.maxY + Size.padding10.rawValue * 2,
                                  width: view.frame.width - Size.padding10.rawValue * 2,
                                  height: Size.button.rawValue)
        
        
        tfRePassword.frame = CGRect(x: Size.padding10.rawValue,
                                    y: tfPassword.frame.maxY + Size.padding10.rawValue,
                                    width: view.frame.width - Size.padding10.rawValue * 2,
                                    height: Size.button.rawValue)
        
    }
}






extension ForgotPasswordStepThirdViewController {
    
    
    
    func loadLanguage() {
        
        labelTitle.text = LanguageSetting.get("title_forgot_pass3", alter: nil)
        tfPassword.attributedPlaceholder =   NSAttributedString(string: LanguageSetting.get("hint_new_password", alter: nil),
                                                                          attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        

        tfRePassword.attributedPlaceholder =   NSAttributedString(string:LanguageSetting.get("hint_redmine_newpass", alter: nil),
                                                                          attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        

        
        btnUpdate.setTitle(LanguageSetting.get("title_button_update", alter: nil), for: .normal)
        
    }
    
    func isPasswordValid(_ password: String?, repassword: String?) -> Bool {
        
        guard let password = password, password.count > 0 else {
            contentAlert = LanguageSetting.get("validator_username_emtry", alter: nil)
            return false
        }
        
        guard password.trimmingCharacters(in: .whitespacesAndNewlines).count >= 6 &&
            password.trimmingCharacters(in: .whitespacesAndNewlines).count <= 32 else {
                contentAlert = LanguageSetting.get("validator_string_length", alter: nil)
                return false
        }
        
//        let rang = password.rangeOfCharacter(from: .letters)
        
        guard password.rangeOfCharacter(from: .letters) != nil else {
            
            contentAlert = LanguageSetting.get("validator_password", alter: nil)
            return false
        }
        
        guard password.rangeOfCharacter(from: .decimalDigits) != nil else {
            
            contentAlert = LanguageSetting.get("validator_password", alter: nil)
            return false
        }
        
        
        guard let repassword = repassword, password == repassword else {
            contentAlert = LanguageSetting.get("validator_confirm", alter: nil)
            return false
        }
        
        return true
    }
    
    
}

extension ForgotPasswordStepThirdViewController {
    
    @objc func changeLanguage() {
        loadLanguage()
    }
    
    @objc func orientationChanged(_ sender: Notification) {
        
        let device = UIDevice.current
        
        switch device.orientation {
        case .faceUp:
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                contentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 340))
                
            } else {
                contentSizeInPopup = CGSize(width: 336, height: 350)
            }
            
            
        case .portrait, .portraitUpsideDown:
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                contentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 340))
                
            } else {
                contentSizeInPopup = CGSize(width: 336, height: 350)
            }
            
        case .landscapeLeft, .landscapeRight:
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                landscapeContentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 320))
                
            } else {
                landscapeContentSizeInPopup = CGSize(width: 336, height: 350)
            }
            
            
        default:
            break
        }
        
        updateFrame()
    }
    
    @objc func btnCloseClick(_ sender: UIButton) {
        popupController?.dismiss()
    }
    
    @objc func btnhiddenText(_ sender: UIButton) {
           sender.isSelected = !sender.isSelected
           
           if sender.tag == 1 {
                tfPassword.isSecureTextEntry = !sender.isSelected
           } else {
                tfRePassword.isSecureTextEntry = !sender.isSelected
           }
       }
    
    
    @objc func btnUpdateClick(_ sender: UIButton) {
        
        guard isPasswordValid(tfPassword.text, repassword: tfRePassword.text) else {
            Utility.showAlertWithMessage(contentAlert, viewController: self)
            return
        }
        
        let userName: String = self.dictInfoStep3?[KEY_USERNAME] as? String ?? ""
        
        guard let password = tfPassword.text, password != userName else {
            
            Utility.showAlertWithMessage(LanguageSetting.get("MessageUserPassDuplicate", alter: nil), viewController: self)
            
            return
        }
        
        
        HUD.showHUD(onView: self.view)
        UtilitieWebService
            .shareSingleton()?
            .forgotmkstepThree(withUsername: userName,
                               step: 3,
                               hiddenNumber: strNumberHidden,
                               otp: strStringOtp,
                               newPassword: password,
                               completion: { (results, error, resultCode) in
                                
                                
                                HUD.dismissHUD()
                                guard resultCode == ResultCodeSuccess else {
                                    
                                    Utility.showAlertFrom(resultCode, results: results, viewController: self)
                                    return
                                }
                                
                                
                                Utility.showAlertWithMessage(LanguageSetting.get("MessagerGetNewPassSuccess", alter: nil),
                                                             viewController: self,
                                                             handler: { _ in
                                                                
                                                                self.popupController?.dismiss()
                                })          
            })
        
    }
}

extension ForgotPasswordStepThirdViewController: CustomHelpViewDelegate {
    
    func showViewSupport() {
        
    }
    
    func showSettingLanguage() {
        
    }
}


//----------------------------------
// MARK: - TEXT FIELD DELEGATE
//----------------------------------

extension ForgotPasswordStepThirdViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        
        
        if textField.isKind(of: DLTextField.self) {
            
            if let nextTextField = (textField as? DLTextField)?.next {
                
                nextTextField.becomeFirstResponder()
                
            } else {
                
                textField.resignFirstResponder()
                btnUpdate.sendActions(for: .touchUpInside)
                
            }
        }
        
        
        return true
    }
}


//-----------------------
// MARK: - SETUP
//-----------------------

extension ForgotPasswordStepThirdViewController {
    
    func setupAllSubviews() {
        
        
        
        title = "Đăng nhập"
        view.backgroundColor = UIColor(red: 248/255.0, green: 248/255.0, blue: 248/255.0, alpha: 1.0)
        view.layer.cornerRadius = 8.0
        view.layer.borderColor =  UIColor(red: 13/255.0, green: 183/255.0, blue: 212/255.0, alpha: 1.0).cgColor
        view.layer.borderWidth = 1.5
        view.layer.masksToBounds = true
        
        setupNotification()
        
        

        helpView.delegate = self
        btnClose.addTarget(self, action: #selector(self.btnCloseClick(_:)), for: .touchUpInside)
        
        
        
        tfPassword = setupTextField("", isSecureTextEntry: true)
        tfPassword.returnKeyType = .next
        
        tfRePassword = setupTextField("", isSecureTextEntry: true)
        
        
        
        tfPassword.rightView = setupButtonPass(tag: 1)
        tfPassword.rightViewRect(forBounds: CGRect(x: 0, y: 0, width: 24, height: Size.button.rawValue))
        
        tfRePassword.rightView = setupButtonPass(tag: 2)
        tfRePassword.rightViewRect(forBounds: CGRect(x: 0, y: 0, width: 24, height: Size.button.rawValue))
        tfPassword.next = tfRePassword
        
        
        btnUpdate = setupButtonCustom(titleColor: .white)
        btnUpdate.setBackgroundImage(UtilitieWebService.shareSingleton()?.getImageFromStringBase(dn_bg_bt_li), for: .normal)
        btnUpdate.addTarget(self, action: #selector(self.btnUpdateClick(_:)), for: .touchUpInside)
        
        
        
        
        view.addSubview(helpView)
        view.addSubview(tfPassword)
        view.addSubview(tfRePassword)
        view.addSubview(btnUpdate)
        
        view.addSubview(seperator)
        view.addSubview(labelTitle)
        view.addSubview(btnClose)
        
        loadLanguage()
        
    }
    
    func setupNotification() {
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.changeLanguage),
                                               name: Notification.Name(rawValue: CHANGE_LANGUAGE_SETTING),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.orientationChanged(_:)),
                                               name: UIDevice.orientationDidChangeNotification,
                                               object: UIDevice.current)
        
        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
        
    }
    
    func setupTextField(_ placeHoler: String, textInsets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0),
                        isSecureTextEntry: Bool = false) -> DLTextField {
        let textField = DLTextField()
        textField.placeholder = placeHoler
        textField.layer.cornerRadius = 3
        textField.layer.borderWidth = 0.5
        textField.layer.borderColor = UIColor.lightGray.cgColor
        textField.layer.masksToBounds = true
        textField.textInsets = textInsets
        textField.rightViewMode = isSecureTextEntry ? .always : .never
        textField.isSecureTextEntry = isSecureTextEntry
        textField.font = UIFont.systemFont(ofSize: 14)
        textField.background = UtilitieWebService.shareSingleton()?.getImageFromStringBase(dn_bg_textfile)
        textField.delegate = self
        textField.returnKeyType = isSecureTextEntry ? .done : .next
        textField.textColor = UIColor.Text.blackMediumColor()
        return textField
    }
    
    func setupButtonPass(image: UIImage? = nil,
                         imageSelected: UIImage? = nil, tag: Int) -> UIButton {
        let button = UIButton()
        
        button.setImage(UtilitieWebService.shareSingleton()?.getImageFromStringBase(img_icon_hidden_mk_tk), for: .normal)
        button.setImage(UtilitieWebService.shareSingleton()?.getImageFromStringBase(img_dn_show_mk), for: .selected)
        
        button.clipsToBounds = true
        button.frame = CGRect(x: 0, y: Size.button.rawValue / 6, width: 24, height: Size.button.rawValue * 2 / 3 )
        button.addTarget(self, action: #selector(self.btnhiddenText(_:)), for: .touchUpInside)
        button.imageView?.contentMode = .scaleAspectFit
        button.contentMode = .scaleAspectFit
        button.imageEdgeInsets = UIEdgeInsets(top: 12, left: 24, bottom: 12, right: 0)
        button.tag = tag
        return button
    }
    
    
    
}



