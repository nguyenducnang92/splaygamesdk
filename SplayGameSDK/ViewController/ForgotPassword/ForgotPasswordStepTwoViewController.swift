//
//  ForgotPasswordStepTwoViewController.swift
//  SplayGameSDK
//
//  Created by Nguyen Nang on 7/15/20.
//  Copyright © 2020 splay. All rights reserved.
//


import UIKit
import GoogleSignIn
import CleanroomLogger
import OTPFieldView

class ForgotPasswordStepTwoViewController: CustomViewController {
    
    //-----------------------
    // MARK: - ENUM
    //-----------------------
    
    enum Size: CGFloat {
        case button = 36, padding10 = 10
    }
    
    //-----------------------
    // MARK: - VAR
    //-----------------------
    
    var strMessager: String = ""
    var strsdt: String = ""
    
    
    var strUserName: String = ""
    var strFourNumber: String = ""
    var dictInfo: [String : Any]?
    
    
    
    //-----------------------
    // MARK: - VIEW
    //-----------------------
    
    
    
    var activeContentView: ActiveRegisterContentView!
    
    var strOtp: String = ""
    var isHasOTP: Bool = false
    
    //-----------------------
    // MARK: - LIFE CYCLE
    //-----------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if UIScreen.main.bounds.width > UIScreen.main.bounds.height {
            // Landscape
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                landscapeContentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 320))
                
            } else {
                landscapeContentSizeInPopup = CGSize(width: 336, height: 350)
            }
            
            
        } else {
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                contentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 340))
                
            } else {
                contentSizeInPopup = CGSize(width: 336, height: 350)
            }
        }
        
        setupAllSubviews()
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            
//            self.activeContentView.otpStackView.textFieldsCollection.first?.becomeFirstResponder()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        updateFrame()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        //        guard timer != nil else { return }
        //        timer.invalidate()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
    }
    
    func updateFrame() {
        
        btnClose.frame = CGRect(x: view.frame.width - 40,
                                y: 0,
                                width: 40,
                                height: 40)
        
        
        labelTitle.frame = CGRect(x: 0,
                                  y: 0,
                                  width: view.frame.width,
                                  height: 40)
        
        seperator.frame = CGRect(x: 0,
                                 y: labelTitle.frame.height - 1 / UIScreen.main.scale,
                                 width: view.frame.width,
                                 height: 1 / UIScreen.main.scale)
        
        helpView.frame = CGRect(x: 0,
                                y: view.frame.height - 40,
                                width: view.frame.width,
                                height: 40)
        
        activeContentView.frame = CGRect(x: 0,
                                         y: seperator.frame.maxY,
                                         width: view.frame.width,
                                         height: helpView.frame.minY - seperator.frame.maxY)
        
    }
}






extension ForgotPasswordStepTwoViewController {
    
    
    func loadLanguage() {
        
        labelTitle.text = LanguageSetting.get("title_recover_pass_step2", alter: nil)
        

        
        let mainString = String(format: LanguageSetting.get("forgot_pass_step_two_use_phone_send_message", alter: nil), strsdt)
        
        activeContentView.lbSendMessage.attributedText = Utility.getStringAttributeWith(mainString,
                                                                                        subStrings: [strsdt],
                                                                                        mainFont: UIFont.systemFont(ofSize: 13),
                                                                                        subFonts: [UIFont.boldSystemFont(ofSize: 14)],
                                                                                        mainColor: .gray,
                                                                                        subColors: [UIColor.Text.blackMediumColor()])
        
        let stringMessage = String(format: LanguageSetting.get("forgot_pass_step_two_send_to", alter: nil), "MID OTP \(strUserName)", "8030")
        let attr = Utility.getStringAttributeWith(stringMessage,
                                                  subStrings: ["MID OTP \(strUserName)", "8030"],
                                                  mainFont: UIFont.systemFont(ofSize: 14),
                                                  subFonts: [UIFont.boldSystemFont(ofSize: 16)],
                                                  mainColor: .white,
                                                  subColors: [.white])
        
        
        activeContentView.btnSendMessage.setAttributedTitle(attr, for: .normal)
        activeContentView.lbMessager.text = LanguageSetting.get("forgot_pass_step_two_label_sub", alter: nil)
        activeContentView.btnConfirm.setTitle(LanguageSetting.get("TitleButtonNext", alter: nil), for: .normal)
        activeContentView.btnBack.setTitle(LanguageSetting.get("title_button_back", alter: nil), for: .normal)
        
        
    }
    
    
    /*
     @objc func getOTTCode() {
     btnGetOTP.isUserInteractionEnabled = true
     //        DVLog(@"_______get otp funtion %@  %@",self.strsdt,self.strmk);
     UtilitieWebService
     .shareSingleton()?
     .dktkmobile(withNumberPhone: strsdt,
     mk: strmk,
     codeCoutry: "",
     completion: { (results, error, resultCode) in
     
     if resultCode == ResultCodeSuccess {
     //  DVLog(@"Thong tin dang ky thanh cong %@",results[KEY_DATA]);
     self.timer.invalidate()
     }
     
     
     })
     }
     
     
     @objc func updateCountSecond() {
     
     timeTinSeconds -= 1
     
     guard timeTinSeconds == 0 else {
     
     lbGetOTP.text = String(format: LanguageSetting.get("waitting_for_otp", alter: nil), timeTinSeconds)
     
     return
     }
     
     
     lbGetOTP.text = LanguageSetting.get("get_verification_code_again", alter: nil)
     timeTinSeconds = 0
     timer.invalidate()
     }
     
     @objc func btnGetOTPClick(_ sender: UIButton) {
     
     if timer == nil {
     
     btnGetOTP.isUserInteractionEnabled = false
     timeTinSeconds = 20
     
     timer = Timer.scheduledTimer(timeInterval: 1.0,
     target: self,
     selector: #selector(self.updateCountSecond),
     userInfo: nil,
     repeats: true)
     
     }
     
     self.perform(#selector(self.getOTTCode), with: nil, afterDelay: 20)
     
     }
     */
}

extension ForgotPasswordStepTwoViewController {
    
    /**
     Sự kiện bàn phím hiện lên
     */
    @objc func keyboardWillShow(_ sender: Foundation.Notification) {
        
        Log.message(.info, message: "Show keyboard")
        

        
        guard let userInfo = sender.userInfo else { return }
        guard let value = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        guard UIScreen.main.bounds.width > UIScreen.main.bounds.height  else { return }
        
   
            let maxPosition = (popupController?.containerView.frame.origin.y ?? 0) + activeContentView.frame.minY + activeContentView.btnConfirm.frame.maxY
            
            Log.message(.info, message: "XXX: \(maxPosition) --- \(value.cgRectValue.height) --- \(UIScreen.main.bounds) ")
            
            guard UIScreen.main.bounds.height - value.cgRectValue.height < maxPosition else { return }
            
            let scrollHeight: CGFloat = maxPosition - 41 - (UIScreen.main.bounds.height - value.cgRectValue.height)
            
            Log.message(.info, message: "AAAA: \(scrollHeight) --- \(view.frame)")
            

//            var paddingButton: CGFloat = (value.cgRectValue.origin.y - loginView.btnLogin.frame.height - 5) - scrollHeight - loginView.frame.minY
            
            
        let paddingButton: CGFloat = value.cgRectValue.origin.y - activeContentView.btnConfirm.frame.height - 5 - abs(popupController?.containerView.frame.origin.y ?? 0) - activeContentView.frame.minY
        
//        paddingButton = min(activeContentView.btnEmail.frame.maxY, paddingButton)
            
        activeContentView.animationWithKeyboard(isShow: true, y: paddingButton)
            
  
    }
    
    /**
     Sự kiện bàn phím ẩn đi
     */
    @objc func keyboardWillHide(_ sender: Foundation.Notification) {
        
        Log.message(.info, message: "keyboardWillHide")
        activeContentView.animationWithKeyboard(isShow: false, y: 0)

    }
    
    @objc func changeLanguage() {
        loadLanguage()
    }
    
    @objc func orientationChanged(_ sender: Notification) {
        
        let device = UIDevice.current
        
        switch device.orientation {
        case .faceUp:
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                contentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 340))
                
            } else {
                contentSizeInPopup = CGSize(width: 336, height: 350)
            }
            
            
        case .portrait, .portraitUpsideDown:
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                contentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 340))
                
            } else {
                contentSizeInPopup = CGSize(width: 336, height: 350)
            }
            
        case .landscapeLeft, .landscapeRight:
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                landscapeContentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 320))
                
            } else {
                landscapeContentSizeInPopup = CGSize(width: 336, height: 350)
            }
            
            
        default:
            break
        }
        
        updateFrame()
    }
    
    @objc func btnCloseClick(_ sender: UIButton) {
        popupController?.dismiss()
    }
    
    @objc func btnBackClick(_ sender: UIButton) {
        popupController?.popViewController(animated: true)
    }
    
    
    @objc func btnSendMessageClick(_ sender: UIButton) {
        
        let sms: String = String(format: "sms:%@&body=MID OTP %@", "8030", strUserName)
        
        guard let strURL = sms.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
        guard let url = URL(string: strURL) else { return }
        
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
        
    }
    
    @objc func btnConfirmClick(_ sender: UIButton) {
        
        view.endEditing(true)

        guard strOtp.count > 0 else {
            
            Utility.showAlertWithMessage(LanguageSetting.get("verification_code_emtry", alter: nil), viewController: self)
            return
        }
        
        guard isHasOTP else {
            
            Utility.showAlertWithMessage(LanguageSetting.get("incorrect_verification_code", alter: nil), viewController: self)
            return
        }
        
        HUD.showHUD(onView: self.view)
        
        UtilitieWebService
            .shareSingleton()?
            .forgotmkstepTwo(withUsername: self.dictInfo?[KEY_USERNAME] as? String ?? strUserName,
                             step: 2,
                             hiddenNumber: strFourNumber,
                             otp: strOtp,
                             completion: { (results, error, resultCode) in
                                
                                
                                HUD.dismissHUD()
                                //                                DVLog(@"resutl b2 = %@", results);
                                guard resultCode == ResultCodeSuccess else {
                                    Utility.showAlertFrom(resultCode, results: results, viewController: self)
                                    return
                                }

                                let qmkb3ViewController = ForgotPasswordStepThirdViewController()
                                qmkb3ViewController.dictInfoStep3 = self.dictInfo
                                qmkb3ViewController.strStringOtp = self.strOtp
                                qmkb3ViewController.strNumberHidden = self.strFourNumber
                                
                                self.popupController?.push(qmkb3ViewController, animated: true)
                                
                                
                                
            })
        
    }
}

extension ForgotPasswordStepTwoViewController: CustomHelpViewDelegate {
    
    func showViewSupport() {
        
    }
    
    func showSettingLanguage() {
        
    }
}

//-----------------------
// MARK: - SETUP
//-----------------------

//extension ForgotPasswordStepTwoViewController: OTPDelegate {
//
//    func didChangeValidity(isValid: Bool) {
//        //        btnConfirm.isHidden = !isValid
//    }
//}


//-----------------------
// MARK: - SETUP
//-----------------------

extension ForgotPasswordStepTwoViewController: OTPFieldViewDelegate {
    
    
    func hasEnteredAllOTP(hasEnteredAll hasEntered: Bool) -> Bool {
        print("Has entered all OTP? \(hasEntered)")
        
        isHasOTP = hasEntered
        
        return false
    }
    
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otp otpString: String) {
        print("OTPString: \(otpString)")
        
        strOtp = otpString
    }
}


//-----------------------
// MARK: - SETUP
//-----------------------

extension ForgotPasswordStepTwoViewController {
    
    func setupAllSubviews() {
        
        
        view.backgroundColor = UIColor(red: 248/255.0, green: 248/255.0, blue: 248/255.0, alpha: 1.0)
        view.layer.cornerRadius = 8.0
        view.layer.borderColor =  UIColor(red: 13/255.0, green: 183/255.0, blue: 212/255.0, alpha: 1.0).cgColor
        view.layer.borderWidth = 1.5
        view.layer.masksToBounds = true
        
        setupNotification()

        helpView.delegate = self
        btnClose.addTarget(self, action: #selector(self.btnCloseClick(_:)), for: .touchUpInside)
        
        activeContentView = setupActiveView()
        
        view.addSubview(helpView)
        view.addSubview(activeContentView)
        view.addSubview(seperator)
        view.addSubview(labelTitle)
        view.addSubview(btnClose)
        
        loadLanguage()
    }
    
    func setupNotification() {
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.changeLanguage),
                                               name: Notification.Name(rawValue: CHANGE_LANGUAGE_SETTING),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.orientationChanged(_:)),
                                               name: UIDevice.orientationDidChangeNotification,
                                               object: UIDevice.current)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillShow(_:)),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillHide(_:)),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
        
        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
        
    }
    
    
    
    func setupActiveView() -> ActiveRegisterContentView {
        let view = ActiveRegisterContentView()
        view.btnBack.isHidden = false
        view.otpStackView.delegate = self
        view.btnBack.isHidden = false
        view.btnConfirm.addTarget(self, action: #selector(self.btnConfirmClick(_:)), for: .touchUpInside)
        view.btnBack.addTarget(self, action: #selector(self.btnBackClick(_:)), for: .touchUpInside)
        view.btnSendMessage.addTarget(self, action: #selector(self.btnSendMessageClick(_:)), for: .touchUpInside)
        return view
    }
    
    
    

}


