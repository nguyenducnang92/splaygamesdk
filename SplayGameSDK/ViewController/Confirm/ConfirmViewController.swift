//
//  ConfirmViewController.swift
//  SplayGameSDK
//
//  Created by Nguyen Nang on 7/10/20.
//  Copyright © 2020 splay. All rights reserved.
//


import UIKit
import GoogleSignIn

class ConfirmViewController: CustomViewController {
    

    //-----------------------
    // MARK: - VAR
    //-----------------------
    
    var stracEmail: String = ""
    var stracMobile: String = ""
    var strAcName: String = ""
    var dictResultLogin: [String : Any]?
    var passWordRegis: String = ""
    var toScreen: ToScreen?
    
    
    //-----------------------
    // MARK: - VIEW
    //-----------------------
    
    
    
    var confirmView: ConfirmContentView!
    
    
    
    //-----------------------
    // MARK: - LIFE CYCLE
    //-----------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if UIScreen.main.bounds.width > UIScreen.main.bounds.height {
            // Landscape
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                landscapeContentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 320))
                
            } else {
                landscapeContentSizeInPopup = CGSize(width: 336, height: 350)
            }
            
            
        } else {
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                contentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 340))
                
            } else {
                contentSizeInPopup = CGSize(width: 336, height: 350)
            }
        }
        
        
        
        setupAllSubviews()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        
        
        updateFrame()
      
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
 
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
    }
    
    func updateFrame() {
        
        btnClose.frame = CGRect(x: view.frame.width - 40,
                                y: 0,
                                width: 40,
                                height: 40)
        

        labelTitle.frame = CGRect(x: 0,
                                  y: 0,
                                  width: view.frame.width,
                                  height: 40)
        
        seperator.frame = CGRect(x: 0,
                                 y: labelTitle.frame.height - 1 / UIScreen.main.scale,
                                 width: view.frame.width,
                                 height: 1 / UIScreen.main.scale)
        
        helpView.frame = CGRect(x: 0,
                                y: view.frame.height - 40,
                                width: view.frame.width,
                                height: 40)
        
        confirmView.frame = CGRect(x: 0,
                                 y: labelTitle.frame.maxY,
                                 width: view.frame.width,
                                 height: helpView.frame.minY - labelTitle.frame.maxY)
        
    }
}






extension ConfirmViewController {
    
    func loadLanguage() {
        
        labelTitle.text = LanguageSetting.get("TitleView", alter: nil)
        confirmView.labelTitle.setTitle(LanguageSetting.get("TitleContent", alter: nil), for: .normal)
        confirmView.btnPlayGame.setTitle(LanguageSetting.get("title_button_playgame", alter: nil), for: .normal)
        
        confirmView.lbTitleAccount.text = LanguageSetting.get("TitleAccount", alter: nil)
        confirmView.lbTitlePhone.text = LanguageSetting.get("TitlePhone", alter: nil)
        confirmView.lbTitleEmail.text = LanguageSetting.get("TitleEmail", alter: nil)
        
        confirmView.lbEmail.text = LanguageSetting.get("TitleNotinfo", alter: nil)
        confirmView.lbPhone.text = LanguageSetting.get("TitleNotinfo", alter: nil)
    
       }
    
    func updateAccountInfo() {
        Utility.updateAccountInfoWith(dictResultLogin, loginMethod: .byTk, sendEvent: false)
        popupController?.dismiss()
    }

    
}

extension ConfirmViewController {
    
    @objc func changeLanguage() {
           loadLanguage()
       }
       
       @objc func orientationChanged(_ sender: Notification) {
           
           let device = UIDevice.current
           
           switch device.orientation {
           case .faceUp:
               
               if UI_USER_INTERFACE_IDIOM() == .phone {
                   
                   let rect = UIScreen.main.bounds
                   
                   let maxWidth = min(rect.size.width - 20, rect.size.height)
                   contentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 340))
                   
               } else {
                   contentSizeInPopup = CGSize(width: 336, height: 350)
               }
               
               
           case .portrait, .portraitUpsideDown:
               
               if UI_USER_INTERFACE_IDIOM() == .phone {
                   
                   let rect = UIScreen.main.bounds
                   
                   let maxWidth = min(rect.size.width - 20, rect.size.height)
                   contentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 340))
                   
               } else {
                   contentSizeInPopup = CGSize(width: 336, height: 350)
               }
               
           case .landscapeLeft, .landscapeRight:
               
               if UI_USER_INTERFACE_IDIOM() == .phone {
                   
                   let rect = UIScreen.main.bounds
                   
                   let maxWidth = min(rect.size.width - 20, rect.size.height)
                   landscapeContentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 320))
                   
               } else {
                   landscapeContentSizeInPopup = CGSize(width: 336, height: 350)
               }
               
               
           default:
               break
           }
           
           updateFrame()
       }
    
    @objc func btnCloseClick(_ sender: UIButton) {
        updateAccountInfo()
    }
    
    @objc func btnPlayGameClick(_ sender: UIButton) {
       
        updateAccountInfo()
    }
}

extension ConfirmViewController: CustomHelpViewDelegate {
    
    func showViewSupport() {
        
    }
    
    func showSettingLanguage() {
        
    }
}



//-----------------------
// MARK: - SETUP
//-----------------------

extension ConfirmViewController {
    
    func setupAllSubviews() {
        
        
        
        title = "Đăng nhập"
        view.backgroundColor = UIColor(red: 248/255.0, green: 248/255.0, blue: 248/255.0, alpha: 1.0)
        view.layer.cornerRadius = 8.0
        view.layer.borderColor =  UIColor(red: 13/255.0, green: 183/255.0, blue: 212/255.0, alpha: 1.0).cgColor
        view.layer.borderWidth = 1.5
        view.layer.masksToBounds = true
        
        setupNotification()
        
        

        
        helpView.delegate = self
        btnClose.addTarget(self, action: #selector(self.btnCloseClick(_:)), for: .touchUpInside)
        
        
 
        confirmView       = setupConfirmView()
        
        
        view.addSubview(confirmView)
        view.addSubview(helpView)

        view.addSubview(seperator)
        view.addSubview(labelTitle)
        view.addSubview(btnClose)
  
        loadLanguage()
        
        
        guard let dictResultLogin = dictResultLogin else { return }
        guard let dictData = dictResultLogin[KEY_DATA] as? [String : Any] else { return }
        
        confirmView.lbAccount.text = dictData[KEY_USERNAME] as? String
    
        switch toScreen {
        case ActiveMobileScreen:
            confirmView.lbPhone.text = dictData[KEY_USERNAME] as? String
            confirmView.lbPhone.font = UIFont.systemFont(ofSize: 13)

        case RegisterAccountScreen:
            confirmView.lbPhone.text = LanguageSetting.get("TitleNotinfo", alter: nil)
             confirmView.lbPhone.font = UIFont.italicSystemFont(ofSize: 13)
            
        default:
            confirmView.lbPhone.text = LanguageSetting.get("TitleNotinfo", alter: nil)
            confirmView.lbPhone.font = UIFont.italicSystemFont(ofSize: 13)
        }
    }
    
    func setupNotification() {
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.changeLanguage),
                                               name: Notification.Name(rawValue: CHANGE_LANGUAGE_SETTING),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.orientationChanged(_:)),
                                               name: UIDevice.orientationDidChangeNotification,
                                               object: UIDevice.current)
        
        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
        
    }
    
    
    
    func setupConfirmView() -> ConfirmContentView {
        let view = ConfirmContentView()
        
        view.btnPlayGame.addTarget(self, action: #selector(self.btnPlayGameClick(_:)), for: .touchUpInside)
  
        return view
    }
    
    
    
  
}

