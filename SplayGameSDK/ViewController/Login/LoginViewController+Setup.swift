//
//  LoginViewController+Setup.swift
//  SplayGameSDK
//
//  Created by Nguyen Nang on 7/6/20.
//  Copyright © 2020 splay. All rights reserved.
//

import UIKit
import CleanroomLogger

//-----------------------
// MARK: - SETUP
//-----------------------

extension LoginViewController {
    
    func setupAllSubviews() {
        
        
        
        title = "Đăng nhập"
        view.backgroundColor = UIColor(red: 248/255.0, green: 248/255.0, blue: 248/255.0, alpha: 1.0)
        view.layer.cornerRadius = 8.0
        view.layer.borderColor =  UIColor(red: 13/255.0, green: 183/255.0, blue: 212/255.0, alpha: 1.0).cgColor
        view.layer.borderWidth = 1.5
        view.layer.masksToBounds = true
        
        Log.message(.info, message: "LoginViewController  setupNotification")

        
        
        setupNotification()
        
        bgView = setupView(UIColor(red: 248/255.0, green: 248/255.0, blue: 248/255.0, alpha: 1.0))
        segmentControl  = setupSegment()
        
        Log.message(.info, message: "LoginViewController  helpView")

        
        helpView.setHiddenLanguage(false)
        helpView.delegate = self
        btnClose.addTarget(self, action: #selector(self.btnCloseClick(_:)), for: .touchUpInside)
        
        
        
        nextKeybroadView = setupNextKeybroadAccessoryView(title: LanguageSetting.get("login_button_title_next", alter: nil), tag: 1)
        doneKeybroadView = setupNextKeybroadAccessoryView(title: LanguageSetting.get("login_button_title_next", alter: nil), tag: 2)

        registerView    = setupRegisterView()
        

        loginView       = setupLoginView()
        
        scrollView      = setupScrollView()
        
        scrollView.addSubview(registerView)
        scrollView.addSubview(loginView)

//        view.addSubview(registerView)
//
//        view.addSubview(loginView)

        view.addSubview(scrollView)
        view.addSubview(helpView)
        

        view.addSubview(bgView)
        view.addSubview(segmentControl)
        view.addSubview(seperator)
        view.addSubview(btnClose)
        
  
//        registerView.isHidden = true
        
    
        loadLanguage()
        
    
    }
    
    func setupNotification() {
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.changeLanguage),
                                               name: Notification.Name(rawValue: CHANGE_LANGUAGE_SETTING),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.orientationChanged(_:)),
                                               name: UIDevice.orientationDidChangeNotification,
                                               object: UIDevice.current)
        
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillShow(_:)),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillHide(_:)),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
        
        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
        
    }
    

    func setupSegment() -> HMSegmentedControl {
        let segmentControl: HMSegmentedControl = HMSegmentedControl(sectionTitles: [LanguageSetting.get("TitleTabLogin", alter: "") as Any,
                                                                LanguageSetting.get("TitleTabRegister", alter: "") as Any])
        
        segmentControl.addTarget(self, action: #selector(self.segmentAction(_:)), for: .valueChanged)
        
        segmentControl.titleTextAttributes = [kCTFontAttributeName: UIFont.boldSystemFont(ofSize: 13),
                                              kCTForegroundColorAttributeName: UIColor.lightGray]
        
        segmentControl.selectedTitleTextAttributes = [kCTForegroundColorAttributeName: UIColor(red: 255.0/255, green: 162.0/255, blue: 37.0/255.0, alpha: 1.0)]

        segmentControl.selectionIndicatorColor = UIColor(red: 255.0/255, green: 162.0/255, blue: 37.0/255, alpha: 1.0)
        segmentControl.segmentEdgeInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        segmentControl.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe
        segmentControl.selectionIndicatorHeight = 2.0
        segmentControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown
        segmentControl.isVerticalDividerEnabled = true
        segmentControl.verticalDividerColor = .clear
        segmentControl.verticalDividerWidth = 0.5
        segmentControl.backgroundColor = .clear
        
//        segmentControl.borderType = .bottom
//        segmentControl.borderColor = UIColor.lightGray
//        segmentControl.borderWidth = 1 / UIScreen.main.scale
        
        return segmentControl
    }

    
    /*
    
    func setupSegment() -> FUISegmentedControl {

        let segment = FUISegmentedControl()
        segment.removeAllSegments()
        
        for (i, title) in [LanguageSetting.get("TitleTabLogin", alter: ""),
                             LanguageSetting.get("TitleTabRegister", alter: "")].enumerated() {
            segment.insertSegment(withTitle: title, at: i, animated: false)
        }
        
        segment.selectedFontColor = UIColor(red: 255.0/255, green: 162.0/255, blue: 37.0/255.0, alpha: 1.0)
        segment.selectedColor = UIColor.Segment.selectedBackgroundColor()
        segment.deselectedFontColor = UIColor.Text.blackMediumColor()
        segment.deselectedColor = UIColor.Segment.deselectedBackgroundColor()
        segment.backgroundColor = .white
        segment.dividerColor = UIColor.Segment.deviderColor()
        segment.cornerRadius = 0
        
        segment.selectedSegmentIndex = 0
        segment.selectedFont = UIFont.boldSystemFont(ofSize: 13)
        segment.disabledFont = UIFont.boldSystemFont(ofSize: 13)
        segment.deselectedFont = UIFont.boldSystemFont(ofSize: 13)
        segment.highlightedColor = UIColor.clear
        
        segment.layer.shadowColor = UIColor.black.withAlphaComponent(0.5).cgColor
        segment.layer.shadowOpacity = 0.3
        segment.layer.shadowRadius = 2.0
        segment.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        segment.clipsToBounds = false
        
        segment.addTarget(self, action: #selector(self.segmentAction(_:)), for: .valueChanged)
  
        return segment
    }
    */
    
    
    func setupLoginView() -> LoginContentView {
        let loginView = LoginContentView()
        
        Log.message(.info, message: "Init Finished Login")
        
        loginView.btnApple.addTarget(self, action: #selector(self.loginApple(_:)), for: .touchUpInside)
//        loginView.btnFacebook.addTarget(self, action: #selector(self.loginFacebook(_:)), for: .touchUpInside)
        loginView.btnGoogle.addTarget(self, action: #selector(self.loginGoogle(_:)), for: .touchUpInside)
        loginView.btnLogin.addTarget(self, action: #selector(self.btnLoginClicked(_:)), for: .touchUpInside)
        loginView.btnPlayNow.addTarget(self, action: #selector(self.btnPlayNowClick(_:)), for: .touchUpInside)
        loginView.btnForgot.addTarget(self, action: #selector(self.btnForgotClick(_:)), for: .touchUpInside)
        
     
        
        Log.message(.info, message: "btnFacebook Delegate")
        
        loginView.btnFacebook.delegate = self
        
        
        
        Log.message(.info, message: "Text Field Delegate")
        loginView.tfPassword.delegate = self
        loginView.tfUsername.delegate = self
        loginView.tfUsername.next = loginView.tfPassword
        loginView.tfPassword.tag = 1

        
        Log.message(.info, message: "Return LoginView")
        
        loginView.tfUsername.inputAccessoryView = nextKeybroadView
        loginView.tfPassword.inputAccessoryView = doneKeybroadView
        return loginView
    }
    
    func setupRegisterView() -> RegisterContentView {
        let view = RegisterContentView()
        
        view.delegate = self
        view.tableView.delegate = self
        view.tableView.dataSource = self
        
        view.tfPassword.delegate = self
        view.tfRePassword.delegate = self
        view.tfUsername.delegate = self
        view.tfPhoneNumber.delegate = self
        
        view.tfPhoneNumber.next = view.tfPassword
        view.tfUsername.next = view.tfPassword
        view.tfPassword.next = view.tfRePassword
        view.tfRePassword.tag = 2
        

        view.btnRegister.addTarget(self, action: #selector(self.registerClick(_:)), for: .touchUpInside)
        view.btnSelectCountry.setTitle(arrCountryName.first, for: .normal)
        
        
        

        return view
    }
    
    func setupScrollView() -> UIScrollView {
        let scrollView = UIScrollView()
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        scrollView.isPagingEnabled = true
        scrollView.delegate = self
        scrollView.isScrollEnabled = false
        
//        let tap = UITapGestureRecognizer(target: self, action: #selector(self.hiddenKeyboard))
//        tap.cancelsTouchesInView = false
//        scrollView.addGestureRecognizer(tap)
         return scrollView
    }
    
    func setupView(_ bgColor: UIColor = UIColor.Misc.seperatorColor()) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        view.clipsToBounds = true
        return view
    }
    
    
    func setupNextKeybroadAccessoryView(title: String, tag: Int) -> NextKeybroadAccessoryView {
        let view = NextKeybroadAccessoryView()
        view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 44)
        
        
        if UIScreen.main.bounds.width > UIScreen.main.bounds.height {
            // Landscape
            view.width = landscapeContentSizeInPopup.width
        } else {
            view.width = contentSizeInPopup.width
        }

        view.buttonClose.addTarget(self, action: #selector(self.nextViewCloseClick(_:)), for: .touchUpInside)
        view.buttonNext.addTarget(self, action: #selector(self.nextViewNextClick(_:)), for: .touchUpInside)
        view.buttonNext.setTitle(title, for: .normal)
        view.buttonNext.tag = tag
        
        return view
    }
}

