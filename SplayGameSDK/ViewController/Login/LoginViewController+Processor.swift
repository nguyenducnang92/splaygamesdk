//
//  LoginViewController+Processor.swift
//  SplayGameSDK
//
//  Created by Nguyen Nang on 7/6/20.
//  Copyright © 2020 splay. All rights reserved.
//

import Foundation
import GoogleSignIn
import FBSDKLoginKit
import FBSDKCoreKit
import Firebase
import CleanroomLogger
import AuthenticationServices

//-----------------------
// MARK: - PRIVATE MOTHOD
//-----------------------

extension LoginViewController {
    
    @objc func hiddenKeyboard() {
        view.endEditing(true)
    }
    
    
    /**
     Chuyển sang table tương ứng với selected segment
     
     - parameter page:
     */
    func scrollViewMoveToPage(_ page: Int) {
        

        var rect = scrollView.bounds
        rect.origin.x = rect.width * CGFloat(page)
        rect.origin.y = 0
        scrollView.scrollRectToVisible(rect, animated: true)
        
    }
    
    
    
    func facebookLoginFailed(_ isFBResponce: Bool) {
        showAlertWithMessage(LanguageSetting.get(isFBResponce ? "request_error" : "loginfb_cancelled", alter: nil))
    }
    
    
    @objc func showWelcome() {
        
        TWMessageBarManager.sharedInstance()?.showMessage(withTitle: LanguageSetting.get("text_welcome", alter: ""),
                                                          description: UserDefaults.standard.object(forKey: KEY_SAVE_USERNAME) as? String ?? "",
                                                          type: TWMessageBarMessageTypeInfo,
                                                          callback: nil)
    }
    
    func sendEventPurchaseWithDictLastTest(_ dictLastTest: [String : Any], accessToken: String, apiKey: String) {
        
        guard let code = dictLastTest["code"] as? Int, code == 1 else { return }
        guard let array = dictLastTest[KEY_DATA] as? [[String : Any]], array.count > 0 else { return }
        
        for (i, obj) in array.enumerated() {
            
            let transactionID   = obj["transactionId"] as? String ?? ""
            let amount          = obj["purchaseAmount"] as? String ?? ""
            let pType           = obj["paymentType"] as? String ?? ""
            let eventName       = obj["eventName"] as? String ?? ""
            
            SplayGameSDKManager.sharedManager().logPurchaseEvent(amount,
                                                                 eventName: eventName,
                                                                 productId: transactionID,
                                                                 paymentInapp: pType)
            
            if i == array.count - 1 {
                
                UtilitieWebService.shareSingleton()?.sendStatusTran(withAccessToken: accessToken,
                                                                    apiKey: apiKey,
                                                                    transactionId: transactionID,
                                                                    completion: { (_, _, _) in
                                                                        
                                                                        
                })
            }
        }
    }
    
    
    
    
    func login() {
        

        let apiKeyApp = UserDefaults.standard.object(forKey: KEY_API_CONFIG_APP) as? String ?? ""
        
        Log.message(.info, message: "Action Login")
        
        guard let username = loginView.tfUsername.text, username.count > 0 else {
            

            loginView.tfUsername.becomeFirstResponder()
            Utility.showAlertWithMessage(LanguageSetting.get("validator_username_emtry", alter: nil), viewController: self)

            return
        }
        
        guard let password = loginView.tfPassword.text, password.count > 0 else {
            

            loginView.tfPassword.becomeFirstResponder()
            Utility.showAlertWithMessage(LanguageSetting.get("validator_password_emtry", alter: nil), viewController: self)
            return
        }
        
        HUD.showHUD(onView: self.view)
        UtilitieWebService.shareSingleton()?.dn(withTentk: username, mk: password) { (results, error, resultCode) in
            
            HUD.dismissHUD()
            
            
            Utility.logEventDebug(.loginSplay)
            
            switch resultCode {
            case ResultCodeSuccess:
                
                
                
                guard let result = results as? [String : Any],
                    let resultData = result[KEY_DATA] as? [String : Any] else { return }
                
                UserDefaults.standard.set(resultData[KEY_USER_ID] as? String ?? "", forKey: KEY_SAVE_USER_FIRST_LOGIN)
                UserDefaults.standard.synchronize()
                
                
                Utility.updateAccountInfoWith(results, loginMethod: .byTk)
                UnitAnalyticHelper.analyticLoginEvent(MethodDn.byTk.key)
                Utility.trackEventCompleteRegisterOldOrNew(MethodDn.byTk.key, results: results)

                
                UtilitieWebService
                    .shareSingleton()?
                    .getListTran(withAccessToken: resultData[KEY_ACCESS_TOKEN] as? String ?? "",
                                 apiKey: apiKeyApp) { (resultsList, errorList, resultCodeList) in

                        Log.message(.info, message: "List transaction \(String(describing: resultsList))")
                        
                        guard let result = resultsList as? [String : Any], resultCodeList == ResultCodeSuccess else { return }
                        guard let code = result["code"] as? Int, code == 1 else { return }
                        self.sendEventPurchaseWithDictLastTest(result,
                                                               accessToken: resultData[KEY_ACCESS_TOKEN] as? String ?? "",
                                                               apiKey: apiKeyApp)
                        
                        
                        
                    }
                
                
                
                self.perform(#selector(self.showWelcome), with: nil, afterDelay: 1.5)
                self.popupController?.dismiss()
                
            case Accountlocked:
                let lockAccountVC = AlertLockAccountViewController()
                lockAccountVC.strMessager = (results as? [String : Any])?[KEY_MESSAGE] as? String ?? ""
                self.popupController?.push(lockAccountVC, animated: true)
                
            case AccountNotActivePhoneNumber:
                
                let activeViewController = ActiveRegisterViewController()
                activeViewController.strsdt = username
                activeViewController.strmk = password
                activeViewController.isGetOtp = true
                
                self.popupController?.push(activeViewController, animated: true)
                
            default:
                Utility.showAlertFrom(resultCode, results: results, viewController: self)
            }
        }
    }
    
    
    
    func loadLanguage() {
        
  
        segmentControl.sectionTitles = [LanguageSetting.get("TitleTabLogin", alter: nil) as Any,
                                        LanguageSetting.get("TitleTabRegister", alter: nil) as Any]
        segmentControl.selectedSegmentIndex = indexSegment
        
        loadContentWebview()
        loginView.lblOr.text = LanguageSetting.get("TitleOr", alter: nil)
        
        loginView.tfUsername.attributedPlaceholder =   NSAttributedString(string: LanguageSetting.get("TextFieldLoginAccountName", alter: nil),
                                                                          attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        
        loginView.tfPassword.attributedPlaceholder = NSAttributedString(string: LanguageSetting.get("TextFieldLoginPass", alter: nil),
                                                                        attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        
        
        
        loginView.btnLogin.setTitle(LanguageSetting.get("TitleButtonLogin", alter: nil), for: .normal)
        loginView.btnPlayNow.setTitle(LanguageSetting.get("TitleButtonPlayNow", alter: nil), for: .normal)
        loginView.btnForgot.setTitle(LanguageSetting.get("FogotPasswordLink", alter: nil), for: .normal)
        loginView.btnGoogle.setTitle(LanguageSetting.get("TitleButtonGoogle", alter: nil), for: .normal)
        
        loginView.btnFacebook.setTitle(LanguageSetting.get("TitleButtonFacebook", alter: nil), for: .normal)
        loginView.btnFacebook.setTitle(LanguageSetting.get("TitleButtonFacebook", alter: nil), for: .selected)
        loginView.btnFacebook.setTitle(LanguageSetting.get("TitleButtonFacebook", alter: nil), for: .highlighted)
        loginView.btnFacebook.setTitle(LanguageSetting.get("TitleButtonFacebook", alter: nil), for: .focused)
        loginView.btnFacebook.titleLabel?.text = LanguageSetting.get("TitleButtonFacebook", alter: nil)
        
        loginView.btnFacebook.isSelected = true
        loginView.btnFacebook.isEnabled = true
        
        loginView.btnApple.setTitle(LanguageSetting.get("TitleButtonApple", alter: nil), for: .normal)
    
        /*
        registerView.tfUsername.placeholder = LanguageSetting.get("TextFieldRegisterAccountName", alter: nil)
        registerView.tfPassword.placeholder = LanguageSetting.get("TextFieldRegisterPass", alter: nil)
        registerView.tfRePassword.placeholder = LanguageSetting.get("TextFieldRegisterConfirmPass", alter: nil)
        registerView.tfPhoneNumber.placeholder = LanguageSetting.get("TextFieldRegisterPhone", alter: nil)
        */
        
        registerView.tfUsername.attributedPlaceholder =   NSAttributedString(string: LanguageSetting.get("TextFieldRegisterAccountName", alter: nil),
                                                                          attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        
        registerView.tfPassword.attributedPlaceholder = NSAttributedString(string: LanguageSetting.get("TextFieldRegisterPass", alter: nil),
                                                                        attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        
        registerView.tfRePassword.attributedPlaceholder =   NSAttributedString(string: LanguageSetting.get("TextFieldRegisterConfirmPass", alter: nil),
                                                                          attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        
        registerView.tfPhoneNumber.attributedPlaceholder = NSAttributedString(string: LanguageSetting.get("TextFieldRegisterPhone", alter: nil),
                                                                        attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        
        registerView.btnRegister.setTitle(LanguageSetting.get("TitleButtonRegister", alter: nil), for: .normal)
        
        var language = UserDefaults.standard.object(forKey: KEY_SAVE_LANGUAGE_SETTING) as? String ?? ""
        
        if language.count == 0 {
            language = Locale.preferredLanguages.first ?? ""
        }
        
        
        Log.message(.info, message: "LoginViewController  btnSwitchAccount")
        
        registerView.btnSwitchAccount
            .setBackgroundImage(UtilitieWebService
                .shareSingleton()?
                .getImageFromStringBase(language.contains("vi") ? dn_bg_bt_dk_account : dn_bg_bt_dk_account_en),
                                for: .selected)
        
        
    
        
        nextKeybroadView.buttonNext.setTitle(LanguageSetting.get("login_button_title_next", alter: nil), for: .normal)
        
        doneKeybroadView.buttonNext.setTitle(LanguageSetting.get("login_button_title_next", alter: nil), for: .normal)

    }
    
    
    func loadContentWebview() {
        
        /*
        let baseURL = URL(fileURLWithPath: Bundle.main.bundlePath)
        guard let filePath = Bundle.main.path(forResource: "Resources.bundle/policyUse", ofType: "html") else { return }
        
        do {
            
            let htmlFormat: String = try String(contentsOfFile: filePath, encoding: .utf8)
            let contentPolicy: String = LanguageSetting.get("PolicyRegister", alter: nil)
            let htmlString = String(format: htmlFormat, contentPolicy)
            
            registerView.webViewPolicy.loadHTMLString(htmlString, baseURL: baseURL)
            
        } catch let error {
            print("\(error)")
        }
        */
        
        

        let baseURL = URL(fileURLWithPath: Bundle.module.bundlePath)
        guard let filePath = Bundle.module.path(forResource: "policyUse", ofType: "html") else { return }
        
        do {
            
            let htmlFormat: String = try String(contentsOfFile: filePath, encoding: .utf8)
            let contentPolicy: String = LanguageSetting.get("PolicyRegister", alter: nil)
            let htmlString = String(format: htmlFormat, contentPolicy)
            
            registerView.webViewPolicy.loadHTMLString(htmlString, baseURL: baseURL)
            
        } catch let error {
            print("\(error)")
        }
    }
    
    
    func isPhoneNumberValid(_ phoneNumber: String?) -> Bool {
        
        guard let phoneNumber = phoneNumber, phoneNumber.count > 0 else {
            contentAlert = LanguageSetting.get("validator_phone_emtry", alter: nil)
            return false
        }
        
        
        guard phoneNumber.count >= 10 && phoneNumber.count <= 11 else {
            contentAlert = LanguageSetting.get("validator_phone", alter: nil)
            return false
        }
        return true
    }
    
    
    func isPasswordValid(_ password: String?, repassword: String?) -> Bool {
        
        guard let password = password, password.count > 0 else {
            contentAlert = LanguageSetting.get("validator_username_emtry", alter: nil)
            return false
        }
        
        guard password.trimmingCharacters(in: .whitespacesAndNewlines).count >= 6 &&
            password.trimmingCharacters(in: .whitespacesAndNewlines).count <= 32 else {
                contentAlert = LanguageSetting.get("validator_string_length", alter: nil)
                return false
        }
        
//        let rang = password.rangeOfCharacter(from: .letters)
        
        guard password.rangeOfCharacter(from: .letters) != nil else {
            
            contentAlert = LanguageSetting.get("validator_password", alter: nil)
            return false
        }
        
        guard password.rangeOfCharacter(from: .decimalDigits) != nil else {
            
            contentAlert = LanguageSetting.get("validator_password", alter: nil)
            return false
        }
        
        
        guard let repassword = repassword, password == repassword else {
            contentAlert = LanguageSetting.get("validator_confirm", alter: nil)
            return false
        }
        
        return true
    }
    
    
    /*
     -(BOOL)isPasswordValid:(NSString *)password repassword:(NSString*)repassword{
     if (password == nil || [password isEqualToString:@""]) {
     contentAlert = [LanguageSetting get:@"validator_username_emtry" alter:nil];
     return NO;
     }
     if ([password stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length < 6 || [password stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 32) {
     contentAlert = [LanguageSetting get:@"validator_string_length" alter:nil];
     return NO;
     }
     NSRange rang;
     rang = [password rangeOfCharacterFromSet:[NSCharacterSet letterCharacterSet]];
     if (![password rangeOfCharacterFromSet:[NSCharacterSet letterCharacterSet]].length) {
     contentAlert = [LanguageSetting get:@"validator_password" alter:nil];
     return NO;
     }
     if (![password rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]].length) {
     contentAlert = [LanguageSetting get:@"validator_password" alter:nil];
     return NO;
     }
     if (![password isEqualToString:repassword]){
     contentAlert = [LanguageSetting get:@"validator_confirm" alter:nil];
     return NO;
     }
     return YES;
     }
     */
    
    func isUserNameValid(_ userName: String?, password: String?) -> Bool {
        
        guard let userName = userName, userName.count > 0 else {
            contentAlert = LanguageSetting.get("validator_username_emtry", alter: nil)
            return false
        }
        
        let set = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789").inverted
        let alphaNums = CharacterSet.decimalDigits
        let inStringSetAc = CharacterSet(charactersIn: userName)
        
        
        
        guard userName.count >= 4 && userName.count <= 32 else {
            contentAlert = LanguageSetting.get("validator_user_name_string_length", alter: nil)
            return false
        }
        
        //        if userName.rangeOfCharacter(from: set).location != NSNotFound {
        //            contentAlert = LanguageSetting.get("validator_username", alter: nil)
        //            return false
        //        }
        
        guard userName.rangeOfCharacter(from: set) == nil else {
            contentAlert = LanguageSetting.get("validator_username", alter: nil)
            return false
        }
        
        guard !alphaNums.isSuperset(of: inStringSetAc) else {
            contentAlert = LanguageSetting.get("validator_user_name_only_number", alter: nil)
            return false
        }
        
        guard userName != "abcdef" && userName != "abc123" else {
            contentAlert = LanguageSetting.get("validator_simple_user", alter: nil)
            return false
        }
        
        
        guard let password = password, userName != password else {
            contentAlert = LanguageSetting.get("validator_username_different_from_pass", alter: nil)
            return false
        }
        
        return true
    }
    
    
    
    func gotoConfirmAccount() {
        
        let confirmViewController = ConfirmViewController()
        confirmViewController.dictResultLogin = dicDataRegister
        confirmViewController.toScreen = RegisterAccountScreen
        popupController?.push(confirmViewController, animated: true)
        
    }
    
    
    /**
     Đăng ký tài khoản
     */
    func registerAccount() {
        
        view.endEditing(true)
        guard registerView.btnTerm.isSelected else { return }
        guard isUserNameValid(registerView.tfUsername.text, password: registerView.tfPassword.text) && isPasswordValid(registerView.tfPassword.text, repassword: registerView.tfRePassword.text) else {
            showAlertWithMessage(contentAlert)
            return
        }
        
        HUD.showHUD(onView: self.view)
        UtilitieWebService
            .shareSingleton()?
            .dktk(withTentk: registerView.tfUsername.text!,
                  mk: registerView.tfPassword.text!,
                  completion: { (results, error, resultCode) in
                    
                    HUD.dismissHUD()
                    //                    DVLog(@"________________result code = %d",resultCode);
                    Log.message(.info, message: "________________result code =  \(resultCode)")
                    
                    
                    Utility.logEventDebug(.registerSuccess, parameters: ["splay_account_result_code" : resultCode])
                    
                    if resultCode == ResultCodeSuccess {
                        
  
                        Utility.updateAccountInfoWith(results, isLogin: nil, sendEvent: false, isPostNoti: false)
                        Utility.trackEventCompleteRegisterOldOrNew("splay_account", results: results)
       
                        if let valueFirstLogin = (results as? [String : Any])?[KEY_FIRST_LOGIN] as? Int, valueFirstLogin == 1 {
                            UnitAnalyticHelper.analyticLoginEvent(MethodDn.byTk.key)
                        }
                        
                        self.dicDataRegister = results as? [String : Any] 
                        
                        self.gotoConfirmAccount()
                        
                    } else if resultCode == AccountEsixted {
                        
                        let existVC = ExistAccountViewController()
                        existVC.dicResultUpdate = results as? [String : Any]
                        existVC.strAccountName = self.registerView.tfUsername.text ?? ""
                        existVC.strPassword = self.registerView.tfPassword.text ?? ""
                        existVC.toScreen = RegisterAccountScreen
                        self.popupController?.push(existVC, animated: true)
                        
                        
                    } else {
                        
                        Utility.sendEventToFirebaseWith(EVEN_SIGNUP_ERROR, parameters: nil)
                        Utility.showAlertFrom(resultCode, results: results, viewController: self)
                    }
            })
    }
    
    
    func registerAcByMobile() {
        
        view.endEditing(true)
        
        guard registerView.btnTerm.isSelected else { return }
        guard isPhoneNumberValid(registerView.tfPhoneNumber.text) &&
            isPasswordValid(registerView.tfPassword.text, repassword: registerView.tfRePassword.text) else {
            showAlertWithMessage(contentAlert)
            return
        }
        
        HUD.showHUD(onView: self.view)
        UtilitieWebService
            .shareSingleton()?
            .dktkmobile(withNumberPhone: registerView.tfPhoneNumber.text,
                        mk: registerView.tfPassword.text,
                        codeCoutry: codeCoutry) { (results, error, resultCode) in
                
                HUD.dismissHUD()
                Log.message(.info, message: "________________result code =  \(resultCode)")
                
                Utility.logEventDebug(.registerSuccess, parameters: ["splay_mobile_result_code" : resultCode])
                
                switch resultCode {
                case ResultCodeSuccess:
                    
                    guard let result = results as? [String : Any] else { return }

                    Log.message(.info, message: "Thong tin dang ky thanh cong  \(String(describing: result[KEY_DATA]))")
                    
                    Utility.updateAccountInfoWith(results, isLogin: nil, sendEvent: false, isPostNoti: false)
                    Utility.trackEventCompleteRegisterOldOrNew("splay_mobile", results: results, isRegisterPushNotification: false)
                    
                    if let valueFirstLogin = result[KEY_FIRST_LOGIN] as? Int, valueFirstLogin == 1 {
                        UnitAnalyticHelper.analyticLoginEvent(MethodDn.byTk.key)
                    }
                    
                    let activeAcVC = ActiveRegisterViewController()
                    activeAcVC.strMessager = result[KEY_MESSAGE] as? String ?? ""
                    activeAcVC.isGetOtp = false
                    activeAcVC.strsdt = self.registerView.tfPhoneNumber.text ?? ""
                    activeAcVC.strmk = self.registerView.tfPassword.text ?? ""
                    
                    self.popupController?.push(activeAcVC, animated: true)
                    
                case NumberPhoneUsedNotActive:
                    
                    guard let result = results as? [String : Any] else { return }
                    
                    let activeAcVC = ActiveRegisterViewController()
                    activeAcVC.strMessager = result[KEY_MESSAGE] as? String ?? ""
                    activeAcVC.isGetOtp = true
                    activeAcVC.strsdt = self.registerView.tfPhoneNumber.text ?? ""
                    activeAcVC.strmk = self.registerView.tfPassword.text ?? ""
                    
                    self.popupController?.push(activeAcVC, animated: true)
                    
                default:
                    Utility.sendEventToFirebaseWith(EVEN_SIGNUP_ERROR, parameters: nil)
                    Utility.showAlertFrom(resultCode, results: results, viewController: self)
                }

            }
    }
    
}

//-----------------------
// MARK: - SELECTOR
//-----------------------

extension LoginViewController {
    
    
    /**
     Sự kiện bàn phím hiện lên
     */
    @objc func keyboardWillShow(_ sender: Foundation.Notification) {
        
        Log.message(.info, message: "Show keyboard")
        

        
        guard let userInfo = sender.userInfo else { return }
        guard let value = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        guard UIScreen.main.bounds.width > UIScreen.main.bounds.height  else { return }
        
        
        switch segmentControl.selectedSegmentIndex {
        case 0:
            guard let textField = selectedTextField, textField.tag == 1 else { return }

            let maxPosition = (popupController?.containerView.frame.origin.y ?? 0) + loginView.frame.minY + loginView.btnLogin.frame.maxY
            
            Log.message(.info, message: "XXX: \(maxPosition) --- \(value.cgRectValue.height) --- \(UIScreen.main.bounds) ")
            
            guard UIScreen.main.bounds.height - value.cgRectValue.height < maxPosition else { return }
            
            let scrollHeight: CGFloat = maxPosition - 41 - (UIScreen.main.bounds.height - value.cgRectValue.height)
            
            Log.message(.info, message: "AAAA: \(scrollHeight) --- \(view.frame)")
            

//            var paddingButton: CGFloat = (value.cgRectValue.origin.y - loginView.btnLogin.frame.height - 5) - scrollHeight - loginView.frame.minY
            
            
            var paddingButton: CGFloat = value.cgRectValue.origin.y - loginView.btnLogin.frame.height - 5 - abs(popupController?.containerView.frame.origin.y ?? 0) - loginView.frame.minY
            
            paddingButton = min(loginView.btnForgot.frame.maxY, paddingButton)
            
            loginView.animationWithKeyboard(isShow: true, y: paddingButton)
            
            
        default:
            guard let textField = selectedTextField, textField.tag == 2 else { return }

            let maxPosition = (popupController?.containerView.frame.origin.y ?? 0) + registerView.frame.minY + registerView.btnRegister.frame.maxY
            
            Log.message(.info, message: "XXX: \(maxPosition) --- \(value.cgRectValue.height) --- \(UIScreen.main.bounds) ")
            
            guard UIScreen.main.bounds.height - value.cgRectValue.height < maxPosition else { return }
            
            let scrollHeight: CGFloat = maxPosition - 41 - (UIScreen.main.bounds.height - value.cgRectValue.height)
            
            Log.message(.info, message: "AAAA: \(scrollHeight) --- \(view.frame)")
            

//            var paddingButton: CGFloat = (value.cgRectValue.origin.y - registerView.btnRegister.frame.height - 5) - scrollHeight - registerView.frame.minY
            
            
            var paddingButton: CGFloat = value.cgRectValue.origin.y - registerView.btnRegister.frame.height - 5 - abs(popupController?.containerView.frame.origin.y ?? 0) - registerView.frame.minY
            
            paddingButton = min(registerView.btnTerm.frame.maxY, paddingButton)
            
            registerView.animationWithKeyboard(isShow: true, y: paddingButton)
        }

        


    }
    
    /**
     Sự kiện bàn phím ẩn đi
     */
    @objc func keyboardWillHide(_ sender: Foundation.Notification) {
        
        Log.message(.info, message: "keyboardWillHide")
        loginView.animationWithKeyboard(isShow: false, y: 0)
        registerView.animationWithKeyboard(isShow: false, y: 0)
    }
    
    
    
    
    @objc func changeLanguage() {
        loadLanguage()
    }
    
    @objc func orientationChanged(_ sender: Notification) {
        
        let device = UIDevice.current
        
        switch device.orientation {
        case .faceUp:
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                contentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 340))
                
            } else {
                contentSizeInPopup = CGSize(width: 336, height: 350)
            }
            
            
        case .portrait, .portraitUpsideDown:
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                contentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 340))
                
            } else {
                contentSizeInPopup = CGSize(width: 336, height: 350)
            }
            
        case .landscapeLeft, .landscapeRight:
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                landscapeContentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 320))
                
            } else {
                landscapeContentSizeInPopup = CGSize(width: 336, height: 350)
            }
            
            
        default:
            break
        }
        
        updateFrame()
        
        
        if UIScreen.main.bounds.width > UIScreen.main.bounds.height {
            // Landscape
            nextKeybroadView.width = landscapeContentSizeInPopup.width
            doneKeybroadView.width = landscapeContentSizeInPopup.width
            
        } else {
            nextKeybroadView.width = contentSizeInPopup.width
            doneKeybroadView.width = contentSizeInPopup.width
        }
        
   
    }
    
    
    @objc func segmentAction(_ sender: UISegmentedControl) {
        
        indexSegment = sender.selectedSegmentIndex
        
        scrollViewMoveToPage(sender.selectedSegmentIndex)
        
//        switch sender.selectedSegmentIndex {
//        case 0:
//
//            loginView.isHidden = false
//            registerView.isHidden = true
//
//
//
//        default:
//            loginView.isHidden = true
//            registerView.isHidden = false
//        }
        
        view.endEditing(true)
        print("Selected Segment: \(sender.selectedSegmentIndex)")
        
    }
    
    @objc func btnCloseClick(_ sender: UIButton) {
        popupController?.dismiss()
    }
    
    @objc func btnForgotClick(_ sender: UIButton) {
        
//        Utility.sendFirebaseEventWith("select_content", parameters: ["item_id" : "my_item_id"])
        
//        print("xxxxxx")
//
//
//        Analytics.setUserProperty("my_item_id", forName: AnalyticsParameterItemID)
//        Utility.sendFirebaseEventWith(AnalyticsEventSelectContent, parameters: [
//            AnalyticsParameterItemID: "my_item_id"
//        ])
        
        
        
        
        
        Utility.logEventDebug(.forgotPassword)

        let forgotPasswordVC = CheckAccountViewController()
        popupController?.push(forgotPasswordVC, animated: true)
    }
    
    
    @objc func btnPlayNowClick(_ sender: UIButton) {
        
        sender.isUserInteractionEnabled = false
        
        
 
        HUD.showHUD(onView: self.view)
        UtilitieWebService.shareSingleton()?.dnnhanh(completion: { (results, error, resultCode) in
            
            //            DVLog(@"status code = %d",resultCode);
            
            
            Utility.logEventDebug(.loginPlayNow)
            
            Log.message(.info, message: "status code =  \(resultCode)")
            
            HUD.dismissHUD()
            
            sender.isUserInteractionEnabled = true
            guard resultCode == ResultCodeSuccess else {
                
                
                Utility.sendEventToFirebaseWith(EVEN_START_PLAYNOW_ERROR, parameters: nil)
                Utility.showAlertFrom(resultCode, results: results, viewController: self)
                return
            }
            
        
            
            guard let dicDataDNN = (results as? [String : Any])?[KEY_DATA] as? [String : Any] else { return }
            
            let userDefault = UserDefaults.standard
            userDefault.set(dicDataDNN, forKey: KEY_DATA_USER_FAST_LOGIN)
            userDefault.synchronize()
            
            Utility.updateAccountInfoWith(results, loginMethod: .byFastTk)
            UnitAnalyticHelper.analyticLoginEvent(MethodDn.byFastTk.key)
            Utility.trackEventCompleteRegisterOldOrNew("guest_account", results: results)
            
  
            self.perform(#selector(self.showWelcome), with: nil, afterDelay: 1.5)
            self.popupController?.dismiss()
            
        })
    }
    
    
    
    @objc func btnLoginClicked(_ sender: UIButton) {
        login()
    }
    
    @objc func loginApple(_ sender: UIButton) {
        

        guard #available(iOS 13.0, *) else {
            Utility.showAlertWithMessage(LanguageSetting.get("apple_signIn_not_available", alter: nil), viewController: self)
            return
        }
        
        
        
        
//        NotificationCenter.default.post(name: NSNotification.Name(rawValue: ACTION_LOGIN_APPLE_SUCCESS), object: self, userInfo: nil)
        
        ///======================
        
        
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        
        let controller = ASAuthorizationController(authorizationRequests: [request])
        controller.delegate = self
        controller.presentationContextProvider = self
        controller.performRequests()
        
        
    }
    
    @objc func loginFacebook(_ sender: UIButton) {
        
        /*
        let login = LoginManager()
        AccessToken.refreshCurrentAccessToken({ (connection, result, error) in
            
        })
        
        login.logOut()
        login.logIn(permissions: ["public_profile", "email"],
                    from: self,
                    handler: { (result, error) in
                        
                        if let _ = error {
                            //                                                        DVLog(@"Process error");
                            
                            Log.message(.error, message: "Process error")
                            NSLog("Process error")
                            
                            
                            self.facebookLoginFailed(true)
                            
                        } else if let result = result, result.isCancelled {
                            
                            self.facebookLoginFailed(false)
                            
                        } else {
                            //                                                        DVLog(@"Process login facebook success1%@",result);
//                            NSLog("Process login facebook success1\(String(describing: result))")
                            
                            Log.message(.info, message: "Process login facebook success1\(String(describing: result))")
                            
                            guard let _ = AccessToken.current else { self.facebookLoginFailed(false); return }
                            //                                DVLog(@"Token is available : %@",[[FBSDKAccessToken currentAccessToken]tokenString]);
                            
                            
                            Log.message(.info, message: "Token is available : \(String(describing: AccessToken.current?.tokenString ?? "XXX"))")
                            
//                            NSLog("Token is available : %@", AccessToken.current?.tokenString ?? "XXX")
                            
                            let parameters: [String : Any] = ["fields" : "id,name,email,first_name,last_name,picture.type(large),gender,token_for_business"]
                            
                            GraphRequest(graphPath: "me", parameters: parameters)
                                .start(completionHandler: {  (connectionFB, resultFB, errorFB) in
                                    
                                    guard let resultData = resultFB as? [String : Any], errorFB == nil else {
                                        
                                        CommonUtils.showAlertMsg(self,
                                                                 title: LanguageSetting.get("TitleNotification", alter: ""),
                                                                 message: String(format: "Login error (code: %@)", errorFB.debugDescription))
                                        
                                        Utility.sendFirebaseEventWith(EVEN_SIGNIN_FACEBOOK_ERROR, parameters: nil)
                                        
                                        return
                                    }
                                    
//                                    NSLog("result login facebook = \(String(describing: resultFB))")
                                    
                                    Log.message(.info, message: "result login facebook = \(String(describing: resultFB))")
                                    
                                    //                                DVLog(@"result login facebook = %@",result);
                                    //                                let fbPhotoUrl = ((resultData["picture"] as? [String : Any] ?? [:])["data"] as? [String : Any] ?? [:])["url"] as? String ?? ""
                                    //                                DVLog(@"%@",fbPhotoUrl);
                                    
                                    let facebookID = resultData[KEY_FACEBOOK_ID] as? String ?? ""
                                    let facebookName = resultData[KEY_FACEBOOK_NAME] as? String ?? ""
                                    let facebookMail = resultData[KEY_FACEBOOK_MAIL] as? String ?? ""
                                    let facebookToken = resultData[KEY_FACEBOOK_TOKEN] as? String ?? ""
                                    
                                    //TODO: Facebook không trả về giới tính
                                    let facebookGender = resultData[KEY_FACEBOOK_MAIL] as? Int ?? 0
                                    
                                    MBProgressHUD.showAdded(to: self.view, animated: true)
                                    
                                    UtilitieWebService
                                        .shareSingleton()?
                                        .authenFacebook(withFacebookID: facebookID,
                                                        fEmail: facebookMail,
                                                        facebookName: facebookName,
                                                        facebookGender: Int32(facebookGender),
                                                        facebookToken: facebookToken,
                                                        completion: { (results, error, resultCode) in
                                                            
                                                            MBProgressHUD.hide(for: self.view, animated: false)
                                                            
                                                            guard resultCode == ResultCodeSuccess else {
                                                                
                                                                Utility.sendFirebaseEventWith(EVEN_SIGNIN_NORMAL_ERROR, parameters: nil)
                                                                Utility.showAlertFrom(resultCode, results: results, viewController: self)
                                                                
                                                                return
                                                            }
                                                            

                                                            UnitAnalyticHelper.analyticLoginEvent(MethodDn.byTkFacebook.key)
                                                            
                                                            
                                                            Utility.trackEventCompleteRegisterOldOrNew(MethodDn.byTkFacebook.key, results: results)
                                                            Utility.updateAccountInfoWith(results, loginMethod: .byTkFacebook)
                                                            
                                                            
                                                            self.perform(#selector(self.showWelcome), with: nil, afterDelay: 1.5)
                                                            self.popupController?.dismiss()
                                        })
                                })
                        }
        })
 */
    }
    
    
    @objc func loginGoogle(_ sender: UIButton) {
        
        guard let path = Bundle.main.path(forResource: "GoogleService-Info", ofType: "plist") else { return }
        guard let dict = NSDictionary(contentsOfFile: path) else { return }
        guard let clientId = dict.object(forKey: "CLIENT_ID") as? String else { return }
        
        // Setup the Sign-In instance.
        GIDSignIn.sharedInstance()?.clientID = clientId
        GIDSignIn.sharedInstance()?.signIn()
    }
    
    
    
    @objc func registerClick(_ sender: UIButton) {
        
        if registerView.btnSwitchAccount.isSelected {
            registerAcByMobile()
        } else {
            registerAccount()
        }
    }
    
    @objc func nextViewCloseClick(_ sender: UIButton) {
        
        view.endEditing(true)
     
    }
    
    
    @objc func nextViewNextClick(_ sender: UIButton) {

        switch sender.tag {
//        case 1:  /// Username
//
//            loginView.tfPassword.becomeFirstResponder()
        
        default:
            view.endEditing(true)
        }
        
        
    }
    
}
