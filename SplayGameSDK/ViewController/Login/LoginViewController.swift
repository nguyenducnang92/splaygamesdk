//
//  LoginViewController.swift
//  SplayGameSDK
//
//  Created by Nguyen Nang on 6/30/20.
//  Copyright © 2020 splay. All rights reserved.
//

import UIKit
import GoogleSignIn
import CleanroomLogger

class LoginViewController: CustomViewController {
    

    //-----------------------
    // MARK: - VAR
    //-----------------------
    var isShowGG: Bool = false
    var isShowFB: Bool = false
    var isShowApple: Bool = false
    var isShowFastLogin: Bool = true
    var isSplay_account: Bool = true
    
    var arrCountryName: [String] = ["+84","+14","+8","+9","+1"]
    
    var indexSegment: Int = 0
    var codeCoutry: String = "+84"
    var contentAlert: String = ""
    
    var dicDataRegister: [String : Any]?
    
    
    //-----------------------
    // MARK: - VIEW
    //-----------------------
    
    
    var segmentControl: HMSegmentedControl!
//    var segmentControl: UISegmentedControl!
    
    var bgView: UIView!
    var selectedTextField: UITextField?
    var scrollView: UIScrollView!
    var loginView: LoginContentView!
    var registerView: RegisterContentView!
    
    
    var nextKeybroadView: NextKeybroadAccessoryView!
    var doneKeybroadView: NextKeybroadAccessoryView!
    
    //-----------------------
    // MARK: - LIFE CYCLE
    //-----------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        Log.message(.info, message: "LoginViewController  viewDidLoad")
        
        if UI_USER_INTERFACE_IDIOM() == .phone {
            
            let rect = UIScreen.main.bounds
            let maxWidth = min(rect.size.width - 20, rect.size.height)
            
            if UIScreen.main.bounds.width > UIScreen.main.bounds.height {
                // Landscape
                landscapeContentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 320))
            } else {
                contentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 340))
            }
            
        } else {
            if UIScreen.main.bounds.width > UIScreen.main.bounds.height {
                landscapeContentSizeInPopup = CGSize(width: 336, height: 350)
            } else {
                contentSizeInPopup = CGSize(width: 336, height: 350)
            }
        }
        
        Log.message(.info, message: "LoginViewController  setupAllSubviews")
        
        setupAllSubviews()
        
        Log.message(.info, message: "LoginViewController  FINISHED SETUP VIEW")

        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        updateFrame()
        
        
        Log.message(.info, message: "LoginViewController  FINISHED UPDATE FRAME")

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        GIDSignIn.sharedInstance()?.delegate = self
        GIDSignIn.sharedInstance()?.presentingViewController = self

        
        Log.message(.info, message: "LoginViewController  viewWillAppear")

    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        Log.message(.info, message: "LoginViewController  viewWillLayoutSubviews")

        
        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
    }
    
    func updateFrame() {
        
        btnClose.frame = CGRect(x: view.frame.width - 40,
                                y: 0,
                                width: 40,
                                height: 40)
 
        segmentControl.frame = CGRect(x: view.frame.width / 5,
                                      y: 0,
                                      width: view.frame.width * 3 / 5,
                                      height: 40)
        
        seperator.frame = CGRect(x: 0,
                                 y: segmentControl.frame.height - onePixel(),
                                 width: view.frame.width,
                                 height: onePixel())
        
        bgView.frame = CGRect(x: 0,
                              y: 0,
                              width: view.frame.width,
                              height: seperator.frame.maxY)
        
        helpView.frame = CGRect(x: 0,
                                y: view.frame.height - 40,
                                width: view.frame.width,
                                height: 40)
        
        scrollView.frame = CGRect(x: 0,
                                  y: segmentControl.frame.maxY,
                                  width: view.frame.width,
                                  height: helpView.frame.minY - segmentControl.frame.maxY)
        
        
        scrollView.contentSize = CGSize(width: view.frame.width * 2,
                                        height: scrollView.frame.height)
        
    
        
        loginView.frame = CGRect(x: 0,
                                 y: 0,
                                 width: scrollView.frame.width,
                                 height: scrollView.frame.height)
        
        registerView.frame = CGRect(x: scrollView.frame.width,
                                 y: 0,
                                 width: scrollView.frame.width,
                                 height: scrollView.frame.height)
        
    

        scrollViewMoveToPage(indexSegment)
    

    }
}
