//
//  LoginViewController+Delegate.swift
//  SplayGameSDK
//
//  Created by Nguyen Nang on 7/6/20.
//  Copyright © 2020 splay. All rights reserved.
//

import GoogleSignIn
import Firebase
import FBSDKCoreKit
import FBSDKLoginKit
import FBSDKShareKit
import CleanroomLogger
import AuthenticationServices

//---------------------------
// MARK: - TABLE DATASOURCE
//---------------------------

extension LoginViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCountryName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListCountryTableViewCell", for: indexPath) as! ListCountryTableViewCell
        
        configuageCell(cell, indexPath: indexPath)
        return cell
    }
    
    func configuageCell(_ cell: ListCountryTableViewCell, indexPath: IndexPath) {
        
        
        cell.textLabel?.text = arrCountryName[indexPath.row]
        
        cell.textLabel?.textColor = .darkGray
        cell.textLabel?.font = UIFont.systemFont(ofSize: 12)
        
        if arrCountryName[indexPath.row] == codeCoutry {
            cell.contentView.backgroundColor = UIColor.gray.alpha(0.1)
        } else {
            cell.contentView.backgroundColor = .white
        }
    }
}

//---------------------------
// MARK: - TABLE DELEGATE
//---------------------------

extension LoginViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        print("indexPath XXX: \(indexPath)")
        
        codeCoutry = arrCountryName[indexPath.row]
        registerView.btnSelectCountry.setTitle(codeCoutry, for: .normal)
        registerView.btnSelectCountry.isSelected = false
        registerView.tableView.isHidden = true
        registerView.tableView.reloadData()
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 25
    }
}



//------------------------------
// MARK: - TEXT FIELD DELEGATE
//------------------------------

extension LoginViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        

        registerView.btnSelectCountry.isSelected = false
        registerView.tableView.isHidden = true
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        selectedTextField = textField
        
        registerView.btnSelectCountry.isSelected = false
        registerView.tableView.isHidden = true
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.isKind(of: DLTextField.self) {
            
            if let nextTextField = (textField as? DLTextField)?.next {
                
                
                DispatchQueue.main.async(execute: {
                    nextTextField.becomeFirstResponder()
                })

                
            } else {
                
                textField.resignFirstResponder()
                
                switch textField.tag {
                case 1:
                    login()
                
                case 2:
                    //Xử lí đăng ký
                    if registerView.btnSwitchAccount.isSelected {
                        registerAcByMobile()
                    } else {
                        registerAccount()
                    }
                
                default:
                    break
                }
            
            }
        }
        return true
    }
    
}



//------------------------------
// MARK: - HELP VIEW DELEGATE
//------------------------------

extension LoginViewController: CustomHelpViewDelegate {
    
    func showViewSupport() {
        
    }
    
    func showSettingLanguage() {
        
    }
}



//------------------------------
// MARK: - FACEBOOK DELEGATE
//------------------------------

extension LoginViewController: LoginButtonDelegate {
    
    func loginButtonWillLogin(_ loginButton: FBLoginButton) -> Bool {
        
        //        let login = LoginManager()
        //        AccessToken.refreshCurrentAccessToken({ (connection, result, error) in
        //
        //        })
        //
        //        login.logOut()
        
        return true
    }
    
    
    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
        
    }
    
    
    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
        
        Log.message(.info, message: "-------------------------")
        Log.message(.info, message: "|     LOGIN FACEBOOK    |")
        Log.message(.info, message: "-------------------------")
        
        guard let result = result, error == nil else {
            Log.message(.error, message: String(format: "Process error: %@", error.debugDescription))
            facebookLoginFailed(true)
            return
        }
        
        
        guard !result.isCancelled else {
            Log.message(.error, message: "Facebook isCancelled")
            facebookLoginFailed(false)
            return
        }
        
        guard let token = result.token else {
            Log.message(.error, message: "Facebook token empty")
            facebookLoginFailed(false)
            return
        }
        
        AccessToken.current = token
        

        Log.message(.info, message: "Process login facebook success: \(result)")
        Log.message(.info, message: "Token is available : \(String(describing: token))")
        
        
        
        let parameters: [String : Any] = ["fields" : "id,name,email,first_name,last_name,picture.type(large),token_for_business"]
         
        GraphRequest(graphPath: "me", parameters: parameters).start { (connectionFB, resultFB, errorFB) in
            
            guard let resultData = resultFB as? [String : Any], errorFB == nil else {

                Utility.showAlertWithMessage(String(format: "Login error (code: %@)", errorFB.debugDescription),
                                             viewController: self)
                
                Log.message(.error, message: String(format: "GraphRequest error: %@", error.debugDescription))
                
                Utility.sendEventToFirebaseWith(EVEN_SIGNIN_FACEBOOK_ERROR, parameters: nil)
                return
            }


            Log.message(.info, message: "result login facebook = \(String(describing: resultData))")
            
            
            //                                let fbPhotoUrl = ((resultData["picture"] as? [String : Any] ?? [:])["data"] as? [String : Any] ?? [:])["url"] as? String ?? ""
            //                                DVLog(@"%@",fbPhotoUrl);
            
            let facebookID = resultData[KEY_FACEBOOK_ID] as? String ?? ""
            let facebookName = resultData[KEY_FACEBOOK_NAME] as? String ?? ""
            let facebookMail = resultData[KEY_FACEBOOK_MAIL] as? String ?? ""
            let facebookToken = resultData[KEY_FACEBOOK_TOKEN] as? String ?? ""
            
            //TODO: Facebook không trả về giới tính
            let facebookGender = resultData[KEY_FACEBOOK_GENDER] as? Int ?? 0
            
            HUD.showHUD(onView: self.view)
            
            UtilitieWebService
                .shareSingleton()?
                .authenFacebook(withFacebookID: facebookID,
                                fEmail: facebookMail,
                                facebookName: facebookName,
                                facebookGender: facebookGender,
                                facebookToken: facebookToken) { (results, error, resultCode) in
                    
                    Log.message(.info, message: "authenFacebook results: \(String(describing: results)) --- \(resultCode)")
                    
                    HUD.dismissHUD()
                    
                    Utility.logEventDebug(.loginFacebook)
                    
                    guard resultCode == ResultCodeSuccess else {
                        
                        Log.message(.error, message: "authenFacebook Failed: \(String(describing: error))")
                        
                        Utility.sendEventToFirebaseWith(EVEN_SIGNIN_NORMAL_ERROR, parameters: nil)
                        Utility.showAlertFrom(resultCode, results: results, viewController: self)
                        
                        return
                    }
                    
                    Utility.updateAccountInfoWith(results, loginMethod: .byTkFacebook)
                    UnitAnalyticHelper.analyticLoginEvent(MethodDn.byTkFacebook.key)
                    Utility.trackEventCompleteRegisterOldOrNew(MethodDn.byTkFacebook.key, results: results)
                    
                    
                    
                    self.perform(#selector(self.showWelcome), with: nil, afterDelay: 1.5)
                    self.popupController?.dismiss()
                }
        }
    }
}



//------------------------------
// MARK: - REGISTER DELEGATE
//------------------------------

extension LoginViewController: RegisterContentViewDelegate {
    
    func pushViewController(_ controller: UIViewController) {
        popupController?.push(controller, animated: true)
    }
}


//------------------------------
// MARK: - GG SIGN IN DELEGATE
//------------------------------

extension LoginViewController: GIDSignInDelegate {
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        Log.message(.error, message: String(format: "didDisconnectWith %@", error.localizedDescription))
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        // Perform any operations on signed in user here.
        Log.message(.info, message: "didSignInForUser \(String(describing: user))")

        guard error == nil else {
            Log.message(.error, message: error.localizedDescription)
            return
        }
        
        Log.message(.info, message: String(format: "_____userID = %@", user.userID))
        
        HUD.showHUD(onView: self.view)
        UtilitieWebService
            .shareSingleton()?
            .authenGoogle(withGoogleID: user.userID,
                          gEmail: user.profile.email,
                          googleName: user.profile.name,
                          gGender: 0) { (results, errorGG, resultCode) in
                
                
                Utility.logEventDebug(.loginGoogle)
                
       
                HUD.dismissHUD()
                
                guard resultCode == ResultCodeSuccess else {
                    
                    Utility.sendEventToFirebaseWith(EVEN_SIGNIN_NORMAL_ERROR, parameters: nil)
                    
                    Utility.showAlertFrom(resultCode, results: results, viewController: self, handler: { _ in
                        GIDSignIn.sharedInstance()?.signOut()
                    })
                    return
                }
                

                Utility.updateAccountInfoWith(results, loginMethod: .byTkGoogle)
                UnitAnalyticHelper.analyticLoginEvent(MethodDn.byTkGoogle.key)
                Utility.trackEventCompleteRegisterOldOrNew(MethodDn.byTkGoogle.key, results: results)
                
                
                self.perform(#selector(self.showWelcome), with: nil, afterDelay: 1.5)
                self.popupController?.dismiss()
                
            }
    }
}



//------------------------------
// MARK: - APPLE SIGN IN DELEGATE
//------------------------------

extension LoginViewController: ASAuthorizationControllerDelegate {
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        

        
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            
            
            guard let appleIDToken = appleIDCredential.identityToken else {
                print("Unable to fetch identity token")
                return
            }
            
            guard let idTokenString = String(data: appleIDToken, encoding: .utf8) else {
                print("Unable to serialize token string from data: \(appleIDToken.debugDescription)")
                return
            }
            
            let userIdentifier = appleIDCredential.user
            
            UserDefaults.standard.setValue(userIdentifier, forKey: "setCurrentIdentifier")
            
            
            let fullName = appleIDCredential.fullName?.givenName ?? ""
            let email = appleIDCredential.email ?? ""
            
            print(idTokenString)
            print(userIdentifier)
            print(fullName)
            print(email)
            
            
            HUD.showHUD(onView: self.view)
            
            SplayGameSDKManager
                .sharedManager()
                .authenAppleWithAppleID(userIdentifier,
                                        email: email,
                                        fullName: fullName,
                                        CompletionHandle: { (suceess, statusResult, message)  in
                                            
                                            print("Apple Login: \(statusResult)")
                                            
                                        })
            
        } else if let passwordCredential = authorization.credential as? ASPasswordCredential {
            
            let user = passwordCredential.user
            let password = passwordCredential.password
            
            print("user: \(user) | password: \(password)")
            
            HUD.showHUD(onView: self.view)
            
            SplayGameSDKManager.sharedManager().authenAppleWithAppleID(user, email: "", fullName: "", CompletionHandle: { (suceess, statusResult, message)  in
                
                print("Apple Login: \(statusResult)")
                
            })
            
        } else {

            print("Login Apple Error")
        }
    }
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        
        print("error: \(error)")
        
        // Handle error.
        guard let error = error as? ASAuthorizationError else { return }
    
        switch error.code {
        case .canceled:
            print("Canceled")
        case .unknown:
            print("Unknown")
        case .invalidResponse:
            print("Invalid Respone")
        case .notHandled:
            print("Not handled")
        case .failed:
            print("Failed")
        @unknown default:
            print("Default")
        }
        print("controller requests: \(controller.authorizationRequests)")
    }
}


extension LoginViewController: ASAuthorizationControllerPresentationContextProviding {
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window ?? UIApplication.shared.keyWindow!
    }
}
