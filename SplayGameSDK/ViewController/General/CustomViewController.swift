//
//  CustomViewController.swift
//  SplayGameSDK
//
//  Created by Nguyen Nang on 7/1/20.
//  Copyright © 2020 splay. All rights reserved.
//

import UIKit

class CustomViewController: UIViewController {
    
    
    /// VIEW
    
    lazy var labelTitle: UILabel = {
        let labelTitle = setupLabelCustom(textColor: UIColor(red: 255.0/255, green: 162.0/255, blue: 37.0/255, alpha: 1.0),
                                    font: UIFont.boldSystemFont(ofSize: 13),
                                    alignment: .center)
        return labelTitle
    }()
    
    lazy var btnClose: UIButton = {
        let button = setupButtonCustom(image: UtilitieWebService.shareSingleton()?.getImageFromStringBase(dn_bg_btn_close))
        button.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        return button
    }()
    
    lazy var seperator: UIView = {
        let view = setupViewCustom(UIColor.Misc.seperatorColor())
        return view
    }()
    

    lazy var helpView: CustomHelpView = {
        let view = CustomHelpView()
        view.setHiddenLanguage(true)
        

        var language = UserDefaults.standard.object(forKey: KEY_SAVE_LANGUAGE_SETTING) as? String ?? ""
        
        if language.count == 0 {
            language = NSLocale.preferredLanguages[0]
        }
        
   
        
        if ["vi", "vi-VN"].contains(language) {
            
            view.btnChangeLanguage.setTitle(LanguageValue.vietnamese.title, for: .normal)
            view.btnChangeLanguage.setImage(LanguageValue.vietnamese.icon, for: .normal)
            
        } else {
            view.btnChangeLanguage.setTitle(LanguageValue.english.title, for: .normal)
            view.btnChangeLanguage.setImage(LanguageValue.english.icon, for: .normal)
        }

        return view
    }()
    
    
    //--------------------------------
    // MARK: - LIFE CYCLE
    //--------------------------------

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.contentSizeInPopup = CGSize(width: 300, height: 200)
        self.landscapeContentSizeInPopup = UIScreen.main.bounds.size
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        self.contentSizeInPopup = CGSize(width: 300, height: 200)
        self.landscapeContentSizeInPopup = UIScreen.main.bounds.size
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    

}


//--------------------------------
// MARK: - PRIVATE METHOD
//--------------------------------

extension CustomViewController {
    
     public func showAlertWithMessage(_ message: String) {
        
        let alert = UIAlertController(title: LanguageSetting.get("TitleNotification", alter: ""),
                                      message: message,
                                      preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: LanguageSetting.get("TitleButtonOK", alter: ""),
                                      style: .default,
                                      handler: nil))
        
        
        present(alert, animated: true, completion: nil)
        
    }
}

//--------------------------------
// MARK: - SETUP VIEW
//--------------------------------

extension CustomViewController {
    
    
    func setupLabelCustom(_ title: String = "", textColor: UIColor = UIColor.black,
                    font: UIFont = UIFont.systemFont(ofSize: 12),
                    bgColor: UIColor = .clear,
                    alignment: NSTextAlignment = .left) -> UILabel {
        
        let label = UILabel()
        label.text = title
        label.textColor = textColor
        label.backgroundColor = bgColor
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = alignment
        label.font = font
        return label
    }
    
    func setupButtonCustom(title: String? = nil,
                          titleSelected: String? = nil,
                          image: UIImage? = nil,
                          imageSelected: UIImage? = nil,
                          titleColor: UIColor = .white) -> UIButton {
        let button = UIButton()
        button.setTitleColor( titleColor, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 11)
        button.setTitle(title, for: .normal)
        button.setTitle(titleSelected, for: .selected)
        button.setImage(image, for: .normal)
        button.setImage(imageSelected, for: .selected)
        button.clipsToBounds = true
        button.imageView?.contentMode = .scaleAspectFit
        return button
    }
    
    func setupViewCustom(_ bgColor: UIColor = UIColor.Misc.seperatorColor()) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        view.clipsToBounds = true
        return view
    }
    
}
