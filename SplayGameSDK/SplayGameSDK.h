//
//  SplayGameSDK.h
//  SplayGameSDK
//
//  Created by Nguyen Nang on 6/30/20.
//  Copyright © 2020 splay. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>



//#import "ShowInforFastAccView.h"
#import "UITextField+Additions.h"
#import "UITextField+Extended.h"
#import "AFHTTPSessionManager.h"
#import "AFNetworking.h"
#import "AFNetworkReachabilityManager.h"
#import "AFSecurityPolicy.h"
#import "AFURLRequestSerialization.h"
#import "AFURLResponseSerialization.h"
#import "AFURLSessionManager.h"
#import "Reachability.h"

#import "DebugUtils.h"
#import "IAPHelper.h"
#import "IAPShare.h"
#import "NSString+Base64.h"
#import "SFHFKeychainUtils.h"
#import "MBProgressHUD.h"
#import "NSData+MD5.h"
#import "NSString+MD5.h"
#import "PopoverAction.h"
#import "PopoverView.h"
#import "PopoverViewCell.h"


#import "Base64.h"
#import "DLRadioButton.h"
#import "FDActionSheet.h"
#import "HMSegmentedControl.h"
#import "FUISegmentedControl.h"
//#import "UIImage+FlatUI.h"
//#import "UIColor+FlatUI.h"
//#import "UIFont+FlatUI.h"
#import "Image.h"
#import "ImageDownload.h"
#import "NIDropDown.h"
#import "RadioButton.h"
#import "TWMessageBarManager.h"

#import "UITextField+Additions.h"
#import "UIView+DLAdditions.h"
#import "UIView+draggable.h"
//#import "ApiTrackingEventService.h"
#import "LanguageSetting.h"
//#import "UnitAnalyticHelper.h"
//#import "UtilitieWebService.h"
#import "SDKIOSLiteMacro.h"
#import "SplayGame-Bridging-Header.h"



//! Project version number for SplayGameSDK.
FOUNDATION_EXPORT double SplayGameSDKVersionNumber;

//! Project version string for SplayGameSDK.
FOUNDATION_EXPORT const unsigned char SplayGameSDKVersionString[];





// In this header, you should import all the public headers of your framework using statements like #import <SplayGameSDK/PublicHeader.h>

//#import "SplayGameSDK/SplayGameSDKManager.h"
//#import "SplayGameSDK/Account.h"



