//
//  CustomWelcomeView.h
//  SDKLite
//
//  Created by King Club on 4/24/18.
//  Copyright © 2018 DucViet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TWMessageBarManager.h"


@class CustomWelcomeView;

@protocol CustomWelcomeViewDelegate <NSObject>
- (NSObject<TWMessageBarStyleSheet> *)styleSheetForMessageView:(CustomWelcomeView*)messageView;

@end
@interface CustomWelcomeView : UIView
{
    
     UILabel *versionLabel;
     UIImageView *_icon_user;
     UILabel *_lbTitle;
}
@property (nonatomic, copy) NSString *titleString;
@property (nonatomic, copy) NSString *descriptionString;

@property (nonatomic, assign) TWMessageBarMessageType messgeType;

@property (nonatomic, assign) BOOL hasCallback;
@property (nonatomic, strong) NSArray *callbacks;

@property (nonatomic, assign) UIStatusBarStyle statusBarStyle;
@property (nonatomic, assign) BOOL statusBarHidden;

@property (nonatomic, assign, getter = isHit) BOOL hit;

@property (nonatomic, assign) CGFloat duration;

@property (nonatomic,weak) id <CustomWelcomeViewDelegate> delegate;
//- (id)initWithFrame:(CGRect)frame title:(NSString*)title description:(NSString*)description type:(TWMessageBarMessageType)type;
- (void)setInfomationWelComeWithTitle:(NSString*)title description:(NSString*)description;

@end
