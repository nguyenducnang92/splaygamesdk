//
//  CustomWelcomeView.m
//  SDKLite
//
//  Created by King Club on 4/24/18.
//  Copyright © 2018 DucViet. All rights reserved.
//

#import "CustomWelcomeView.h"
#import "TWMessageBarManager.h"
#import "SDKIOSLiteMacro.h"
//#import "UtilitieWebService.h"
#import "LanguageSetting.h"
#import "DebugUtils.h"
@implementation CustomWelcomeView
- (void)awakeFromNib
{
    [super awakeFromNib];
//    _lbTitle.text = self.titleString;
    _icon_user.image = [self getImageFromStringBase:img_icon_user_login];
}


- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setupAllSubviews];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        

        [self setupAllSubviews];
    }
    return self;
}


- (void)layoutSubviews {
    [super layoutSubviews];
    
    _icon_user.frame = CGRectMake(0, 0, self.bounds.size.height, self.bounds.size.height);
    _lbTitle.frame = CGRectMake(self.bounds.size.height + 8,
                                10,
                                self.bounds.size.width - self.bounds.size.height - 16,
                                self.bounds.size.height - 10 * 2);
    
    versionLabel.frame = CGRectMake(0,
                                    self.bounds.size.height - 16,
                                    self.bounds.size.width - 8,
                                    13);
    
}

- (void)setupAllSubviews {
    
     
    
    versionLabel = [[UILabel alloc] initWithFrame: CGRectZero];
    versionLabel.font = [UIFont systemFontOfSize: 8];
    versionLabel.textColor = [UIColor darkGrayColor];
    versionLabel.textAlignment = NSTextAlignmentRight;
     
    
    _lbTitle = [[UILabel alloc] initWithFrame: CGRectZero];
    _lbTitle.font = [UIFont systemFontOfSize:15];
    _lbTitle.textColor = [UIColor darkGrayColor];
    _lbTitle.textAlignment = NSTextAlignmentCenter;
    
    _icon_user = [[UIImageView alloc] init];
    _icon_user.image = [self getImageFromStringBase:img_icon_user_login];
    _icon_user.contentMode = UIViewContentModeScaleToFill;
    
    [self addSubview:_icon_user];
    [self addSubview:versionLabel];
    [self addSubview:_lbTitle];
    self.backgroundColor = [UIColor whiteColor];
}

- (void)setInfomationWelComeWithTitle:(NSString *)title description:(NSString *)description
{
    self.layer.cornerRadius = self.frame.size.height/2;
    self.layer.masksToBounds = YES;
    self.layer.borderWidth = 0.5;
    self.layer.borderColor = [[UIColor colorWithRed:13./255. green:183./255 blue:212./255. alpha:1] CGColor];

    _lbTitle.text = [NSString stringWithFormat:@"%@ %@", [LanguageSetting get:@"text_welcome" alter:nil], description];
    versionLabel.text = [[NSBundle bundleWithIdentifier:@"splay.SDK"].infoDictionary objectForKey:@"CFBundleShortVersionString"];
    _messgeType = TWMessageBarMessageTypeSuccess;
}

-(UIImage*)getImageFromStringBase:(NSString*)stringBase
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",stringBase]];
    NSData *decodedImageData = [NSData dataWithContentsOfURL:url];
    UIImage *image = [UIImage imageWithData:decodedImageData];
    return image;
}



//- (id)initWithFrame:(CGRect)frame title:(NSString *)title description:(NSString *)description type:(TWMessageBarMessageType)type
//{
//
//    self = [super initWithFrame:frame];
//    if (self) {
//
//        [self setupAllSubviews];
//
//        self.backgroundColor = [UIColor clearColor];
//        self.clipsToBounds = NO;
//        self.userInteractionEnabled = YES;
//        _titleString = title;
//        _descriptionString = description;
//        _messgeType = type;
//        _hasCallback = NO;
//        _hit = NO;
//    }
//    return self;
//}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
