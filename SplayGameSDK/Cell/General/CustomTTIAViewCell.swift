//
//  CustomTTIAViewCell.swift
//  SplayGameSDK
//
//  Created by Nguyen Nang on 8/7/20.
//  Copyright © 2020 splay. All rights reserved.
//

import UIKit

class CustomTTIAViewCell: UITableViewCell {
    
    var img_icon_inapp: UIImageView!
    var lbvnd: UILabel!
    var lbknb: UILabel!
    var img_icon_items_inapp: UIImageView!
    
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupAllSubviews()
    }
    
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        setupAllSubviews()
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.frame = bounds
        
        img_icon_inapp.frame = contentView.bounds
        
        let heightImage = contentView.frame.height * 39 / 54
        
        img_icon_items_inapp.frame = CGRect(x: 8,
                                            y: (contentView.frame.height - heightImage) / 2,
                                            width: heightImage,
                                            height: heightImage)
        
        lbvnd.frame = CGRect(x: img_icon_items_inapp.frame.maxX + 4,
                             y: 0,
                             width: contentView.frame.width / 2 - (img_icon_items_inapp.frame.maxX + 4) - 4,
                             height: contentView.frame.height)
        
        lbknb.frame = CGRect(x: contentView.frame.width / 2 + 4,
                             y: 0,
                             width: contentView.frame.width / 2 - 12,
                             height: contentView.frame.height)
        
        
    }
    
}


extension CustomTTIAViewCell {
    
    func setupAllSubviews() {
        
        contentView.backgroundColor = .clear
        contentView.clipsToBounds = true
        
        img_icon_inapp = setupImageView()
        img_icon_items_inapp = setupImageView()
        lbvnd = setupLabel(textColor: UIColor.Text.grayNormalColor(),
                           font: UIFont.systemFont(ofSize: 15))
        
        lbknb = setupLabel(textColor: UIColor.Navigation.mainColor(),
                           font: UIFont.systemFont(ofSize: 15),
                           alignment: .center)
        
        contentView.addSubview(img_icon_inapp)
        contentView.addSubview(img_icon_items_inapp)
        contentView.addSubview(lbvnd)
        contentView.addSubview(lbknb)
        
        
    }
    
    func setupImageView(_ contentMod: UIView.ContentMode = .scaleToFill) -> UIImageView {
        let imageView = UIImageView()
        imageView.contentMode = contentMod
        
        return imageView
    }
    
    func setupLabel(_ title: String = "", textColor: UIColor = UIColor.Text.blackMediumColor(),
                    font: UIFont = UIFont.systemFont(ofSize: 14),
                    bgColor: UIColor = .clear,
                    alignment: NSTextAlignment = .left) -> UILabel {
        
        let label = UILabel()
        label.text = title
        label.textColor = textColor
        label.backgroundColor = bgColor
        label.lineBreakMode = .byTruncatingTail
        label.numberOfLines = 1
        label.textAlignment = alignment
        label.font = font
        return label
    }
}
