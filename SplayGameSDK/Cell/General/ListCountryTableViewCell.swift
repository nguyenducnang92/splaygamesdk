//
//  ListCountryTableViewCell.swift
//  SplayGameSDK
//
//  Created by Nguyen Nang on 9/1/20.
//  Copyright © 2020 splay. All rights reserved.
//

import UIKit

class ListCountryTableViewCell: UITableViewCell {
    

    var seperator: UIView!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        
        setupAllSubviews()
    }
    
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        setupAllSubviews()
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.frame = bounds
        
        textLabel?.frame = CGRect(x: 10,
                                  y: 0,
                                  width: contentView.frame.width - 20,
                                  height: contentView.frame.height)

        seperator.frame = CGRect(x: 0,
                                 y: contentView.frame.height - onePixel(),
                                 width: contentView.frame.width,
                                 height: onePixel())
        
    }
    
}


extension ListCountryTableViewCell {
    
    func setupAllSubviews() {
        
        contentView.backgroundColor = .clear
        contentView.clipsToBounds = true
        
        setupLabelDefault()
        seperator = setupView()
        contentView.addSubview(seperator)
    }
    
    func setupLabelDefault() {
        
        textLabel?.backgroundColor = .clear
        textLabel?.textAlignment = .left
        
        detailTextLabel?.backgroundColor = .clear
    
    }

    func setupView(_ bgColor: UIColor = UIColor.Misc.seperatorColor()) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        view.clipsToBounds = true
        return view
    }
    
    func setupLabel(_ title: String = "", textColor: UIColor = UIColor.Text.blackMediumColor(),
                    font: UIFont = UIFont.systemFont(ofSize: 14),
                    bgColor: UIColor = .clear,
                    alignment: NSTextAlignment = .left) -> UILabel {
        
        let label = UILabel()
        label.text = title
        label.textColor = textColor
        label.backgroundColor = bgColor
        label.lineBreakMode = .byTruncatingTail
        label.numberOfLines = 1
        label.textAlignment = alignment
        label.font = font
        return label
    }
}

