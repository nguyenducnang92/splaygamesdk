//
//  Constants.swift
//  SplayGameSDK
//
//  Created by Nguyen Nang on 12/25/20.
//  Copyright © 2020 splay. All rights reserved.
//

import Foundation


/**
 Type Info Log
 */
public enum LogType: Int {
    case info = 0, error
}

/**
 METHOD LOGIN
 */
public enum MethodDn: Int {
    case byFastTk = 0, byTk, byTkFacebook, byTkApple, byTkGoogle
    
    var key: String {
        switch self {
        case .byFastTk:     return "guest_account"
        case .byTk:         return "splay_account"
        case .byTkFacebook: return "facebook"
        case .byTkGoogle:   return "google"
        case .byTkApple:    return "apple"
        }
    }
    
}

/**
 Các sự kiện
 */
public enum TrackingType {
    
    /*
    "vtc_complete_create_role", hoàn thành tạo nhân vật
    "vtc_complete_extract_obb", hoàn thành giải nén tập tin obb thành công
    "vtc_complete_first_level", hoàn thành leel đầu
    "vtc_complete_first_open", lần đầu mở app
    "vtc_complete_first_update", lần đầu update resource
    "vtc_complete_load_role", hoàn thành load nhân vật
    "vtc_complete_load_sdk", hoàn thành load sdk(các game đều nhúng sdk của vtc)
    "vtc_complete_load_server_list", load danh sách server game
    "vtc_complete_over_tutorial", qua phần hướng dẫn trong game
    "vtc_complete_registration", đăng ký tài khoản thành công(có thể là tài khoản mới hoặc cũ đăng ký vào dịch vụ mới - dịch vụ hiện tại)
    "vtc_complete_registration_new", đăng ký tài khoản mới tham gia game
    "vtc_complete_registration_old", tài khoản cũ tham gia game(mới - hiện tại)
    "vtc_complete_tutorial", hoàn thành hưỡng dẫn trong game
 */
    
    case firstOpen
    case firstLevel
    case firstUpdateResource
    case completeTutorialEvent
    case completeOverTutorialEvent
    case completeLoadServerListEvent
    case completeLoadSdkEvent
    case completeLoadRoleEvent
    case completeCreateRoleEvent
    
    case completeExtractObb
    case completeRegistration
    case completeRegistrationNew
    case completeRegistrationOld
    
    
    var key: String {
        switch self {
        case .firstOpen:                    return "first_open"
        case .firstLevel:                   return "first_level"
        case .firstUpdateResource:          return "first_update_resource"
        case .completeTutorialEvent:        return "complete_tutorial_event"
        case .completeOverTutorialEvent:    return "complete_over_tutorial_event"
        case .completeLoadServerListEvent:  return "complete_load_server_list_event"
        case .completeLoadSdkEvent:         return "complete_load_sdk_event"
        case .completeLoadRoleEvent:        return "complete_load_role_event"
        case .completeCreateRoleEvent:      return "complete_create_role_event"
        case .completeExtractObb:           return "complete_extract_obb"
        case .completeRegistration:         return "complete_registration"
        case .completeRegistrationNew:      return "complete_registration_new"
        case .completeRegistrationOld:      return "complete_registration_old"
        }
    }
    
    var eventName: String {
        switch self {
        case .firstOpen:                    return EVEN_COMPLETE_FIRST_OPEN_VTC
        case .firstLevel:                   return EVEN_COMPLETE_FIRST_LEVEL_VTC
        case .firstUpdateResource:          return EVEN_COMPLETE_FIRST_UPDATE_VTC
        case .completeTutorialEvent:        return EVEN_COMPLETE_TUTORIAL_VTC
        case .completeOverTutorialEvent:    return EVEN_COMPLETE_OVER_TUTORIAL_VTC
        case .completeLoadServerListEvent:  return EVEN_COMPLETE_LOAD_SERVER_LIST_VTC
        case .completeLoadSdkEvent:         return EVEN_COMPLETE_LOAD_SDK_VTC
        case .completeLoadRoleEvent:        return EVEN_COMPLETE_LOAD_ROLE_VTC
        case .completeCreateRoleEvent:      return EVEN_COMPLETE_CREATE_ROLE_VTC
        case .completeExtractObb:           return EVEN_COMPLETE_EXTRACT_OBB_VTC
        case .completeRegistration:         return EVEN_COMPLETE_REGISTRATION
        case .completeRegistrationNew:      return EVEN_COMPLETE_REGISTRATION_NEW
        case .completeRegistrationOld:      return EVEN_COMPLETE_REGISTRATION_OLD
        }
    }
    
    var sendLimit: Bool {
        
        switch self {
        case .completeRegistration, .completeRegistrationNew, .completeRegistrationOld:
            return false
        default:
            return true
        }
    }
}


/**
 Cấu hình Config
 */

struct ConfigAPI {
    
    let agencyID: Int
    let isReview: Bool
    let apiRelease: String
    let apiBackup: String
    
    init(data: [String : Any]) {
        
        if let agencyString = data["ag"] as? String {
            agencyID = Int(agencyString) ?? 0
        } else {
            agencyID = data["ag"] as? Int ?? 0
        }
        
        isReview = data["rv"] as? Bool ?? false
        
        
        if let ar = data["ar"] as? String, let data = Data(base64Encoded: ar) {
            apiRelease = String(data: data, encoding: .utf8) ?? ""
        } else {
            apiRelease = ""
        }
        
        if let ab = data["ab"] as? String, let data = Data(base64Encoded: ab) {
            apiBackup = String(data: data, encoding: .utf8) ?? ""
        } else {
            apiBackup = ""
        }
        
        print("ConfigAPI: agencyID: \(agencyID) | isReview: \(isReview) | apiRelease: \(apiRelease) | apiBackup: \(apiBackup)")
    }
}
