//
//  Utilities.swift
//  G5TaxiUser
//
//  Created by NangND on 9/7/15.
//  Copyright © 2015 Hoan Pham. All rights reserved.
//


import UIKit
import Firebase
import Foundation
import CleanroomLogger
import AdBrixRM
import Firebase
import FBSDKCoreKit
import FBSDKLoginKit
import FBSDKShareKit
import AppsFlyerLib
import GoogleSignIn

import WebKit
import SystemConfiguration.CaptiveNetwork
import CoreTelephony
import AdSupport


/**
 Các CustomDimension
 */

public enum DimensionType: Int {
    
    case eventName = 2          // Tên sự kiện
    case username = 3           // Username
    case email = 4              // Email
    case gameProject = 5        // GameID
    case revenue = 6       // Giá trị: SDK không gửi lên
}


/**
 Các hành động
 */
public enum TypeEventLog: Int {
    
    case getConfig              = 0
    case getAppStoreID          = 1
    case configFacebook         = 2
    case configAppsFlyer        = 3
    case autoLogin              = 4
    case openURLOptions         = 5
    
    /// Action thanh toán
    case iapReceiveProductID    = 6
    case iapSendProductID       = 7
    case iapReceiveProduct      = 8
    case iapSendProduct         = 9
    case iapReceiveResultApple  = 10
    case iapPaymentAppleTransactionNil  = 11
    case iapPaymentAppleTransactionError = 12
    case iapPaymentAppleSuccess = 13
    case iapPaymentAppleFailed  = 14
    case iapSendPaymentInfoServer = 15
    case iapReveiceResultServer = 16
    case iapFinishedPaymentSuccess     = 17
    
    /// Action game
    case showLoginView          = 18
    case loginSuccess           = 19
    case loginSplay             = 20
    case loginFacebook          = 21
    case loginGoogle            = 22
    case loginApple             = 23
    case loginPlayNow           = 24
    
    case registerSuccess        = 25
    case updateInfoSuccess      = 26
    case forgotPassword         = 27
    
    case iapReceiveProductNil   = 28
    case iapPaymentAppleDefaultFailed = 29
    case iapFinishedPaymentFailed     = 30
    
    var eventName: String {
        switch self {
        case .getConfig:                return "get_config"
        case .getAppStoreID:            return "get_app_store_id"
        case .configFacebook:           return "config_facebook"
        case .configAppsFlyer:          return "config_appsflyer"
        case .autoLogin:                return "auto_login"
        case .openURLOptions:           return "open_url_options"
            
        case .showLoginView:            return "show_login_view"
        case .loginSuccess:             return "login_success"
        case .loginSplay:               return "login_splay"
        case .loginFacebook:            return "login_facebook"
        case .loginGoogle:              return "login_google"
        case .loginApple:               return "login_apple"
        case .loginPlayNow:             return "login_play_now"
        case .registerSuccess:          return "register_success"
        case .updateInfoSuccess:        return "update_info_success"
        case .forgotPassword:           return "forgot_password"
            
            
        /// 1. Nhận ProductID từ game
        case .iapReceiveProductID:      return "iap_receive_product_id"
        
        /// 2. Gửi ProductID lên IAP của Apple
        case .iapSendProductID:         return "iap_send_product_id"
            
        /// 3. Nhận về Product từ IAP của Apple
        case .iapReceiveProduct:        return "iap_receive_product"
            
        /// 3.1. Product nhận về từ IAP của Apple Nil
        case .iapReceiveProductNil:     return "iap_receive_product_nil"
            
        /// 4. Gửi yêu cầu thanh toán Product cho Apple
        case .iapSendProduct:           return "iap_send_product"
            
        /// 5. Nhận kết quả thanh toán từ Apple
        case .iapReceiveResultApple:    return "iap_receive_result_apple"
            
        /// 6.1. Thanh toán với Apple không trả về kết quả
        case .iapPaymentAppleTransactionNil:    return "iap_payment_apple_transaction_nil"
            
        /// 6.2. Thanh toán với Apple trả về kết quả lỗi
        case .iapPaymentAppleTransactionError:    return "iap_payment_apple_transaction_error"
            
        /// 6.3. Thanh toán với Apple thành công
        case .iapPaymentAppleSuccess:   return "iap_payment_apple_success"
            
        /// 6.4. Thanh toán với Apple Transaction State Failed
        case .iapPaymentAppleFailed:    return "iap_payment_apple_failed"
            
        /// 6.5. Thanh toán với Apple thất bại
        case .iapPaymentAppleDefaultFailed: return "iap_payment_apple_default_failed"
            
        /// 7. Gửi thông tin thanh toán sang Server
        case .iapSendPaymentInfoServer: return "iap_send_payment_info_server"
            
        /// 8. Nhận kết quả thanh toán từ Server
        case .iapReveiceResultServer:   return "iap_reveice_result_server"
            
        /// 9.1. Kết thúc thanh toán với param "is_success": True | False
        case .iapFinishedPaymentSuccess:       return "iap_finished_payment_success"
            
        /// 9.2. Kết thúc thanh toán với param "is_success": True | False
        case .iapFinishedPaymentFailed:       return "iap_finished_payment_failed"
            
        }
    }
}


struct Utility {
    
    /**
     Tạo folder cho app
     */
//    static func createAppDirectory(_ name: String, skipBackupAttribute: Bool = true) {
//        let docPaths = NSSearchPathForDirectoriesInDomains(.libraryDirectory, .userDomainMask, true)
//        let dbPath = docPaths[0].stringByAppendingPathComponent(name)
//        if !FileManager.default.fileExists(atPath: dbPath) {
//            do {
//                try FileManager.default.createDirectory(atPath: dbPath, withIntermediateDirectories: false, attributes: nil)
//            }
//            catch { }
//            
//            if skipBackupAttribute {
//                do {
//                    try (URL(fileURLWithPath: dbPath) as NSURL).setResourceValue(NSNumber(value: true as Bool), forKey: URLResourceKey.isExcludedFromBackupKey)
//                }
//                catch { }
//            }
//        }
//    }
    

    /**
    Chỉnh lại giao diện navigation bar
    
    - parameter navController:
    */
    static func configureAppearance(navigation navController: UINavigationController, tintColor: UIColor = UIColor.white) {
        
        navController.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 16),
            NSAttributedString.Key.foregroundColor: UIColor.Text.blackMediumColor()]
        navController.navigationBar.barTintColor = tintColor
     
        
        navController.navigationBar.barStyle = .default
        navController.navigationBar.shadowImage = UIImage()
        navController.navigationBar.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        navController.navigationBar.isTranslucent = false

    }
    
    /**
     Hiển thị 1 view full màn hình
     */
    static func showView(_ contentView: UIView?, onView supperView: UIView) {
        
        guard let view = contentView else { return }
        
        view.alpha = 0.0
        if let window = UIApplication.shared.keyWindow, let controller = window.rootViewController  {
            
            view.frame = CGRect(origin: CGPoint.zero, size: UIScreen.main.bounds.size)
            controller.view.addSubview(view)
        } else {
            view.frame = CGRect(origin: CGPoint.zero, size: supperView.frame.size)
            supperView.addSubview(view)
        }
        
        UIView.animate(withDuration: 0.3, animations: { view.alpha = 1.0 })
    }
    
    
    /**
    Căn giữa cho text và image in button
    */
    
    static func centeredTextAndImageForButton(_ button: UIButton, spacing: CGFloat = 3.0) {
     
        let imageSize: CGSize = button.imageView!.image!.size
        button.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: -imageSize.width, bottom: -(imageSize.height + spacing), right: 0.0)
        let labelString = NSString(string: button.titleLabel?.text ?? "")
        let titleSize = labelString.size(withAttributes: [NSAttributedString.Key.font: button.titleLabel!.font as Any])
        button.imageEdgeInsets = UIEdgeInsets(top: -(titleSize.height + spacing), left: 0.0, bottom: 0.0, right: -titleSize.width)
    }
    
    /**
     Căn Phải image in button
     */
    
    static func rightedTextAndImageForButton(_ button: UIButton) {
        let spacing: CGFloat = 10.0
        let imageSize: CGSize = button.imageView!.image!.size
        button.titleEdgeInsets = UIEdgeInsets(top: 0,
                                              left: -(imageSize.width + spacing * 4),
                                              bottom: 0,
                                              right: imageSize.width + spacing)
        
        
        let labelString = NSString(string: button.titleLabel!.text ?? "")
        let titleSize = labelString.size(withAttributes: [NSAttributedString.Key.font: button.titleLabel!.font as Any])
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: titleSize.width , bottom: 0, right: -titleSize.width )
    }


    
    /**
    Tính độ cao cho text
    */
    static func heightForView(_ text:String, font:UIFont, width:CGFloat) -> CGFloat{
//        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
//        label.numberOfLines = 0
//        label.lineBreakMode = NSLineBreakMode.byWordWrapping
//        label.font = font
//        label.text = text
//        label.sizeToFit()
//
//
//
//        return label.frame.height
        
//        Utility.printLog("textLabelHeight: \(label.frame.height)")
//
//
        let height =  NSString(string: text).boundingRect(with: CGSize(width: width, height: CGFloat.greatestFiniteMagnitude),
                                                          options: [.usesLineFragmentOrigin],
                                                   attributes: [NSAttributedString.Key.font: font],
            context: nil).height

//        Utility.printLog("boundingRect: \(height)")

        return height
    }
    
    /**
     Tính độ cao cho textView
     */
    static func heightForTextView(_ text:String, font:UIFont, width:CGFloat) -> CGFloat {
        let label:UITextView = UITextView(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.textAlignment = .justified
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
    
    /**
     Tính độ cao cho textView Attribulte
     */
    static func heightForTextViewAttributed(_ text: NSAttributedString, width:CGFloat) -> CGFloat{
        let label:UITextView = UITextView(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.textAlignment = .justified
        //        label.font = font
        label.attributedText = text
        label.sizeToFit()
        return label.frame.height
    }
    
    /**
     Tính độ cao cho textView Attribulte
     */
    static func heightForTextAttributed(_ text: NSAttributedString, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.textAlignment = .left
        //        label.font = font
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.attributedText = text
        label.sizeToFit()
        return label.frame.height
    
    }
    
    
    /**
    Tính chiều rộng của text
    */
    static func widthForView(_ text:String, font:UIFont, height:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0,  width: CGFloat.greatestFiniteMagnitude, height: height))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.width
    }
    
    
    /**
     Tính độ rộng cho textView Attribulte
     */
    static func widthForAttributed(_ text: NSAttributedString, height: CGFloat) -> CGFloat{
        let label: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: CGFloat.greatestFiniteMagnitude, height: height))
        label.textAlignment = .right
        //        label.font = font
        label.attributedText = text
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.sizeToFit()
        return label.frame.width
    }
    
    
    /**
     Tính độ cao cho textView
     */
    static func widthForTextView(_ text:String, font:UIFont, height:CGFloat) -> CGFloat {
        let label:UITextView = UITextView(frame: CGRect(x: 0, y: 0, width: CGFloat.greatestFiniteMagnitude, height: height ))
        label.textAlignment = .justified
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.width
    }
    

    /**
    Format lại định dạng số điện thoại cho dễ đọc
    
    - parameter textField:
    - parameter range:
    - parameter string:
    
    - returns:
    */
    static func formatPhoneNumberForDisplay(_ textField: UITextField, range: NSRange, string: NSString) -> Bool {
        guard let phoneNumber = textField.text, phoneNumber.count >= 0 else { return false }
        
        
        return true
//        // Khi xóa nếu là khoảng trắng thì xóa thêm số tiếp theo
//        if phoneNumber.count >= 2 && string.length == 0 {
//            let rangeOfLastChar = [phoneNumber.index(phoneNumber.endIndex, offsetBy: -1) ..< phoneNumber.endIndex]
//
//            if phoneNumber[phoneNumber.index(phoneNumber.endIndex, offsetBy: -1) ..< phoneNumber.endIndex] == " " {
//                textField.text = phoneNumber.replacingCharacters(in: rangeOfLastChar, with: "")
//                return true
//            }
//        }
//
//        // Chèn thêm khoảng trắng cho dãy số
//        loop: for (addition, numberHeader) in Utility.phoneHeader {
//            if phoneNumber.hasPrefix(numberHeader) && string.length > 0 {
//                let pos = [2, 6, 9].map{ $0 + addition }.takeFirst { $0 == phoneNumber.count }
//                if let _ = pos {
//                    textField.text = phoneNumber + " "
//                    break loop
//                }
//            }
//        }
//
//        let newLength = phoneNumber.count + string.length - range.length
//
//
//        for (addition, numberHeader) in Utility.phoneHeader {
//            if phoneNumber.hasPrefix(numberHeader) {
//                return (newLength > 12 + addition) ? false : true
//            }
//        }
//        return (newLength > 12) ? false : true
    }
    
    /**
    Kiểm tra định dạng email
    
    - parameter string:
    
    - returns:
    */
    static func validateEmail(_ email: String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: email)
    }



    /**
     Insert trong String
     */
    fileprivate static func insert(_ source: String, insertedString: String, index: Int) -> String {
        return  String(source.prefix(index)) +
            insertedString +
            String(source.suffix(source.count - index))
    }

    

    /**
     Gọi điện thoại
     */
    static func callPhoneNumber(_ phoneNumber: String) {
        let phone = phoneNumber
            .trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            .replacingOccurrences(of: " ", with: "")
            .replacingOccurrences(of: ".", with: "")
        
        guard !validatePhone(value: phone), phone.count > 0 else { return }
        
        #if targetEnvironment(simulator)
//            log("on simulator - not calling")
        #else
            let tel = "tel://" + phone
        guard let url = URL(string: tel) else { return }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
        #endif
    }
    
    
    static func callPhoneNumberShowAlert(_ phoneNumber: String) {
        let phone = phoneNumber
            .trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            .replacingOccurrences(of: " ", with: "")
            .replacingOccurrences(of: ".", with: "")
        
        guard !validatePhone(value: phone), phone.count > 0 else { return }
      
        
        #if targetEnvironment(simulator)
//            log("on simulator - not calling")
        #else
            let tel = "telprompt://" + phone
        
        guard let url = URL(string: tel) else { return }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
        #endif
    }
    
    static func validatePhone(value: String) -> Bool {
        let PHONE_REGEX = "^((\\+)|(00))[0-9]{6,14}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
    
    
    static func stringNormalFromServer(_ string: String) -> String {
        var newString: NSString = NSString(string: string)
        newString = newString.replacingOccurrences(of: "##s", with: "%@") as NSString
        newString = newString.replacingOccurrences(of: "%s", with: "%@") as NSString
        
        return newString as String
    }

    
    /**
     Đổi "đ" -> "d"
     */
    
    static func stringFromReplaceTheD(_ string: String) -> String {
//        let charSet = CharacterSet(charactersIn: "đ")
        var newString = NSString(format: "%@", string)
//        if newString.rangeOfCharacter(from: charSet).location != NSNotFound {
//            return string.lowercased().replacingOccurrences(of: "đ", with: "d")
//        }
        
        newString = newString.lowercased.replacingOccurrences(of: "đ", with: "d") as NSString
        
        return newString as String
    }

    
    /**
     *  Loại bỏ dấu câu, dấu của từ, chuyển thành chữ thường
     *  Phục vụ mục đích tạo ra search text để insert vào DB
     */
    
    static func normalizeString(_ string: String) -> String {
        
        guard let data = Utility.stringFromReplaceTheD(string).lowercased().data(using: String.Encoding.ascii, allowLossyConversion: true) else { return ""}
        
        guard let newString = String(data: data, encoding: String.Encoding.ascii) else { return "" }
        
        
        var charSet = NSMutableCharacterSet.letter()
        charSet.formUnion(with: CharacterSet.decimalDigits)
        charSet.addCharacters(in: " ")
        charSet = (charSet.inverted as NSCharacterSet).copy() as! NSMutableCharacterSet
        
        return newString.components(separatedBy: charSet as CharacterSet).joined(separator: "")
    }
    
    
    
    /**
     Attribute Stirng
     */
    
    
    //Lấy Attributes
    
    static func getStringAttributeWith(_ mainString: String,
                                       subStrings: [String],
                                       mainFont: UIFont,
                                       subFonts: [UIFont],
                                       mainColor: UIColor,
                                       subColors: [UIColor]) -> NSMutableAttributedString {
        
        
        let mutableString =  NSMutableAttributedString(string: mainString,
                                                       attributes: [NSAttributedString.Key.font: mainFont,
                                                                    NSAttributedString.Key.foregroundColor: mainColor])
        
        
        subStrings.enumerated().forEach { (i, subString) in
            
            var subColor: UIColor = mainColor
            var subFont: UIFont = mainFont
            
            if i < subColors.count {
                subColor = subColors[i]
                
            } else if let color = subColors.first {
                subColor = color
            }
            
            if i < subFonts.count {
                subFont = subFonts[i]
                
            } else if let font = subFonts.first {
                subFont = font
            }
            
            
            mutableString.addAttributes([NSAttributedString.Key.foregroundColor: subColor,
                                         NSAttributedString.Key.font: subFont],
                                        range: (mainString as NSString).range(of: subString))
        }
        
        return mutableString
    }
    
    
     //Lấy Attributes gạch chân
    static func getAttributedUnderline(_ text: String,
                                       font: UIFont = UIFont.systemFont(ofSize: 13),
                                       textColor: UIColor = UIColor.Text.grayNormalColor()) -> NSAttributedString {
        
        let attribute = NSAttributedString(string: text,
                                           attributes: [NSAttributedString.Key.foregroundColor: textColor,
                                                        NSAttributedString.Key.font: font,
                                                        NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue])
        return attribute
    }
    


    
    /**
     Hàm faddingView
     */
    static func setVisibilityOf(_ view: UIView, to visible: Bool, duration: TimeInterval = 0.3, completion: ((Bool) -> Void)? = nil) {
        
        if visible { view.isHidden = false }
        UIView.animate(withDuration: duration,
                       animations: {
                        view.alpha = visible ? 1.0 : 0.0
        },
                       completion: { finished in
                        view.isHidden = !visible
                        if let completion = completion { completion(true) }
        })
    }
    
    
    

    /**
     Lấy string từ Time
     */

    
    /// DATE TIME FORMATTER
    static func getTimeFormatter(_ string: String) -> DateFormatter {
        
        var language = UserDefaults.standard.object(forKey: KEY_SAVE_LANGUAGE_SETTING) as? String ?? ""
               
               if language.count == 0 {
                   language = Locale.preferredLanguages.first ?? ""
               }
        
        let formatter = DateFormatter()
        formatter.dateFormat = string
        if language.contains("vi") {
            formatter.locale = Locale(identifier: "vi_VN")
        } else {
            formatter.locale = Locale(identifier: "en_US_POSIX")
        }
        
        return formatter
    }
    
    /// DATE TIME FORMATTER
    static func getStringTime(_ time: Double, formatter: String) -> String {
        return getTimeFormatter(formatter).string(from: Date(timeIntervalSince1970: time))
    }
    
    
    
    static func updateAccountInfoWith(_ results: Any?,
                                      loginMethod: MethodDn? = nil,
                                      isLogin: Bool? = true,
                                      sendEvent: Bool = true,
                                      isPostNoti: Bool = true) {
        
        guard let resultJson = results as? [String : Any],
            let dictData = resultJson[KEY_DATA] as? [String : Any]  else { return }
        
        
        
        let account = Account(data: dictData)
    
        let userDefault = UserDefaults.standard
        userDefault.set(account.accountID, forKey: KEY_SAVE_USERID)
        userDefault.set(account.accountName, forKey: KEY_SAVE_USERNAME)
        userDefault.set(account.accessToken, forKey: KEY_SAVE_USER_TOKEN)
        
        if let isLogin = isLogin { userDefault.set(isLogin, forKey: KEY_CHECK_ACCOUNT_LOGIN) }
        if let loginMethod = loginMethod { userDefault.set(loginMethod.rawValue , forKey: KEY_SAVE_METHOD_LOGIN) }

        userDefault.synchronize()
        

        print(String(format: "___________method login %d", userDefault.integer(forKey: KEY_SAVE_METHOD_LOGIN)))
        
        

        if sendEvent {
            
            if AppConfig.isUseGTM {
                Utility.sendEventToFirebaseWith(EVEN_LOGIN_GAME_VTC)
            } else {
                Utility.sendEventToScoinWith(EVEN_LOGIN_GAME_VTC)
            }
            
        }
        
        
        guard isPostNoti else { return }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: ACTION_LOGIN_USER_SUCCESS),
                                        object: self,
                                        userInfo: [KEY_INFO_USER_LOGIN: account])
    
    }

    
    
    /// Event Complete Register Old or New
    static func trackEventCompleteRegisterOldOrNew(_ registrationMethod: String, results: Any?, isRegisterPushNotification: Bool = true) {
        
        if isRegisterPushNotification {
            UtilitieWebService.shareSingleton()?.dkPushNotification()
        }
        
        
        guard let resultJson = results as? [String : Any] else { return }
        guard let firstLogin = resultJson[KEY_FIRST_LOGIN] as? Int else { return }
        guard [1, 2].contains(firstLogin) else { return }

        print("XX: SEND completeRegistration")
        
        SplayGameSDKManager
            .shared
            .trackEvent(.completeRegistration,
                        parameters: [AnalyticsParameterSignUpMethod: registrationMethod])
        

        SplayGameSDKManager
            .shared
            .trackEvent(firstLogin == 1 ? .completeRegistrationNew : .completeRegistrationOld,
                        parameters: [AnalyticsParameterSignUpMethod: registrationMethod])
        
        
        print("XXX: SEND \(firstLogin == 1 ? TrackingType.completeRegistrationNew.eventName : TrackingType.completeRegistrationOld.eventName)")
        
        
    }

    /// Show Alert when request failed
    static func showAlertFrom(_ resultCode: ResultCode,
                              results: Any?,
                              viewController: UIViewController?,
                              handler: ((UIAlertAction) -> Void)? = nil) {
        
        let messagerResult: String
        
        switch resultCode {
        case ResultCodeNetWorkError:
            messagerResult = LanguageSetting.get("MessagerNetworkResult", alter: nil)
            
        case RequestCodeUnknowError:
            messagerResult = LanguageSetting.get("error_connect_server", alter: nil)
            
        default:
            if let resultJson = results as? [String : Any],
                let messagerString = resultJson[KEY_MESSAGE] as? String,
                messagerString.count > 0 {
                
                if resultCode == ResultCodeDataError {
                    messagerResult = UnitAnalyticHelper.standardizeLog(messagerString)
                } else {
                    messagerResult = messagerString
                }
                
                
                
            } else {
                messagerResult = LanguageSetting.get("MessagerUnknowError", alter: nil)
            }
        }

        showAlertWithMessage(messagerResult, viewController: viewController, handler: handler)
    }

    /// Show alert with message
    static func showAlertWithMessage(_ message: String,
                                     alertTitle: String = LanguageSetting.get("TitleNotification", alter: nil),
                                     buttonTitle: String = LanguageSetting.get("TitleButtonOK", alter: nil),
                                     viewController: UIViewController?,
                                     handler: ((UIAlertAction) -> Void)? = nil) {
        
        let alert = UIAlertController(title: alertTitle,
                                      message: message,
                                      preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: buttonTitle,
                                      style: .default,
                                      handler: handler))
        

        DispatchQueue.main.async {
            if let viewController = viewController {
                viewController.present(alert, animated: true, completion: nil)
            } else {
                UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    

    
}


//==========================
// - MARK: TRACK EVENT
//==========================

extension Utility {
    
    
    static func logEventDebug(_ type: TypeEventLog, parameters: [String : Any]? = nil) {
        
        guard let config = AppData.instance.config, config.isReview else { return }
        
        let userDefault = UserDefaults.standard
     
        var defaultParam: [String : Any] = ["account_id": userDefault.string(forKey: KEY_SAVE_USERID) ?? "",
                                            "time": Date().timeIntervalSince1970,
                                            "client_ip": AppData.instance.IPAddress,
                                            "device_type": UIDevice.modelName,
                                            "device_os":   "IOS",
                                            "osVersion": UIDevice.current.systemVersion]
        
        
        parameters?.forEach { item in
            defaultParam[item.key] = item.value
        }
        
        
        sendEventToFirebaseWith(type.eventName, parameters: defaultParam)

        Log.message(.info, message: "Event Name: \(type.eventName) | Param: \(defaultParam)")
    }
    
    static func sendEventToAllWith(_ eventName: String, parameters: [String : Any]? = nil) {
        
        sendEventToAppsFlyerWith(eventName, parameters: parameters)
        sendEventToFirebaseWith(eventName, parameters: parameters)
        sendEventToFacebookWith(eventName, parameters: parameters)
        sendEventToAdBrixWith(eventName, parameters: parameters)
        sendEventToHubJSWith(eventName, parameters: parameters)
        sendEventToScoinWith(eventName, parameters: parameters)
        
    }
    
    static func sendEventToFirebaseWith(_ eventName: String, parameters: [String : Any]? = nil) {
        
        
        guard AppConfig.isUseGTM else { Analytics.logEvent(eventName, parameters: parameters); return }
        

        let userDefault = UserDefaults.standard

        var param: [String : Any] = ["apple_app_id": "id" + AppsFlyerLib.shared().appleAppID,
                                     "af_id": AppsFlyerLib.shared().getAppsFlyerUID(),
                                     "dev_key": AppsFlyerLib.shared().appsFlyerDevKey,
                                     "pixel_id": Bundle.main.object(forInfoDictionaryKey: "FacebookAppID") as? String ?? "",
                                     "accountId": userDefault.string(forKey: KEY_SAVE_USERID) ?? "",
                                     "accountName":userDefault.string(forKey: KEY_SAVE_USERNAME) ?? "",
                                     "platform": "IOS",
                                     "deviceType": UIDevice.modelName,
                                     "osVersion": UIDevice.current.systemVersion,
                                     "userAgent": WKWebView(frame: .zero).customUserAgent ?? "",
                                     "clientIp": externalIPAddress(),
                                     "wifi": checkIfUsingWifi() ? "true" : "false",
                                     "carrier": CTTelephonyNetworkInfo().subscriberCellularProvider?.carrierName ?? "",
                                     "iDFA": ASIdentifierManager.shared().advertisingIdentifier.uuidString,
                                     "api_key": userDefault.string(forKey: KEY_API_CONFIG_APP) ?? "",
                                     "agency_id": userDefault.string(forKey: KEY_AGENCY_APP) ?? "",
                                     "packageName": Bundle.main.bundleIdentifier ?? ""]
        
        if let parameters = parameters {
            parameters.forEach { param[$0.key] = $0.value }
        }
        
        Analytics.logEvent(eventName, parameters: param)
    }
    
    static func sendEventToScoinWith(_ eventName: String, parameters: [String : Any]? = nil) {
        
        guard !AppConfig.isUseGTM else { return }
        ApiTrackingEventService.shareSingleton()?.sendEventApiTracking(eventName: eventName)
    }
    
    static func sendEventToFacebookWith(_ eventName: String, parameters: [String : Any]? = nil) {
        
        guard !AppConfig.isUseGTM else { return }
        AppEvents.logEvent(AppEvents.Name(rawValue: eventName), parameters: parameters ?? [:])
    }
    
    static func sendEventToAppsFlyerWith(_ eventName: String, parameters: [String : Any]? = nil) {
        
        guard !AppConfig.isUseGTM else { return }
        AppsFlyerLib.shared().logEvent(eventName, withValues: parameters)
    }
    
    static func sendEventToAdBrixWith(_ eventName: String, parameters: [String : Any]? = nil) {
        
        let attrModel = AdBrixRmAttrModel()

        parameters?.forEach { item in
            
            if let valueString = item.value as? String {
                attrModel.setAttrDataString(item.key, valueString)
            } else if let valueInt = item.value as? Int {
                attrModel.setAttrDataInt(item.key, valueInt)
            } else if let valueDouble = item.value as? Double {
                attrModel.setAttrDataDouble(item.key, valueDouble)
            } else if let valueBool = item.value as? Bool {
                attrModel.setAttrDataBool(item.key, valueBool)
            } else if let valueInt64 = item.value as? Int64 {
                attrModel.setAttrDataInt64(item.key, valueInt64)
            }
        }

        AdBrixRM.sharedInstance().eventWithAttr(eventName: eventName, value: attrModel)
    }
    
    static func sendEventToHubJSWith(_ eventName: String, parameters: [String : Any]? = nil) {
        
        
        guard let hubjsTracker = AppData.instance.hubjsTracker else { return }
        
        hubjsTracker.track(eventWithCategory: "VTC event in_app",
                                             action: eventName,
                                             name: Bundle.main.bundleIdentifier ?? "",
                                             value: nil)
 
        
        if let param = parameters {
            for (i, item) in param.enumerated() {
                hubjsTracker.setCustomVariable(withIndex: UInt(i + 1),
                                                                 name: item.key,
                                                                 value: "\(item.value)")
            }
        }

        hubjsTracker.setDimension(AppData.instance.gameID,
                                                    forIndex: DimensionType.gameProject.rawValue)
        hubjsTracker.setDimension(UserDefaults.standard.string(forKey: KEY_SAVE_USERNAME) ?? "",
                                                    forIndex: DimensionType.username.rawValue)
        hubjsTracker.userId = UserDefaults.standard.string(forKey: KEY_SAVE_USERNAME) ?? ""
        
    }
}


extension Utility {

    
    static func appDirectory() -> String {
        return NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    }
    
    fileprivate static func addSkipBackupAttributeToItem(_ URL:Foundation.URL) -> Bool {
        
        assert( FileManager.default.fileExists(atPath: URL.path))
        
        var error:NSError?
        let success:Bool
        do {
            try (URL as NSURL).setResourceValue(NSNumber(value: true as Bool),forKey: URLResourceKey.isExcludedFromBackupKey)
            success = true
        } catch let error1 as NSError {
            error = error1
            success = false
        }
        
        if !success{
            NSLog("Error excluding \(URL.lastPathComponent) from backup \(String(describing: error) )")
        }
        return success
    }
    

    static func cropImage(_ image: UIImage, toSize size: CGSize) -> UIImage {
        
        
        guard image.size.height > size.height || image.size.width > size.width else { return image }

        var newSize: CGSize
        /**
         *  Resize xuống 720 x 720
         */
        if image.size.width >= image.size.height { newSize = CGSize(width: size.width, height: size.width * image.size.height / image.size.width) }
        else { newSize = CGSize(width: size.height * image.size.width / image.size.height, height: size.height) }
        
        UIGraphicsBeginImageContext(newSize)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        /**
         *  Crop to size
         */
        
        let x = (((newImage?.cgImage)?.width)! - Int(size.width)) / 2
        let y = (((newImage?.cgImage)?.height)! - Int(size.height)) / 2
        let cropRect = CGRect(x: x, y: y, width: Int(size.height), height: Int(size.width))
        let imageRef = (newImage?.cgImage)?.cropping(to: cropRect)
        
        let cropped = UIImage(cgImage: imageRef!, scale: 0.0, orientation: (newImage?.imageOrientation)!)
        return cropped
    }
    
    
    
    /* Lấy thông tin link
     */
    static func getVideoLink(_ url: String) -> String {
        
        if let id = url.youtubeID {
            
            return String(format: "https://www.youtube.com/embed/%@", id)
            
        } else {
            
            return url
        }
    }

    /*
     Print
     */
    
    static func printLog(_ string: String?) {
    
        #if DEBUG
        print(string ?? "nil")
        
        #endif
     }
}



//---------------------------------
// MARK: - NETWORK
//---------------------------------
extension Utility {
    
    
    static func checkNetWork() -> Bool {
        
        guard let networkStatus =  Reachability.forInternetConnection()?.currentReachabilityStatus() else { return false }
        guard networkStatus == NotReachable else { return true }
        return false
    }
    
    static func checkIfUsingWifi() -> Bool {
        
        let reachability = Reachability.forInternetConnection()
        reachability?.startNotifier()
        
        guard let status = reachability?.currentReachabilityStatus() else { return false }
        return status == ReachableViaWiFi
    }
    
    static func connectedVia3G() -> Bool {
        
        let reachability = Reachability.forInternetConnection()
        reachability?.startNotifier()
        
        guard let status = reachability?.currentReachabilityStatus() else { return false }
        return status == ReachableViaWWAN
    }
    
    static func getUrlApiTracking() -> String {

//        guard let apiTracking = Bundle.main.object(forInfoDictionaryKey: "ApiTrackingURL") as? Bool, apiTracking else { return "" }
        return "https://s.qdht.vn/ads"

    }
    
    
    static func getIPAddress() -> String {
        var address: String?
        var ifaddr: UnsafeMutablePointer<ifaddrs>? = nil
        if getifaddrs(&ifaddr) == 0 {
            var ptr = ifaddr
            while ptr != nil {
                defer { ptr = ptr?.pointee.ifa_next }
                
                let interface = ptr?.pointee
                let addrFamily = interface?.ifa_addr.pointee.sa_family
                if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                    
                    // wifi = ["en0"]
                    // wired = ["en2", "en3", "en4"]
                    // cellular = ["pdp_ip0","pdp_ip1","pdp_ip2","pdp_ip3"]
                    
                    let name: String = String(cString: (interface!.ifa_name))
                    
                    if ["en0", "en2", "en3", "en4", "pdp_ip0","pdp_ip1","pdp_ip2","pdp_ip3"].contains(name) {

                        var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                        getnameinfo(interface?.ifa_addr, socklen_t((interface?.ifa_addr.pointee.sa_len)!), &hostname, socklen_t(hostname.count), nil, socklen_t(0), NI_NUMERICHOST)
                        address = String(cString: hostname)
                    }
                }
            }
            freeifaddrs(ifaddr)
        }
        
        print("Get Address: \(String(describing: address))")
        
        return address ?? ""
    }
    
    
    static func getISPName() -> String? {
        
        var wifiName: String? = nil

        if let interfaces = CNCopySupportedInterfaces() {
            for i in 0..<CFArrayGetCount(interfaces){
                let interfaceName = CFArrayGetValueAtIndex(interfaces, i)
                let rec = unsafeBitCast(interfaceName, to: AnyObject.self)
                guard let unsafeInterfaceData = CNCopyCurrentNetworkInfo("\(rec)" as CFString) else {
                    return nil
                }
                guard let interfaceData = unsafeInterfaceData as? [String: Any] else {
                    return nil
                }
                wifiName = interfaceData["SSID"] as? String
            }
        }
        
        print("wifiName: \(String(describing: wifiName))")
        return wifiName
    }

    
    static func externalIPAddress() -> String {
        
        guard Utility.checkIfUsingWifi() || Utility.connectedVia3G() else { return "" }
        
        do {
            
            guard let url = URL(string: "http://www.dyndns.org/cgi-bin/check_ip.cgi") else { return ""  }
            var ipHTML = try String(contentsOf: url, encoding: .utf8)
            let scanner = Scanner(string: ipHTML)
   
            var externalIPAddress:String?
            var ipItems:[String]?
            var text:NSString?

            while !scanner.isAtEnd {
                scanner.scanUpTo("<", into: nil)
                scanner.scanUpTo(">", into: &text)

                ipHTML = ipHTML.replacingOccurrences(of: String(text ?? "") + ">", with: " ")
                ipItems = ipHTML.components(separatedBy: " ")
                
                if let items = ipItems,
                    let index = items.firstIndex(of: "Address:"), index + 1 < items.count {

                    externalIPAddress = items[index + 1]
                }
            }

            guard let ip = externalIPAddress else { return "" }
            print("External IP Address: \(ip)")
            return ip

        } catch {
            print("error: \(error)")
            return ""
        }
    }
}



