//
//  ExString.swift
//  STaxi
//
//  Created by Hoan Pham on 4/8/15.
//  Copyright (c) 2015 Hoan Pham. All rights reserved.
//

import Foundation



extension String {
    
    public func stringByAddingPercentEncodingForURLQueryValue() -> String? {
        let characterSet = NSMutableCharacterSet.alphanumeric()
        characterSet.addCharacters(in: "-._~")
        
        return self.addingPercentEncoding(withAllowedCharacters: characterSet as CharacterSet)
    }
    
    public func stringByRemoveUnrecognizedCharacter() -> String {
        return self.lowercased().replacingOccurrences(of: "đ", with: "d")
    }
 
    
    public static func random (length len: Int = 0, charset: String = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789") -> String {
        
        let randomLength = len == 0 ? Int.random(max: 16) : len
        
        let max = charset.count - 1
        
        return (0..<randomLength)
            .map { _ in charset[charset.index(charset.startIndex, offsetBy: Int.random(0, max: max))] }
            .map { String($0) }
            .reduce("", +)
        
    }
    
    public func toDouble() -> Double? {
        
        let scanner = Scanner(string: self)
        var double: Double = 0
        
        if scanner.scanDouble(&double) {
            return double
        }
        return nil
        
    }
    
    public func toFloat() -> Float? {
        
        let scanner = Scanner(string: self)
        var float: Float = 0
        
        if scanner.scanFloat(&float) {
            return float
        }
        
        return nil
        
    }
}


