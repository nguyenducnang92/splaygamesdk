//
//  ExRange.swift
//  Staxi
//
//  Created by Hoan Pham on 8/12/15.
//  Copyright © 2015 Hoan Pham. All rights reserved.
//

import Foundation


extension CountableRange {
    public func eachIndex (_ function: (Element) -> Void) {
        for i in self { function(i) }
    }
    
    public func each (_ function: () -> Void) {
        for _ in self { function() }
    }
    
    public var randomInt: Int
        {
            var offset = 0
            
            if (lowerBound as! Int) < 0   // allow negative ranges
            {
                offset = abs(lowerBound as! Int)
            }
            
            let mini = UInt32(lowerBound as! Int + offset)
            let maxi = UInt32(upperBound   as! Int + offset)
            
            return Int(mini + arc4random_uniform(maxi - mini)) - offset
            
    }
}

