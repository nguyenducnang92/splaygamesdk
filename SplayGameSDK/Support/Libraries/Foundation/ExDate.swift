//
//  ExDate.swift
//  Staxi
//
//  Created by Hoan Pham on 9/3/15.
//  Copyright © 2015 Hoan Pham. All rights reserved.
//

import Foundation


/*
public func +(date: Date, timeInterval: Double) -> Date {
    return date.addingTimeInterval(Foundation.TimeInterval(timeInterval))
}

public func -(date: Date, timeInterval: Double) -> Date {
    return date.addingTimeInterval(Foundation.TimeInterval(-timeInterval))
}


public func -(date: Date, otherDate: Date) -> Foundation.TimeInterval {
    return date.timeIntervalSince(otherDate)
}

public func ==(lhs: Date, rhs: Date) -> Bool {
    return lhs.compare(rhs) == ComparisonResult.orderedSame
}

public func <(lhs: Date, rhs: Date) -> Bool {
    return lhs.compare(rhs) == ComparisonResult.orderedAscending
}
 */

public extension Date {
    /**
    Date year
    */
    var year : Int {
        get {
            return getComponent(.year)
        }
    }
    
    /**
    Date month
    */
    var month : Int {
        get {
            return getComponent(.month)
        }
    }
    
    /**
    Date weekday
    */
    var weekday : Int {
        get {
            return getComponent(.weekday)
        }
    }
    
    /**
    Date weekMonth
    */
    var weekMonth : Int {
        get {
            return getComponent(.weekOfMonth)
        }
    }
    
    
    /**
    Date days
    */
    var days : Int {
        get {
            return getComponent(.day)
        }
    }
    
    /**
    Date hours
    */
    var hours : Int {
        
        get {
            return getComponent(.hour)
        }
    }
    
    /**
    Date minuts
    */
    var minutes : Int {
        get {
            return getComponent(.minute)
        }
    }
    
    /**
    Date seconds
    */
    var seconds : Int {
        get {
            return getComponent(.second)
        }
    }
    
    /**
    Returns the value of the NSDate component
    
    - parameter component: NSCalendarUnit
    - returns: the value of the component
    */
    
    func getComponent (_ component : Calendar.Component) -> Int {
        return Calendar.current.component(component, from: self)
    }
}
