//
//  ExDouble.swift
//  G5TaxiUser
//
//  Created by Hoan Pham on 10/20/15.
//  Copyright © 2015 Hoan Pham. All rights reserved.
//

import Foundation


extension Double {
    public func toString(_ length: Int) -> String {
        return String(format: "%.\(length)f", self)
    }
}

extension Float {
    public func toString(_ length: Int) -> String {
        return String(format: "%.\(length)f", self)
    }
}

extension Double {
    public var second:  TimeInterval { return self }
    public var seconds: TimeInterval { return self }
    public var minute:  TimeInterval { return self * 60 }
    public var minutes: TimeInterval { return self * 60 }
    public var hour:    TimeInterval { return self * 3600 }
    public var hours:   TimeInterval { return self * 3600 }
}

