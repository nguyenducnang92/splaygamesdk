//
//  ExCoreLocation.swift
//  PHUtility
//
//  Created by Hoan Pham on 12/26/15.
//  Copyright © 2015 Hoan Pham. All rights reserved.
//

import Foundation

import CoreLocation

public typealias Coordinate = CLLocationCoordinate2D

public var kInvalidCoordinate: Coordinate {
    return Coordinate(latitude: 0, longitude: 0)
}

extension Coordinate: CustomStringConvertible {
    
    public var isValid: Bool {
        return CLLocationCoordinate2DIsValid(self) && latitude != 0 && longitude != 0
    }
    
    public var description: String {
        return "\(self.latitude);\(self.longitude)"
    }
	
	func toGoogleParam() -> String {
		return "\(latitude),\(longitude)"
	}
    
}


public func == (l: Coordinate, r: Coordinate) -> Bool {
    return fabs(l.latitude - r.latitude) < 0.00001 && fabs(l.longitude - r.longitude) < 0.00001
}

public func != (l: Coordinate, r: Coordinate) -> Bool {
    return fabs(l.latitude - r.latitude) >= 0.00001 && fabs(l.longitude - r.longitude) >= 0.00001
}