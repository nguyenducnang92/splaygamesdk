//
//  ExData.swift
//  STaxi
//
//  Created by Hoan Pham on 4/8/15.
//  Copyright (c) 2015 Hoan Pham. All rights reserved.
//

import Foundation
import ObjectiveC

enum DataParsingError: Error {
    case couldNotParse(type: Data.ParsableType, at: Int, debug: String)
}

var dataAssociationKey: UInt8 = 0


public extension Data {
    
    
    mutating func xor(key: Data) {
        for i in 0..<self.count {
            self[i] ^= key[i % key.count]
        }
    }
    
    func xored(key: Data) -> Data {
        var copied = Data(self.map { $0 })
        copied.xor(key: key)
        return copied
    }
   
    var uint8String: String {
        return String(describing: self.map { $0 })
    }
    
//    var hexadecimalString: String {
//        return self.map { String(format: "%02X", $0) }.reduce("", +)
//    }
   
    mutating func reverse() {
        self = reversed()
    }
    
    /// Tạo mảng `Data` mới là nghịch đảo của mảng hiện tại
    ///
    /// - returns: mảng nghịch đảo
    func reversed() -> Data {
        return Data(self.map { $0 }.reversed())
    }
    
    func checkSum() -> Int {
        return self.map { Int($0) }.reduce(0, +) & 0xff
    }
    
    
    /// Tạo `Data` với dữ liệu random và độ dài bất kỳ
    ///
    /// - parameter length: độ dài `Data`
    ///
    /// - returns: dữ liệu random
    static func random(length: Int) -> Data {
        return Data((0..<length)
                .map { _ in arc4random_uniform(UInt32(UInt8.max)) }
                .map { UInt8($0) }
        )
    }
}

public extension Data {
    
    /// Những loại dữ liệu có thể `parse` được
    ///
    /// - int8:
    /// - int16:
    /// - int32:
    /// - int64:
    /// - float32:
    /// - float64:
    /// - bool:
    /// - data:
    /// - string:
    indirect enum ParsableType {
        case int8, int16, uint16, int32, uint32, int64, float32, float64, bool, data, fixedString(ParsableType), customString(Int)
        
    }
    
    /// Vị trí đang đọc dữ liệu hiện tại
    var position: Int {
        get {
            return objc_getAssociatedObject(self, &dataAssociationKey) as? Int ?? 0
        }
        set {
            objc_setAssociatedObject(self, &dataAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_COPY_NONATOMIC)
        }
	}
    
    /// Đọc `string` trong `Data`, có quy định độ dài đứng trước
    ///
    /// - parameter size: Loại dữ liệu dùng để lưu trữ độ dài của `string`
    ///
    /// - throws:
    ///
    /// - returns: chuỗi `string`
    mutating func readString(_ type: ParsableType = .int16, _ debug: String = "") throws -> String {
        
        let length: Int
        
        switch type {
        case .int8:
            length = try readInt8(debug)
            
        case .int16:
            length = try readInt16(debug)
            
        case .int32:
            length = try readInt32(debug)
            
        default:
            length = 0
        }
        
        guard length > 0 else  { return "" }
        
        try validateLength(length, type: .fixedString(type))
        
        guard let string = String(
            data: self.subdata(in: position..<position + length),
            encoding: .utf8
            )
            else { throw DataParsingError.couldNotParse(type: .fixedString(type), at: position, debug: debug) }
        
        position += length
        
        return string
        
    }
    
    
    /// Đọc `string` trong `Data` mà không có quy định độ dài phía trước
    ///
    /// - parameter length: Khoảng cách đọc
    ///
    /// - throws:
    ///
    /// - returns: chuỗi `string`
    mutating func readString(_ size: Int, _ debug: String = "") throws -> String {
        
        try validateLength(size, type: .customString(size))
        
        guard let string = String(
            data: self.subdata(in: position..<position + size),
            encoding: .utf8
            )
            else {
                throw DataParsingError.couldNotParse(type: .customString(size), at: position, debug: debug)
            }
        
        position += size
        
        return string
    }
    
    mutating func readBool(_ debug: String = "") throws -> Bool {
        let bool = try readInt8(debug)
        guard bool == 0 || bool == 1 else {
            throw DataParsingError.couldNotParse(type: .bool, at: position, debug: debug)
        }
        return Bool(bool == 1 ? true : false)
    }
    
    mutating func readInt8(_ debug: String = "") throws -> Int {
        let size = MemoryLayout<UInt8>.size
        
        try validateLength(size, type: .int8)
//        let result: UInt8 = subdata(in: position..<position + size).withUnsafeBytes { $0.pointee }
        let result: UInt8 = subdata(in: position..<position + size).withUnsafeBytes { $0.load(as: UInt8.self) }
        position += size
        return Int(result)
    }
    
    mutating func readInt16(_ debug: String = "") throws -> Int {
        let size = MemoryLayout<Int16>.size
        
        try validateLength(size, type: .int16)
//        let result: Int16 = subdata(in: position..<position + size).withUnsafeBytes { $0.pointee }
        let result: Int16 = subdata(in: position..<position + size).withUnsafeBytes { $0.load(as: Int16.self) }
        
        position += size
        return Int(result)
    }
    
    mutating func readUInt16(_ debug: String = "") throws ->  UInt {
        let size = MemoryLayout<UInt16>.size
        
        try validateLength(size, type: .uint16)
//        let result: UInt16 = subdata(in: position..<position + size).withUnsafeBytes { $0.pointee }
        let result: UInt16 = subdata(in: position..<position + size).withUnsafeBytes { $0.load(as: UInt16.self) }
        
        position += size
        return UInt(result)
    }
    
    mutating func readInt32(_ debug: String = "") throws ->  Int {
        let size = MemoryLayout<Int32>.size
        
        try validateLength(size, type: .int32)
//        let result: Int32 = subdata(in: position..<position + size).withUnsafeBytes { $0.pointee }
        let result: Int32 = subdata(in: position..<position + size).withUnsafeBytes { $0.load(as: Int32.self) }
        
        position += size
        return Int(result)
    }
    
    mutating func readUInt32(_ debug: String = "") throws -> UInt {
        let size = MemoryLayout<UInt32>.size
        
        try validateLength(size, type: .uint32)
//        let result: UInt32 = subdata(in: position..<position + size).withUnsafeBytes { $0.pointee }
        let result: UInt32 = subdata(in: position..<position + size).withUnsafeBytes { $0.load(as: UInt32.self) }
        
        position += size
        return UInt(result)
    }
    
    mutating func readInt64(_ debug: String = "") throws -> Int64 {
        let size = MemoryLayout<Int64>.size
        
        try validateLength(size, type: .int64)
//        let result: Int64 = subdata(in: position..<position + size).withUnsafeBytes { $0.pointee }
        
        let result: Int64 = subdata(in: position..<position + size).withUnsafeBytes { $0.load(as: Int64.self) }
        
        position += size
        return Int64(result)
    }
    
    mutating func readFloat32(_ debug: String = "") throws -> Double {
        let size = MemoryLayout<Float32>.size
        
        try validateLength(size, type: .float32)
//        let result: Float32 = subdata(in: position..<position + size).withUnsafeBytes { $0.pointee }
        
        let result: Float32 = subdata(in: position..<position + size).withUnsafeBytes { $0.load(as: Float32.self) }
        
        position += size
        return Double(result)
    }
    
    mutating func readFloat64(_ debug: String = "") throws -> Double {
        let size = MemoryLayout<Double>.size
        
        try validateLength(size, type: .float64)
//        let result: Double = subdata(in: position..<position + size).withUnsafeBytes { $0.pointee }
        
        let result: Double = subdata(in: position..<position + size).withUnsafeBytes { $0.load(as: Double.self) }
        
        position += size
        return result
    }
    
    mutating func readData(_ size: Int, _ debug: String = "") throws -> Data {
        try validateLength(size, type: .data)
        let data = self.subdata(in: position..<position + size)
        position += size
        return data
    }
    
    private func validateLength(_ length: Int, error: Error) throws {
        guard position + length > 0 && position + length <= count else { throw error }
        
    }
    
    private func validateLength(_ length: Int, type: ParsableType, _ debug: String = "") throws {
        guard position + length > 0 && position + length <= count else {
            throw DataParsingError.couldNotParse(type: type, at: position, debug: debug)
        }
        
    }
    
   
}



public extension Data {
    
    mutating func appendString(_ string: String?, size: ParsableType = .int16) {
        
        guard   let string = string,
                let data = string.data(using: .utf8),
                data.count > 0
        else {
            switch size {
            case .int8:
                appendInt8(0)
                
            case .int16:
                appendInt16(0)
                
            case .int32:
                appendInt32(0)
                
            default:
                break
            }
            return
        }
        
        switch size {
        case .int8:
            appendInt8(data.count)
            append(data)
            
        case .int16:
            appendInt16(data.count)
            append(data)
            
        case .int32:
            appendInt32(data.count)
            append(data)
            
        default:
            break
        }
    }
    
    mutating func appendPredefinedString(_ string: String) {
        append(string.data(using: .utf8)!)
    }
    
    mutating func appendBool(_ value: Bool) {
        appendInt8(value ? 1 : 0)
    }
    
    mutating func appendInt8(_ value: Int) {
        
        var input = UInt8(value)
        self.append(UnsafeBufferPointer(start: &input, count: 1))
    }
    
    mutating func appendInt16(_ value: Int) {
        var input = Int16(value)
        self.append(UnsafeBufferPointer(start: &input, count: 1))
    }
    
    mutating func appendInt32(_ value: Int) {
        var input = Int32(value)
        self.append(UnsafeBufferPointer(start: &input, count: 1))
    }
    
    mutating func appendInt64(_ value: Int64) {
        var input = value
        self.append(UnsafeBufferPointer(start: &input, count: 1))
    }
    
    mutating func appendFloat32(_ value: Double) {
        var input = Float32(value)
        self.append(UnsafeBufferPointer(start: &input, count: 1))
    }
    
    mutating func appendFloat64(_ value: Double) {
        var input = Double(value)
        self.append(UnsafeBufferPointer(start: &input, count: 1))
    }
    
    mutating func appendData(_ value: Data) {
        append(value)
    }
}

