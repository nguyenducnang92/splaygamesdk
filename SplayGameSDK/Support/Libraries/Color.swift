//
//  Color.swift
//  G5TaxiUser
//
//  Created by NangND on 9/7/15.
//  Copyright © 2015 Hoan Pham. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    func alpha(_ alpha: CGFloat) -> UIColor {
        return self.withAlphaComponent(alpha)
    }
    
    convenience init(rgba: String) {
        var red:   CGFloat = 0.0
        var green: CGFloat = 0.0
        var blue:  CGFloat = 0.0
        var alpha: CGFloat = 1.0
        
//        Utility.printLog("Khởi tạo màu từ: \(rgba)")
        
        let formattedCode: String = rgba.replacingOccurrences(of: "#", with: "")
        let formattedCodeLength = formattedCode.count
        if formattedCodeLength != 3 && formattedCodeLength != 4 && formattedCodeLength != 6 && formattedCodeLength != 8 {
//            fatalError("invalid color")
//            Utility.printLog("invalid color")
            self.init(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            return
        }
        
        var hexValue: UInt32 = 0
        if Scanner(string: formattedCode).scanHexInt32(&hexValue) {
            switch formattedCodeLength {
            case 3:
                red   = CGFloat((hexValue & 0xF00) >> 8)       / 15.0
                green = CGFloat((hexValue & 0x0F0) >> 4)       / 15.0
                blue  = CGFloat(hexValue & 0x00F)              / 15.0
            case 4:
                red   = CGFloat((hexValue & 0xF000) >> 12)     / 15.0
                green = CGFloat((hexValue & 0x0F00) >> 8)      / 15.0
                blue  = CGFloat((hexValue & 0x00F0) >> 4)      / 15.0
                alpha = CGFloat(hexValue & 0x000F)             / 15.0
            case 6:
                red   = CGFloat((hexValue & 0xFF0000) >> 16)   / 255.0
                green = CGFloat((hexValue & 0x00FF00) >> 8)    / 255.0
                blue  = CGFloat(hexValue & 0x0000FF)           / 255.0
            case 8:
                red   = CGFloat((hexValue & 0xFF000000) >> 24) / 255.0
                green = CGFloat((hexValue & 0x00FF0000) >> 16) / 255.0
                blue  = CGFloat((hexValue & 0x0000FF00) >> 8)  / 255.0
                alpha = CGFloat(hexValue & 0x000000FF)         / 255.0
            default:
//                Utility.printLog("Invalid RGB string, number of characters after '#' should be either 3, 4, 6 or 8", terminator: "")

                red   = 1.0
                green = 1.0
                blue  = 1.0
                alpha = 1.0
            }
        }
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
    
    convenience init(argb: String) {
        var red:   CGFloat = 0.0
        var green: CGFloat = 0.0
        var blue:  CGFloat = 0.0
        var alpha: CGFloat = 1.0
        
//        Utility.printLog("Khởi tạo màu từ: \(argb)")
        
        let formattedCode: String = argb.replacingOccurrences(of: "#", with: "")
        let formattedCodeLength = formattedCode.count
        if formattedCodeLength != 3 && formattedCodeLength != 4 && formattedCodeLength != 6 && formattedCodeLength != 8 {
            //            fatalError("invalid color")
            
//            Utility.printLog("Khởi tạo màu từ argb:  - invalid color")
            
            self.init(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            return
        }
        
        var hexValue: UInt32 = 0
        if Scanner(string: formattedCode).scanHexInt32(&hexValue) {
            switch formattedCodeLength {
            case 3:
                red   = CGFloat((hexValue & 0xF00) >> 8)       / 15.0
                green = CGFloat((hexValue & 0x0F0) >> 4)       / 15.0
                blue  = CGFloat(hexValue & 0x00F)              / 15.0
            case 4:
                red   = CGFloat((hexValue & 0xF000) >> 12)     / 15.0
                green = CGFloat((hexValue & 0x0F00) >> 8)      / 15.0
                blue  = CGFloat((hexValue & 0x00F0) >> 4)      / 15.0
                alpha = CGFloat(hexValue & 0x000F)             / 15.0
            case 6:
                red   = CGFloat((hexValue & 0xFF0000) >> 16)   / 255.0
                green = CGFloat((hexValue & 0x00FF00) >> 8)    / 255.0
                blue  = CGFloat(hexValue & 0x0000FF)           / 255.0
//            case 8:
//                red   = CGFloat((hexValue & 0xFF000000) >> 24) / 255.0
//                green = CGFloat((hexValue & 0x00FF0000) >> 16) / 255.0
//                blue  = CGFloat((hexValue & 0x0000FF00) >> 8)  / 255.0
//                alpha = CGFloat(hexValue & 0x000000FF)         / 255.0
                
                
            case 8:
                alpha   = CGFloat((hexValue & 0xFF000000) >> 24) / 255.0
                red     = CGFloat((hexValue & 0x00FF0000) >> 16) / 255.0
                green   = CGFloat((hexValue & 0x0000FF00) >> 8)  / 255.0
                blue    = CGFloat(hexValue & 0x000000FF)         / 255.0
                
                
            default:
                //                Utility.printLog("Invalid RGB string, number of characters after '#' should be either 3, 4, 6 or 8", terminator: "")
                
                red   = 1.0
                green = 1.0
                blue  = 1.0
                alpha = 1.0
            }
        }
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
    
    
    struct Navigation {
        
        static func backgroundColor() -> UIColor { return UIColor.white }

        static func tintColor() -> UIColor { return UIColor(rgba: "#2B84FF") }
        
        /**
         Màu chính trong app
         */
        
         // Màu Cam 1D89E0
        static func mainColor() -> UIColor {
            return UIColor(red: 255.0/255, green: 162.0/255, blue: 37.0/255, alpha: 1.0)
        }
        
        // Màu Xanh lá
        static func subColor() -> UIColor {
            return UIColor(red: 131/255.0, green: 196/255.0, blue: 88/255.0, alpha: 1.0)
        } 
        

        
        static func highLightTextColor() -> UIColor { return UIColor(rgba: "#bdddf5") }

    }
    

    
    struct Segment {
        static func selectedTextColor() -> UIColor { return UIColor(rgba: "3B5998")}
        
        static func deselectedTextColor() -> UIColor { return UIColor.white }
        
        static func selectedBackgroundColor() -> UIColor { return UIColor.clear }
        
        static func deselectedBackgroundColor() -> UIColor { return UIColor.clear }
        
        static func deviderColor() -> UIColor { return UIColor.clear }
        
        static func backgroundColor() -> UIColor { return UIColor.clear }
        
        static func indicatorColor() -> UIColor { return UIColor.white }
    }
    
    struct Misc {
        static func seperatorColor() -> UIColor { return UIColor(rgba: "#dddddd") }
        
    }
    
    
    /**
     Màu nền cho các Table
     */
    struct Table {
        static func tableEmptyColor() -> UIColor { return UIColor(rgba: "f5f7fa")}
        
        static func tablePlainColor() -> UIColor { return UIColor.white}
        
        static func tableGroupColor() -> UIColor { return UIColor(rgba: "#F5F6F8")}
        
        
        static func tableGrayColor() -> UIColor { return UIColor(rgba: "#f5f7f7")}

    }
    
    
    /**
     Màu nền cho các Button
     */
    struct Button {
        
        static func whiteBackgroundColor() -> UIColor { return UIColor.white}
        
        static func normalBackgroundColor() -> UIColor { return UIColor(red: 220/255.0, green: 225/255.0, blue: 231/255.0, alpha: 1.0)}
        
        static func highlightBackgroundColor() -> UIColor { return UIColor(rgba: "2196F3")}

        
       
        //        static func cancelBackgroundColor() -> UIColor { return UIColor(rgba: "EE3233")}
        static func cancelBackgroundColor() -> UIColor { return UIColor(rgba: "FF5252")}
        
        // Màu Đỏ (Cho các button bên trái)
        static func leftBackgroundColor() -> UIColor { return UIColor(rgba: "ED5921")}
        //        static func leftBackgroundColor() -> UIColor { return UIColor(rgba: "009A3D")}
        
        
        // Màu Xanh (Cho các button bên phải và trong trường hợp footer chỉ có 1 nút)
        static func rightBackgroundColor() -> UIColor { return UIColor(rgba: "297FC8")}
        //        static func rightBackgroundColor() -> UIColor { return UIColor(rgba: "F3802C")}
        
        
        
        // Màu xám khi button bị disable
        static func disableBackgroundColor() -> UIColor { return UIColor.lightGray}
        
        //        static func disableBackgroundColor() -> UIColor { return UIColor(rgba: "F78181")}
        
        // Màu xanh nút đã gặp xe
        static func greenBackgroundColor() -> UIColor { return UIColor(rgba: "4CAF50")}
        
    }
    
    
    /**
     Màu nền cho các Text
     */
    struct Text {
        
        static func blackNormalColor() -> UIColor {return UIColor.black }
        
        static func whiteNormalColor() -> UIColor {return UIColor.white }
        
        static func grayNormalColor() -> UIColor {return UIColor(rgba: "6E6E6E")}
        
        static func blackMediumColor() -> UIColor { return UIColor.black.withAlphaComponent(0.8)}
        
        static func grayMediumColor() -> UIColor { return UIColor.gray.withAlphaComponent(0.8) }
        
        static func disableTextColor() -> UIColor { return UIColor.gray.withAlphaComponent(0.8) }
        
        // Màu text của empty table
        static func emptyColor() -> UIColor {  return UIColor.gray.withAlphaComponent(0.7)}
        
    }

    
    /**
     Màu cho các nút gradient
     */
    struct Gradient {
        
        static func firstColor() -> UIColor {
            return UIColor(rgba: "#feaa32")
        }
        
        static func secondColor() -> UIColor {
            return UIColor(rgba: "#ff9101")
        }
    }
}
