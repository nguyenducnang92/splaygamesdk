//
//  Image.swift
//  G5TaxiUser
//
//  Created by NangND on 9/7/15.
//  Copyright © 2015 Hoan Pham. All rights reserved.
//


import UIKit

struct Icon {
    
    /// Icon các Items cho Note
    struct General {
    
        static var LogoVTC: UIImage {
            
          
                return UIImage(named: "LogoVTC", in: Bundle.module, compatibleWith: nil) ?? UIImage()

        }
    }
}
