//
//  Language.swift
//  STaxi
//
//  Created by Hoan Pham on 4/8/15.
//  Copyright (c) 2015 Hoan Pham. All rights reserved.
//

import Foundation

func LocalizedString(_ key: String, comment: String? = nil) -> String {
    return Language.instance.localizedStringForKey(key)
}


enum LanguageValue: Int {
    case vietnamese = 0
    case english    = 1
    
    var title: String {
        switch self {
        case .vietnamese:
            return "Tiếng Việt"
        case .english:
            return "English"
        }
    }
    
    var code: String {
        
        switch self {
        case .vietnamese:
            return "vi"
        case .english:
            return "en"
        }
    }
    
    
    var icon: UIImage? {
        switch self {
        case .vietnamese:
            return UtilitieWebService.shareSingleton()?.getImageFromStringBase(dn_ic_language_vi)
        case .english:
            return UtilitieWebService.shareSingleton()?.getImageFromStringBase(dn_ic_language_en)
        }
    }
}


class Language {
    static let instance = Language()
    var bundle = Bundle.module
    
    func localizedStringForKey(_ key: String, comment: String? = nil) -> String {
        return bundle.localizedString(forKey: key, value: comment, table: nil)
    }
    
    func setLanguage(_ language: String) {
        
        guard let path = Bundle.module.path(forResource: language, ofType: "lproj") else { return }
        guard let newBundle = Bundle(path: path) else { return }
        bundle = newBundle
    }
    
    func setLanguage(_ language: LanguageValue) {
        var lang: String
        switch language {
        case .vietnamese:
            lang = "vi"
        case .english:
            lang = "en"
        }
        setLanguage(lang)
    }
}
