//
//  AppData.swift
//  SplayGameSDK
//
//  Created by Nguyen Nang on 11/24/20.
//  Copyright © 2020 splay. All rights reserved.
//

import Foundation
import HubjsTracker

class AppData: NSObject {
    
    static let instance = AppData()
    
    var config: ConfigAPI? = nil
    
    var isPayment: Bool = false
    
    lazy var gameID: String = {
        
        /// Check nguồn lấy
        return Bundle.main.bundleIdentifier ?? ""
    }()
    
    lazy var hubjsTracker: HubjsTracker? = {
        
        
        guard AppConfig.isUseHubJS else { return nil }
        
        return nil
        
        /// TODO: Check lại với bên HubJS
        /// TODO: Test
//        guard let url = URL(string: "https://vtcdev-analytics.hub-js.com/tracking.php") else { return nil }
//        return HubjsTracker(siteId: "2", baseURL: url)
    }()
    
    lazy var IPAddress: String = {
        return Utility.externalIPAddress()
    }()
    
    func reset() {
        config      = nil
        isPayment   = false
        IPAddress   = Utility.externalIPAddress()
    }
}
