//
//  AppConfig.swift
//  SplayGameSDK
//
//  Created by Nguyen Nang on 5/24/21.
//  Copyright © 2021 splay. All rights reserved.
//

import Foundation


public struct AppConfig {
    
    /// Sử dụng GoogleTagManager
    static let isUseGTM: Bool = true
    
    
    /// Sử dụng HubJS
    static let isUseHubJS: Bool = false
    
}
