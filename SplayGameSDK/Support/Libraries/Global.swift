//
//  Global.swift
//  G5TaxiUser
//
//  Created by NangND on 9/8/15.
//  Copyright © 2015 Hoan Pham. All rights reserved.
//
import UIKit



func crashApp(message:String) -> Never  {
    let log = "CRASH - " + message
//    print( log)
    fatalError(log)
}

func onePixel() -> CGFloat {
    return 1 / UIScreen.main.scale
}


func getBuildVersion() -> Int {
    return Int((Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as! String))!
}

