//
//  DebugUtils.h
//  wallpaper
//
//  Created by Bui Duc Viet on 1/14/16.
//  Copyright © 2016 Bui Duc Viet. All rights reserved.
//

#import <Foundation/Foundation.h>

#define DD_DEBUG [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"Dvdebug"] boolValue]
#define DVLog(frmt, ...) do{ if(DD_DEBUG) NSLog((frmt), ##__VA_ARGS__); } while(0)

