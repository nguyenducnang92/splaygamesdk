//
//  UITextField+Extended.h
//  SplayGameSDK
//
//  Created by Nguyen Duy Khanh on 12/23/19.
//  Copyright © 2019 DucViet. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITextField (Extended)

@property(retain, nonatomic)UITextField* nextTextField;

@end

NS_ASSUME_NONNULL_END
