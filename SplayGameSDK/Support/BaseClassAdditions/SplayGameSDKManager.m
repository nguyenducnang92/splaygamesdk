//
//  LiteManager.m
//  SDKLite
//
//  Created by DucViet on 6/6/17.
//  Copyright © 2017 DucViet. All rights reserved.
//

#import "SplayGameSDKManager.h"
#import "UIView+DLAdditions.h"
// #import "STPopupController.h"
#import "DNViewController.h"
#import "UtilitieWebService.h"
#import "DebugUtils.h"
#import "DNViewController.h"
#import "Account.h"
#import "Base64.h"
#import "NSString+MD5.h"
#import <OneSignal/OneSignal.h>
#import <AppsFlyerLib/AppsFlyerTracker.h>
 #import <GoogleSignIn/GoogleSignIn.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FirebaseCore/FirebaseCore.h>
#import <IAPHelper.h>
#import "CNTTTKViewController.h"
#import "UnitAnalyticHelper.h"
#import "AlertUpdateFastAccountViewController.h"
#import "ShowInforFastAccView.h"
#import "UIView+draggable.h"
#import "TWMessageBarManager.h"
//#import <FacebookSDK/FacebookSDK.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "LanguageSetting.h"
#import <IAPHelper.h>
#import "CommonUtils.h"
#import "ApiTrackingEventService.h"
// #import "STPopupController.h"



//#import <FirebaseAnalytics/FirebaseAnalytics.h>
@interface SplayGameSDKManager()

@property(nonatomic, copy) InAppPurchaseCompletionHandle inAppPurchaseCompletion;

@end
@implementation SplayGameSDKManager

+ (instancetype) sharedManager {
    static SplayGameSDKManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[[self class] alloc] init];
    });
    return sharedManager;
}

+ (void)splayGameSDKInit:(NSString *)apikeyLive APIKeySandbox:(NSString *)apikeySandbox
{
    DVLog(@"========apikeylive = %@, ========apikeysndbox = %@",apikeyLive,apikeySandbox);
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *language = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_SAVE_LANGUAGE_SETTING];
    DVLog(@"========language = %@",language);
    if ([language isEqualToString:@""] || language == nil) {
        language = [[NSLocale preferredLanguages] objectAtIndex:0];
    }
    if ([language containsString:@"vi"]) {
        [LanguageSetting setLanguage:@"vi"];
    }else{
        [LanguageSetting setLanguage:@"en"];
    }
    BOOL typeConfig = [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"Sandbox"] boolValue];
    if (typeConfig) {
        [userDefault setObject:apikeySandbox forKey:KEY_API_CONFIG_APP];
    }else{
        [userDefault setObject:apikeyLive forKey:KEY_API_CONFIG_APP];
    }
    NSString *agencyIDApp = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"AgencyID"];
    DVLog(@"agencyIDApp = %@",agencyIDApp);
    [userDefault setObject:agencyIDApp forKey:KEY_AGENCY_APP];
    [userDefault synchronize];
    [[SplayGameSDKManager sharedManager] getConfigApp];
    [[SplayGameSDKManager sharedManager] getAppStoreID];
}

/*

-(void) checkOrientation {
    if([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft)
    {
          //Do what you want in Landscape Left
    }
    else if([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight)
    {
          //Do what you want in Landscape Right
    }
    else if([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait)
    {
         //Do what you want in Portrait
    }
    else if([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
          //Do what you want in Portrait Upside Down
    }
}

-(void)sdkLogout{
    BOOL dntk = [[NSUserDefaults standardUserDefaults] boolForKey:KEY_CHECK_ACCOUNT_LOGIN];
    if (dntk) {
        [[UtilitieWebService shareSingleton] dxtkWithCompletion:^(id results, NSError *error, ResultCode resultCode) {
            if (resultCode == ResultCodeSuccess) {
                for(UIView *aView in [[UIApplication sharedApplication] keyWindow].subviews){
                    if([aView isKindOfClass:[ShowInforFastAccView class]]){
                        [aView removeFromSuperview];
                    }
                }
                NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                [userDefault setBool:NO forKey:KEY_CHECK_ACCOUNT_LOGIN];
                [userDefault setObject:@"" forKey:KEY_SAVE_USERID];
                [userDefault setObject:@"" forKey:KEY_SAVE_USERNAME];
                [userDefault setObject:@"" forKey:KEY_SAVE_USER_TOKEN];
                [userDefault synchronize];
                [[NSNotificationCenter defaultCenter] postNotificationName:ACTION_LOGOUT_USER_SUCCESS object:self];
                [[NSNotificationCenter defaultCenter] removeObserver:self name:ACTION_LOGOUT_USER_SUCCESS object:nil];
                [[GIDSignIn sharedInstance] signOut];
                DVLog(@"logout success with result =%@",results);
            }
        }];
    }else{
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:NSLocalizedString(@"Note", nil)
                                     message:@"You have not login with account"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        //Add Buttons
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:NSLocalizedString(@"Ok", nil)
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
            //Handle your yes please button action here
        }];
        [alert addAction:yesButton];
        [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alert animated:YES completion:nil];
    }
    
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation] || [[GIDSignIn sharedInstance] handleURL:url];
}
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options
{
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey] annotation:options[UIApplicationOpenURLOptionsAnnotationKey]] || [[GIDSignIn sharedInstance] handleURL:url];
}

-(void)configFacebookApplication:(UIApplication *)application launchOptions:(NSDictionary *)launchOptions
{
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
}

-(void)configPushNotificationApp:(UIApplication *)application launchOptions:(NSDictionary *)launchOptions
{
    // Replace '11111111-2222-3333-4444-0123456789ab' with your OneSignal App ID.
    NSString *oneSignalAppID = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"OneSignalAppID"];
    [OneSignal setLogLevel:ONE_S_LL_VERBOSE visualLevel:ONE_S_LL_NONE];
    if (![oneSignalAppID isEqualToString:@""]) {
        DVLog(@"onesignal appid : %@", oneSignalAppID);
        [OneSignal initWithLaunchOptions:launchOptions
                                   appId:oneSignalAppID
                handleNotificationAction:nil
                                settings:@{kOSSettingsKeyAutoPrompt: @false}];
        OneSignal.inFocusDisplayType = OSNotificationDisplayTypeNotification;
        
        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
        [OneSignal promptForPushNotificationsWithUserResponse:^(BOOL accepted) {
            DVLog(@"User accepted notifications: %d", accepted);
        }];
    }
    // Call syncHashedEmail anywhere in your iOS app if you have the user's email.
    // This improves the effectiveness of OneSignal's "best-time" notification scheduling feature.
    // [OneSignal syncHashedEmail:userEmail];
}

-(void)sdkShowLoginForm
{
    NSString *apiKeyApp = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_API_CONFIG_APP];
    NSString *agencyIDApp = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:KEY_AGENCY_APP]];
    NSDictionary *dictConfigData = [NSDictionary dictionary];
    dictConfigData = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_USERDEFAULT_DATA_CONFIG];
    if (dictConfigData == nil) {
        [self getConfigApp];
    }
    if (apiKeyApp && agencyIDApp) {
        [self checkDeviceLogin];
    }else{
        DVLog(@"Apikey or agency not enter in plist. Please check your app");
    }
}

-(void)showFromLoginView
{
    STPopupController *popupViewController = [[STPopupController alloc] initWithRootViewController:[DNViewController new]];
    popupViewController.containerView.layer.cornerRadius = 4;
    [popupViewController presentInViewController:[[[UIApplication sharedApplication] keyWindow] rootViewController]];
    popupViewController.hidesCloseButton = self.closeButton;
    popupViewController.navigationBarHidden = YES;
}

-(void)autoLogin
{
    [[UtilitieWebService shareSingleton] getConfigDefaultAppWithCompletion:^(id results, NSError *error, ResultCode resultCode) {
        if (resultCode == ResultCodeSuccess) {
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            NSDictionary *dict = [NSDictionary dictionary];
            dict = results[KEY_DATA];
            [userDefault setObject:dict forKey:KEY_USERDEFAULT_DATA_CONFIG];
            [userDefault synchronize];
            [self checkDeviceLogin];
        }else {
            NSString *messagerResult;
            if (resultCode == ResultCodeNetWorkError) {
                messagerResult = [LanguageSetting get:@"MessagerNetworkResult" alter:nil];
            }else{
                messagerResult = results[KEY_MESSAGE];
            }
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:[LanguageSetting get:@"TitleNotification" alter:nil]
                                         message:messagerResult
                                         preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:[LanguageSetting get:@"TitleButtonOK" alter:nil]
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action) {
                //Handle your yes please button action here
            }];
            [alert addAction:yesButton];
            [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alert animated:YES completion:nil];
        }
    }];
}


- (void)authenAppleWithAppleID:(NSString *)appleID email:(NSString *)email fullName:(NSString *)fullName CompletionHandle:(AuthenCompletionHandle)completionHandle {
    [[UtilitieWebService shareSingleton] authenAppleWithAppleID:appleID email:email fullName:fullName completion:^(id results, NSError *error, ResultCode resultCode) {
        if (resultCode == ResultCodeSuccess) {
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            Account *account = [[Account alloc] init];
            account.accountID = [NSString stringWithFormat:@"%@",results[KEY_DATA][KEY_USER_ID]];
            account.accountName = [NSString stringWithFormat:@"%@",results[KEY_DATA][KEY_USERNAME]];
            account.accessToken = [NSString stringWithFormat:@"%@",results[KEY_DATA][KEY_ACCESS_TOKEN]];
            NSDictionary *userLoginInfo = [NSDictionary dictionaryWithObject:account forKey:KEY_INFO_USER_LOGIN];
            [[NSNotificationCenter defaultCenter] postNotificationName:ACTION_LOGIN_USER_SUCCESS object:self userInfo:userLoginInfo];
            NSString *userID = [NSString stringWithFormat:@"%@",results[KEY_DATA][KEY_USER_ID]];
            NSString *userName = [NSString stringWithFormat:@"%@",results[KEY_DATA][KEY_USERNAME]];
            NSString *accessToken = [NSString stringWithFormat:@"%@",results[KEY_DATA][KEY_ACCESS_TOKEN]];
            [userDefault setBool:YES forKey:KEY_CHECK_ACCOUNT_LOGIN];
            [userDefault setInteger:MethoDnByTkApple forKey:KEY_SAVE_METHOD_LOGIN];
            [userDefault setObject:userID forKey:KEY_SAVE_USERID];
            [userDefault setObject:userName forKey:KEY_SAVE_USERNAME];
            [userDefault setObject:accessToken forKey:KEY_SAVE_USER_TOKEN];
            [userDefault synchronize];
            [[UtilitieWebService shareSingleton] dkPushNotification];
            [[ApiTrackingEventService shareSingleton] sendEventApiTrackingWithOrignalUrl:@"" eventName:EVEN_LOGIN_GAME_VTC eventParams:nil httpReferrer:@"" googlePlayerReferrer:@"" deepLink:@"" completion:nil];
            [[[UIApplication sharedApplication] keyWindow].rootViewController dismissViewControllerAnimated:YES completion:nil];
            [self performSelector:@selector(showWelcome) withObject:nil afterDelay:1.5];
//            [self.popupController dismiss];
            NSInteger valueFirstLogin = [results[KEY_FIRST_LOGIN] integerValue];
            if (valueFirstLogin == 1 || valueFirstLogin == 2) {
                [UnitAnalyticHelper trackEventCompleteRegisterOldOrNew:@"facebook" firstLogin:valueFirstLogin];
            }
            if (completionHandle) completionHandle(YES,1,@"Authen Success");
        }else{
            NSString *messagerResult;
            [MBProgressHUD hideHUDForView:[[[UIApplication sharedApplication] keyWindow] rootViewController].view  animated:NO];
            if (resultCode == ResultCodeNetWorkError) {
                messagerResult = [LanguageSetting get:@"MessagerNetworkResult" alter:nil];
            }else if (resultCode == RequestCodeUnknowError){
                messagerResult = [LanguageSetting get:@"error_connect_server" alter:nil];
            }else{
                messagerResult = results[KEY_MESSAGE];
            }
            [MBProgressHUD hideHUDForView:[[[UIApplication sharedApplication] keyWindow] rootViewController].view animated:NO];
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:[LanguageSetting get:@"TitleNotification" alter:nil]
                                         message:messagerResult
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:[LanguageSetting get:@"TitleButtonOK" alter:nil]
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action) {
                                            //Handle your yes please button action here
                                        }];
            [alert addAction:yesButton];
            [[[[UIApplication sharedApplication] keyWindow] rootViewController]  presentViewController:alert animated:YES completion:nil];
            if (completionHandle) completionHandle(NO,2,@"Authen Fail");
        }
    }];
}

- (void)showWelcome{
    [[TWMessageBarManager sharedInstance] showMessageWithTitle:[LanguageSetting get:@"text_welcome" alter:nil]
                                                   description:[[NSUserDefaults standardUserDefaults] objectForKey:KEY_SAVE_USERNAME]
                                                          type:TWMessageBarMessageTypeInfo
                                               statusBarHidden:YES
                                                      callback:nil];
}


-(void)getConfigApp
{
    [[UtilitieWebService shareSingleton] getConfigDefaultAppWithCompletion:^(id results, NSError *error, ResultCode resultCode) {
        if (resultCode == ResultCodeSuccess) {
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            NSDictionary *dict = [NSDictionary dictionary];
            dict = results[KEY_DATA];
            [userDefault setObject:dict forKey:KEY_USERDEFAULT_DATA_CONFIG];
            [userDefault synchronize];
        }else {
            NSString *messagerResult;
            
            if (resultCode == ResultCodeNetWorkError) {
                messagerResult = [LanguageSetting get:@"MessagerNetworkResult" alter:nil];
            }else if (resultCode == RequestCodeUnknowError){
                messagerResult = [LanguageSetting get:@"error_connect_server" alter:nil];
            }
            else{
                messagerResult = results[KEY_MESSAGE];
            }
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:[LanguageSetting get:@"TitleNotification" alter:nil]
                                         message:messagerResult
                                         preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:[LanguageSetting get:@"TitleButtonOK" alter:nil]
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action) {
                //Handle your yes please button action here
            }];
            [alert addAction:yesButton];
            [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alert animated:YES completion:nil];
        }
    }];
}

-(void)getAppStoreID
{
    [[UtilitieWebService shareSingleton] getAppStoreId:^(id results, NSError *error, ResultCode resultCode) {
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        if (resultCode == ResultCodeSuccess) {
            NSMutableArray *topLevelArray = results[@"results"];
            if ((topLevelArray.count > 0) || (!topLevelArray)) {
                NSDictionary *dict = topLevelArray[0];
                NSString *trackId = dict[@"trackId"];
                NSString *appID = [NSString stringWithFormat:@"id%@",trackId];
                [userDefault setObject:appID forKey:KEY_APPID];
            } else {
                [userDefault setObject:@"" forKey:KEY_APPID];
            }
        }else {
            [userDefault setObject:@"" forKey:KEY_APPID];
        }
        [userDefault synchronize];
    }];
}



-(void)checkDeviceLogin
{
    [[UtilitieWebService shareSingleton] checkDeviceWithCompletion:^(id results, NSError *error, ResultCode resultCode) {
        //
        if (resultCode == ResultCodeSuccess) {
            DVLog(@"Check device dang nhap game %@",results[KEY_DATA]);
            [self performSelector:@selector(showWelcome) withObject:nil afterDelay:1.5];
            [UnitAnalyticHelper analyticFirebaseLoginEven:EVEN_SPLAY_ACCOUNT_LOGIN];
            [UnitAnalyticHelper analyticFacebookLoginEven:EVEN_SPLAY_ACCOUNT_LOGIN];
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            Account *account = [[Account alloc] init];
            account.accountID = [NSString stringWithFormat:@"%@",results[KEY_DATA][KEY_USER_ID]];
            account.accountName = [NSString stringWithFormat:@"%@",results[KEY_DATA][KEY_USERNAME]];
            account.accessToken = [NSString stringWithFormat:@"%@",results[KEY_DATA][KEY_ACCESS_TOKEN]];
            NSDictionary *userLoginInfo = [NSDictionary dictionaryWithObject:account forKey:KEY_INFO_USER_LOGIN];
            [[NSNotificationCenter defaultCenter] postNotificationName:ACTION_LOGIN_USER_SUCCESS object:self userInfo:userLoginInfo];
            NSString *userID = [NSString stringWithFormat:@"%@",results[KEY_DATA][KEY_USER_ID]];
            NSString *userName = [NSString stringWithFormat:@"%@",results[KEY_DATA][KEY_USERNAME]];
            NSString *accessToken = [NSString stringWithFormat:@"%@",results[KEY_DATA][KEY_ACCESS_TOKEN]];
            [userDefault setBool:YES forKey:KEY_CHECK_ACCOUNT_LOGIN];
            [userDefault setObject:userID forKey:KEY_SAVE_USERID];
            [userDefault setObject:userName forKey:KEY_SAVE_USERNAME];
            [userDefault setObject:accessToken forKey:KEY_SAVE_USER_TOKEN];
            [userDefault synchronize];
            [[ApiTrackingEventService shareSingleton] sendEventApiTrackingWithOrignalUrl:@"" eventName:EVEN_LOGIN_GAME_VTC eventParams:nil httpReferrer:@"" googlePlayerReferrer:@"" deepLink:@"" completion:nil];
            NSDictionary *dictDataconfig = [NSDictionary dictionary];
            dictDataconfig = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_USERDEFAULT_DATA_CONFIG];
            //            NSInteger methodLogin = (NSInteger)[[NSUserDefaults standardUserDefaults] integerForKey:KEY_SAVE_METHOD_LOGIN];
            if ([userName containsString:@"_fastlogin_"]) {
                STPopupController *cntttkFastViewController = [[STPopupController alloc] initWithRootViewController:[AlertUpdateFastAccountViewController new]];
                cntttkFastViewController.containerView.layer.cornerRadius = 4;
                [cntttkFastViewController presentInViewController:[[[UIApplication sharedApplication] keyWindow] rootViewController]];
                cntttkFastViewController.hidesCloseButton = self.closeButton;
                cntttkFastViewController.navigationBarHidden = YES;
            }
            
        }else{
            [self showFromLoginView];
        }
    }];
}


 
-(void)sendDataInappPuschaseToSeverWithPartnerInfo:(NSString*)partnerInfo transaction:(SKPaymentTransaction*)transaction skProduct:(SKProduct*)skproduct
{
    NSData *dataReceipt = [NSData dataWithContentsOfURL:[[NSBundle mainBundle] appStoreReceiptURL]];
    NSString *receipt = [dataReceipt base64EncodedStringWithOptions:0];
    NSString *strpartnerinfo = partnerInfo;
    NSString *accessToken = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:KEY_SAVE_USER_TOKEN]];
    
    [[UtilitieWebService shareSingleton] sendDataInAppPurchaseToSeverWithAccessToken:accessToken receiptData:receipt partnerInfo:strpartnerinfo completion:^(id results, NSError *error, ResultCode resultCode) {
        //code
        NSString *messager = [NSString stringWithFormat:@"%@",results[KEY_MESSAGE]];
        switch (resultCode) {
            case ResultCodeSuccess:
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
//                [[SplayGameSDKManager sharedManager] logPurchaseEvent:[NSString stringWithFormat:@"%@",skproduct.price] productId:skproduct.productIdentifier paymentInapp:@"IAP"];
                [IAPHelper shared].production = NO;
                [CommonUtils showAlertMsg:[[UIApplication sharedApplication] keyWindow].rootViewController title:[LanguageSetting get:@"TitleNotification" alter:nil] message:messager];
                if (_inAppPurchaseCompletion != nil){
                    _inAppPurchaseCompletion(YES,0,messager);
                }
                break;
            case ResultCodeDataError:
            case ResultCodeDataEmpty:
            case ResultCodeConnectionError:
            case ResultCodeNetWorkError:
            default:
                [CommonUtils showAlertMsg:[[UIApplication sharedApplication] keyWindow].rootViewController title:[LanguageSetting get:@"TitleNotification" alter:nil] message:messager];
                if (_inAppPurchaseCompletion != nil){
                    _inAppPurchaseCompletion(NO,1,messager);
                }
                break;
        }
    }];
}
 


-(void)buyInAppItemPartnerInfo:(NSString *)partnerinfo andProductID:(NSString *)productID CompletionHandle:(InAppPurchaseCompletionHandle)completionHandle
{
    if (!productID || !partnerinfo || [productID isEqualToString:@""] || [partnerinfo isEqualToString:@""] ) {
        [CommonUtils showAlertMsg:[[UIApplication sharedApplication] keyWindow].rootViewController title:[LanguageSetting get:@"TitleNotification" alter:nil] message:[LanguageSetting get:@"invalid_purchase_information" alter:nil]];
        return;
    }
    self.inAppPurchaseCompletion = completionHandle;
    [[UtilitieWebService shareSingleton] getProductInfo:^(id results, NSError *error, ResultCode resultCode) {
        if ((results[KEY_DATA]) == 0 || !(results[KEY_DATA])) {
            [CommonUtils showAlertMsg:[[UIApplication sharedApplication] keyWindow].rootViewController title:[LanguageSetting get:@"TitleNotification" alter:nil] message:[LanguageSetting get:@"no_available_product" alter:nil]];
            return;
        }
        if (resultCode == ResultCodeNetWorkError) {
            [CommonUtils showAlertMsg:[[UIApplication sharedApplication] keyWindow].rootViewController title:[LanguageSetting get:@"TitleNotification" alter:nil] message:[LanguageSetting get:@"MessagerNetworkResult" alter:nil]];
            return;
        }else if (resultCode == RequestCodeUnknowError){
            [CommonUtils showAlertMsg:[[UIApplication sharedApplication] keyWindow].rootViewController title:[LanguageSetting get:@"TitleNotification" alter:nil] message:[LanguageSetting get:@"error_connect_server" alter:nil]];
            return;
        }else if (resultCode == ResultCodeDataEmpty){
            [CommonUtils showAlertMsg:[[UIApplication sharedApplication] keyWindow].rootViewController title:[LanguageSetting get:@"TitleNotification" alter:nil] message:[LanguageSetting get:@"no_available_product_-2" alter:nil]];
            return;
        }else if (resultCode == ResultCodeDataError){
                   [CommonUtils showAlertMsg:[[UIApplication sharedApplication] keyWindow].rootViewController title:[LanguageSetting get:@"TitleNotification" alter:nil] message:[LanguageSetting get:@"no_available_product_1" alter:nil]];
                   return;
        }else if (resultCode == ResultCodeSuccess){
            [results[KEY_DATA] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                NSString *producIDItem = obj[@"productid"];
                if ([productID isEqualToString:producIDItem]) {
                    *stop = YES;
                    NSSet *dataSet = [[NSSet alloc] initWithObjects:productID, nil];
                    [[IAPHelper shared] setProductIdentifiers:dataSet];
                    [[IAPHelper shared] requestProductsWithCompletion:^(SKProductsRequest *request, SKProductsResponse *response) {
                        if (response > 0) {
                            SKProduct *__skProduct = [[response products] firstObject];
                            DVLog(@"produc_________ %@",__skProduct);
                            [[IAPHelper shared] buyProduct:__skProduct onCompletion:^(SKPaymentTransaction *transaction) {
                                if (transaction.error) {
                                    DVLog(@"Fail %@",[transaction.error localizedDescription]);
                                    if (self->_inAppPurchaseCompletion != nil){
                                        self->_inAppPurchaseCompletion(NO,2,@"Inapp Purchase items Fail");
                                    }
                                    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                                    return;
                                }
                                switch (transaction.transactionState) {
                                    case SKPaymentTransactionStateFailed:
                                        [CommonUtils showAlertMsg:[[UIApplication sharedApplication] keyWindow].rootViewController title:[LanguageSetting get:@"TitleNotification" alter:nil] message:[LanguageSetting get:@"payment_failed" alter:nil]];
                                        if (self->_inAppPurchaseCompletion != nil){
                                            [CommonUtils showAlertMsg:[[UIApplication sharedApplication] keyWindow].rootViewController title:[LanguageSetting get:@"TitleNotification" alter:nil] message:[LanguageSetting get:@"payment_failed" alter:nil]];
                                            self->_inAppPurchaseCompletion(NO,2,@"Inapp Purchase items Fail");
                                        }
                                        [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                                        break;
                                    case SKPaymentTransactionStatePurchased:
                                        [self sendDataInappPuschaseToSeverWithPartnerInfo:partnerinfo transaction:transaction skProduct:__skProduct];
                                        break;
                                    case SKPaymentTransactionStateRestored:
                                        [self sendDataInappPuschaseToSeverWithPartnerInfo:partnerinfo transaction:transaction skProduct:__skProduct];
                                        break;
                                    default:
                                        break;
                                }
                            }];
                        }else{
                            [CommonUtils showAlertMsg:[[UIApplication sharedApplication] keyWindow].rootViewController title:[LanguageSetting get:@"TitleNotification" alter:nil] message:[LanguageSetting get:@"payment_failed" alter:nil]];
                            DVLog(@"Inapp not success!.");
                            if (self->_inAppPurchaseCompletion != nil){
                                [CommonUtils showAlertMsg:[[UIApplication sharedApplication] keyWindow].rootViewController title:[LanguageSetting get:@"TitleNotification" alter:nil] message:[LanguageSetting get:@"payment_failed" alter:nil]];
                                self->_inAppPurchaseCompletion(NO,2,@"In-app Purchase items Fail");
                            }
                        }
                    }];
                }
            }];
        } else {
            [CommonUtils showAlertMsg:[[UIApplication sharedApplication] keyWindow].rootViewController title:[LanguageSetting get:@"TitleNotification" alter:nil] message:[LanguageSetting get:@"MessagerUnknowError" alter:nil]];
            return;
        }
    }];
}
 
   

-(void)reStoreInAppPurchase
{
    IAPHelper *iapHelper = [[IAPHelper alloc] init];
    [iapHelper restoreProductsWithCompletion:^(SKPaymentQueue *payment, NSError *error) {
        DVLog(@"Retore purchase success");
    }];
}
 


-(void)configFireBase
{
    [FIRApp configure];
    NSString *path = [[NSBundle mainBundle] pathForResource:@"GoogleService-Info"
                                                     ofType:@"plist"];
    NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:path];
    NSString *clientId = [dict objectForKey:@"CLIENT_ID"];
    
    // Setup the Sign-In instance.
    GIDSignIn *signIn = [GIDSignIn sharedInstance];
    signIn.clientID = clientId;
}

-(void)configAppsFlyer
{
    NSString *_appsFlyerDevKey = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"AppsFlyerDevKey"];
    NSString *_appleAppID = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"AppleAppID"];
    [AppsFlyerTracker sharedTracker].isDebug = true;
    DVLog(@"key = %@, appid = %@",_appsFlyerDevKey,_appleAppID);
    if (![_appleAppID isEqualToString:@""] && ![_appsFlyerDevKey isEqualToString:@""] ) {
        [AppsFlyerTracker sharedTracker].appsFlyerDevKey = _appsFlyerDevKey;
        [AppsFlyerTracker sharedTracker].appleAppID = _appleAppID;
    }
}


 
//Call function in didFinishLaunchingWithOptions appdelegate
-(void)configAppsFlyerTrackUninstall:(UIApplication *)application launchOptions:(NSDictionary *)launchOptions
{
    // AppsFlyer SDK Initialization
    // Register for Push Notifications
    UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                    UIUserNotificationTypeBadge |
                                                    UIUserNotificationTypeSound);
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                                             categories:nil];
    [application registerUserNotificationSettings:settings];
    [application registerForRemoteNotifications];
    
    [application setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalMinimum];
    application.applicationIconBadgeNumber = 0;
}
 


-(void) trackFirstOpenEvent {
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"first_open"]) {
        [FIRAnalytics logEventWithName:EVEN_COMPLETE_FIRST_OPEN_VTC parameters:nil];
        [FBSDKAppEvents logEvent:EVEN_COMPLETE_FIRST_OPEN_VTC ];
        [[AppsFlyerTracker sharedTracker] trackEvent:EVEN_COMPLETE_FIRST_OPEN_VTC withValues:nil];
        [[ApiTrackingEventService shareSingleton] sendEventApiTrackingWithOrignalUrl:@"" eventName:EVEN_COMPLETE_FIRST_OPEN_VTC eventParams:nil httpReferrer:@"" googlePlayerReferrer:@"" deepLink:@"" completion:nil];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"first_open"];
    }
}

//Call function in applicationDidBecomeActive appdelagate
-(void)trackingInstallAppWithAppsflyer
{
    // Track Installs, updates & sessions(app opens) (You must include this API to enable tracking)
    [[AppsFlyerTracker sharedTracker] trackAppLaunch];
}

   

-(void)appsFlyerRegisterUninstallWithDeviceToken:(NSData*)deviceToken
{
    [[AppsFlyerTracker sharedTracker] registerUninstall:deviceToken];
    [AppsFlyerTracker sharedTracker].useUninstallSandbox = NO;
}
//Call function in applicationDidBecomeActive appdelagate
-(void)installEvenFacebookApp
{
    //Log the application activation
    [FBSDKAppEvents activateApp];
    
}
 

//- (void)showWelcome{
//    [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Xin chào!"
//                                                   description:[[NSUserDefaults standardUserDefaults] objectForKey:KEY_SAVE_USERNAME]
//                                                          type:TWMessageBarMessageTypeInfo
//                                               statusBarHidden:YES
//                                                      callback:nil];
//}



- (void)trackAchievedLevelEvent:(NSString *)level {
    NSDictionary *params = @{FBSDKAppEventParameterNameLevel : level};
    [FBSDKAppEvents logEvent:FBSDKAppEventNameAchievedLevel parameters:params];
    NSDictionary *paramsFir = @{kFIRParameterLevel : level};
    [FIRAnalytics logEventWithName:kFIREventLevelUp parameters:paramsFir];
    NSDictionary *paramAppsFlyer = @{AFEventParamLevel: level};
    [[AppsFlyerTracker sharedTracker] trackEvent:AFEventLevelAchieved withValues:paramAppsFlyer];
}

- (void)trackAchievedFirstLevelEvent {
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"first_level"]) {
        [FBSDKAppEvents logEvent:EVEN_COMPLETE_FIRST_LEVEL_VTC parameters:nil];
        [FIRAnalytics logEventWithName:kFIREventTutorialBegin parameters:nil];
        [FIRAnalytics logEventWithName:EVEN_COMPLETE_FIRST_LEVEL_VTC parameters:nil];
        [[AppsFlyerTracker sharedTracker] trackEvent:EVEN_COMPLETE_FIRST_LEVEL_VTC withValues:nil];
        [[ApiTrackingEventService shareSingleton] sendEventApiTrackingWithOrignalUrl:@"" eventName:EVEN_COMPLETE_FIRST_LEVEL_VTC eventParams:nil httpReferrer:@"" googlePlayerReferrer:@"" deepLink:@"" completion:nil];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"first_level"];
    }
}

- (void)trackCompletedFirstUpdateEvent {
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"first_update_resource"]) {
        [FIRAnalytics logEventWithName:EVEN_COMPLETE_FIRST_UPDATE_VTC parameters:nil];
        [FBSDKAppEvents logEvent:EVEN_COMPLETE_FIRST_UPDATE_VTC];
        [[AppsFlyerTracker sharedTracker] trackEvent:EVEN_COMPLETE_FIRST_UPDATE_VTC withValues:nil];
        [[AppsFlyerTracker sharedTracker] trackEvent:AFEventUpdate withValues:nil];
        [[ApiTrackingEventService shareSingleton] sendEventApiTrackingWithOrignalUrl:@"" eventName:EVEN_COMPLETE_FIRST_UPDATE_VTC eventParams:nil httpReferrer:@"" googlePlayerReferrer:@"" deepLink:@"" completion:nil];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"first_update_resource"];
    }
}

- (void)trackCompleteTutorialEvent {
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"complete_tutorial_event"]) {
        [FIRAnalytics logEventWithName:EVEN_COMPLETE_TUTORIAL_VTC parameters:nil];
        [FIRAnalytics logEventWithName:kFIREventTutorialComplete parameters:nil];
        [FBSDKAppEvents logEvent:EVEN_COMPLETE_TUTORIAL_VTC];
        [FBSDKAppEvents logEvent:FBSDKAppEventNameCompletedTutorial];
        [[AppsFlyerTracker sharedTracker] trackEvent:AFEventTutorial_completion withValues:nil];
        [[AppsFlyerTracker sharedTracker] trackEvent:EVEN_COMPLETE_TUTORIAL_VTC withValues:nil];
        [[ApiTrackingEventService shareSingleton] sendEventApiTrackingWithOrignalUrl:@"" eventName:EVEN_COMPLETE_TUTORIAL_VTC eventParams:nil httpReferrer:@"" googlePlayerReferrer:@"" deepLink:@"" completion:nil];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"complete_tutorial_event"];
    }
}

- (void)trackCompleteOverTutorialEvent {
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"complete_over_tutorial_event"]) {
        [FIRAnalytics logEventWithName:EVEN_COMPLETE_OVER_TUTORIAL_VTC parameters:nil];
        [FBSDKAppEvents logEvent:EVEN_COMPLETE_OVER_TUTORIAL_VTC];
        [[AppsFlyerTracker sharedTracker] trackEvent:EVEN_COMPLETE_OVER_TUTORIAL_VTC withValues:nil];
        [[ApiTrackingEventService shareSingleton] sendEventApiTrackingWithOrignalUrl:@"" eventName:EVEN_COMPLETE_OVER_TUTORIAL_VTC eventParams:nil httpReferrer:@"" googlePlayerReferrer:@"" deepLink:@"" completion:nil];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"complete_over_tutorial_event"];
    }
}

- (void)trackCompleteLoadServerList {
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"complete_load_server_list_event"]) {
        [FIRAnalytics logEventWithName:EVEN_COMPLETE_LOAD_SERVER_LIST_VTC parameters:nil];
        [FBSDKAppEvents logEvent:EVEN_COMPLETE_LOAD_SERVER_LIST_VTC];
        [[AppsFlyerTracker sharedTracker] trackEvent:EVEN_COMPLETE_LOAD_SERVER_LIST_VTC withValues:nil];
        [[ApiTrackingEventService shareSingleton] sendEventApiTrackingWithOrignalUrl:@"" eventName:EVEN_COMPLETE_LOAD_SERVER_LIST_VTC eventParams:nil httpReferrer:@"" googlePlayerReferrer:@"" deepLink:@"" completion:nil];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"complete_load_server_list_event"];
    }
}

- (void)trackCompleteLoadSDK {
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"complete_load_sdk_event"]) {
        [FIRAnalytics logEventWithName:EVEN_COMPLETE_LOAD_SDK_VTC parameters:nil];
        [FBSDKAppEvents logEvent:EVEN_COMPLETE_LOAD_SDK_VTC];
        [[AppsFlyerTracker sharedTracker] trackEvent:EVEN_COMPLETE_LOAD_SDK_VTC withValues:nil];
        [[ApiTrackingEventService shareSingleton] sendEventApiTrackingWithOrignalUrl:@"" eventName:EVEN_COMPLETE_LOAD_SDK_VTC eventParams:nil httpReferrer:@"" googlePlayerReferrer:@"" deepLink:@"" completion:nil];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"complete_load_sdk_event"];
    }
}

- (void)trackCompleteLoadRole{
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"complete_load_role_event"]) {
        [FIRAnalytics logEventWithName:EVEN_COMPLETE_LOAD_ROLE_VTC parameters:nil];
        [FBSDKAppEvents logEvent:EVEN_COMPLETE_LOAD_ROLE_VTC];
        [[AppsFlyerTracker sharedTracker] trackEvent:EVEN_COMPLETE_LOAD_ROLE_VTC withValues:nil];
        [[ApiTrackingEventService shareSingleton] sendEventApiTrackingWithOrignalUrl:@"" eventName:EVEN_COMPLETE_LOAD_ROLE_VTC eventParams:nil httpReferrer:@"" googlePlayerReferrer:@"" deepLink:@"" completion:nil];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"complete_load_role_event"];
        }
}

- (void)trackCompleteCreateRole{
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"complete_create_role_event"]) {
        [FIRAnalytics logEventWithName:EVEN_COMPLETE_CREATE_ROLE_VTC parameters:nil];
        [FBSDKAppEvents logEvent:EVEN_COMPLETE_CREATE_ROLE_VTC];
        [[AppsFlyerTracker sharedTracker] trackEvent:EVEN_COMPLETE_CREATE_ROLE_VTC withValues:nil];
        [[ApiTrackingEventService shareSingleton] sendEventApiTrackingWithOrignalUrl:@"" eventName:EVEN_COMPLETE_CREATE_ROLE_VTC eventParams:nil httpReferrer:@"" googlePlayerReferrer:@"" deepLink:@"" completion:nil];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"complete_create_role_event"];
        }
}



- (void)logStartTutorialEvent:(NSString *)userId success:(BOOL)success {
    NSDictionary *params = @{
        FBSDKAppEventParameterNameContentID : userId,
        FBSDKAppEventParameterNameSuccess : @(success ? 1 : 0)
    };
    NSDictionary *paramsFir = @{
        kFIRParameterCharacter : userId,
        kFIRParameterSuccess : @(success ? 1 : 0)
    };
    [FBSDKAppEvents logEvent:@"fb_mobile_tutorial_start" parameters:params];
    [FIRAnalytics logEventWithName:kFIREventTutorialBegin parameters:paramsFir];
}


- (void)logCompletedTutorialEvent:(NSString *)userId success:(BOOL)success {
    NSDictionary *params = @{
        FBSDKAppEventParameterNameContentID : userId,
        FBSDKAppEventParameterNameSuccess : @(success ? 1 : 0)
    };
    [FBSDKAppEvents
     logEvent:FBSDKAppEventNameCompletedTutorial
     parameters:params];
    NSDictionary *paramsFir = @{
        kFIRParameterCharacter : userId,
        kFIRParameterSuccess : @(success ? 1 : 0)
    };
    [FIRAnalytics logEventWithName:kFIREventTutorialComplete parameters:paramsFir];
}


- (void)logInitiatedCheckoutEvent:(NSString *)productId contentType:(NSString *)contentType {
    NSDictionary *params = @{
        FBSDKAppEventParameterNameContentID : productId,
        FBSDKAppEventParameterNameContentType : contentType,
        FBSDKAppEventParameterNameCurrency : @"VND"
    };
    [FBSDKAppEvents logEvent:FBSDKAppEventNameInitiatedCheckout parameters:params];
    NSDictionary *paramsFir = @{
        kFIRParameterItemID : productId,
        kFIRParameterItemCategory : contentType,
        kFIRParameterCurrency: @"VND"
    };
    [FIRAnalytics logEventWithName:kFIREventBeginCheckout parameters:paramsFir];
}



- (void)logPurchaseEvent:(NSString *)purchaseAmount eventName:(NSString *)eventName productId:(NSString *)productId paymentInapp:(NSString *) paymentInapp {
//    NSDictionary *params = @{
//        FBSDKAppEventParameterNameContentID : productId,
//        FBSDKAppEventParameterNameContentType : paymentInapp,
//    };
//    [FBSDKAppEvents logPurchase:[purchaseAmount doubleValue] currency:@"VND" parameters: params];
    
    NSDictionary *paramsFir = @{
        kFIRParameterItemID : productId,
        kFIRParameterItemCategory : paymentInapp,
        kFIRParameterCurrency: @"VND",
        kFIRParameterValue:@([purchaseAmount doubleValue])
    };
    [FIRAnalytics logEventWithName:eventName parameters:paramsFir];
    
}

- (void)logStartUpdateResourceEvent:(NSString *)userId success:(BOOL)success
{
    NSDictionary *params = @{
        FBSDKAppEventParameterNameContentID : userId,
        FBSDKAppEventParameterNameSuccess : @(success ? 1 : 0)
    };
    [FBSDKAppEvents
     logEvent:@"fb_mobile_update_resource_start"
     parameters:params];
    NSDictionary *paramsFir = @{
        kFIRParameterCharacter : userId,
        kFIRParameterSuccess : @(success ? 1 : 0)
    };
    [FIRAnalytics logEventWithName:@"update_resource_start" parameters:paramsFir];
}

- (void)logCompletedUpdateResourceEvent:(NSString *)userId success:(BOOL)success
{
    NSDictionary *params = @{
        FBSDKAppEventParameterNameContentID : userId,
        FBSDKAppEventParameterNameSuccess : @(success ? 1 : 0)
    };
    [FBSDKAppEvents
     logEvent:@"fb_mobile_update_resource_completed"
     parameters:params];
    NSDictionary *paramsFir = @{
        kFIRParameterCharacter : userId,
        kFIRParameterSuccess : @(success ? 1 : 0)
    };
    [FIRAnalytics logEventWithName:@"update_resource_complete" parameters:paramsFir];
}

- (void)logSpentCreditsEvent:(NSString*)contentId virtualCurrencyName:(NSString*)virtualCurrencyName totalValue:(double)totalValue {
    NSDictionary *params = @{
        FBSDKAppEventParameterNameContentID : contentId,
        FBSDKAppEventParameterNameContentType : virtualCurrencyName
    };
    [FBSDKAppEvents logEvent: FBSDKAppEventNameSpentCredits valueToSum: totalValue parameters: params];
    
    NSDictionary *paramsFir = @{
        kFIRParameterItemID : contentId,
        kFIRParameterValue : @(totalValue),
        kFIRParameterVirtualCurrencyName : virtualCurrencyName,
    };
    [FIRAnalytics logEventWithName:kFIREventSpendVirtualCurrency parameters:paramsFir];
}
 
 


#pragma mark- FACEBOOK
-(void)inviteFacebookWithCompletionHandle:(InviteFriendCompleteHandle)completionHandle
{
    //    [self logoutFacebook];
    [[UtilitieWebService shareSingleton] getListExcludeIdsFromServerCompletion:^(id results, NSError *error, ResultCode resultCode) {
        __block NSMutableDictionary *params = [NSMutableDictionary dictionary];
        if (resultCode == ResultCodeSuccess) {
            DVLog(@"__________exclude 13 %@",results);
            NSString *strExcludeIds = [NSString stringWithFormat:@"%@",results[KEY_DATA_FACEBOOK]];
            params = [NSMutableDictionary dictionaryWithObjectsAndKeys:strExcludeIds,@"exclude_ids",nil];
        }else{
            params = nil;
        }
    }];
    
}

-(void)publishFacebookFeedWithLinkShare:(NSString *)linkshare handler:(void (^)(BOOL))completionHandler
{
    self.completionHandler = [completionHandler copy];
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentURL = [NSURL URLWithString:linkshare];
    UIViewController *parentViewController = [[[UIApplication sharedApplication] delegate] window].rootViewController;
    FBSDKShareDialog *shareDialog = [[FBSDKShareDialog alloc] init];
    [shareDialog setMode:FBSDKShareDialogModeNative];
    [shareDialog setShareContent:content];
    [shareDialog setFromViewController:parentViewController];
    shareDialog.delegate = (id<FBSDKSharingDelegate>)self;
    if (![shareDialog canShow]) {
        [[NSUserDefaults standardUserDefaults] setObject:@"WebFacebook" forKey:@"WebFacebook"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [shareDialog setMode:FBSDKShareDialogModeFeedBrowser];
    }
    [shareDialog show];
}
- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results {
    NSLog(@"results share = %@",results);
    NSString *strPostID = [NSString stringWithFormat:@"%@",[results valueForKey:@"postId"]];
    if ([strPostID isEqualToString:@""]) {
        self.completionHandler(NO);
    }
    else {
        self.completionHandler(YES);
    }
    
}

- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error {
    NSLog(@"error");
    self.completionHandler(NO);
}

- (void)sharerDidCancel:(id<FBSDKSharing>)sharer {
    NSLog(@"cancel");
    self.completionHandler(NO);
}
 


#pragma mark-END FACEBOOK
-(void)showStatus
{
    [[UtilitieWebService shareSingleton] getConfigDefaultAppWithCompletion:^(id results, NSError *error, ResultCode resultCode) {
        if (resultCode == ResultCodeSuccess) {
            NSString *_messagerStatus;
            _messagerStatus = results[KEY_DATA][KEY_STATUS_APP];
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:NSLocalizedString(@"Thông báo", nil)
                                         message:_messagerStatus
                                         preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:NSLocalizedString(@"Đồng ý", nil)
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action) {
                //Handle your yes please button action here
            }];
            [alert addAction:yesButton];
            [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alert animated:YES completion:nil];
        }
    }];
}
 
    
 
- (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val = [[kv objectAtIndex:1]
                         stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [params setObject:val forKey:[kv objectAtIndex:0]];
    }
    return params;
}


 
-(void)getAppStatusWithCompletionHandler:(CompletionHandlerResult)completionHandle
{
    [self getDataConfigWithCondition:@"status" completionHandle:^(NSString *completionResult) {
        //
        NSLog(@"________Data handle %@",completionResult);
        if (completionHandle) completionHandle(completionResult);
        
    }];
}
-(void)getAvatarWithCompletionHandler:(void (^)(NSString *))completionHandler
{
    self.completionHandleAvatar = [completionHandler copy];
    UIViewController *parentViewController = [[[UIApplication sharedApplication] delegate] window].rootViewController;
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = (id <UINavigationControllerDelegate, UIImagePickerControllerDelegate>)self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [picker setModalPresentationStyle: UIModalPresentationOverCurrentContext];
    [parentViewController presentViewController:picker animated:YES completion:NULL];
}

-(void)getDataConfigWithCondition:(NSString *)condition completionHandle:(CompletionHandlerResult)comletionHandle
{
    [[UtilitieWebService shareSingleton] getConfigDefaultAppWithCompletion:^(id results, NSError *error, ResultCode resultCode) {
        if (resultCode == ResultCodeSuccess) {
            NSString *_messagerStatus;
            if ([condition isEqualToString:@"rating"]) {
                _messagerStatus = [NSString stringWithFormat:@"%@",results[KEY_DATA][KEY_RATING]];
            }else if ([condition isEqualToString:@"status"]){
                _messagerStatus = [NSString stringWithFormat:@"%@",results[KEY_DATA][KEY_STATUS_APP]];
            }else{
                _messagerStatus = [NSString stringWithFormat:@"%@",results[KEY_DATA][KEY_REDMINED]];
            }
            if (comletionHandle) comletionHandle(_messagerStatus);
            
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:NSLocalizedString(@"Thông báo", nil)
                                         message:_messagerStatus
                                         preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:NSLocalizedString(@"Đồng ý", nil)
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action) {
                //Handle your yes please button action here
            }];
            [alert addAction:yesButton];
            [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alert animated:YES completion:nil];
        }else{
            if (comletionHandle) comletionHandle(@"Error");
        }
    }];
}
 
  */

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //You can retrieve the actual UIImage
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    NSData *myImageData = UIImageJPEGRepresentation(image, 0.4f);
    CGFloat avatarWidth = [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"AvatarWidth"] floatValue];
    CGFloat avatarHeight = [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"AvatarHeight"] floatValue];
    CGSize newAvatarSize = CGSizeMake(0.0, 0.0);
    if (!avatarWidth || !avatarHeight) {
        newAvatarSize.width = 200.0;
        newAvatarSize.height = 200.0;
    } else {
        newAvatarSize.width = avatarWidth;
        newAvatarSize.height = avatarHeight;
    }
    NSString *_strEncode = [Base64 encode:myImageData];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:_strEncode forKey:KEY_DATA_IMAGE_UPLOAD_TO_SERVER];
    [userDefault synchronize];
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_SAVE_USER_TOKEN];
    [picker dismissViewControllerAnimated:YES completion:^{
        [[UtilitieWebService shareSingleton] uploadavatarWithStringImage:[self handleImage:image scaledToSize:newAvatarSize] accessToken:accessToken completion:^(id results, NSError *error, ResultCode resultCode) {
            NSString *urlImage = results[KEY_MESSAGE];
            self.completionHandleAvatar(urlImage);
            //        if (completionHandle) completionHandle(urlImage);
            
        }];
    }];
}



/*

- (NSString*)handleImage:(UIImage*)image
            scaledToSize:(CGSize)newSize;
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    NSData *myImageData = UIImageJPEGRepresentation(newImage, 0.4f);
    NSString *_strEncode = [Base64 encode:myImageData];
    return _strEncode;
}

- (void) logoutFacebook
{
    NSLog(@"Close session");
    NSHTTPCookie *cookie;
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (cookie in [storage cookies])
    {
        NSString *domainName = [cookie domain];
        NSRange domainRange = [domainName rangeOfString:@"facebook"];
        if(domainRange.length > 0)
        {
            [storage deleteCookie:cookie];
        }
    }
}
*/
@end
