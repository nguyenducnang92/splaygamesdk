/***********************************************************************
 *	File:           UITextField+Additions.h
 *	Project:        HouseLog
 *
 *  Author:         Nguyen Mau Dat
 *  Created:        8/1/14
 *
 *  Copyright:      2014 All rights reserved.
 ***********************************************************************/

#import <UIKit/UIKit.h>


@interface UITextField (Additions)

- (void)hiddenCursor;
- (void)showCursor;
@property (weak, nonatomic) IBOutlet UITextField* nextTextField;

@end


typedef NS_OPTIONS(NSUInteger, DLPasteboardType) {
    DLPasteboardTypeDisable     = 0,
    DLPasteboardTypeCut         = 1 << 0,
    DLPasteboardTypePaste       = 1 << 1,
    DLPasteboardTypeCopy        = 1 << 2,
    DLPasteboardTypeSelect      = 1 << 3,
    DLPasteboardTypeSelectAll   = 1 << 4,
    DLPasteboardTypeEnableAll   = 1 << 5,
};



@class DLTextField;

@protocol DLTextFieldBackspaceDelegate <UITextFieldDelegate>

@optional

- (void)textFieldDidClickBackspace:(DLTextField*)textField;

@end



@interface DLTextField : UITextField

@property (nonatomic, assign) UIEdgeInsets textInsets;
@property(retain, nonatomic) UITextField* nextTextField;

@end




@interface KSTextField : DLTextField


/**
 *  Control UIPasteboard for uitextfield
 *  default = DLPasteboardTypeEnableAll
 */
@property (nonatomic, assign) DLPasteboardType pasteboardType;
/**
 *  default 0 (no limit)
 */
@property (nonatomic, assign) NSUInteger maximumTextLenght;

@end




