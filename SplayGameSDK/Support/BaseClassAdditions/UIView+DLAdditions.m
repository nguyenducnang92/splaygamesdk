/***********************************************************************
 *	File name:	___________
 *	Project:	DLBaseClassFramework
 *	Description:
 *  Author:		Dat Nguyen Mau
 *  Created:    on 9/4/2013.
 *	Device:		Iphone vs IPad
 *  Company:	__MyCompanyName__
 *  Copyright:	2012 . All rights reserved.
 ***********************************************************************/

#import "UIView+DLAdditions.h"
#import <QuartzCore/QuartzCore.h>
#import <objc/runtime.h>

#pragma mark -
#pragma mark UIView Properties ________________________________________________________________
/*********************************************************************************************/
@implementation UIView (DLFrame)

- (CGFloat)frameX
{
    return self.frame.origin.x;
}
- (void)setFrameX:(CGFloat)x__
{
    CGRect frame__ = self.frame;
    frame__.origin.x = x__;
    self.frame = frame__;
}

- (CGFloat)frameY
{
    return self.frame.origin.y;
}
- (void)setFrameY:(CGFloat)y__
{
    CGRect frame__ = self.frame;
    frame__.origin.y = y__;
    self.frame = frame__;
}

- (CGFloat)centerX
{
    return self.center.x;
}
- (void)setCenterX:(CGFloat)centerX__
{
    CGPoint center__ = self.center;
    center__.x = centerX__;
    self.center = center__;
}

- (CGFloat)centerY
{
    return self.center.y;
}
- (void)setCenterY:(CGFloat)centerY__
{
    CGPoint center__ = self.center;
    center__.y = centerY__;
    self.center = center__;
}

- (CGFloat)frameWidth
{
    return self.frame.size.width;
}
- (void)setFrameWidth:(CGFloat)width__
{
    CGRect frame__ = self.frame;
    frame__.size.width = width__;
    self.frame = frame__;
}

- (CGFloat)frameHeight
{
    return self.frame.size.height;
}
- (void)setFrameHeight:(CGFloat)height__
{
    CGRect frame__ = self.frame;
    frame__.size.height = height__;
    self.frame = frame__;
}

- (CGSize)frameSize
{
    return self.frame.size;
}
- (void)setFrameSize:(CGSize)frameSize__
{
    self.frame = CGRectMake(self.frameX, self.frameY, frameSize__.width, frameSize__.height);
}

- (CGFloat)frameMaxX
{
    return CGRectGetMaxX(self.frame);
}
- (CGFloat)frameMaxY
{
    return CGRectGetMaxY(self.frame);
}

- (CGPoint)centerOnSelf
{
    return CGPointMake(self.bounds.size.width/2., self.bounds.size.height/2.);
}

@end




@implementation UIView (DLRemoveAllSubviews)

- (void)removeAllSubSubviews
{
    for (UIView *sub in self.subviews) {
        [sub removeAllSubSubviews];
        
        [sub removeFromSuperview];
    }
}

- (void)removeAllSubviews
{
    for (UIView *sub in self.subviews) [sub removeFromSuperview];
}

@end





@implementation UIView (DLFromNib)

+ (id)shareInstanceWithNibNamed:(NSString *)nibName
{
    NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:NULL];
    return (nibContents && nibContents.count > 0) ? [nibContents firstObject] : nil;
}

@end




@implementation UIView (DLScreenshot)

- (UIImage*)screenShot
{
    return [self getScreenShotWithScale:0.0f];
}

- (UIImage*)image2x
{
    return [self getScreenShotWithScale:2.f];
}

- (UIImage*)getScreenShotWithScale:(CGFloat)scale
{
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO, scale);
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}

@end


@implementation UIView (DLAutoresizingMask)

+ (UIViewAutoresizing)dlAutoresizingMaskWithLeft:(BOOL)maskLeft width:(BOOL)maskWidth right:(BOOL)maskRight
                                             top:(BOOL)maskTop height:(BOOL)maskHeight bottom:(BOOL)maskBottom
{
    UIViewAutoresizing realResizingMask = UIViewAutoresizingNone;
    
    // Horizontal Mask
    if (maskLeft) {
        if (maskRight) {
            if (maskWidth) realResizingMask |= UIViewAutoresizingFlexibleWidth;
        }
        else {
            if (maskWidth) realResizingMask |= UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin;
            else realResizingMask |= UIViewAutoresizingFlexibleRightMargin;
        }
    }
    else {
        if (maskRight) {
            if (maskWidth) realResizingMask |= UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin;
            else realResizingMask |= UIViewAutoresizingFlexibleLeftMargin;
        }
        else {
            if (maskWidth) realResizingMask |= UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
            else realResizingMask |= UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        }
    }
    
    // Vertical Mask
    if (maskTop) {
        if (maskBottom) {
            if (maskHeight) realResizingMask |= UIViewAutoresizingFlexibleHeight;
        }
        else {
            if (maskHeight) realResizingMask |= UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
            else realResizingMask |= UIViewAutoresizingFlexibleBottomMargin;
        }
    }
    else {
        if (maskBottom) {
            if (maskHeight) realResizingMask |= UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleTopMargin;
            else realResizingMask |= UIViewAutoresizingFlexibleTopMargin;
        }
        else {
            if (maskHeight) realResizingMask |= UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
            else realResizingMask |= UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        }
    }
    
    return realResizingMask;
}

+ (UIViewAutoresizing)dlAutoresizingMaskAll
{
    return [UIView dlAutoresizingMaskWithLeft:YES width:YES right:YES top:YES height:YES bottom:YES];
}
+ (UIViewAutoresizing)dlAutoresizingMaskTopFullWidth
{
    return [UIView dlAutoresizingMaskWithLeft:YES width:YES right:YES top:YES height:NO bottom:NO];
}
+ (UIViewAutoresizing)dlAutoresizingMaskBottomFullWidth
{
    return [UIView dlAutoresizingMaskWithLeft:YES width:YES right:YES top:NO height:NO bottom:YES];
}
+ (UIViewAutoresizing)dlAutoresizingMaskLeftFullHeight
{
    return [UIView dlAutoresizingMaskWithLeft:YES width:NO right:NO top:YES height:YES bottom:YES];
}
+ (UIViewAutoresizing)dlAutoresizingMaskRightFullHeight
{
    return [UIView dlAutoresizingMaskWithLeft:NO width:NO right:YES top:YES height:YES bottom:YES];
}

+ (UIViewAutoresizing)dlAutoresizingMaskCenterFullHeight
{
    return [UIView dlAutoresizingMaskWithLeft:NO width:NO right:NO top:YES height:YES bottom:YES];
}

+ (UIViewAutoresizing)dlAutoresizingMaskCenterFullWidth
{
    return [UIView dlAutoresizingMaskWithLeft:YES width:YES right:YES top:NO height:NO bottom:NO];
}

+ (UIViewAutoresizing)dlAutoresizingMaskNon
{
    return [UIView dlAutoresizingMaskWithLeft:NO width:NO right:NO top:NO height:NO bottom:NO];
}

@end



@implementation UIColor (DL_RGB_HEX)

+ (UIColor*)colorWithRGBHex:(UInt32)rgbHexValue
{
    return [UIColor colorWithRed:((float)((rgbHexValue & 0xFF0000) >> 16))/255.0f green:((float)((rgbHexValue & 0xFF00) >> 8))/255.0f blue:((float)(rgbHexValue & 0xFF))/255.0f alpha:1.0f];
}
@end



@implementation UIView (DLShowPopUp)

- (void)showAsPopUp
{
    [self showAsPopUpOnView:[[[UIApplication sharedApplication] delegate] window]];
}

- (void)showAsPopUpOnView:(UIView*)superView
{
    [self showAsPopUpOnView:superView completion:nil];
}

- (void)showAsPopUpCompletion:(DLUIViewPopUpCompletion)completion
{
    [self showAsPopUpOnView:[[[UIApplication sharedApplication] delegate] window] completion:completion];
}
- (void)showAsPopUpOnView:(UIView*)superView completion:(DLUIViewPopUpCompletion)completion
{
    if (self.superview == superView && self.hidden == NO && self.alpha > 0.f) {
        if (completion) completion (YES);
        return;
    }
    
    [superView addSubview:self];
    BOOL __userPre = self.userInteractionEnabled;
    
    self.userInteractionEnabled = NO;
    self.transform = CGAffineTransformMakeScale(0.25, 0.25);
    self.alpha = 0.0;
    self.hidden = NO;
    
    __unsafe_unretained typeof(self) __weakObject = self;
    
    [UIView animateWithDuration:0.225f
                     animations:^{
                         __weakObject.transform = CGAffineTransformMakeScale(1., 1.);
                         __weakObject.alpha = 1.;
                     }
                     completion:^(BOOL finished){
                         __weakObject.userInteractionEnabled = __userPre;
                         
                         if (finished) {
                             if (completion) completion (YES);
                             __weakObject.transform = CGAffineTransformMakeScale(1., 1.);
                             __weakObject.alpha = 1.;
                         }
                         else {
                             __weakObject.alpha = 1.0;
                             __weakObject.transform = CGAffineTransformMakeScale(1., 1.);
                             if (completion) completion (NO);
                         }
                     }];
}

- (void)showAsAlertView
{
    [self showAsAlertViewOnView:[[[UIApplication sharedApplication] delegate] window]];
}
- (void)showAsAlertViewOnView:(UIView*)superView
{
    [self showAsAlertViewOnView:superView completion:nil];
}
- (void)showAsAlertViewCompletion:(DLUIViewPopUpCompletion)completion
{
    [self showAsAlertViewOnView:[[[UIApplication sharedApplication] delegate] window] completion:completion];
}
- (void)showAsAlertViewOnView:(UIView*)superView completion:(DLUIViewPopUpCompletion)completion
{
    if (self.superview == superView && self.hidden == NO && self.alpha > 0.f) {
        if (completion) completion (YES);
        return;
    }
    
    [superView addSubview:self];
    
    BOOL __userPre = self.userInteractionEnabled;
    
    self.userInteractionEnabled = NO;
    self.transform = CGAffineTransformMakeScale(0.25, 0.25);
    self.hidden = NO;
    
    __unsafe_unretained typeof(self) __weakObject = self;
    [UIView animateWithDuration:0.25f
                     animations:^{
                         __weakObject.transform = CGAffineTransformMakeScale(1.05, 1.05);
                         __weakObject.alpha = 0.8;
                     }
                     completion:^(BOOL finished){
                         if (finished) {
                             __weakObject.transform = CGAffineTransformMakeScale(1.05, 1.05);
                             
                             [UIView animateWithDuration:1./12.f
                                              animations:^{
                                                  __weakObject.transform = CGAffineTransformMakeScale(0.9, 0.9);
                                                  __weakObject.alpha = 0.9;
                                              }
                                              completion:^(BOOL finished) {
                                                  if (finished) {
                                                      __weakObject.transform = CGAffineTransformMakeScale(0.9, 0.9);
                                                      
                                                      [UIView animateWithDuration:1./7.5
                                                                       animations:^{
                                                                           __weakObject.transform = CGAffineTransformMakeScale(1., 1.);
                                                                           __weakObject.alpha = 1.0;
                                                                       }
                                                                       completion:^(BOOL finished) {
                                                                           __weakObject.userInteractionEnabled = __userPre;
                                                                           
                                                                           if (finished) {
                                                                               __weakObject.transform = CGAffineTransformMakeScale(1., 1.);
                                                                               if (completion) completion (YES);
                                                                           }
                                                                           else {
                                                                               __weakObject.alpha = 1.0;
                                                                               __weakObject.transform = CGAffineTransformMakeScale(1., 1.);
                                                                               if (completion) completion (NO);
                                                                           }
                                                                       }];
                                                  }
                                                  else {
                                                      __weakObject.userInteractionEnabled = __userPre;
                                                      __weakObject.alpha = 1.0;
                                                      __weakObject.transform = CGAffineTransformMakeScale(1., 1.);
                                                      if (completion) completion (NO);
                                                  }
                                              }];
                         }
                         else {
                             __weakObject.userInteractionEnabled = __userPre;
                             __weakObject.alpha = 1.0;
                             __weakObject.transform = CGAffineTransformMakeScale(1., 1.);
                             if (completion) completion (NO);
                         }
                     }];
}


- (void)hideAsPopUp
{
    [self hideAsPopUpCompletion:nil];
}

- (void)hideAsPopUpCompletion:(DLUIViewPopUpCompletion)completion
{
    BOOL __userPre = self.userInteractionEnabled;
    self.userInteractionEnabled = NO;
    
    __unsafe_unretained typeof(self) __weakObject = self;
    [UIView animateWithDuration:0.22
                     animations:^{
                         __weakObject.alpha = 0.0;
                         __weakObject.transform = CGAffineTransformMakeScale(0.6, 0.6);
                     }completion:^(BOOL finished) {
                         if (completion) completion (finished);
                         __weakObject.userInteractionEnabled = __userPre;
                         if(finished){
                             
                             __weakObject.hidden = YES;
                             __weakObject.alpha = 0.0;
                             __weakObject.transform = CGAffineTransformMakeScale(1., 1.);
                             [__weakObject removeFromSuperview];
                         }
                         else {
                             __weakObject.alpha = 0.0;
                             __weakObject.transform = CGAffineTransformMakeScale(1., 1.);
                         }
                     }];
}

@end




static NSString const *dlBaseIOS7OverViewStatusBar = @"dlBaseIOS7OverViewStatusBar associated object key";


@interface DLOverStatusBarIOS7 : UIView

@property (nonatomic, retain) UIColor *originColor;

@end

@implementation DLOverStatusBarIOS7

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.originColor = self.backgroundColor;
    }
    
    return self;
}

@end



@implementation UIWindow (DLOverStatusBarIOS7)

- (DLOverStatusBarIOS7*)overViewStatusBar
{
    return objc_getAssociatedObject(self, (__bridge const void *)(dlBaseIOS7OverViewStatusBar));
}

//- (void)addOverViewStatusBarIOS7:(UIColor*)color
//{
//    if ([UIDevice isIOS7OrLater]) {
//        DLOverStatusBarIOS7 *__overViewStatusBar = [self overViewStatusBar];
//        if (__overViewStatusBar) {
//            __overViewStatusBar.frame = [[UIApplication sharedApplication] statusBarFrame];
//            __overViewStatusBar.autoresizingMask = [UIView dlAutoresizingMaskTopFullWidth];
//            __overViewStatusBar.backgroundColor = color;
//        }
//        else {
//            __overViewStatusBar = [[DLOverStatusBarIOS7 alloc] initWithFrame:[[UIApplication sharedApplication] statusBarFrame]];
//            __overViewStatusBar.autoresizingMask = [UIView dlAutoresizingMaskTopFullWidth];
//            __overViewStatusBar.backgroundColor = color;
//            objc_setAssociatedObject(self, (__bridge const void *)(dlBaseIOS7OverViewStatusBar), __overViewStatusBar, OBJC_ASSOCIATION_RETAIN);
//            
//            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willChangeStatusBarFrame:) name:UIApplicationWillChangeStatusBarFrameNotification object:nil];
//            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRotateFromInterfaceOrientation:) name:UIDeviceOrientationDidChangeNotification object:nil];
//        }
//        __overViewStatusBar.originColor = color;
//        
//        [self addSubview:__overViewStatusBar];
//    }
//}

- (void)removeOverViewStatusBarIOS7
{
    DLOverStatusBarIOS7 *__overViewStatusBar = [self overViewStatusBar];
    [__overViewStatusBar removeFromSuperview];
    objc_removeAssociatedObjects(self);
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillChangeStatusBarFrameNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
}

- (void)setHiddenOverViewStatusBar:(BOOL)hidden
{
    DLOverStatusBarIOS7 *__overViewStatusBar = [self overViewStatusBar];
    __overViewStatusBar.hidden = hidden;
    __overViewStatusBar.frame = [[UIApplication sharedApplication] statusBarFrame];
}

- (void)willChangeStatusBarFrame:(NSNotification*)notif
{
    DLOverStatusBarIOS7 *__overViewStatusBar = [self overViewStatusBar];
    __overViewStatusBar.backgroundColor = [UIColor clearColor];
}

- (void)didRotateFromInterfaceOrientation:(NSNotification*)notif
{
    DLOverStatusBarIOS7 *__overViewStatusBar = [self overViewStatusBar];
    __overViewStatusBar.backgroundColor = [UIColor clearColor];
    __overViewStatusBar.frame = [[UIApplication sharedApplication] statusBarFrame];
    __overViewStatusBar.backgroundColor = __overViewStatusBar.originColor;
}


@end



