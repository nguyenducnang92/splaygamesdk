/***********************************************************************
 *	File:           UITextField+Additions.m
 *	Project:        HouseLog
 *
 *  Author:         Nguyen Mau Dat
 *  Created:        8/1/14
 *
 *  Copyright:      2014 All rights reserved.
 ***********************************************************************/

#import "UITextField+Additions.h"
#import <objc/runtime.h>
static char defaultHashKey;

@implementation UITextField (Additions)

- (void)hiddenCursor
{
    if ([[UIDevice currentDevice].systemVersion floatValue] == 7.0f) [self setTintColor:[UIColor clearColor]];
    else [[self valueForKey:@"textInputTraits"] setValue:[UIColor clearColor] forKey:@"insertionPointColor"];
}
- (void)showCursor {
    if ([[UIDevice currentDevice].systemVersion floatValue] > 7.0f) [self setTintColor:[UIColor colorWithRed:53/255 green:102/255 blue:255/255 alpha:1]];
    else [[self valueForKey:@"textInputTraits"] setValue:[UIColor colorWithRed:53/255 green:102/255 blue:255/255 alpha:1] forKey:@"insertionPointColor"];
}

- (UITextField*) nextTextField {
    return objc_getAssociatedObject(self, &defaultHashKey);
}

- (void) setNextTextField:(UITextField *)nextTextField{
    objc_setAssociatedObject(self, &defaultHashKey, nextTextField, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
@end





@interface DLTextField ()

- (void)setUpInitDlTextField;

@end


@implementation DLTextField

- (instancetype)init
{
    if (self = [super init]) {
        [self setUpInitDlTextField];
    }
    
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self setUpInitDlTextField];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self setUpInitDlTextField];
    }
    
    return self;
}

- (void)setUpInitDlTextField
{
    self.textInsets = self.textInsets = UIEdgeInsetsMake(0.0, 5.0, 0.0, 0.0);
    
    if (self.borderStyle == UITextBorderStyleNone) self.textInsets = UIEdgeInsetsMake(0.0, 5.0, 0.0, 0.0);
}

- (CGRect)textRectForBounds:(CGRect)bounds
{
    return [super textRectForBounds:UIEdgeInsetsInsetRect(bounds, self.textInsets)];
}

- (CGRect)editingRectForBounds:(CGRect)bounds
{
    return [super editingRectForBounds:UIEdgeInsetsInsetRect(bounds, self.textInsets)];
}


- (void)setTextInsets:(UIEdgeInsets)textInsets
{
    if (UIEdgeInsetsEqualToEdgeInsets(textInsets, _textInsets) == NO) {
        _textInsets = textInsets;
        [self setNeedsDisplay];
    }
}

/**
 *  Detect backspace button press, only ios under 8.
 */
- (void)deleteBackward {
    [super deleteBackward];
    
    if ([self.delegate respondsToSelector:@selector(textFieldDidClickBackspace:)]){
        [(id<DLTextFieldBackspaceDelegate>)self.delegate textFieldDidClickBackspace:self];
    }
}


/**
 *  Detect backspace button press with ios under 8.
 *  However, maybe rejected by Apple,
 */
/*
 - (BOOL)keyboardInputShouldDelete:(UITextField *)textField {
 BOOL shouldDelete = YES;
 
 if ([UITextField instancesRespondToSelector:_cmd]) {
 BOOL (*keyboardInputShouldDelete)(id, SEL, UITextField *) = (BOOL (*)(id, SEL, UITextField *))[UITextField instanceMethodForSelector:_cmd];
 
 if (keyboardInputShouldDelete) {
 shouldDelete = keyboardInputShouldDelete(self, _cmd, textField);
 }
 }
 
 if (![textField.text length] && [[[UIDevice currentDevice] systemVersion] intValue] >= 8) {
 [self deleteBackward];
 }
 
 return shouldDelete;
 }
 */


- (void)paste:(id)sender
{
    [super paste:sender];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:UITextFieldTextDidChangeNotification object:self];
}

@end








@interface KSTextFieldDelegateHandler : NSObject<UITextFieldDelegate>

@property (nonatomic, weak) KSTextField *textField;
@property (nonatomic, weak) id<UITextFieldDelegate>textFieldDelegate;

@end


@implementation KSTextFieldDelegateHandler


#pragma mark -
#pragma mark UITextFieldDelegate _________________________________

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (self.textFieldDelegate && [self.textFieldDelegate respondsToSelector:@selector(textFieldShouldBeginEditing:)]) {
        return [self.textFieldDelegate textFieldShouldBeginEditing:textField];
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (self.textFieldDelegate && [self.textFieldDelegate respondsToSelector:@selector(textFieldDidBeginEditing:)]) {
        [self.textFieldDelegate textFieldDidBeginEditing:textField];
    }
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if (self.textFieldDelegate && [self.textFieldDelegate respondsToSelector:@selector(textFieldShouldEndEditing:)]) {
        return [self.textFieldDelegate textFieldShouldEndEditing:textField];
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (self.textFieldDelegate && [self.textFieldDelegate respondsToSelector:@selector(textFieldDidEndEditing:)]) {
        [self.textFieldDelegate textFieldDidEndEditing:textField];
    }
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    if (self.textFieldDelegate && [self.textFieldDelegate respondsToSelector:@selector(textFieldShouldClear:)]) {
        return [self.textFieldDelegate textFieldShouldClear:textField];
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (self.textFieldDelegate && [self.textFieldDelegate respondsToSelector:@selector(textFieldShouldReturn:)]) {
        return [self.textFieldDelegate textFieldShouldReturn:textField];
    }
    return YES;
}



- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    BOOL result = YES;
    
    if (self.textFieldDelegate && [self.textFieldDelegate respondsToSelector:@selector(textField:shouldChangeCharactersInRange:replacementString:)]) {
        result = [self.textFieldDelegate textField:textField shouldChangeCharactersInRange:range replacementString:string];
    }
    
    if (self.textField.maximumTextLenght > 0) {
        if (string.length > 1) {
            // paste event
            NSString *textControl = [textField.text stringByReplacingCharactersInRange:range withString:string];
            
            if (textControl.length > self.textField.maximumTextLenght) {
                NSRange rangeEnum = NSMakeRange(self.textField.maximumTextLenght - 2, 4);
                if (rangeEnum.location + rangeEnum.length > textControl.length) {
                    rangeEnum.length = textControl.length - rangeEnum.location;
                }
                
                __block NSInteger maxLenght = self.textField.maximumTextLenght;
                
                [textControl enumerateSubstringsInRange:rangeEnum options:NSStringEnumerationByComposedCharacterSequences usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
                    if (substringRange.location + substringRange.length < maxLenght) {
                        maxLenght = (substringRange.location + substringRange.length);
                    }
                }];
                
                textControl = [textControl substringToIndex:maxLenght];
                
                self.textField.text = textControl;
                result = NO;
            }
        }
        else {
            if (range.length > 0) { result = YES; }
            else {
                NSString *textControl = [self.textField.text stringByReplacingCharactersInRange:range withString:string];
                if (textControl.length > self.textField.maximumTextLenght) {
                    result = NO;
                }
            }
        }
    }
    
    return result;
}


- (void)textFieldDidClickBackspace:(DLTextField*)textField
{
    if ([self.textFieldDelegate respondsToSelector:@selector(textFieldDidClickBackspace:)]){
        [(id<DLTextFieldBackspaceDelegate>)self.textFieldDelegate textFieldDidClickBackspace:textField];
    }
}

@end















/*
 *  must be use KSTextFieldDelegateHandler because:
 *  shouldChangeTextInRange is error when press on quicktype suggestions.
 */


@interface KSTextField ()

@property (nonatomic, strong) KSTextFieldDelegateHandler *delegateHandler;

@end


@implementation KSTextField

- (void)dealloc
{
    self.delegateHandler.textField = nil;
    self.delegateHandler.textFieldDelegate = nil;
    [super setDelegate:nil];
    self.delegateHandler = nil;
}


- (void)setUpInitDlTextField
{
    [super setUpInitDlTextField];
    self.pasteboardType = DLPasteboardTypeEnableAll;
}


/**
 *  Control actions
 *
 */
- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    if (self.pasteboardType == DLPasteboardTypeDisable) {
        return NO;
    }
    else if (self.pasteboardType == DLPasteboardTypeEnableAll) {
        if ([self respondsToSelector:action]) {
            return [super canPerformAction:action withSender:sender];
        }
        return NO;
    }
    
    else if (action == @selector(cut:)) {
        return ((self.pasteboardType & DLPasteboardTypeCut) == DLPasteboardTypeCut);
    }
    else if (action == @selector(paste:)) {
        return ((self.pasteboardType & DLPasteboardTypePaste) == DLPasteboardTypePaste);
    }
    else if (action == @selector(copy:)) {
        return ((self.pasteboardType & DLPasteboardTypeCopy) == DLPasteboardTypeCopy);
    }
    else if (action == @selector(select:)) {
        return ((self.pasteboardType & DLPasteboardTypeSelect) == DLPasteboardTypeSelect);
    }
    else if (action == @selector(selectAll:)) {
        return ((self.pasteboardType & DLPasteboardTypeSelectAll) == DLPasteboardTypeSelectAll);
    }
    else {
        if ([self respondsToSelector:action]) {
            return [super canPerformAction:action withSender:sender];
        }
        return NO;
    }
}

- (void)setDelegate:(id<UITextFieldDelegate>)delegate
{
    if (delegate != nil) {
        if (self.delegateHandler == nil) {
            self.delegateHandler = [[KSTextFieldDelegateHandler alloc] init];
            self.delegateHandler.textField = self;
        }
    }
    [super setDelegate:self.delegateHandler];
    self.delegateHandler.textFieldDelegate = delegate;
}

//- (void)setPasteboardType:(DLPasteboardType)pasteboardType
//{
////    self.pasteboardType = pasteboardType;
//}

@end











