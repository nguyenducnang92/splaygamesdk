//
//  ImageDownload.h
//  XMPPChatClient
//
//  Created by BuiDucViet on 4/29/16.
//  Copyright © 2016 Bui Duc Viet. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ImageDownload : NSObject

//- (NSData*)paserNSdatafromBaseString:(NSString*)baseString;
+ (NSString *)encodeToBase64String:(UIImage *)image;
+ (UIImage *)decodeBase64ToImage:(NSString *)strEncodeData;

@end
