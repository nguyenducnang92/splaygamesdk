//
//  SplayGameSDKManager.h
//  SplayGameSDKManager
//
//  Created by DucViet on 6/6/17.
//  Copyright © 2017 DucViet. All rights reserved.
//

typedef enum {
    MethodDnByFastTk = 0,
    MethoDnByTk,
    MethoDnByTkFacebook,
    MethoDnByTkApple,
    MethodDnByTkGoogle
}MethodDn;

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define ACTION_LOGIN_USER_SUCCESS           @"key_login_user_success_action"
#define ACTION_LOGIN_APPLE_SUCCESS           @"key_login_apple_success_action"
#define KEY_INFO_USER_LOGIN                 @"key_info_user_login"
#define ACTION_LOGOUT_USER_SUCCESS          @"key_logout_user_success_action"
#define KEY_INFO_USER_LOGOUT                @"key_login_user_success_action"

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)


typedef void (^CompletionHandlerResult) (NSString* _Nullable completionResult);
typedef void (^InviteFriendCompleteHandle) (NSString * _Nullable requestID, NSInteger numberPeople);
typedef void(^AuthenCompletionHandle) (BOOL suceess, NSInteger statusResult, NSString * _Nullable message);
typedef void(^InAppPurchaseCompletionHandle) (BOOL suceess, NSInteger statusResult, NSString * _Nullable message);

@interface SplayGameSDKManager : NSObject

+(instancetype _Nullable ) sharedManager;
/**
 Setting APIKeyLive and APIKeySandbox for app use SDK
 */
+ (void)splayGameSDKInit:(NSString *_Nullable)apikeyLive APIKeySandbox:(NSString *_Nullable)apikeySandbox;


@property (nonatomic, retain) NSSet * _Nullable productIdentifiers;

@property (nonatomic, strong) UIView * _Nullable overPopupView;

@property (nonatomic, copy) void(^ _Nullable completionHandleAvatar)(NSString * _Nullable avatarString);
/**
 Callback Inapp purchase for items.
 */
//@property(nonatomic, copy) InAppPurchaseCompletionHandle inAppPurchaseCompletion;
//@property (nonatomic,copy) void(^InAppPurchaseCompletionHandle)(BOOL success, NSInteger statusResult, NSString * message);

@property (nonatomic, copy) void (^ _Nullable completionHandler)(BOOL finished);
/**
 Hides close button if there is only one view controller in the view controllers stack.
 */
@property (nonatomic, assign) BOOL closeButton;
/**
 Hides navigationbar view controller in the view controllers stack.
 */
@property (nonatomic, assign) BOOL navigationbarHidden;

/**
 Check method DN in SDK .
 */
@property (nonatomic, assign) MethodDn methodDnSDK;
/**
 Popup controller which is containing the view controller.
 Will be nil if the view controller is not contained in any popup controller.
 */
@property (nonatomic, strong) NSMutableArray * _Nullable alertViews;
/**
 Init the viewcontroller with root view controller.
 Show viewcontroller login SDK
 */
//-(void)showLiteDnGame;

-(void)sdkShowLoginForm;

-(void)autoLogin;

-(void)buyInAppItemPartnerInfo:(NSString*_Nullable)partnerinfo andProductID:(NSString*_Nullable)productID CompletionHandle:(InAppPurchaseCompletionHandle _Nullable )completionHandle;

//-(void)buyInAppItem:(NSString*)transactionID andProductID:(NSString*)productID CompletionHandler:(void(^)(BOOL success, NSInteger statusResult, NSString * message))completionHandle;

-(void)reStoreInAppPurchase;

-(void)sdkLogout;

-(void)showStatus;

- (BOOL)application:(UIApplication *_Nullable)application openURL:(NSURL *_Nullable)url sourceApplication:(NSString *_Nullable)sourceApplication annotation:(id _Nullable )annotation;

/**
 Open url scheme for ios 8.0 and older
 */
- (BOOL)application:(UIApplication *_Nullable)application openURL:(NSURL *_Nullable)url options:(nonnull NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options;
/**
 Config push notification use in SDK Lite
 */

-(void)configFacebookApplication:(UIApplication*_Nullable)application launchOptions:(NSDictionary*_Nullable)launchOptions;
-(void)configPushNotificationApp:(UIApplication*_Nullable)application launchOptions:(NSDictionary*_Nullable)launchOptions;

/**
 Config firebase for tracking even.
 */
-(void)configFireBase;
/**
 Config Appflyer and install for tracking even app.
 */
-(void)installEvenFacebookApp;

-(void)configAppsFlyer;
/**
 Config Appflyer and Register for Push Notifications uninstall for tracking.
 */
//-(void)configAppsplyerTrackUninstall:(UIApplication*)application launchOptions:(NSDictionary*)launchOptions;

-(void)trackingInstallAppWithAppsflyer;

-(void)appsFlyerRegisterUninstallWithDeviceToken:(NSData*_Nullable)deviceToken;

- (void)trackAchievedLevelEvent:(NSString *_Nullable)level;
- (void)trackAchievedFirstLevelEvent;
- (void)trackCompletedFirstUpdateEvent;
- (void)trackFirstOpenEvent;
- (void)trackCompleteTutorialEvent;
- (void)trackCompleteOverTutorialEvent;
- (void)trackCompleteLoadServerList;
- (void)trackCompleteLoadSDK;
- (void)trackCompleteLoadRole;
- (void)trackCompleteCreateRole;

/**************************Event StartTutorial******************************************/

- (void)logStartTutorialEvent:(NSString *_Nullable)userId success:(BOOL)success;

/**************************Event CompletedTutorial******************************************/

- (void)logCompletedTutorialEvent:(NSString *_Nullable)userId success:(BOOL)success;

/**************************Event InitiatedCheckout******************************************/

- (void)logInitiatedCheckoutEvent:(NSString *_Nullable)productId contentType:(NSString *_Nullable)contentType;

/**************************Event Purchase******************************************/
// productId in case IAP

- (void)logPurchaseEvent:(NSString *_Nullable)purchaseAmount eventName:(NSString *_Nullable)eventName productId:(NSString *_Nullable)productId paymentInapp:(NSString *_Nullable) paymentInapp;


/**************************Event SpentCredits ******************************************/

- (void)logSpentCreditsEvent:(NSString*_Nullable)contentId virtualCurrencyName:(NSString*_Nullable)virtualCurrencyName totalValue:(double)totalValue;


//***************************InviteFacebook*********************************************/
//- (void)inviteFacebook;
//-(void)inviteFacebookWithCompletionHandle:(InviteFriendCompleteHandle)completionHandle;

- (void)publishFacebookFeedWithLinkShare:(NSString *_Nullable)linkshare handler:(void (^_Nullable)(BOOL finished))completionHandler;


/**************************Get app status **********************************/

-(void)getAppStatusWithCompletionHandler:(CompletionHandlerResult _Nullable )completionHandle;

/**************************Get avatar **********************************/

- (void)getAvatarWithCompletionHandler:(void(^_Nullable)(NSString * _Nullable urlAvatar))completionHandler;

/**************************Handle Sign in with AppleID **********************************/
-(void)authenAppleWithAppleID:(NSString *_Nullable)appleID email:(NSString *_Nullable)email fullName:(NSString *_Nullable)fullName CompletionHandle:(AuthenCompletionHandle _Nullable )completionHandle;


@end
