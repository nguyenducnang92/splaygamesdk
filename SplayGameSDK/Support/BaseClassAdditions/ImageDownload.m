//
//  ImageDownload.m
//  XMPPChatClient
//
//  Created by BuiDucViet on 4/29/16.
//  Copyright © 2016 Bui Duc Viet. All rights reserved.
//

#import "ImageDownload.h"

@implementation ImageDownload

//- (NSData*)paserNSdatafromBaseString:(NSString*)baseString
//{
//    __block NSData *_dataResurl;
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        _dataResurl = [[NSData alloc] initWithBase64EncodedString:baseString options:0];
//    });
//    return _dataResurl;
//}
+ (NSString *)encodeToBase64String:(UIImage *)image {
    NSData *data = UIImagePNGRepresentation(image);
    NSString *imgStr = [data base64EncodedStringWithOptions:0];
    return imgStr;
}

+ (UIImage *)decodeBase64ToImage:(NSString *)strEncodeData {
    NSData *nsDataDecode = [[NSData alloc] initWithBase64EncodedString:strEncodeData options:0];
    UIImage *imageResult = [UIImage imageWithData:nsDataDecode];
    return imageResult;
}
@end
