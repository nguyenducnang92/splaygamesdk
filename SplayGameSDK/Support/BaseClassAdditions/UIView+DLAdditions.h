/***********************************************************************
 *	File name:	___________
 *	Project:	DLBaseClassFramework
 *	Description:
 *  Author:		Dat Nguyen Mau
 *  Created:    on 9/4/2013.
 *	Device:		Iphone vs IPad
 *  Company:	__MyCompanyName__
 *  Copyright:	2012 . All rights reserved.
 ***********************************************************************/

#import <UIKit/UIKit.h>

#pragma mark -
#pragma mark UIView Properties ________________________________________________________________
/*********************************************************************************************/
//                                  UIView+
/*********************************************************************************************/
@interface UIView (DLFrame)

@property (nonatomic, assign) CGFloat frameX;                   // get and set frame.origin.x;
@property (nonatomic, assign) CGFloat frameY;                   // get and set frame.origin.y;
@property (nonatomic, assign) CGFloat centerX;                  // get and set center.x;
@property (nonatomic, assign) CGFloat centerY;                  // get and set center.y;
@property (nonatomic, assign) CGFloat frameWidth;               // get and set frame.size.width;
@property (nonatomic, assign) CGFloat frameHeight;              // get and set frame.size.height;
@property (nonatomic, assign) CGSize frameSize;                 // get and set frame.size;

@property (nonatomic, readonly) CGFloat frameMaxX;
@property (nonatomic, readonly) CGFloat frameMaxY;

@property (nonatomic, readonly) CGPoint centerOnSelf;           // return CGPointMake(self.bounds.size.width/2., self.bounds.size.height/2.);

@end



@interface UIView (DLRemoveAllSubviews)
- (void)removeAllSubSubviews;
- (void)removeAllSubviews;
@end




@interface UIView (DLFromNib)

+ (id)shareInstanceWithNibNamed:(NSString *)nibName;

@end


@interface UIView (DLScreenshot)

@property (nonatomic, readonly) UIImage *screenShot;    // create screenshot with scale 0.0f
@property (nonatomic, readonly) UIImage *image2x;       // create screenshot with scale 2.0f

- (UIImage*)getScreenShotWithScale:(CGFloat)scale;      // create screenshot with scale x.0f

@end


@interface UIView (DLAutoresizingMask)
// get UIViewAutoresizing for view with mask points
// correct UIViewAutoresizing value for Views when add autoresizingMask
// YES = mask, NO = not mask
+ (UIViewAutoresizing)dlAutoresizingMaskWithLeft:(BOOL)maskLeft width:(BOOL)maskWidth right:(BOOL)maskRight
                                             top:(BOOL)maskTop height:(BOOL)maskHeight bottom:(BOOL)maskBottom;
+ (UIViewAutoresizing)dlAutoresizingMaskAll;                    //Mask => Left: YES, Width: YES, Right: YES, Top: YES, Height: YES, Bottom: YES
+ (UIViewAutoresizing)dlAutoresizingMaskTopFullWidth;           //Mask => Left: YES, Width: YES, Right: YES, Top: YES, Height: NO,  Bottom: NO
+ (UIViewAutoresizing)dlAutoresizingMaskBottomFullWidth;        //Mask => Left: YES, Width: YES, Right: YES, Top: NO,  Height: NO,  Bottom: YES
+ (UIViewAutoresizing)dlAutoresizingMaskLeftFullHeight;         //Mask => Left: YES, Width: NO,  Right: NO,  Top: YES, Height: YES, Bottom: YES
+ (UIViewAutoresizing)dlAutoresizingMaskRightFullHeight;        //Mask => Left: NO,  Width: NO,  Right: YES, Top: YES, Height: YES, Bottom: YES
+ (UIViewAutoresizing)dlAutoresizingMaskCenterFullHeight;       //Mask => Left: NO,  Width: NO,  Right: NO,  Top: YES, Height: YES, Bottom: YES
+ (UIViewAutoresizing)dlAutoresizingMaskCenterFullWidth;        //Mask => Left: YES, Width: YES, Right: YES, Top: NO,  Height: NO,  Bottom: NO
+ (UIViewAutoresizing)dlAutoresizingMaskNon;                    //Mask => Left: NO,  Width: NO,  Right: NO,  Top: NO,  Height: NO,  Bottom: NO

@end

#define DLColorWithRGBHex(rgbHexValue) [UIColor colorWithRGBHex:rgbHexValue]

@interface UIColor (DL_RGB_HEX)

+ (UIColor*)colorWithRGBHex:(UInt32)rgbHexValue;

@end



@interface UIView (DLShowPopUp)

typedef void(^DLUIViewPopUpCompletion)(BOOL finish);

- (void)showAsPopUp;                                                                                // animation: 0.225s (show on UIWindow)
- (void)showAsPopUpOnView:(UIView*)superView;                                                       // animation: 0.225s
- (void)showAsPopUpCompletion:(DLUIViewPopUpCompletion)completion;                                  // animation: 0.225s (show on UIWindow)
- (void)showAsPopUpOnView:(UIView*)superView completion:(DLUIViewPopUpCompletion)completion;        // animation: 0.225s

- (void)showAsAlertView;                                                                            // max duration = 0.47s (show on UIWindow)
- (void)showAsAlertViewOnView:(UIView*)superView;                                                   // max duration = 0.47s
- (void)showAsAlertViewCompletion:(DLUIViewPopUpCompletion)completion;                              // max duration = 0.47s (show on UIWindow)
- (void)showAsAlertViewOnView:(UIView*)superView completion:(DLUIViewPopUpCompletion)completion;    // max duration = 0.47s


- (void)hideAsPopUp;                                                // animation: 0.22s
- (void)hideAsPopUpCompletion:(DLUIViewPopUpCompletion)completion;  // animation: 0.22s

@end


@interface UIWindow (DLOverStatusBarIOS7)

//- (void)addOverViewStatusBarIOS7:(UIColor*)color;
- (void)removeOverViewStatusBarIOS7;
- (void)setHiddenOverViewStatusBar:(BOOL)hidden;

@end








