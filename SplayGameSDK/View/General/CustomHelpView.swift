//
//  CustomHelpView.swift
//  SplayGameSDK
//
//  Created by Nguyen Nang on 7/6/20.
//  Copyright © 2020 splay. All rights reserved.
//

import UIKit


protocol CustomHelpViewDelegate: class {
    
    func showViewSupport()
    func showSettingLanguage()
    
}

class CustomHelpView: UIView {
    
    enum Size: CGFloat {
        case padding10 = 8, textField = 36
    }
    
    
    weak var delegate: CustomHelpViewDelegate?
    
    var isHiddenChangLanguage: Bool = false
    var btnChangeLanguage: UIButton!
    var iconArrow: UIImageView!
    
    lazy var QQActions: [PopoverAction] = {
        
        let languages: [LanguageValue] = [.vietnamese, .english]
        
        return languages.map { language in
            return PopoverAction(image: language.icon,
                                 title: language.title,
                                 handler: { action in
                                    
                                    
                                    LanguageSetting.setLanguage(language.code)
                                    UserDefaults.standard.set(language.code, forKey: KEY_SAVE_LANGUAGE_SETTING)
                                    UserDefaults.standard.synchronize()
                                    self.btnChangeLanguage.setTitle(language.title, for: .normal)
                                    self.btnChangeLanguage.setImage(language.icon, for: .normal)
                                    
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: CHANGE_LANGUAGE_SETTING), object: nil)
                                    
            })
        }
    
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupAllSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        setupAllSubviews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        setupFrame()
    }
    
    
    func setupFrame() {
        
        btnChangeLanguage.frame = CGRect(x: Size.padding10.rawValue / 2,
                                         y: 0,
                                         width: 100,
                                         height: bounds.height)
        
        iconArrow.frame = CGRect(x: btnChangeLanguage.frame.maxX + Size.padding10.rawValue / 2,
                                         y: (bounds.height - 18) / 2,
                                         width: 16,
                                         height: 16)
    }
    
}

//--------------------------
// MARK: - SETUP
//--------------------------

extension CustomHelpView {
    
    @objc func btnSettingLanguage(_ sender: UIButton) {

        let popoverView = PopoverView()
        popoverView.arrowStyle = .triangle
        popoverView.show(to: sender, with: QQActions)
    }
    
    
    func setHiddenLanguage(_ hidden: Bool) {
        btnChangeLanguage.isHidden = hidden
        iconArrow.isHidden = hidden
    }
}



//--------------------------
// MARK: - SETUP
//--------------------------

extension CustomHelpView {
    
    func setupAllSubviews() {
        
        btnChangeLanguage = setupButton(title: LanguageSetting.get("TitleLanguageName", alter: ""),
                                        image: nil,
                                        titleColor: .darkGray)
        btnChangeLanguage.addTarget(self, action: #selector(self.btnSettingLanguage(_:)), for: .touchUpInside)
        
        iconArrow = setupImageView()
        
        addSubview(btnChangeLanguage)
        addSubview(iconArrow)
    }
    
    
    func setupButton(title: String? = nil,
                     titleSelected: String? = nil,
                     image: UIImage? = nil,
                     imageSelected: UIImage? = nil,
                     titleColor: UIColor = .white) -> UIButton {
        let button = UIButton()
        button.setTitleColor( titleColor, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 11)
        button.setTitle(title, for: .normal)
        button.setTitle(titleSelected, for: .selected)
        button.setImage(image, for: .normal)
        button.setImage(imageSelected, for: .selected)
        button.clipsToBounds = false
        button.contentHorizontalAlignment = .left
        button.imageView?.contentMode = .scaleAspectFit
        button.imageEdgeInsets = UIEdgeInsets(top: 6, left: -12, bottom: 6, right: 22)
        button.contentMode = .left
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: -24, bottom: 0, right: 0)
        return button
    }
    
    func setupLabel(_ title: String = "", textColor: UIColor = .black,
                    font: UIFont = UIFont.systemFont(ofSize: 12),
                    bgColor: UIColor = .clear,
                    alignment: NSTextAlignment = .left) -> UILabel {
        
        let label = UILabel()
        label.text = title
        label.textColor = textColor
        label.backgroundColor = bgColor
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = alignment
        label.font = font
        return label
    }
    
    func setupView(_ bgColor: UIColor = .lightGray) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        view.clipsToBounds = true
        return view
    }
    
    func setupImageView() -> UIImageView {
        let imageView = UIImageView(image: UtilitieWebService.shareSingleton()?.getImageFromStringBase(img_bg_aroww)?.tint(.lightGray))
        imageView.contentMode = .scaleAspectFit
        return imageView
    }
}

