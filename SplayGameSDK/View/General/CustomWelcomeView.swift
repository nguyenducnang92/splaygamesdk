//
//  CustomWelcomeView.swift
//  SplayGameSDK
//
//  Created by Nguyen Nang on 7/27/20.
//  Copyright © 2020 splay. All rights reserved.
//

import UIKit



protocol CustomWelcomeViewDelegate: class {
    
    func styleSheetForMessageView(messageView: CustomWelcomeView) -> TWMessageBarStyleSheet
}

class CustomWelcomeView: UIView {
    
    
    
    var iconUser: UIImageView!
    var lbTitle: UILabel!
    var lbVersion: UILabel!
    
    
    weak var delegate: CustomWelcomeViewDelegate?
    
    var titleString: String = ""
    var descriptionString: String = ""

    var messgeType: TWMessageBarMessageType = .success

    var hasCallback: Bool = false
    var callbacks: [Any] = []

    var statusBarStyle: UIStatusBarStyle = .default
    var statusBarHidden: Bool = false

    var hit: Bool = false
    var duration: CGFloat = 0.3
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupAllSubviews()
    }
    
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupAllSubviews()
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        iconUser.frame = CGRect(x: 0,
                                y: 0,
                                width: bounds.height,
                                height: bounds.height)
        
        lbTitle.frame = CGRect(x: iconUser.frame.maxX + 8,
                               y: 0,
                               width: bounds.width - iconUser.frame.maxX - 16,
                               height: bounds.height)
        
        lbVersion.frame = CGRect(x: 0,
                                 y: bounds.height - 2 - 13,
                                 width: bounds.width - 8,
                                 height: 13)
    }
}

extension CustomWelcomeView {
    
    func setInfomationWelComeWithTitle(_ title: String?, description: String?) {
        
        layer.cornerRadius = frame.size.height / 2
        layer.masksToBounds = true
        layer.borderWidth = 0.5
        layer.borderColor = UIColor(red: 13/255.0, green: 183/255.0, blue: 212/255.0, alpha: 1.0).cgColor
        
        messgeType = .success
        lbTitle.text = String(format: "%@ %@", LanguageSetting.get("text_welcome", alter: nil), (description ?? ""))
        
        //TODO: Dùng lệnh tự lấy trong SDK
        lbVersion.text = "4.1.17"
        //        DVLog(@"__________version sdk: 4.1.15");
    }
    
}

extension CustomWelcomeView {
    
    func setupAllSubviews() {
        
        iconUser = setupImageView()
        
        lbTitle = setupLabel(textColor: UIColor.Text.blackMediumColor(),
                             font: UIFont.systemFont(ofSize: 15))
        lbVersion = setupLabel(textColor: .darkGray,
                               font: UIFont.systemFont(ofSize: 8),
                               alignment: .right)
        
        addSubview(iconUser)
        addSubview(lbTitle)
        addSubview(lbVersion)
    
        
    }
    
    
    
    
    func setupImageView() -> UIImageView {
        let imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleToFill
        imageView.image = UtilitieWebService.shareSingleton()?.getImageFromStringBase(img_icon_user_login)
        return imageView
    }
    
    func setupLabel(_ title: String = "", textColor: UIColor = UIColor.Text.blackMediumColor(),
                    font: UIFont = UIFont.systemFont(ofSize: 12),
                    bgColor: UIColor = .clear,
                    alignment: NSTextAlignment = .left) -> UILabel {
        
        let label = UILabel()
        label.text = title
        label.textColor = textColor
        label.backgroundColor = bgColor
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = alignment
        label.font = font
        return label
    }
}
