//
//  LoginContentView.swift
//  SplayGameSDK
//
//  Created by Nguyen Nang on 6/30/20.
//  Copyright © 2020 splay. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import CleanroomLogger

class LoginContentView: UIView {
    
    enum Size: CGFloat {
        case padding10 = 8, textField = 36, button = 34
    }
    
    var tfUsername: DLTextField!
    var tfPassword: DLTextField!
    var btnhiddenTextpass: UIButton!
    
    var btnForgot: UIButton!
    
    var btnLogin: UIButton!
    var layerGradient: CAGradientLayer!
    var btnPlayNow: UIButton!
    
    var seperator: UIView!
    var lblOr: UILabel!
    
//    var btnFacebook: UIButton!
    var btnFacebook: FBLoginButton!
    var btnGoogle: UIButton!
    var btnApple: UIButton!

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
        Log.message(.info, message: "Init frame")
        
        setupAllSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        Log.message(.info, message: "Init coder")
        
        setupAllSubviews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        setupFrame()
    }

    
    func setupFrame() {
        
        
        Log.message(.info, message: "Setup frame")
        
        tfUsername.frame = CGRect(x: Size.padding10.rawValue,
                                  y: Size.padding10.rawValue,
                                  width: bounds.width - Size.padding10.rawValue * 2,
                                  height: Size.textField.rawValue)
        
        tfPassword.frame = CGRect(x: Size.padding10.rawValue,
                                  y: tfUsername.frame.maxY + Size.padding10.rawValue,
                                  width: bounds.width - Size.padding10.rawValue * 2,
                                  height: Size.textField.rawValue)
        
        btnForgot.frame = CGRect(x: bounds.width / 4,
                                 y: tfPassword.frame.maxY,
                                 width: bounds.width * 3 / 4 - Size.padding10.rawValue,
                                 height: Size.textField.rawValue)
        
        btnLogin.frame = CGRect(x: Size.padding10.rawValue,
                                y: btnForgot.frame.maxY,
                                width: bounds.width / 2 - Size.padding10.rawValue * 3 / 2,
                                height: Size.textField.rawValue)
        
        layerGradient.frame = btnLogin.bounds
        
        btnPlayNow.frame = CGRect(x: bounds.width / 2 + Size.padding10.rawValue / 2,
                                  y: btnForgot.frame.maxY,
                                  width: bounds.width / 2 - Size.padding10.rawValue * 3 / 2,
                                  height: Size.textField.rawValue)
        
        
//        btnPlayNow.layer.shadowPath = UIBezierPath(roundedRect: btnPlayNow.bounds,
//                                                   cornerRadius: btnPlayNow.layer.cornerRadius).cgPath
        
        lblOr.sizeToFit()
        lblOr.frame = CGRect(x: (bounds.width - (lblOr.frame.width + 40)) / 2,
                             y: btnLogin.frame.maxY,
                             width: lblOr.frame.width + 40,
                             height: 20)
        
        seperator.frame = CGRect(x: Size.padding10.rawValue * 3,
                                 y: lblOr.frame.midY - (1 / UIScreen.main.scale),
                                 width: bounds.width - Size.padding10.rawValue * 6,
                                 height: 1 / UIScreen.main.scale)
        
        let maxY = lblOr.frame.maxY
        
        let arrayButton = [btnFacebook, btnGoogle, btnApple]
            .map{ $0!}
            .filter { !$0.isHidden }
        
        var widthButton = bounds.width - Size.padding10.rawValue * 2
        
        if arrayButton.count > 1 {
            widthButton = (bounds.width - Size.padding10.rawValue * 3) / 2
        }
        
        arrayButton.enumerated()
            .forEach { (i, button) in
   
                if arrayButton.count - 1 == i, i % 2 == 0 {
                    
                    button.frame = CGRect(x: Size.padding10.rawValue,
                                          y: maxY + CGFloat(i / 2) * (Size.textField.rawValue + Size.padding10.rawValue),
                                          width: bounds.width - Size.padding10.rawValue * 2,
                                          height: Size.button.rawValue)
                    
                    
                } else {
                    
                    button.frame = CGRect(x: Size.padding10.rawValue + CGFloat(i % 2) * (widthButton + Size.padding10.rawValue),
                                          y: maxY + CGFloat(i / 2) * Size.padding10.rawValue,
                                          width: widthButton,
                                          height: Size.button.rawValue)
                }
        }
        
        
        Log.message(.info, message: "Finished setup frame")
        
    }
    
}


//--------------------------
// MARK: - SELECTOR
//--------------------------

extension LoginContentView {
    
    @objc func btnhiddenText(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        tfPassword.isSecureTextEntry = !sender.isSelected
    }
}

//--------------------------
// MARK: - PRIVATE METHOD
//--------------------------

extension LoginContentView {

    func animationWithKeyboard(isShow: Bool, y: CGFloat) {
        
        
        let minY: CGFloat = isShow ? max(tfPassword.frame.maxY + 5, y) : btnForgot.frame.maxY
        UIView.animate(withDuration: 0.3) {  self.btnLogin.frame.origin.y = minY;  self.btnPlayNow.frame.origin.y = minY }
    }
    
}


//--------------------------
// MARK: - SETUP
//--------------------------

extension LoginContentView {
    
    func setupAllSubviews() {
        
        Log.message(.info, message: "Setup text field")
        
        tfUsername = setupTextField("Tên đăng nhâp, số điện thoại hoặc email")
        tfPassword = setupTextField("Mật khẩu", isSecureTextEntry: true)
        
        
        Log.message(.info, message: "Setup button")

        btnhiddenTextpass = setupButtonPass()
        tfPassword.rightView = btnhiddenTextpass
        tfPassword.rightViewRect(forBounds: CGRect(x: 0, y: 0, width: 24, height: Size.textField.rawValue))
        
        btnForgot = setupButton(title: "Quên mật khẩu?",
                                titleColor: UIColor(red: 13/255.0, green: 183/255.0, blue: 212/255.0, alpha: 1.0))
        btnForgot.contentHorizontalAlignment = .right
        btnForgot.titleLabel?.font = UIFont.systemFont(ofSize: 13)
        
        btnLogin = setupButton(title: "Đăng nhập".uppercased(), titleColor: .white)
//        btnLogin.setBackgroundImage(UtilitieWebService.shareSingleton()?.getImageFromStringBase(dn_bg_bt_li), for: .normal)
        
        
        btnLogin.layer.cornerRadius = 3
        btnLogin.layer.shadowColor = UIColor.black.alpha(0.7).cgColor
        btnLogin.layer.shadowRadius = 0.5
        btnLogin.layer.shadowOpacity = 0.5
        btnLogin.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
       
        
        layerGradient = CAGradientLayer()
        layerGradient.colors = [UIColor.Gradient.firstColor().cgColor, UIColor.Gradient.secondColor().cgColor]
        layerGradient.cornerRadius = 3
        
        btnLogin.layer.insertSublayer(layerGradient, at: 0)
        
        
        
        btnPlayNow = setupButton(title: "Chơi ngay".uppercased(), titleColor: UIColor.gray)
//        btnPlayNow.setBackgroundImage(UtilitieWebService.shareSingleton()?.getImageFromStringBase(dn_bg_bt_play_now), for: .normal)
        
        btnPlayNow.layer.cornerRadius = 3
        btnPlayNow.clipsToBounds = false
        btnPlayNow.backgroundColor = .white
        btnPlayNow.layer.borderWidth = onePixel()
        btnPlayNow.layer.borderColor = UIColor.Misc.seperatorColor().cgColor
        btnPlayNow.layer.shadowColor = UIColor.black.alpha(0.7).cgColor
        btnPlayNow.layer.shadowRadius = 0.5
        btnPlayNow.layer.shadowOpacity = 0.5
        btnPlayNow.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
        
        
        
        

        
        
        lblOr = setupLabel("Hoặc", textColor: UIColor.gray,
                           font: UIFont.systemFont(ofSize: 11),
                           bgColor: UIColor(red: 248/255.0, green: 248/255.0, blue: 248/255.0, alpha: 1.0),
                           alignment: .center)
        
        seperator = setupView(.lightGray)
        
//        btnFacebook = setupButton(title: "Đăng nhập Facebook", titleColor: .white)
//        btnFacebook.setBackgroundImage(UtilitieWebService.shareSingleton()?.getImageFromStringBase(dn_bg_bt_f), for: .normal)
//        btnFacebook.titleEdgeInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)
        
        
        Log.message(.info, message: "Setup button facebook")
        
        btnFacebook = setupButtonFacebook()
        
        
        Log.message(.info, message: "Setup button google")

        
        btnGoogle = setupButton()
        btnGoogle.setBackgroundImage(UtilitieWebService.shareSingleton()?.getImageFromStringBase(dn_bg_bt_g), for: .normal)
        btnGoogle.layer.cornerRadius = 3
        btnGoogle.clipsToBounds = true
        btnGoogle.titleEdgeInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)
        
        

        
        btnApple = setupButton(titleColor: .black)
        
        btnApple.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
        btnApple.titleLabel?.adjustsFontSizeToFitWidth = true
        btnApple.layer.borderWidth = 1
        btnApple.clipsToBounds = true
        btnApple.layer.cornerRadius = 4
        btnApple.layer.borderColor = UIColor.black.cgColor
        btnApple.setImage(UtilitieWebService.shareSingleton()?.getImageFromStringBase(dn_icon_apple), for: .normal)
        btnApple.imageView?.contentMode = .scaleAspectFit
        btnApple.imageEdgeInsets = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        btnApple.titleEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 16)


        
        addSubview(tfUsername)
        addSubview(tfPassword)
        addSubview(btnForgot)

        
        addSubview(seperator)
        addSubview(lblOr)
        addSubview(btnLogin)
        addSubview(btnPlayNow)
    
        addSubview(btnFacebook)
        addSubview(btnGoogle)
        addSubview(btnApple)
    }
    
    func setupTextField(_ placeHoler: String, textInsets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0),
                        isSecureTextEntry: Bool = false) -> DLTextField {
        let textField = DLTextField()
        textField.placeholder = placeHoler
        textField.layer.cornerRadius = 3
        textField.layer.borderWidth = 0.5
        textField.layer.borderColor = UIColor.lightGray.cgColor
        textField.layer.masksToBounds = true
        textField.textInsets = textInsets
        textField.rightViewMode = isSecureTextEntry ? .always : .never
        textField.isSecureTextEntry = isSecureTextEntry
        textField.font = UIFont.systemFont(ofSize: 14)
        textField.background = UtilitieWebService.shareSingleton()?.getImageFromStringBase(dn_bg_textfile)
        
        textField.inputAccessoryView = nil
        textField.returnKeyType = isSecureTextEntry ? .done : .next
        textField.textColor = UIColor.Text.blackMediumColor()
      
        
        
        if isSecureTextEntry {
            
            
            if #available(iOS 12, *) {
                // iOS 12 & 13: Not the best solution, but it works.
                textField.textContentType = .oneTimeCode
            } else {
                // iOS 11: Disables the autofill accessory view.
                // For more information see the explanation below.
                textField.textContentType = .init(rawValue: "")
            }
            
        }
        
        return textField
    }
    
    func setupButtonPass(image: UIImage? = nil,
                          imageSelected: UIImage? = nil) -> UIButton {
        let button = UIButton()
        button.setImage(UtilitieWebService.shareSingleton()?.getImageFromStringBase(img_icon_hidden_mk_tk), for: .normal)
        button.setImage(UtilitieWebService.shareSingleton()?.getImageFromStringBase(img_dn_show_mk), for: .selected)
        
        
        button.clipsToBounds = true
        button.frame = CGRect(x: 0, y: 0, width: 32, height: Size.textField.rawValue)
        button.addTarget(self, action: #selector(self.btnhiddenText(_:)), for: .touchUpInside)
        button.imageView?.contentMode = .scaleAspectFit
        button.contentMode = .scaleAspectFit
        
        
        
        if #available(iOS 13, *) {
            // iOS 12 & 13: Not the best solution, but it works.
  
            button.imageEdgeInsets = UIEdgeInsets(top: 12, left: 24, bottom: 12, right: 0)
            
        } else {
            // iOS 11: Disables the autofill accessory view.
            // For more information see the explanation below.
            button.imageEdgeInsets = UIEdgeInsets(top: 6, left: 6, bottom: 6, right: 6)
        }
        
       
        
        return button
    }
    
    func setupButtonFacebook() -> FBLoginButton {
        let button = FBLoginButton(frame: .zero, permissions: [.email, .publicProfile])
        button.tooltipColorStyle = .friendlyBlue
        button.setTitle("Đăng nhập Facebook", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 11)
        return button
    }
    
    func setupButton(title: String? = nil,
                     titleSelected: String? = nil,
                     image: UIImage? = nil,
                     imageSelected: UIImage? = nil,
                     titleColor: UIColor = .white) -> UIButton {
        let button = UIButton()
        button.setTitleColor( titleColor, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 11)
        button.setTitle(title, for: .normal)
        button.setTitle(titleSelected, for: .selected)
        button.setImage(image, for: .normal)
        button.setImage(imageSelected, for: .selected)
        button.clipsToBounds = false

        return button
    }
    
    func setupLabel(_ title: String = "", textColor: UIColor = .black,
                    font: UIFont = UIFont.systemFont(ofSize: 12),
                    bgColor: UIColor = .clear,
                    alignment: NSTextAlignment = .left) -> UILabel {
        
        let label = UILabel()
        label.text = title
        label.textColor = textColor
        label.backgroundColor = bgColor
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = alignment
        label.font = font
        return label
    }
    
    func setupView(_ bgColor: UIColor = .lightGray) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        view.clipsToBounds = true
        return view
    }
}
