//
//  NextKeybroadAccessoryView.swift
//  SplayGameSDK
//
//  Created by Nguyen Nang on 4/12/21.
//  Copyright © 2021 splay. All rights reserved.
//

import Foundation

class NextKeybroadAccessoryView: UIView {
    
    
    var contentView: UIView!
    var buttonClose: UIButton!
    var buttonNext: UIButton!
    
    
    var width: CGFloat = UIScreen.main.bounds.width
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupAllSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        setupAllSubviews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        setupFrame()
    }

    
    func setupFrame() {
        
        
        contentView.frame = CGRect(x: (bounds.width - width) / 2,
                                   y: 0,
                                   width: width,
                                   height: bounds.height)
        
        buttonClose.frame = CGRect(x: 0,
                                   y: 0,
                                   width: 44,
                                   height: contentView.frame.height)
        
        buttonNext.frame = CGRect(x: buttonClose.frame.maxX + 10,
                                  y: 5,
                                  width: contentView.frame.width - buttonClose.frame.maxX - 10,
                                  height: contentView.frame.height - 10)
    }
}

extension NextKeybroadAccessoryView {
    
    func setupAllSubviews() {
        
        backgroundColor = .white
        
        contentView = setupView(.clear)
        
        buttonClose = setupButton(image: UtilitieWebService.shareSingleton()?.getImageFromStringBase(login_ic_close)?.tint(.gray), titleColor: .gray)
        
        buttonClose.imageEdgeInsets = UIEdgeInsets(top: 14, left: 14, bottom: 14, right: 14)
        buttonClose.imageView?.contentMode = .scaleAspectFit

        buttonNext = setupButton(title: LocalizedString("login_button_title_next", comment: "Tiếp tục"), titleColor: .white)
        buttonNext.backgroundColor = UIColor.Navigation.mainColor()
        
        
        addSubview(contentView)
        
        contentView.addSubview(buttonClose)
        contentView.addSubview(buttonNext)
        
    }
    
    func setupView(_ bgColor: UIColor = UIColor.Misc.seperatorColor()) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        view.clipsToBounds = true
        return view
    }
    
    func setupButton(title: String? = nil,
                          titleSelected: String? = nil,
                          image: UIImage? = nil,
                          imageSelected: UIImage? = nil,
                          titleColor: UIColor = .white) -> UIButton {
        let button = UIButton()
        button.setTitleColor( titleColor, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        button.setTitle(title, for: .normal)
        button.setTitle(titleSelected, for: .selected)
        button.setImage(image, for: .normal)
        button.setImage(imageSelected, for: .selected)
        button.clipsToBounds = true
        button.layer.cornerRadius = 5
        return button
    }
}
