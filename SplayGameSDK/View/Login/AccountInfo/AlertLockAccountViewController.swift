//
//  AlertLockAccountViewController.swift
//  SplayGameSDK
//
//  Created by Nguyen Nang on 7/14/20.
//  Copyright © 2020 splay. All rights reserved.
//



import UIKit


class AlertLockAccountViewController: CustomViewController {
    
    //-----------------------
    // MARK: - ENUM
    //-----------------------
    
    enum Size: CGFloat {
        case button = 36, padding10 = 10
    }
    
    //-----------------------
    // MARK: - VAR
    //-----------------------
    var strMessager: String = ""
    
    //-----------------------
    // MARK: - VIEW
    //-----------------------
    
    var imageLogo: UIImageView!
    var lbMessager: UILabel!
    var btnCallCare: UIButton!
    

    //-----------------------
    // MARK: - LIFE CYCLE
    //-----------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if UIScreen.main.bounds.width > UIScreen.main.bounds.height {
            // Landscape
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                landscapeContentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.5, 150))
                
            } else {
                landscapeContentSizeInPopup = CGSize(width: 320, height: 150)
            }
            
            
        } else {
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                contentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.5, 150))
                
            } else {
                contentSizeInPopup = CGSize(width: 320, height: 150)
            }
        }
        
        setupAllSubviews()
        
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        updateFrame()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
    }
    
    func updateFrame() {
        
        btnClose.frame = CGRect(x: Size.padding10.rawValue * 3,
                                y: view.frame.height - Size.button.rawValue - Size.padding10.rawValue,
                                width: view.frame.width / 2 - Size.padding10.rawValue * 3.5,
                                height: Size.button.rawValue)
        
        btnCallCare.frame = CGRect(x: view.frame.width / 2 + Size.padding10.rawValue / 2,
                                y: view.frame.height - Size.button.rawValue - Size.padding10.rawValue,
                                width: view.frame.width / 2 - Size.padding10.rawValue * 3.5,
                                height: Size.button.rawValue)
        
        imageLogo.frame = CGRect(x: Size.padding10.rawValue * 3 / 2,
                                 y: Size.padding10.rawValue * 3 / 2 ,
                                width: 47,
                                height: 47)
        
        lbMessager.frame = CGRect(x: imageLogo.frame.maxX +  Size.padding10.rawValue ,
                                  y: imageLogo.frame.minY,
                                  width: view.frame.width - imageLogo.frame.maxX - Size.padding10.rawValue * 5 / 2,
                                  height: CGFloat.leastNonzeroMagnitude)
        
        lbMessager.sizeToFit()

    }
}






extension AlertLockAccountViewController {
    
    func loadLanguage() {
        
        btnClose.setTitle(LanguageSetting.get("title_button_close", alter: nil), for: .normal)
        btnCallCare.setTitle(LanguageSetting.get("title_button_call", alter: nil), for: .normal)
    }
}

extension AlertLockAccountViewController {
    
    @objc func changeLanguage() {
        loadLanguage()
    }
    
    @objc func orientationChanged(_ sender: Notification) {
        
        let device = UIDevice.current
        
        switch device.orientation {
        case .faceUp:
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                contentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.5, 150))
                
            } else {
                contentSizeInPopup = CGSize(width: 320, height: 150)
            }
            
            
        case .portrait, .portraitUpsideDown:
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                contentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.5, 150))
                
            } else {
                contentSizeInPopup = CGSize(width: 320, height: 150)
            }
            
        case .landscapeLeft, .landscapeRight:
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                landscapeContentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.5, 150))
                
            } else {
                landscapeContentSizeInPopup = CGSize(width: 320, height: 150)
            }
            
            
        default:
            break
        }
        
        updateFrame()
    }
    
    @objc func btnCloseClick(_ sender: UIButton) {
        popupController?.dismiss()
    }
    
    @objc func btnCallClick(_ sender: UIButton) {
        
        Utility.callPhoneNumberShowAlert("19001104")
        
    }
}
    

extension AlertLockAccountViewController: CustomHelpViewDelegate {
    
    func showViewSupport() {
        
    }
    
    func showSettingLanguage() {
        
    }
}


//-----------------------
// MARK: - SETUP
//-----------------------

extension AlertLockAccountViewController {
    
    func setupAllSubviews() {
        
        
        
        title = "Đăng nhập"
        view.backgroundColor = UIColor(red: 248/255.0, green: 248/255.0, blue: 248/255.0, alpha: 1.0)
        view.layer.cornerRadius = 8.0
        view.layer.borderColor =  UIColor(red: 13/255.0, green: 183/255.0, blue: 212/255.0, alpha: 1.0).cgColor
        view.layer.borderWidth = 1.5
        view.layer.masksToBounds = true
        
        setupNotification()
        
    
        
        
        btnClose        = setupButtonCustom(title: LanguageSetting.get("title_button_close", alter: nil), titleColor: .white)
        helpView.delegate = self
        btnClose.addTarget(self, action: #selector(self.btnCloseClick(_:)), for: .touchUpInside)
        btnClose.setBackgroundImage(UtilitieWebService.shareSingleton()?.getImageFromStringBase(dn_bg_bt_li), for: .normal)
    
        btnCallCare   = setupButtonCustom(title: LanguageSetting.get("title_button_call", alter: nil), titleColor: .gray)
        btnCallCare.addTarget(self, action: #selector(self.btnCallClick(_:)), for: .touchUpInside)
        btnCallCare.setBackgroundImage(UtilitieWebService.shareSingleton()?.getImageFromStringBase(dn_bg_bt_play_now), for: .normal)
        
        
        lbMessager = setupLabelCustom(strMessager,
                                textColor: .gray,
                                font: UIFont.systemFont(ofSize: 13),
                                alignment: .center)
        
        imageLogo = setupImageView()
        
     

        view.addSubview(lbMessager)
        view.addSubview(imageLogo)
        view.addSubview(btnClose)
        view.addSubview(btnCallCare)
        
        loadLanguage()
        
   
    }
    
    func setupNotification() {
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.changeLanguage),
                                               name: Notification.Name(rawValue: CHANGE_LANGUAGE_SETTING),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.orientationChanged(_:)),
                                               name: UIDevice.orientationDidChangeNotification,
                                               object: UIDevice.current)
        
        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
        
    }
    
    
    
    
    func setupImageView() -> UIImageView {
        
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = UtilitieWebService.shareSingleton()?.getImageFromStringBase(img_icon_warning_lock)
        return imageView

    }
}



