//
//  InfoFastAccountViewController.swift
//  SplayGameSDK
//
//  Created by Nguyen Nang on 7/24/20.
//  Copyright © 2020 splay. All rights reserved.
//


import UIKit
import GoogleSignIn

class InfoFastAccountViewController: CustomViewController {
    

    //-----------------------
    // MARK: - VAR
    //-----------------------

    var dictInfoFast: [String : Any]?

    
    //-----------------------
    // MARK: - VIEW
    //-----------------------
    
    
    
    var infoAccountView: InfoFastAccountContentView!
    
    
    
    //-----------------------
    // MARK: - LIFE CYCLE
    //-----------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if UIScreen.main.bounds.width > UIScreen.main.bounds.height {
            // Landscape
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                landscapeContentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 320))
                
            } else {
                landscapeContentSizeInPopup = CGSize(width: 336, height: 350)
            }
            
            
        } else {
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                contentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 340))
                
            } else {
                contentSizeInPopup = CGSize(width: 336, height: 350)
            }
        }
        
        
        
        setupAllSubviews()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        
        
        updateFrame()
      
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
 
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
    }
    
    func updateFrame() {
        
        btnClose.frame = CGRect(x: view.frame.width - 40,
                                y: 0,
                                width: 40,
                                height: 40)
        

        labelTitle.frame = CGRect(x: 0,
                                  y: 0,
                                  width: view.frame.width,
                                  height: 40)
        
        seperator.frame = CGRect(x: 0,
                                 y: labelTitle.frame.height - 1 / UIScreen.main.scale,
                                 width: view.frame.width,
                                 height: 1 / UIScreen.main.scale)
        
        helpView.frame = CGRect(x: 0,
                                y: view.frame.height - 40,
                                width: view.frame.width,
                                height: 40)
        
        infoAccountView.frame = CGRect(x: 0,
                                 y: labelTitle.frame.maxY,
                                 width: view.frame.width,
                                 height: helpView.frame.minY - labelTitle.frame.maxY)
        
    }
}






extension InfoFastAccountViewController {
    
    func loadLanguage() {
        
        labelTitle.text = LanguageSetting.get("title_info_account", alter: nil)
        
        infoAccountView.lbTitleSplayID.text = LanguageSetting.get("title_splayid", alter: nil)
        infoAccountView.lbTitleAccount.text = LanguageSetting.get("TitleAccount", alter: nil)
        infoAccountView.lbTitlePhone.text = LanguageSetting.get("TitlePhone", alter: nil)
        infoAccountView.lbTitleEmail.text = "Email"
        
        infoAccountView.lbDescription.text = LanguageSetting.get("fast_login_account", alter: nil)
        infoAccountView.btnRegister.setTitle(LanguageSetting.get("title_button_update", alter: nil), for: .normal)
        
        if let mobile = dictInfoFast?[KEY_MOBILE] as? String, mobile.count > 0 {
            infoAccountView.lbPhone.font = UIFont.systemFont(ofSize: 13)
            infoAccountView.lbPhone.text = mobile
        } else {
            infoAccountView.lbPhone.font = UIFont.italicSystemFont(ofSize: 13)
            infoAccountView.lbPhone.text =  LanguageSetting.get("TitleNotinfo", alter: nil)
        }
        
        if let email = dictInfoFast?[KEY_EMAIL] as? String, email.count > 0 {
            infoAccountView.lbEmail.font = UIFont.systemFont(ofSize: 13)
            infoAccountView.lbEmail.text = email
        } else {
            infoAccountView.lbEmail.font = UIFont.italicSystemFont(ofSize: 13)
            infoAccountView.lbEmail.text =  LanguageSetting.get("TitleNotinfo", alter: nil)
        }
    }
    
    
    func getInfoFastAccount() {
        
        let accessToken = UserDefaults.standard.object(forKey: KEY_SAVE_USER_TOKEN) as? String ?? ""
        UtilitieWebService.shareSingleton()?.gettttk(withAccessToken: accessToken, completion: { (results, error, resultCode) in
            
            guard let resultJson = results as? [String : Any], resultCode == ResultCodeSuccess else { return }
            self.dictInfoFast = resultJson[KEY_DATA] as? [String : Any]
            
            self.infoAccountView.lbSplayID.text = self.dictInfoFast?[KEY_USER_ID] as? String ?? ""
            self.infoAccountView.lbAccount.text = self.dictInfoFast?[KEY_USERNAME] as? String ?? ""
            
            if let mobile = self.dictInfoFast?[KEY_MOBILE] as? String, mobile.count > 0 {
                self.infoAccountView.lbPhone.font = UIFont.systemFont(ofSize: 13)
                self.infoAccountView.lbPhone.text = mobile
            }
            
            if let email = self.dictInfoFast?[KEY_EMAIL] as? String, email.count > 0 {
                self.infoAccountView.lbEmail.font = UIFont.systemFont(ofSize: 13)
                self.infoAccountView.lbEmail.text = email
            }
        })
        
    }

    
}

extension InfoFastAccountViewController {
    
    @objc func changeLanguage() {
           loadLanguage()
       }
       
       @objc func orientationChanged(_ sender: Notification) {
           
           let device = UIDevice.current
           
           switch device.orientation {
           case .faceUp:
               
               if UI_USER_INTERFACE_IDIOM() == .phone {
                   
                   let rect = UIScreen.main.bounds
                   
                   let maxWidth = min(rect.size.width - 20, rect.size.height)
                   contentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 340))
                   
               } else {
                   contentSizeInPopup = CGSize(width: 336, height: 350)
               }
               
               
           case .portrait, .portraitUpsideDown:
               
               if UI_USER_INTERFACE_IDIOM() == .phone {
                   
                   let rect = UIScreen.main.bounds
                   
                   let maxWidth = min(rect.size.width - 20, rect.size.height)
                   contentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 340))
                   
               } else {
                   contentSizeInPopup = CGSize(width: 336, height: 350)
               }
               
           case .landscapeLeft, .landscapeRight:
               
               if UI_USER_INTERFACE_IDIOM() == .phone {
                   
                   let rect = UIScreen.main.bounds
                   
                   let maxWidth = min(rect.size.width - 20, rect.size.height)
                   landscapeContentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 320))
                   
               } else {
                   landscapeContentSizeInPopup = CGSize(width: 336, height: 350)
               }
               
               
           default:
               break
           }
           
           updateFrame()
       }
    
    @objc func btnCloseClick(_ sender: UIButton) {
        popupController?.dismiss()
    }
    
    @objc func btnGotoUpdateFast(_ sender: UIButton) {
        
        let updateVC = UpdateFastAccountViewController()
        updateVC.dictProfile = dictInfoFast
        popupController?.push(updateVC, animated: true)
    }
}

extension InfoFastAccountViewController: CustomHelpViewDelegate {
    
    func showViewSupport() {
        
    }
    
    func showSettingLanguage() {
        
    }
}



//-----------------------
// MARK: - SETUP
//-----------------------

extension InfoFastAccountViewController {
    
    func setupAllSubviews() {
        
        
        
        title = LanguageSetting.get("title_info_account", alter: nil)
        view.backgroundColor = UIColor(red: 248/255.0, green: 248/255.0, blue: 248/255.0, alpha: 1.0)
        view.layer.cornerRadius = 8.0
        view.layer.borderColor =  UIColor(red: 13/255.0, green: 183/255.0, blue: 212/255.0, alpha: 1.0).cgColor
        view.layer.borderWidth = 1.5
        view.layer.masksToBounds = true
        
        setupNotification()
        
        

        
        helpView.delegate = self
        btnClose.addTarget(self, action: #selector(self.btnCloseClick(_:)), for: .touchUpInside)
        
        
 
        infoAccountView     = setupInfoAccountView()

        view.addSubview(infoAccountView)
        view.addSubview(helpView)

        view.addSubview(seperator)
        view.addSubview(labelTitle)
        view.addSubview(btnClose)
        
        
        loadLanguage()
        getInfoFastAccount()
        
    }
    
    func setupNotification() {
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.changeLanguage),
                                               name: Notification.Name(rawValue: CHANGE_LANGUAGE_SETTING),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.orientationChanged(_:)),
                                               name: UIDevice.orientationDidChangeNotification,
                                               object: UIDevice.current)
        
        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
        
    }
    
    
    
    func setupInfoAccountView() -> InfoFastAccountContentView {
        let view = InfoFastAccountContentView()
        
        
        view.btnRegister.addTarget(self, action: #selector(self.btnGotoUpdateFast(_:)), for: .touchUpInside)
        return view
    }
    
    
    

}


