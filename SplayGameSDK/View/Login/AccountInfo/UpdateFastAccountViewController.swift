//
//  UpdateFastAccountViewController.swift
//  SplayGameSDK
//
//  Created by Nguyen Nang on 7/24/20.
//  Copyright © 2020 splay. All rights reserved.
//

import UIKit
import GoogleSignIn
import CleanroomLogger

class UpdateFastAccountViewController: CustomViewController {
    
    
    //-----------------------
    // MARK: - VAR
    //-----------------------
    
    var titleAlert: String      = ""
    var contentAlert: String    = ""
    
    var dictProfile: [String : Any]?
    var fromscreen: ToScreen?
    
    
    //-----------------------
    // MARK: - VIEW
    //-----------------------
    
    
    
    var updateAccountView: UpdateFastAccountContentView!
    
    
    
    //-----------------------
    // MARK: - LIFE CYCLE
    //-----------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if UIScreen.main.bounds.width > UIScreen.main.bounds.height {
            // Landscape
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                landscapeContentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 320))
                
            } else {
                landscapeContentSizeInPopup = CGSize(width: 336, height: 350)
            }
            
            
        } else {
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                contentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 340))
                
            } else {
                contentSizeInPopup = CGSize(width: 336, height: 350)
            }
        }
        
        
        
        setupAllSubviews()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        
        
        updateFrame()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
    }
    
    func updateFrame() {
        
        btnClose.frame = CGRect(x: view.frame.width - 40,
                                y: 0,
                                width: 40,
                                height: 40)
        
        
        labelTitle.frame = CGRect(x: 0,
                                  y: 0,
                                  width: view.frame.width,
                                  height: 40)
        
        seperator.frame = CGRect(x: 0,
                                 y: labelTitle.frame.height - 1 / UIScreen.main.scale,
                                 width: view.frame.width,
                                 height: 1 / UIScreen.main.scale)
        
        helpView.frame = CGRect(x: 0,
                                y: view.frame.height - 40,
                                width: view.frame.width,
                                height: 40)
        
        updateAccountView.frame = CGRect(x: 0,
                                         y: labelTitle.frame.maxY,
                                         width: view.frame.width,
                                         height: helpView.frame.minY - labelTitle.frame.maxY)
        
    }
}




//-----------------------
// MARK: - PRIVATE METHOD
//-----------------------

extension UpdateFastAccountViewController {
    
    
    
    func loadContentWebview() {
        
        /*
        let baseURL = URL(fileURLWithPath: Bundle.main.bundlePath)
        guard let filePath = Bundle.main.path(forResource: "Resources.bundle/policyUse", ofType: "html") else { return }
        
        do {
            
            let htmlFormat: String = try String(contentsOfFile: filePath, encoding: .utf8)
            let contentPolicy: String = LanguageSetting.get("PolicyRegister", alter: nil)
            let htmlString = String(format: htmlFormat, contentPolicy)
            
            updateAccountView.webViewPolicy.loadHTMLString(htmlString, baseURL: baseURL)
            
        } catch let error {
            print("\(error)")
        }
        */
        
        
        let baseURL = URL(fileURLWithPath: Bundle.module.bundlePath)
        guard let filePath = Bundle.module.path(forResource: "policyUse", ofType: "html") else { return }
        
        do {
            
            let htmlFormat: String = try String(contentsOfFile: filePath, encoding: .utf8)
            let contentPolicy: String = LanguageSetting.get("PolicyRegister", alter: nil)
            let htmlString = String(format: htmlFormat, contentPolicy)
            
            updateAccountView.webViewPolicy.loadHTMLString(htmlString, baseURL: baseURL)
            
        } catch let error {
            print("\(error)")
        }
    }
    
    
    func isPhoneNumberValid(_ phoneNumber: String?) -> Bool {
        
        guard let phoneNumber = phoneNumber, phoneNumber.count > 0 else {
            contentAlert = LanguageSetting.get("validator_phone_emtry", alter: nil)
            return false
        }
        
        
        guard phoneNumber.count >= 10 && phoneNumber.count <= 11 else {
            contentAlert = LanguageSetting.get("validator_phone", alter: nil)
            return false
        }
        return true
    }
    
    
    func isPasswordValid(_ password: String?, repassword: String?) -> Bool {
        
        guard let password = password, password.count > 0 else {
            contentAlert = LanguageSetting.get("validator_username_emtry", alter: nil)
            return false
        }
        
        guard password.trimmingCharacters(in: .whitespacesAndNewlines).count >= 6 &&
            password.trimmingCharacters(in: .whitespacesAndNewlines).count <= 32 else {
                contentAlert = LanguageSetting.get("validator_string_length", alter: nil)
                return false
        }
        
//        let rang = password.rangeOfCharacter(from: .letters)
        
        guard password.rangeOfCharacter(from: .letters) != nil else {
            
            contentAlert = LanguageSetting.get("validator_password", alter: nil)
            return false
        }
        
        guard password.rangeOfCharacter(from: .decimalDigits) != nil else {
            
            contentAlert = LanguageSetting.get("validator_password", alter: nil)
            return false
        }
        
        
        guard let repassword = repassword, password == repassword else {
            contentAlert = LanguageSetting.get("validator_confirm", alter: nil)
            return false
        }
        
        return true
    }
    
    
    /*
     -(BOOL)isPasswordValid:(NSString *)password repassword:(NSString*)repassword{
     if (password == nil || [password isEqualToString:@""]) {
     contentAlert = [LanguageSetting get:@"validator_username_emtry" alter:nil];
     return NO;
     }
     if ([password stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length < 6 || [password stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 32) {
     contentAlert = [LanguageSetting get:@"validator_string_length" alter:nil];
     return NO;
     }
     NSRange rang;
     rang = [password rangeOfCharacterFromSet:[NSCharacterSet letterCharacterSet]];
     if (![password rangeOfCharacterFromSet:[NSCharacterSet letterCharacterSet]].length) {
     contentAlert = [LanguageSetting get:@"validator_password" alter:nil];
     return NO;
     }
     if (![password rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]].length) {
     contentAlert = [LanguageSetting get:@"validator_password" alter:nil];
     return NO;
     }
     if (![password isEqualToString:repassword]){
     contentAlert = [LanguageSetting get:@"validator_confirm" alter:nil];
     return NO;
     }
     return YES;
     }
     */
    
    func isUserNameValid(_ userName: String?, password: String?) -> Bool {
        
        guard let userName = userName, userName.count > 0 else {
            contentAlert = LanguageSetting.get("validator_username_emtry", alter: nil)
            return false
        }
        
        let set = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789").inverted
        let alphaNums = CharacterSet.decimalDigits
        let inStringSetAc = CharacterSet(charactersIn: userName)
        
        
        
        guard userName.count >= 4 && userName.count <= 32 else {
            contentAlert = LanguageSetting.get("validator_user_name_string_length", alter: nil)
            return false
        }
        
        //        if userName.rangeOfCharacter(from: set).location != NSNotFound {
        //            contentAlert = LanguageSetting.get("validator_username", alter: nil)
        //            return false
        //        }
        
        guard userName.rangeOfCharacter(from: set) == nil else {
            contentAlert = LanguageSetting.get("validator_username", alter: nil)
            return false
        }
        
        guard !alphaNums.isSuperset(of: inStringSetAc) else {
            contentAlert = LanguageSetting.get("validator_user_name_only_number", alter: nil)
            return false
        }
        
        guard userName != "abcdef" && userName != "abc123" else {
            contentAlert = LanguageSetting.get("validator_simple_user", alter: nil)
            return false
        }
        
        
        guard let password = password, userName != password else {
            contentAlert = LanguageSetting.get("validator_username_different_from_pass", alter: nil)
            return false
        }
        
        return true
    }
    
    
    func loadLanguage() {
        
        labelTitle.text = LanguageSetting.get("TitleViewBindAccount", alter: nil)
        
        
        updateAccountView.tfUsername.attributedPlaceholder =   NSAttributedString(string:LanguageSetting.get("TextFieldRegisterAccountName", alter: nil),
                                                                          attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        


        
        updateAccountView.tfPassword.attributedPlaceholder =   NSAttributedString(string:LanguageSetting.get("TextFieldRegisterPass", alter: nil),
                                                                          attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        


        
        updateAccountView.tfRePassword.attributedPlaceholder =   NSAttributedString(string:LanguageSetting.get("TextFieldRegisterConfirmPass", alter: nil),
                                                                          attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        


        
        updateAccountView.btnRegister.setTitle(LanguageSetting.get("TitleButtonBind", alter: nil), for: .normal)
        
        loadContentWebview()
    }
    
    func updateFastAccount() {
        
        view.endEditing(true)
        guard updateAccountView.btnTerm.isSelected else { return }
        
        guard isUserNameValid(updateAccountView.tfUsername.text, password: updateAccountView.tfPassword.text),
            isPasswordValid(updateAccountView.tfPassword.text, repassword: updateAccountView.tfRePassword.text) else {
            showAlertWithMessage(contentAlert)
            return
        }
        
        let userId = dictProfile?[KEY_USER_ID] as? String ?? ""
        let email = dictProfile?[KEY_EMAIL] as? String ?? ""
        
        HUD.showHUD(onView: self.view)
        
        UtilitieWebService
            .shareSingleton()?
            .updateProfileFastAc(withUserId: userId,
                                 useName: updateAccountView.tfUsername.text ?? "",
                                 passWord: updateAccountView.tfPassword.text ?? "",
                                 email: email,
                                 completion: { (results, error, resultCode) in
                                    
                                    HUD.dismissHUD()
                                    
                                    Utility.logEventDebug(.updateInfoSuccess, parameters: ["user_id" : userId])
                                    
                                    switch resultCode {
                                    case ResultCodeSuccess:
                                        UIApplication.shared.keyWindow?.subviews.forEach { aView in
                                            if aView.isKind(of: ShowInforFastAccView.self) {
                                                aView.removeFromSuperview()
                                            }
                                        }
                                        
                                        Utility.updateAccountInfoWith(results, loginMethod: .byTk)
                                        Utility.trackEventCompleteRegisterOldOrNew("splay_account", results: results)
               

                                        let confirmVC = ConfirmViewController()
                                        confirmVC.dictResultLogin = results as? [String : Any]
                                        confirmVC.toScreen = UpdateFastAccountScreen
                                        self.popupController?.push(confirmVC, animated: true)
                                        
                                    case AccountEsixted:
                                        let existVC = ExistAccountViewController()
                                        existVC.dicResultUpdate = results as? [String : Any]
                                        existVC.dictProfileFast = self.dictProfile
                                        existVC.strAccountName = self.updateAccountView.tfUsername.text ?? ""
                                        existVC.strPassword = self.updateAccountView.tfPassword.text ?? ""
                                        existVC.toScreen = UpdateAccountFastScreen
                                        self.popupController?.push(existVC, animated: true)
                                        
                                    default:
                                        Utility.showAlertFrom(resultCode, results: results, viewController: self)
                                    }
                                    
            })
    }
}


//-----------------------
// MARK: - SELECTOR
//-----------------------

extension UpdateFastAccountViewController {
    
    /**
     Sự kiện bàn phím hiện lên
     */
    @objc func keyboardWillShow(_ sender: Foundation.Notification) {
        
        Log.message(.info, message: "Show keyboard")
        

        
        guard let userInfo = sender.userInfo else { return }
        guard let value = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        guard UIScreen.main.bounds.width > UIScreen.main.bounds.height  else { return }
        
   
            let maxPosition = (popupController?.containerView.frame.origin.y ?? 0) + updateAccountView.frame.minY + updateAccountView.btnRegister.frame.maxY
            
            Log.message(.info, message: "XXX: \(maxPosition) --- \(value.cgRectValue.height) --- \(UIScreen.main.bounds) ")
            
            guard UIScreen.main.bounds.height - value.cgRectValue.height < maxPosition else { return }
            
            let scrollHeight: CGFloat = maxPosition - 41 - (UIScreen.main.bounds.height - value.cgRectValue.height)
            
            Log.message(.info, message: "AAAA: \(scrollHeight) --- \(view.frame)")
            

//            var paddingButton: CGFloat = (value.cgRectValue.origin.y - loginView.btnLogin.frame.height - 5) - scrollHeight - loginView.frame.minY
            
            
            var paddingButton: CGFloat = value.cgRectValue.origin.y - updateAccountView.btnRegister.frame.height - 5 - abs(popupController?.containerView.frame.origin.y ?? 0) - updateAccountView.frame.minY
            
            paddingButton = min(updateAccountView.btnTerm.frame.maxY, paddingButton)
            
        updateAccountView.animationWithKeyboard(isShow: true, y: paddingButton)
            
  
    }
    
    /**
     Sự kiện bàn phím ẩn đi
     */
    @objc func keyboardWillHide(_ sender: Foundation.Notification) {
        
        Log.message(.info, message: "keyboardWillHide")
        updateAccountView.animationWithKeyboard(isShow: false, y: 0)

    }
    
    @objc func changeLanguage() {
        loadLanguage()
    }
    
    @objc func orientationChanged(_ sender: Notification) {
        
        let device = UIDevice.current
        
        switch device.orientation {
        case .faceUp:
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                contentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 340))
                
            } else {
                contentSizeInPopup = CGSize(width: 336, height: 350)
            }
            
            
        case .portrait, .portraitUpsideDown:
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                contentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 340))
                
            } else {
                contentSizeInPopup = CGSize(width: 336, height: 350)
            }
            
        case .landscapeLeft, .landscapeRight:
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                landscapeContentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 320))
                
            } else {
                landscapeContentSizeInPopup = CGSize(width: 336, height: 350)
            }
            
            
        default:
            break
        }
        
        updateFrame()
    }
    
    @objc func btnCloseClick(_ sender: UIButton) {
        popupController?.dismiss()
    }
    
    @objc func btnRegisterClick(_ sender: UIButton) {
        
        updateFastAccount()
    }
}

extension UpdateFastAccountViewController: CustomHelpViewDelegate {
    
    func showViewSupport() {
        
    }
    
    func showSettingLanguage() {
        
    }
}



extension UpdateFastAccountViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        
        
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.isKind(of: DLTextField.self) {
            
            if let nextTextField = (textField as? DLTextField)?.next {
                
                nextTextField.becomeFirstResponder()
                
            } else {
                
                if textField.tag == 2 {
                    updateFastAccount()
                }
            }
        }
        
        
        return true
    }
    
}


//-----------------------
// MARK: - SETUP
//-----------------------

extension UpdateFastAccountViewController: RegisterContentViewDelegate {
    
    func pushViewController(_ controller: UIViewController) {
        popupController?.push(controller, animated: true)
    }
}

//-----------------------
// MARK: - SETUP
//-----------------------

extension UpdateFastAccountViewController {
    
    func setupAllSubviews() {
        
        
        
        title = "Đăng nhập"
        view.backgroundColor = UIColor(red: 248/255.0, green: 248/255.0, blue: 248/255.0, alpha: 1.0)
        view.layer.cornerRadius = 8.0
        view.layer.borderColor =  UIColor(red: 13/255.0, green: 183/255.0, blue: 212/255.0, alpha: 1.0).cgColor
        view.layer.borderWidth = 1.5
        view.layer.masksToBounds = true
        
        setupNotification()
        
        
        
        
        helpView.delegate = self
        btnClose.addTarget(self, action: #selector(self.btnCloseClick(_:)), for: .touchUpInside)
        
        
        
        updateAccountView       = setupUpdateAccountView()
        
        
        view.addSubview(updateAccountView)
        view.addSubview(helpView)
        
        view.addSubview(seperator)
        view.addSubview(labelTitle)
        view.addSubview(btnClose)
        
        loadLanguage()
        
        
    }
    
    func setupNotification() {
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.changeLanguage),
                                               name: Notification.Name(rawValue: CHANGE_LANGUAGE_SETTING),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.orientationChanged(_:)),
                                               name: UIDevice.orientationDidChangeNotification,
                                               object: UIDevice.current)
        
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillShow(_:)),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillHide(_:)),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
        
        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
        
    }
    
    
    
    func setupUpdateAccountView() -> UpdateFastAccountContentView {
        let view = UpdateFastAccountContentView()
        view.delegate = self
        view.tfUsername.delegate = self
        view.tfPassword.delegate = self
        view.tfRePassword.delegate = self
        view.tfUsername.next = view.tfPassword
        view.tfPassword.next = view.tfRePassword
        view.tfRePassword.tag = 2
        
        view.btnRegister.addTarget(self, action: #selector(self.btnRegisterClick(_:)), for: .touchUpInside)
        return view
    }
    
    
}


