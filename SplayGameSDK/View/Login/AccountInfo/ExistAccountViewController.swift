//
//  ExistAccountViewController.swift
//  SplayGameSDK
//
//  Created by Nguyen Nang on 7/24/20.
//  Copyright © 2020 splay. All rights reserved.
//


import UIKit
import GoogleSignIn
import Firebase
import CleanroomLogger

class ExistAccountViewController: CustomViewController {
    
    
    //-----------------------
    // MARK: - VAR
    //-----------------------
    
    var strAcNameSuggest: String = ""
    var contentAlert: String = ""
    var strPassword: String = ""
    var strAccountName: String = ""
    
    
    var dictProfileFast: [String : Any]?
    var dicResultUpdate: [String : Any]?
    
    var toScreen: ToScreen?
    
    
    //-----------------------
    // MARK: - VIEW
    //-----------------------
    
    
    
    var existAccountView: ExistAccountContentView!
    
    
    
    //-----------------------
    // MARK: - LIFE CYCLE
    //-----------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if UIScreen.main.bounds.width > UIScreen.main.bounds.height {
            // Landscape
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                landscapeContentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 320))
                
            } else {
                landscapeContentSizeInPopup = CGSize(width: 336, height: 350)
            }
            
            
        } else {
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                contentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 340))
                
            } else {
                contentSizeInPopup = CGSize(width: 336, height: 350)
            }
        }
        
        
        
        setupAllSubviews()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        
        
        updateFrame()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
    }
    
    func updateFrame() {
        
        btnClose.frame = CGRect(x: view.frame.width - 40,
                                y: 0,
                                width: 40,
                                height: 40)
        
        
        labelTitle.frame = CGRect(x: 0,
                                  y: 0,
                                  width: view.frame.width,
                                  height: 40)
        
        seperator.frame = CGRect(x: 0,
                                 y: labelTitle.frame.height - 1 / UIScreen.main.scale,
                                 width: view.frame.width,
                                 height: 1 / UIScreen.main.scale)
        
        helpView.frame = CGRect(x: 0,
                                y: view.frame.height - 40,
                                width: view.frame.width,
                                height: 40)
        
        existAccountView.frame = CGRect(x: 0,
                                        y: labelTitle.frame.maxY,
                                        width: view.frame.width,
                                        height: helpView.frame.minY - labelTitle.frame.maxY)
        
    }
}






extension ExistAccountViewController {
    
    func isUserNameValid(_ userName: String?, password: String?) -> Bool {
        
        guard let userName = userName, userName.count > 0 else {
            contentAlert = LanguageSetting.get("validator_username_emtry", alter: nil)
            return false
        }
        
        let set = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789").inverted
        let alphaNums = CharacterSet.decimalDigits
        let inStringSetAc = CharacterSet(charactersIn: userName)
        
        
        
        guard userName.count >= 4 && userName.count <= 32 else {
            contentAlert = LanguageSetting.get("validator_user_name_string_length", alter: nil)
            return false
        }
        
        //        if userName.rangeOfCharacter(from: set).location != NSNotFound {
        //            contentAlert = LanguageSetting.get("validator_username", alter: nil)
        //            return false
        //        }
        
        guard userName.rangeOfCharacter(from: set) == nil else {
            contentAlert = LanguageSetting.get("validator_username", alter: nil)
            return false
        }
        
        guard !alphaNums.isSuperset(of: inStringSetAc) else {
            contentAlert = LanguageSetting.get("validator_user_name_only_number", alter: nil)
            return false
        }
        
        guard userName != "abcdef" && userName != "abc123" else {
            contentAlert = LanguageSetting.get("validator_simple_user", alter: nil)
            return false
        }
        
        
        guard let password = password, userName != password else {
            contentAlert = LanguageSetting.get("validator_username_different_from_pass", alter: nil)
            return false
        }
        
        return true
    }
    
    
    @objc func showWelcome() {
        
        TWMessageBarManager.sharedInstance()?.showMessage(withTitle: LanguageSetting.get("text_welcome", alter: nil),
                                                          description: UserDefaults.standard.object(forKey: KEY_SAVE_USERNAME) as? String ?? "",
                                                          type: TWMessageBarMessageTypeInfo,
                                                          statusBarHidden: true,
                                                          callback: nil)
        
//        MessageBarManager.sharedInstance().showMessage(withTitle: LanguageSetting.get("text_welcome", alter: nil),
//                                                       description: UserDefaults.standard.object(forKey: KEY_SAVE_USERNAME) as? String ?? "",
//                                                       type: .info,
//                                                       duration: 0.7,
//                                                       statusBarStyle: .default,
//                                                       callback: nil)
        
    }
    
    
    func loadLanguage() {
        
        labelTitle.text = LanguageSetting.get("TitleView", alter: nil)
        existAccountView.lbTitle.text = LanguageSetting.get("lbl_duplicate_user", alter: nil)
        
        existAccountView.tfOtherAccount.attributedPlaceholder =   NSAttributedString(string:LanguageSetting.get("Input_other_name", alter: nil),
                                                                          attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        


        
        
        existAccountView.btnPlayNow.setTitle(LanguageSetting.get("TitleButtonPlayNow", alter: nil), for: .normal)
        existAccountView.btnRegister.setTitle(LanguageSetting.get("TitleButtonRegister", alter: nil), for: .normal)
        
    }
    
    func setDataForUserSelectWith(_ dictUsersuggest: [String : Any]?) {
        
        guard let dictData = dictUsersuggest else { return }
        guard let arr = dictData[KEY_DATA] as? [[String : Any]] else { return }
        
        let arrayUsername = arr.map { $0["UserName"] as? String ?? "" }
        print("arrayUsername: \(arrayUsername)")
        
        existAccountView.btnOption1.isSelected = true
        
        strAcNameSuggest = arrayUsername.first ?? ""
        
        [existAccountView.btnOption1, existAccountView.btnOption2, existAccountView.btnOption3]
            .enumerated()
            .forEach { (i, button) in
                
                guard i < arrayUsername.count else { return }
                button?.setTitle(arrayUsername[i], for: .normal)
        }
        
        
    }
}




extension ExistAccountViewController {
    
    /**
     Sự kiện bàn phím hiện lên
     */
    @objc func keyboardWillShow(_ sender: Foundation.Notification) {
        
        Log.message(.info, message: "Show keyboard")
        

        
        guard let userInfo = sender.userInfo else { return }
        guard let value = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        guard UIScreen.main.bounds.width > UIScreen.main.bounds.height  else { return }
        
   
            let maxPosition = (popupController?.containerView.frame.origin.y ?? 0) + existAccountView.frame.minY + existAccountView.btnRegister.frame.maxY
            
            Log.message(.info, message: "XXX: \(maxPosition) --- \(value.cgRectValue.height) --- \(UIScreen.main.bounds) ")
            
            guard UIScreen.main.bounds.height - value.cgRectValue.height < maxPosition else { return }
            
            let scrollHeight: CGFloat = maxPosition - 41 - (UIScreen.main.bounds.height - value.cgRectValue.height)
            
            Log.message(.info, message: "AAAA: \(scrollHeight) --- \(view.frame)")
            

//            var paddingButton: CGFloat = (value.cgRectValue.origin.y - loginView.btnLogin.frame.height - 5) - scrollHeight - loginView.frame.minY
            
            
            var paddingButton: CGFloat = value.cgRectValue.origin.y - existAccountView.btnRegister.frame.height - 5 - abs(popupController?.containerView.frame.origin.y ?? 0) - existAccountView.frame.minY
            
            paddingButton = min(existAccountView.frame.height - existAccountView.btnRegister.frame.height, paddingButton)
            
        existAccountView.animationWithKeyboard(isShow: true, y: paddingButton)
            
  
    }
    
    /**
     Sự kiện bàn phím ẩn đi
     */
    @objc func keyboardWillHide(_ sender: Foundation.Notification) {
        
        Log.message(.info, message: "keyboardWillHide")
        existAccountView.animationWithKeyboard(isShow: false, y: 0)

    }
    
    @objc func changeLanguage() {
        loadLanguage()
    }
    
    @objc func orientationChanged(_ sender: Notification) {
        
        let device = UIDevice.current
        
        switch device.orientation {
        case .faceUp:
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                contentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 340))
                
            } else {
                contentSizeInPopup = CGSize(width: 336, height: 350)
            }
            
            
        case .portrait, .portraitUpsideDown:
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                contentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 340))
                
            } else {
                contentSizeInPopup = CGSize(width: 336, height: 350)
            }
            
        case .landscapeLeft, .landscapeRight:
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                landscapeContentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.9, 320))
                
            } else {
                landscapeContentSizeInPopup = CGSize(width: 336, height: 350)
            }
            
            
        default:
            break
        }
        
        updateFrame()
    }
    
    @objc func btnCloseClick(_ sender: UIButton) {
        popupController?.dismiss()
    }
    
    @objc func btnPlayGameClick(_ sender: UIButton) {
        
        HUD.showHUD(onView: self.view)
        UtilitieWebService
            .shareSingleton()?
            .dnnhanh(completion: { (results, error, resultCode) in
                
                HUD.dismissHUD()
                //                DVLog(@"status code = %d",resultCode);
                guard resultCode == ResultCodeSuccess else {
                    Utility.sendEventToFirebaseWith(EVEN_START_PLAYNOW_ERROR, parameters: nil)
                    Utility.showAlertFrom(resultCode, results: results, viewController: self)
                    return
                }

               
                
                guard let result = results as? [String : Any] else { return }
                guard let dicDataDNN = result[KEY_DATA] as? [String : Any] else { return }
                
                let userDefault = UserDefaults.standard
                userDefault.set(dicDataDNN, forKey: KEY_DATA_USER_FAST_LOGIN)
                userDefault.synchronize()
                
                Utility.updateAccountInfoWith(results, loginMethod: .byFastTk)
                UnitAnalyticHelper.analyticLoginEvent(MethodDn.byFastTk.key)
                Utility.trackEventCompleteRegisterOldOrNew(MethodDn.byFastTk.key, results: results)
                
                self.perform(#selector(self.showWelcome), with: nil, afterDelay: 1.5)
                self.popupController?.dismiss()

            })
    }
    
    
    func registerWith(_ username: String, password: String) {
        
        if toScreen == RegisterAccountScreen {
            
            HUD.showHUD(onView: self.view)
            UtilitieWebService
                .shareSingleton()?
                .dktk(withTentk: username,
                      mk: password) { (results, error, resultCode) in
                        
                    HUD.dismissHUD()
                        
                        guard resultCode == ResultCodeSuccess else {
                            
                            Utility.sendEventToFirebaseWith(EVEN_SIGNIN_NORMAL_ERROR, parameters: nil)
                            Utility.showAlertFrom(resultCode, results: results, viewController: self)
                            return
                        }

                        Log.message(.info, message: "Register sussess with Username: \(username) | Password: \(password)")
                        
                        Utility.updateAccountInfoWith(results, isLogin: nil, sendEvent: false, isPostNoti: false)
                        Utility.trackEventCompleteRegisterOldOrNew("splay_account", results: results)
       
                        let confirmVC = ConfirmViewController()
                        confirmVC.dictResultLogin = results as? [String : Any]
                        confirmVC.toScreen = ExistAccountScreen
                        self.popupController?.push(confirmVC, animated: true)
                }
            
        } else {
            
            let userId = dictProfileFast?[KEY_USER_ID] as? String ?? ""
            let email = dictProfileFast?[KEY_EMAIL] as? String ?? ""
            
            HUD.showHUD(onView: self.view)
            UtilitieWebService
                .shareSingleton()?
                .updateProfileFastAc(withUserId: userId,
                                     useName: username,
                                     passWord: password,
                                     email: email,
                                     completion: { (results, error, resultCode) in
                                        
                                        HUD.dismissHUD()
                                        
                                     
                                        guard resultCode == ResultCodeSuccess else {
                                            Utility.showAlertFrom(resultCode, results: results, viewController: self)
                                            return
                                        }

                                        UIApplication.shared.keyWindow?.subviews.forEach { aView in
                                            
                                            if aView.isKind(of: ShowInforFastAccView.self) {
                                                aView.removeFromSuperview()
                                            }
                                        }
                                        
                                        Utility.updateAccountInfoWith(results, isLogin: nil, sendEvent: false, isPostNoti: false)
                                        Utility.trackEventCompleteRegisterOldOrNew("splay_account", results: results)
                                       
                                        

                                        let confirmVC = ConfirmViewController()
                                        confirmVC.dictResultLogin = results as? [String : Any]
                                        confirmVC.toScreen = ExistAccountScreen
                                        self.popupController?.push(confirmVC, animated: true)
                                        
                })
            
        }
    }
    
    
    @objc func btnRegisterClick(_ sender: UIButton) {
        
        guard let acOther = existAccountView.tfOtherAccount.text, acOther.count > 0 else {
            registerWith(strAcNameSuggest, password: strPassword)
            return
        }
        
        guard isUserNameValid(acOther, password: strPassword) else {

            Utility.showAlertWithMessage(contentAlert, viewController: self)
            return
        }
        
        if toScreen == RegisterAccountScreen {
            
            HUD.showHUD(onView: self.view)
            UtilitieWebService
                .shareSingleton()?
                .dktk(withTentk: acOther,
                      mk: self.strPassword,
                      completion: { (results, error, resultCode) in
                        
                        HUD.dismissHUD()
                        
                        if resultCode == ResultCodeSuccess {
                            //                            DVLog(@"Register sussess");
                            
                            guard let dicDataRegister = results as? [String : Any] else { return }
              
                            Utility.trackEventCompleteRegisterOldOrNew("splay_account", results: results)

                            let confirmVC = ConfirmViewController()
                            confirmVC.dictResultLogin = dicDataRegister
                            self.popupController?.push(confirmVC, animated: true)
                            
                        } else if resultCode == AccountEsixted {
                            
                            self.setDataForUserSelectWith(results as? [String : Any])
                            self.strAccountName = acOther
                            self.existAccountView.tfOtherAccount.text = ""
                        }
                        
                })
            
            
        } else {
            
            let userId = dictProfileFast?[KEY_USER_ID] as? String ?? ""
            let email = dictProfileFast?[KEY_EMAIL] as? String ?? ""
            
            HUD.showHUD(onView: self.view)
            UtilitieWebService
                .shareSingleton()?
                .updateProfileFastAc(withUserId: userId,
                                     useName: acOther,
                                     passWord: strPassword,
                                     email: email,
                                     completion: { (results, error, resultCode) in
                                        
                                        HUD.dismissHUD()
                                        
                                        guard resultCode == ResultCodeSuccess else {
                                            
                                            Utility.showAlertFrom(resultCode, results: results, viewController: self)
                                            return
                                        }
                                        
                                        Utility.trackEventCompleteRegisterOldOrNew("splay_account", results: results)
                          
                                        UIApplication.shared.keyWindow?.subviews.forEach { aView in
                                            if aView.isKind(of: ShowInforFastAccView.self) {
                                                aView.removeFromSuperview()
                                            }
                                        }
                                        
                                        let confirmVC = ConfirmViewController()
                                        confirmVC.dictResultLogin = results as? [String : Any]
                                        confirmVC.toScreen = ExistAccountScreen
                                        self.popupController?.push(confirmVC, animated: true)
                                        
                                        
                })
        }
        
    }
    
    @objc func btnRefreshClick(_ sender: UIButton) {
        
        HUD.showHUD(onView: self.view)
        
        UtilitieWebService
            .shareSingleton()?
            .dktk(withTentk: strAccountName,
                  mk: strPassword) { (results, error, resultCode) in
                
                HUD.dismissHUD()
                
                guard resultCode == AccountEsixted else {
                    
                    if resultCode == ResultCodeSuccess {
                        Utility.showAlertFrom(resultCode, results: results, viewController: self)
                    }
                    return
                    
                }

                Log.message(.info, message: "+++++++++++++++data \(String(describing: results))")
                self.setDataForUserSelectWith(results as? [String : Any])
                
            }
    }
    
    @objc func onRadioButtonValueChanged(_ sender: RadioButton) {
        
        guard sender.isSelected else { return }
        strAcNameSuggest = sender.titleLabel?.text ?? ""
    }
}

extension ExistAccountViewController: CustomHelpViewDelegate {
    
    func showViewSupport() {
        
    }
    
    func showSettingLanguage() {
        
    }
}

//--------------------------------
// MARK: - TEXT FIELD DELEGATE
//--------------------------------

extension ExistAccountViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        var newLength =  string.count - range.length
        
        if let name = textField.text {
            newLength += name.count
        }
        
        return !(newLength > 40)
    }
}


//-----------------------
// MARK: - SETUP
//-----------------------

extension ExistAccountViewController {
    
    func setupAllSubviews() {
        
        
        
        title = "Exist Account"
        view.backgroundColor = UIColor(red: 248/255.0, green: 248/255.0, blue: 248/255.0, alpha: 1.0)
        view.layer.cornerRadius = 8.0
        view.layer.borderColor =  UIColor(red: 13/255.0, green: 183/255.0, blue: 212/255.0, alpha: 1.0).cgColor
        view.layer.borderWidth = 1.5
        view.layer.masksToBounds = true
        
        setupNotification()
        
        
        
        
        helpView.delegate = self
        btnClose.addTarget(self, action: #selector(self.btnCloseClick(_:)), for: .touchUpInside)
        
        
        
        existAccountView       = setupExistAccountView()
        
        
        view.addSubview(existAccountView)
        view.addSubview(helpView)
        
        view.addSubview(seperator)
        view.addSubview(labelTitle)
        view.addSubview(btnClose)
        
        loadLanguage()
        
        setDataForUserSelectWith(dicResultUpdate)
        
        //        DVLog(@"user register %@",self.strAccountName);
    }
    
    func setupNotification() {
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.changeLanguage),
                                               name: Notification.Name(rawValue: CHANGE_LANGUAGE_SETTING),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.orientationChanged(_:)),
                                               name: UIDevice.orientationDidChangeNotification,
                                               object: UIDevice.current)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillShow(_:)),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillHide(_:)),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
        
        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
        
    }
    
    
    
    func setupExistAccountView() -> ExistAccountContentView {
        let view = ExistAccountContentView()
        view.btnRegister.addTarget(self, action: #selector(self.btnRegisterClick(_:)), for: .touchUpInside)
        view.btnPlayNow.addTarget(self, action: #selector(self.btnPlayGameClick(_:)), for: .touchUpInside)
        view.btnRefresh.addTarget(self, action: #selector(self.btnRefreshClick(_:)), for: .touchUpInside)
        
        view.tfOtherAccount.delegate = self
        
        view.btnOption1.addTarget(self, action: #selector(self.onRadioButtonValueChanged(_:)), for: .valueChanged)
        view.btnOption2.addTarget(self, action: #selector(self.onRadioButtonValueChanged(_:)), for: .valueChanged)
        view.btnOption3.addTarget(self, action: #selector(self.onRadioButtonValueChanged(_:)), for: .valueChanged)
        
        return view
    }
    
    
}


