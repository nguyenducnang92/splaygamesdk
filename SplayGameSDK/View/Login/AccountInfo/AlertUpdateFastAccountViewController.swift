//
//  AlertUpdateFastAccountViewController.swift
//  SplayGameSDK
//
//  Created by Nguyen Nang on 7/17/20.
//  Copyright © 2020 splay. All rights reserved.
//


import UIKit


class AlertUpdateFastAccountViewController: CustomViewController {
    
    //-----------------------
    // MARK: - ENUM
    //-----------------------
    
    enum Size: CGFloat {
        case button = 36, padding10 = 10
    }
    
    //-----------------------
    // MARK: - VAR
    //-----------------------
    var accessToken: String = ""
    
    //-----------------------
    // MARK: - VIEW
    //-----------------------
    
    var imageLogo: UIImageView!
    var lbMessager: UILabel!
    var btnNextPlay: UIButton!
    var btnAgree: UIButton!
    
    var infoView: ShowInforFastAccView?


    //-----------------------
    // MARK: - LIFE CYCLE
    //-----------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if UIScreen.main.bounds.width > UIScreen.main.bounds.height {
            // Landscape
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                landscapeContentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.5, 150))
                
            } else {
                landscapeContentSizeInPopup = CGSize(width: 320, height: 150)
            }
            
            
        } else {
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                contentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.5, 150))
                
            } else {
                contentSizeInPopup = CGSize(width: 320, height: 150)
            }
        }
        
        setupAllSubviews()
        
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        updateFrame()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
    }
    
    func updateFrame() {
        
        btnAgree.frame = CGRect(x: Size.padding10.rawValue * 3,
                                y: view.frame.height - Size.button.rawValue - Size.padding10.rawValue,
                                width: view.frame.width / 2 - Size.padding10.rawValue * 3.5,
                                height: Size.button.rawValue)
        
        btnNextPlay.frame = CGRect(x: view.frame.width / 2 + Size.padding10.rawValue / 2,
                                y: view.frame.height - Size.button.rawValue - Size.padding10.rawValue,
                                width: view.frame.width / 2 - Size.padding10.rawValue * 3.5,
                                height: Size.button.rawValue)
        
        imageLogo.frame = CGRect(x: Size.padding10.rawValue * 3 / 2,
                                 y: Size.padding10.rawValue * 3 / 2 ,
                                width: 47,
                                height: 47)
        
        lbMessager.frame = CGRect(x: imageLogo.frame.maxX +  Size.padding10.rawValue ,
                                  y: imageLogo.frame.minY,
                                  width: view.frame.width - imageLogo.frame.maxX - Size.padding10.rawValue * 5 / 2,
                                  height: CGFloat.leastNonzeroMagnitude)
        
        lbMessager.sizeToFit()

    }
}






extension AlertUpdateFastAccountViewController {
    
    func loadLanguage() {
        
        lbMessager.text =  LanguageSetting.get("fast_login_account", alter: nil)
        btnAgree.setTitle(LanguageSetting.get("title_button_agree", alter: nil), for: .normal)
        btnNextPlay.setTitle(LanguageSetting.get("title_button_nextplay", alter: nil), for: .normal)

    }
}

extension AlertUpdateFastAccountViewController {
    
    @objc func changeLanguage() {
        loadLanguage()
    }
    
    @objc func orientationChanged(_ sender: Notification) {
        
        let device = UIDevice.current
        
        switch device.orientation {
        case .faceUp:
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                contentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.5, 150))
                
            } else {
                contentSizeInPopup = CGSize(width: 320, height: 150)
            }
            
            
        case .portrait, .portraitUpsideDown:
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                contentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.5, 150))
                
            } else {
                contentSizeInPopup = CGSize(width: 320, height: 150)
            }
            
        case .landscapeLeft, .landscapeRight:
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                
                let rect = UIScreen.main.bounds
                
                let maxWidth = min(rect.size.width - 20, rect.size.height)
                landscapeContentSizeInPopup = CGSize(width: maxWidth, height: max(maxWidth * 0.5, 150))
                
            } else {
                landscapeContentSizeInPopup = CGSize(width: 320, height: 150)
            }
            
            
        default:
            break
        }
        
        updateFrame()
    }
    
    @objc func btnAgree(_ sender: UIButton) {
        
        UtilitieWebService
            .shareSingleton()?
            .gettttk(withAccessToken: accessToken,
                     completion: { (results, error, resultCode) in
                        
                        guard resultCode == ResultCodeSuccess else { return }
                        
                        let updateAccountVC = UpdateFastAccountViewController()
                        updateAccountVC.dictProfile = (results as? [String : Any])?[KEY_DATA] as? [String : Any]
                        self.popupController?.push(updateAccountVC, animated: true)
                        
            })
    }
    
    @objc func btnGotoGame(_ sender: UIButton) {
        
        UIApplication.shared.keyWindow?.subviews.forEach { aView in
            
            if aView.isKind(of: ShowInforFastAccView.self) {
                aView.removeFromSuperview()
            }
        }
        
        infoView = ShowInforFastAccView(frame: CGRect(x: 5, y: 5, width: 50, height: 50))
        infoView?.enableDragging()
        UIApplication.shared.keyWindow?.addSubview(infoView!)

        popupController?.dismiss()
    }
}
    

extension AlertUpdateFastAccountViewController: CustomHelpViewDelegate {
    
    func showViewSupport() {
        
    }
    
    func showSettingLanguage() {
        
    }
}


//-----------------------
// MARK: - SETUP
//-----------------------

extension AlertUpdateFastAccountViewController {
    
    func setupAllSubviews() {
        
        
        
        title = "Đăng nhập"
        view.backgroundColor = UIColor(red: 248/255.0, green: 248/255.0, blue: 248/255.0, alpha: 1.0)
        view.layer.cornerRadius = 8.0
        view.layer.borderColor =  UIColor(red: 13/255.0, green: 183/255.0, blue: 212/255.0, alpha: 1.0).cgColor
        view.layer.borderWidth = 1.5
        view.layer.masksToBounds = true
        
        setupNotification()
        
        accessToken = UserDefaults.standard.string(forKey: KEY_SAVE_USER_TOKEN) ?? ""
        
        
        btnAgree        = setupButton(title: LanguageSetting.get("title_button_agree", alter: nil), titleColor: .white)
        btnAgree.addTarget(self, action: #selector(self.btnAgree(_:)), for: .touchUpInside)
        btnAgree.setBackgroundImage(UtilitieWebService.shareSingleton()?.getImageFromStringBase(dn_bg_bt_li), for: .normal)
    
        btnNextPlay   = setupButton(title: LanguageSetting.get("title_button_nextplay", alter: nil), titleColor: .gray)
        btnNextPlay.addTarget(self, action: #selector(self.btnGotoGame(_:)), for: .touchUpInside)
        btnNextPlay.setBackgroundImage(UtilitieWebService.shareSingleton()?.getImageFromStringBase(dn_bg_bt_play_now), for: .normal)
        
        
        lbMessager = setupLabel(LanguageSetting.get("fast_login_account", alter: nil),
                                textColor: .gray,
                                font: UIFont.systemFont(ofSize: 13),
                                alignment: .center)
        
        imageLogo = setupImageView()


        view.addSubview(lbMessager)
        view.addSubview(imageLogo)
        view.addSubview(btnAgree)
        view.addSubview(btnNextPlay)
        
        loadLanguage()
        
   
    }
    
    func setupNotification() {
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.changeLanguage),
                                               name: Notification.Name(rawValue: CHANGE_LANGUAGE_SETTING),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.orientationChanged(_:)),
                                               name: UIDevice.orientationDidChangeNotification,
                                               object: UIDevice.current)
        
        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
        
    }
    
    
    

    
    
    
    func setupImageView() -> UIImageView {
        
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = UtilitieWebService.shareSingleton()?.getImageFromStringBase(img_icon_w)
        return imageView

    }
    
    func setupButton(title: String? = nil,
                     titleSelected: String? = nil,
                     image: UIImage? = nil,
                     imageSelected: UIImage? = nil,
                     titleColor: UIColor = .white) -> UIButton {
        let button = UIButton()
        button.setTitleColor( titleColor, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 11)
        button.setTitle(title, for: .normal)
        button.setTitle(titleSelected, for: .selected)
        button.setImage(image, for: .normal)
        button.setImage(imageSelected, for: .selected)
        button.clipsToBounds = true
        button.imageView?.contentMode = .scaleAspectFit
        return button
    }
    

    
    func setupLabel(_ title: String = "", textColor: UIColor = UIColor.black,
                    font: UIFont = UIFont.systemFont(ofSize: 12),
                    bgColor: UIColor = .clear,
                    alignment: NSTextAlignment = .left) -> UILabel {
        
        let label = UILabel()
        label.text = title
        label.textColor = textColor
        label.backgroundColor = bgColor
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = alignment
        label.font = font
        return label
    }
}




