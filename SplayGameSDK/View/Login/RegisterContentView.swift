//
//  RegisterContentView.swift
//  SplayGameSDK
//
//  Created by Nguyen Nang on 7/7/20.
//  Copyright © 2020 splay. All rights reserved.
//


import UIKit
import WebKit


protocol RegisterContentViewDelegate: class {
    func pushViewController(_ controller: UIViewController)
}

class RegisterContentView: UIView {
    
    enum Size: CGFloat {
        case padding10 = 8, textField = 36, country = 65
    }
    
    weak var delegate: RegisterContentViewDelegate?
    
    var tfUsername: DLTextField!
    var tfPassword: DLTextField!
    var tfRePassword: DLTextField!
    
    var tfPhoneNumber: DLTextField!
    
    var btnSelectCountry: UIButton!
    var btnSwitchAccount: UIButton!

    var btnTerm: UIButton!
    var webViewPolicy: WKWebView!
    var btnRegister: UIButton!
    
    var tableView: UITableView!
   

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupAllSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        setupAllSubviews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        setupFrame()
    }

    
    func setupFrame() {
        
        
        btnSwitchAccount.frame = CGRect(x: bounds.width - Size.padding10.rawValue - Size.textField.rawValue,
                                        y: Size.padding10.rawValue,
                                        width: Size.textField.rawValue,
                                        height: Size.textField.rawValue)
        
        btnSelectCountry.frame = CGRect(x: Size.padding10.rawValue,
                                        y: Size.padding10.rawValue,
                                        width: Size.country.rawValue,
                                        height: Size.textField.rawValue)
        
        tableView.frame = CGRect(x: btnSelectCountry.frame.minX,
                                 y: btnSelectCountry.frame.maxY,
                                 width: btnSelectCountry.frame.width,
                                 height: 100)
        
        tfUsername.frame = CGRect(x: Size.padding10.rawValue,
                                  y: Size.padding10.rawValue,
                                  width: btnSwitchAccount.frame.minX - Size.padding10.rawValue * 2,
                                  height: Size.textField.rawValue)
        
        tfPhoneNumber.frame = CGRect(x: btnSelectCountry.frame.maxX + Size.padding10.rawValue,
                                     y: Size.padding10.rawValue,
                                     width: btnSwitchAccount.frame.minX - btnSelectCountry.frame.maxX - Size.padding10.rawValue * 2,
                                     height: Size.textField.rawValue)
        
        tfPassword.frame = CGRect(x: Size.padding10.rawValue,
                                  y: tfUsername.frame.maxY + Size.padding10.rawValue,
                                  width: bounds.width - Size.padding10.rawValue * 2,
                                  height: Size.textField.rawValue)
        
        tfRePassword.frame = CGRect(x: Size.padding10.rawValue,
                                  y: tfPassword.frame.maxY + Size.padding10.rawValue,
                                  width: bounds.width - Size.padding10.rawValue * 2,
                                  height: Size.textField.rawValue)
        
        btnTerm.frame = CGRect(x: Size.padding10.rawValue,
                                 y: tfRePassword.frame.maxY + Size.padding10.rawValue * 3 / 2,
                                 width: Size.textField.rawValue + Size.padding10.rawValue,
                                 height: Size.textField.rawValue)
        
        
        webViewPolicy.frame = CGRect(x: btnTerm.frame.maxX - Size.padding10.rawValue * 2,
                                  y: btnTerm.frame.minY ,
                                  width: bounds.width - btnTerm.frame.maxX ,
                                  height: btnTerm.frame.height)
        
        btnRegister.frame = CGRect(x: bounds.width / 3,
                                y: btnTerm.frame.maxY + Size.padding10.rawValue,
                                width: bounds.width / 3,
                                height: Size.textField.rawValue)
        
       
    }
    
}


//--------------------------
// MARK: - PRIVATE METHOD
//--------------------------

extension RegisterContentView {

    func animationWithKeyboard(isShow: Bool, y: CGFloat) {
        
        
        let minY: CGFloat = isShow ? max(tfRePassword.frame.maxY + 5, y) : (btnTerm.frame.maxY + Size.padding10.rawValue)
        UIView.animate(withDuration: 0.3) {  self.btnRegister.frame.origin.y = minY }
    }
    
}


//--------------------------
// MARK: - SELECTOR
//--------------------------

extension RegisterContentView {
    
    @objc func btnhiddenText(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        if sender.tag == 1 {
             tfPassword.isSecureTextEntry = !sender.isSelected
        } else {
             tfRePassword.isSecureTextEntry = !sender.isSelected
        }
    }
    
    
    
    @objc func btnTermClick(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        btnRegister.isSelected = sender.isSelected
        btnRegister.isUserInteractionEnabled = sender.isSelected
    }
    
    
    @objc func swicthAccountClick(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        
        tfUsername.isHidden = sender.isSelected
        tfPhoneNumber.isHidden = !sender.isSelected
        btnSelectCountry.isHidden = !sender.isSelected
        tableView.isHidden = true
        
        if !sender.isSelected {
            btnSelectCountry.isSelected = false
        }
    }
    
    
    @objc func btnSelectCountryClick(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        tableView.isHidden = !sender.isSelected
        
    }
    
}



//--------------------------
// MARK: - DELEGATE
//--------------------------

extension RegisterContentView : WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        
    }
    
    
    
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        


    }

    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
                
        let myURL = navigationAction.request.url
        
        switch navigationAction.navigationType {
        case .linkActivated:
            
            guard webView == webViewPolicy else {
                 
                if let url = myURL, UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
                
                decisionHandler(.cancel)
                return
            }
            
            
            let myString = myURL?.lastPathComponent
            
            let privacyViewController = PrivacyViewController()
            privacyViewController.pageLoad = myString
            delegate?.pushViewController(privacyViewController)

        default:
            break
        }
        
         decisionHandler(.allow)
    }
    

}

extension RegisterContentView : WKUIDelegate {
    
    
    
    @available(iOS 10.0, *)
    func webView(_ webView: WKWebView, shouldPreviewElement elementInfo: WKPreviewElementInfo) -> Bool {
        
        return true
    }
    
    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping () -> Void) {
        
    }
    
    func webView(_ webView: WKWebView, commitPreviewingViewController previewingViewController: UIViewController) {
        
    }
    
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        
        
        if let targetFrame = navigationAction.targetFrame {
            
            if !targetFrame.isMainFrame {
                webView.load(navigationAction.request)
            }
        } else {
            webView.load(navigationAction.request)
        }
        return nil
    }
}

//--------------------------
// MARK: - SETUP
//--------------------------

extension RegisterContentView {
    
    func setupAllSubviews() {
        
        tfUsername = setupTextField("Tài khoản")
        tfPhoneNumber = setupTextField("Điện thoại")
        tfPhoneNumber.keyboardType = .numberPad
        
        tfPassword = setupTextField("Mật khẩu", isSecureTextEntry: true)
        tfPassword.returnKeyType = .next
        
        tfRePassword = setupTextField("Nhập lại mật khẩu", isSecureTextEntry: true)
 
        btnSwitchAccount = setupButton(image: UtilitieWebService.shareSingleton()?.getImageFromStringBase(dn_bg_bt_dk_mobile),
                                       imageSelected: UtilitieWebService.shareSingleton()?.getImageFromStringBase(dn_bg_bt_dk_account))
        
        btnSwitchAccount.contentHorizontalAlignment = .right
        btnSwitchAccount.contentMode = .scaleAspectFit
        btnSwitchAccount.imageView?.contentMode = .scaleAspectFit
        btnSwitchAccount.addTarget(self, action: #selector(self.swicthAccountClick(_:)), for: .touchUpInside)
        
        btnSelectCountry = setupButton(titleColor: .gray)
        btnSelectCountry.addTarget(self, action: #selector(self.btnSelectCountryClick(_:)), for: .touchUpInside)
        btnSelectCountry.titleEdgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 0)
        btnSelectCountry.titleLabel?.font = UIFont.systemFont(ofSize: 13)
        btnSelectCountry.contentHorizontalAlignment = .left
        btnSelectCountry.setBackgroundImage(UtilitieWebService.shareSingleton()?.getImageFromStringBase(img_bg_coutry), for: .normal)

        tfPassword.rightView = setupButtonPass(tag: 1)
        tfPassword.rightViewRect(forBounds: CGRect(x: 0, y: 0, width: 24, height: Size.textField.rawValue))
        
        tfRePassword.rightView = setupButtonPass(tag: 2)
        tfRePassword.rightViewRect(forBounds: CGRect(x: 0, y: 0, width: 24, height: Size.textField.rawValue))
        
        btnTerm = setupButton(image: UtilitieWebService.shareSingleton()?.getImageFromStringBase(dk_bg_un_checkbox),
                              imageSelected: UtilitieWebService.shareSingleton()?.getImageFromStringBase(dk_bg_checkbox))
        
        btnTerm.contentHorizontalAlignment = .left
        btnTerm.contentMode = .center
        btnTerm.imageView?.contentMode = .center
        btnTerm.addTarget(self, action: #selector(self.btnTermClick(_:)), for: .touchUpInside)
        
        webViewPolicy = setupWebView()
        
        btnRegister = setupButton(title: "Đăng ký".uppercased(), titleColor: .white)
        btnRegister.setBackgroundImage(UtilitieWebService.shareSingleton()?.getImageFromStringBase(dn_bg_bt_li), for: .selected)
        btnRegister.setBackgroundImage(UtilitieWebService.shareSingleton()?.getImageFromStringBase(dn_bg_bt_dis), for: .normal)
    
        tableView = setupTable()
        
        addSubview(btnSelectCountry)
        addSubview(tfPhoneNumber)
        addSubview(tfUsername)
        addSubview(btnSwitchAccount)
        addSubview(tfPassword)
        addSubview(tfRePassword)
        addSubview(webViewPolicy)
        addSubview(btnTerm)
        addSubview(btnRegister)
        addSubview(tableView)
        
        tableView.isHidden = true
        
        
        btnTerm.isSelected = false
        btnRegister.isSelected = btnTerm.isSelected
        
        btnSwitchAccount.isSelected = true
        btnSwitchAccount.sendActions(for: .touchUpInside)
        
        

        
    }
    
    func setupTextField(_ placeHoler: String, textInsets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0),
                        isSecureTextEntry: Bool = false) -> DLTextField {
        let textField = DLTextField()
        textField.placeholder = placeHoler
        textField.layer.cornerRadius = 3
        textField.layer.borderWidth = 0.5
        textField.layer.borderColor = UIColor.lightGray.cgColor
        textField.layer.masksToBounds = true
        textField.textInsets = textInsets
        textField.rightViewMode = isSecureTextEntry ? .always : .never
        textField.isSecureTextEntry = isSecureTextEntry
        textField.font = UIFont.systemFont(ofSize: 14)
        textField.background = UtilitieWebService.shareSingleton()?.getImageFromStringBase(dn_bg_textfile)
        
        textField.returnKeyType = isSecureTextEntry ? .done : .next
        textField.textColor = UIColor.Text.blackMediumColor()
        
        //TODO: NangND
        /*
        if isSecureTextEntry {
            if #available(iOS 12, *) {
                // iOS 12 & 13: Not the best solution, but it works.
                textField.textContentType = .oneTimeCode
            } else {
                // iOS 11: Disables the autofill accessory view.
                // For more information see the explanation below.
                textField.textContentType = .init(rawValue: "")
            }
        }
        */
        

            
        
        return textField
    }
    
    func setupButtonPass(image: UIImage? = nil,
                         imageSelected: UIImage? = nil, tag: Int) -> UIButton {
        let button = UIButton()

        button.setImage(UtilitieWebService.shareSingleton()?.getImageFromStringBase(img_icon_hidden_mk_tk), for: .normal)
        button.setImage(UtilitieWebService.shareSingleton()?.getImageFromStringBase(img_dn_show_mk), for: .selected)
        
        button.clipsToBounds = true
        button.frame = CGRect(x: 0, y: 0, width: 32, height: Size.textField.rawValue )
        button.addTarget(self, action: #selector(self.btnhiddenText(_:)), for: .touchUpInside)
        button.imageView?.contentMode = .scaleAspectFit
        button.contentMode = .scaleAspectFit
        if #available(iOS 13, *) {
            // iOS 12 & 13: Not the best solution, but it works.
  
            button.imageEdgeInsets = UIEdgeInsets(top: 12, left: 24, bottom: 12, right: 0)
            
        } else {
            // iOS 11: Disables the autofill accessory view.
            // For more information see the explanation below.
            button.imageEdgeInsets = UIEdgeInsets(top: 6, left: 6, bottom: 6, right: 6)
        }
        button.tag = tag
        return button
    }
    
    func setupButton(title: String? = nil,
                     titleSelected: String? = nil,
                     image: UIImage? = nil,
                     imageSelected: UIImage? = nil,
                     titleColor: UIColor = .white) -> UIButton {
        let button = UIButton()
        button.setTitleColor( titleColor, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 11)
        button.setTitle(title, for: .normal)
        button.setTitle(titleSelected, for: .selected)
        button.setImage(image, for: .normal)
        button.setImage(imageSelected, for: .selected)
        button.clipsToBounds = false

        return button
    }
    
    func setupLabel(_ title: String = "", textColor: UIColor = .black,
                    font: UIFont = UIFont.systemFont(ofSize: 12),
                    bgColor: UIColor = .clear,
                    alignment: NSTextAlignment = .left) -> UILabel {
        
        let label = UILabel()
        label.text = title
        label.textColor = textColor
        label.backgroundColor = bgColor
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = alignment
        label.font = font
        return label
    }
    
    func setupView(_ bgColor: UIColor = .lightGray) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        view.clipsToBounds = true
        return view
    }
    
    func setupWebView() -> WKWebView {
        
        let webConfiguration = WKWebViewConfiguration()
        
        let web = WKWebView(frame: .zero, configuration: webConfiguration)
        
        web.backgroundColor = .clear
        web.uiDelegate = self
        web.navigationDelegate = self
        //        web.delegate = self
        //        web.scalesPageToFit = true
        web.isOpaque = false
        web.scrollView.isScrollEnabled = false
        web.scrollView.maximumZoomScale = 1 // set as you want.
        web.scrollView.minimumZoomScale = 1 // set as you want.
        web.scrollView.showsHorizontalScrollIndicator = false
        // web.enableLongPressingToSaveImage()
        web.allowsBackForwardNavigationGestures = true
        if #available(iOS 9.0, *) {
            web.allowsLinkPreview = true
        } else {
            // Fallback on earlier versions
        }
        
        return web
    }
    
    func setupTable() -> UITableView {
        
        let table = UITableView(frame: .zero, style: .plain)
        table.backgroundColor = .white
        table.layer.borderWidth = 1 / UIScreen.main.scale
        table.layer.borderColor = UIColor.lightGray.cgColor
        table.register(ListCountryTableViewCell.self, forCellReuseIdentifier: "ListCountryTableViewCell")
        table.separatorStyle = .none
        return table
        
        
        
    }
}

