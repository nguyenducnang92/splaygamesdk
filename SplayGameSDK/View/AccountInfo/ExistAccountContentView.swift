//
//  ExistAccountContentView.swift
//  SplayGameSDK
//
//  Created by Nguyen Nang on 7/24/20.
//  Copyright © 2020 splay. All rights reserved.
//



import UIKit


class ExistAccountContentView: UIView {
    
    enum Size: CGFloat {
        case padding10 = 10, button = 36, label = 30
    }

    

    var lbTitle: UILabel!
    var btnOption1: RadioButton!
    var btnOption2: RadioButton!
    var btnOption3: RadioButton!
    var tfOtherAccount: UITextField!
    var btnRefresh: UIButton!
    var btnRegister: UIButton!
    var btnPlayNow: UIButton!
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupAllSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        setupAllSubviews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        setupFrame()
    }
    
    
    func setupFrame() {
        
        
        btnRegister.frame = CGRect(x: bounds.width / 2 - Size.padding10.rawValue / 2 - bounds.width / 3,
                               y: bounds.height - Size.button.rawValue,
                               width: bounds.width / 3,
                               height: Size.button.rawValue)
        
        btnPlayNow.frame = CGRect(x: bounds.width / 2 + Size.padding10.rawValue / 2,
                               y: bounds.height - Size.button.rawValue,
                               width: bounds.width / 3,
                               height: Size.button.rawValue)
        
        
        lbTitle.frame = CGRect(x: Size.padding10.rawValue,
                                  y: Size.padding10.rawValue,
                                  width: bounds.width - Size.padding10.rawValue * 2,
                                  height: Size.label.rawValue)
        
        
        var maxY: CGFloat = lbTitle.frame.maxY + Size.padding10.rawValue / 2
        
        let arrayButton: [RadioButton] = [btnOption1, btnOption2, btnOption3]

        
        arrayButton.enumerated().forEach { (i, button) in
            
            button.frame = CGRect(x: Size.padding10.rawValue,
                                  y: maxY,
                                  width: bounds.width - Size.padding10.rawValue * 2,
                                  height: Size.button.rawValue)
            
            maxY = button.frame.maxY
        }
        
        maxY += Size.padding10.rawValue / 2
        
        btnRefresh.frame = CGRect(x: bounds.width - Size.button.rawValue - Size.padding10.rawValue,
                                  y: maxY,
                                  width: Size.button.rawValue,
                                  height: Size.button.rawValue)
        
        tfOtherAccount.frame = CGRect(x: Size.padding10.rawValue * 1.5,
                                      y: maxY + 2,
                                      width: btnRefresh.frame.minX - Size.padding10.rawValue * 1.5,
                                      height: Size.button.rawValue - 4)
    }
    
}


//--------------------------
// MARK: - PRIVATE METHOD
//--------------------------

extension ExistAccountContentView {

    func animationWithKeyboard(isShow: Bool, y: CGFloat) {
        
        
        let minY: CGFloat = isShow ? max(tfOtherAccount.frame.maxY + 5, y) : (frame.height - Size.button.rawValue)
        UIView.animate(withDuration: 0.3) {  self.btnRegister.frame.origin.y = minY; self.btnPlayNow.frame.origin.y = minY }
    }
    
}



//--------------------------
// MARK: - SETUP
//--------------------------

extension ExistAccountContentView {
    
    func setupAllSubviews() {
        

        lbTitle = setupLabel(textColor: UIColor.Text.blackMediumColor(),
                             font: UIFont.systemFont(ofSize: 13),
                             alignment: .center)
        
        btnRegister = setupButton(titleColor: .white)
        btnRegister.setBackgroundImage(UtilitieWebService.shareSingleton()?.getImageFromStringBase(dn_bg_bt_li), for: .normal)
        btnPlayNow = setupButton(titleColor: .darkGray)
        btnPlayNow.setBackgroundImage(UtilitieWebService.shareSingleton()?.getImageFromStringBase(dn_bg_bt_play_now), for: .normal)

        

    
        
        tfOtherAccount = setupTextField("")
        btnRefresh = setupButton(image: UtilitieWebService.shareSingleton()?.getImageFromStringBase(img_bg_bt_refessh))
        
        btnOption1 = setupButtonRadio(image: UtilitieWebService.shareSingleton()?.getImageFromStringBase(img_qmk_unchecked),
                               imageSelected: UtilitieWebService.shareSingleton()?.getImageFromStringBase(img_qmk_checked),
                               titleColor: UIColor.Text.blackMediumColor(), tag: 0)
        
        btnOption2 = setupButtonRadio(image: UtilitieWebService.shareSingleton()?.getImageFromStringBase(img_qmk_unchecked),
                               imageSelected: UtilitieWebService.shareSingleton()?.getImageFromStringBase(img_qmk_checked),
                               titleColor: UIColor.Text.blackMediumColor(), tag: 1)
        
        btnOption3 = setupButtonRadio(image: UtilitieWebService.shareSingleton()?.getImageFromStringBase(img_qmk_unchecked),
                               imageSelected: UtilitieWebService.shareSingleton()?.getImageFromStringBase(img_qmk_checked),
                               titleColor: UIColor.Text.blackMediumColor(), tag: 2)

        btnOption1.groupButtons = [btnOption2 as Any, btnOption3 as Any]

        addSubview(lbTitle)
        addSubview(tfOtherAccount)
        addSubview(btnRefresh)
        addSubview(btnRegister)
        addSubview(btnPlayNow)
  
        addSubview(btnOption1)
        addSubview(btnOption2)
        addSubview(btnOption3)
        
        
    }
    
    
    func setupTextField(_ placeHoler: String, textInsets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0),
                        isSecureTextEntry: Bool = false) -> DLTextField {
        let textField = DLTextField()
        textField.textColor = .black
        textField.placeholder = placeHoler
        textField.layer.cornerRadius = 3
        textField.layer.borderWidth = 0.5
        textField.layer.borderColor = UIColor.lightGray.cgColor
        textField.layer.masksToBounds = true
        textField.textInsets = textInsets
        textField.rightViewMode = isSecureTextEntry ? .always : .never
        textField.isSecureTextEntry = isSecureTextEntry
        textField.font = UIFont.systemFont(ofSize: 14)
        textField.background = UtilitieWebService.shareSingleton()?.getImageFromStringBase(dn_bg_textfile)
        textField.returnKeyType = .done
        textField.textColor = UIColor.Text.blackMediumColor()
        return textField
    }
    
    func setupButtonRadio(image: UIImage? = nil,
                          imageSelected: UIImage? = nil,
                          titleColor: UIColor = .white, tag: Int) -> RadioButton {
        let button = RadioButton()
        button.setTitleColor( titleColor, for: .normal)
        button.titleLabel?.font = UIFont.italicSystemFont(ofSize: 14)
        button.setImage(image, for: .normal)
        button.setImage(imageSelected, for: .selected)
        button.clipsToBounds = false
        button.tag = tag
        button.imageView?.contentMode = .center
        button.contentHorizontalAlignment = .center
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 2, bottom: 0, right: 0)
        return button
    }
    
    func setupButton(title: String? = nil,
                     titleSelected: String? = nil,
                     image: UIImage? = nil,
                     imageSelected: UIImage? = nil,
                     titleColor: UIColor = .white) -> UIButton {
        let button = UIButton()
        button.setTitleColor( titleColor, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 11)
        button.setTitle(title, for: .normal)
        button.setTitle(titleSelected, for: .selected)
        button.setImage(image, for: .normal)
        button.setImage(imageSelected, for: .selected)
        button.clipsToBounds = false
        button.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        button.imageView?.contentMode = .scaleAspectFit
        return button
    }
    
    func setupLabel(_ title: String = "", textColor: UIColor = .black,
                    font: UIFont = UIFont.systemFont(ofSize: 12),
                    bgColor: UIColor = .clear,
                    alignment: NSTextAlignment = .left) -> UILabel {
        
        let label = UILabel()
        label.text = title
        label.textColor = textColor
        label.backgroundColor = bgColor
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = alignment
        label.font = font
        return label
    }
    
    func setupView(_ bgColor: UIColor = .lightGray) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        view.clipsToBounds = true
        return view
    }
}




