//
//  InfoFastAccountContentView.swift
//  SplayGameSDK
//
//  Created by Nguyen Nang on 7/24/20.
//  Copyright © 2020 splay. All rights reserved.
//


import UIKit


class InfoFastAccountContentView: UIView {
    
    enum Size: CGFloat {
        case padding10 = 10, button = 36, label = 30
    }

    

    var lbDescription: UILabel!
    
    var lbTitleSplayID: UILabel!
    var lbTitleAccount: UILabel!
    var lbTitlePhone: UILabel!
    var lbTitleEmail: UILabel!
    
    var lbSplayID: UILabel!
    var lbAccount: UILabel!
    var lbPhone: UILabel!
    var lbEmail: UILabel!
   
    var btnRegister: UIButton!
    
   
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupAllSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        setupAllSubviews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        setupFrame()
    }
    
    
    func setupFrame() {
        
        
        btnRegister.frame = CGRect(x: bounds.width / 3,
                                     y: bounds.height - Size.button.rawValue,
                                     width: bounds.width / 3,
                                     height: Size.button.rawValue)
        
        lbTitleSplayID.frame = CGRect(x: Size.padding10.rawValue,
                                      y: Size.padding10.rawValue * 2,
                                      width: bounds.width / 2 - Size.padding10.rawValue * 2,
                                      height: Size.label.rawValue)
        
        lbTitleAccount.frame = CGRect(x: Size.padding10.rawValue,
                                      y: lbTitleSplayID.frame.maxY,
                                      width: bounds.width / 2 - Size.padding10.rawValue * 2,
                                      height: Size.label.rawValue)
        
        lbTitlePhone.frame = CGRect(x: Size.padding10.rawValue,
                                      y: lbTitleAccount.frame.maxY,
                                      width: bounds.width / 2 - Size.padding10.rawValue * 2,
                                      height: Size.label.rawValue)
        
        lbTitleEmail.frame = CGRect(x: Size.padding10.rawValue,
                                      y: lbTitlePhone.frame.maxY,
                                      width: bounds.width / 2 - Size.padding10.rawValue * 2,
                                      height: Size.label.rawValue)
        
        
        lbSplayID.frame = CGRect(x: bounds.width / 2,
                                 y: lbTitleSplayID.frame.minY,
                                 width: bounds.width / 2 - Size.padding10.rawValue,
                                 height: lbTitleSplayID.frame.height)
        
        lbAccount.frame = CGRect(x: bounds.width / 2,
                                 y: lbTitleAccount.frame.minY,
                                 width: bounds.width / 2 - Size.padding10.rawValue,
                                 height: lbTitleAccount.frame.height)
        
        lbPhone.frame = CGRect(x: bounds.width / 2,
                                 y: lbTitlePhone.frame.minY,
                                 width: bounds.width / 2 - Size.padding10.rawValue,
                                 height: lbTitlePhone.frame.height)
        
        lbEmail.frame = CGRect(x: bounds.width / 2,
                                 y: lbTitleEmail.frame.minY,
                                 width: bounds.width / 2 - Size.padding10.rawValue,
                                 height: lbTitleEmail.frame.height)
        
        
        lbDescription.frame = CGRect(x: Size.padding10.rawValue,
                                     y: lbTitleEmail.frame.maxY + Size.padding10.rawValue ,
                                     width: bounds.width - Size.padding10.rawValue * 2,
                                     height: btnRegister.frame.minY - lbTitleEmail.frame.maxY - Size.padding10.rawValue)
        
        
    }
    
}




//--------------------------
// MARK: - SETUP
//--------------------------

extension InfoFastAccountContentView {
    
    func setupAllSubviews() {
        
        lbDescription = setupLabel(textColor: UIColor(red: 214/255.0, green: 42/255.0, blue: 56/255.0, alpha: 1.0),
                                   font: UIFont.italicSystemFont(ofSize: 12),
                                   bgColor: .clear,
                                   alignment: .center)
        
        
        lbTitleSplayID = setupLabel(textColor: .gray,
                                  font: UIFont.systemFont(ofSize: 13),
                                  alignment: .right)
        
        lbTitleAccount = setupLabel(textColor: .gray,
                               font: UIFont.systemFont(ofSize: 13),
                               alignment: .right)
        
        
        lbTitlePhone = setupLabel(textColor: .gray,
                               font: UIFont.systemFont(ofSize: 13),
                               alignment: .right)
        
        
        lbTitleEmail = setupLabel(textColor: .gray,
                               font: UIFont.systemFont(ofSize: 13),
                               alignment: .right)
        
        
        lbSplayID = setupLabel(textColor: .gray,
                               font: UIFont.systemFont(ofSize: 13))
        
        lbAccount = setupLabel(textColor: .gray,
                               font: UIFont.systemFont(ofSize: 13))
        
        
        lbPhone = setupLabel(textColor: .gray,
                               font: UIFont.italicSystemFont(ofSize: 13))
        
        
        lbEmail = setupLabel(textColor: .gray,
                               font: UIFont.italicSystemFont(ofSize: 13))
        
        
        
        btnRegister = setupButton(titleColor: .white)
        btnRegister.setBackgroundImage(UtilitieWebService.shareSingleton()?.getImageFromStringBase(dn_bg_bt_li), for: .normal)

        addSubview(lbDescription)
        addSubview(lbTitleSplayID)
        addSubview(lbTitleAccount)
        addSubview(lbTitlePhone)
        addSubview(lbTitleEmail)
        addSubview(lbSplayID)
        addSubview(lbAccount)
        addSubview(lbPhone)
        addSubview(lbEmail)
        addSubview(btnRegister)
    }
    

    
    func setupButton(title: String? = nil,
                     titleSelected: String? = nil,
                     image: UIImage? = nil,
                     imageSelected: UIImage? = nil,
                     titleColor: UIColor = .white) -> UIButton {
        let button = UIButton()
        button.setTitleColor( titleColor, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 11)
        button.setTitle(title, for: .normal)
        button.setTitle(titleSelected, for: .selected)
        button.setImage(image, for: .normal)
        button.setImage(imageSelected, for: .selected)
        button.clipsToBounds = false

        return button
    }
    
    func setupLabel(_ title: String = "", textColor: UIColor = .black,
                    font: UIFont = UIFont.systemFont(ofSize: 12),
                    bgColor: UIColor = .clear,
                    alignment: NSTextAlignment = .left) -> UILabel {
        
        let label = UILabel()
        label.text = title
        label.textColor = textColor
        label.backgroundColor = bgColor
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = alignment
        label.font = font
        return label
    }
    
    func setupView(_ bgColor: UIColor = .lightGray) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        view.clipsToBounds = true
        return view
    }
}



