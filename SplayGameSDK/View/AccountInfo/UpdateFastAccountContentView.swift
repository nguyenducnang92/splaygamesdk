//
//  UpdateFastAccountContentView.swift
//  SplayGameSDK
//
//  Created by Nguyen Nang on 7/24/20.
//  Copyright © 2020 splay. All rights reserved.
//

import UIKit
import WebKit
class UpdateFastAccountContentView: UIView {
    
    enum Size: CGFloat {
        case padding10 = 8, textField = 36, country = 65
    }
    
 
    var tfUsername: DLTextField!
    var tfPassword: DLTextField!
    var tfRePassword: DLTextField!
    

    var btnTerm: UIButton!
    var webViewPolicy: WKWebView!
    var btnRegister: UIButton!
    
    weak var delegate: RegisterContentViewDelegate?


    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupAllSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        setupAllSubviews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        setupFrame()
    }

    
    func setupFrame() {
        
        

        
        tfUsername.frame = CGRect(x: Size.padding10.rawValue,
                                  y: Size.padding10.rawValue,
                                  width: bounds.width - Size.padding10.rawValue * 2,
                                  height: Size.textField.rawValue)
     
        tfPassword.frame = CGRect(x: Size.padding10.rawValue,
                                  y: tfUsername.frame.maxY + Size.padding10.rawValue,
                                  width: bounds.width - Size.padding10.rawValue * 2,
                                  height: Size.textField.rawValue)
        
        tfRePassword.frame = CGRect(x: Size.padding10.rawValue,
                                  y: tfPassword.frame.maxY + Size.padding10.rawValue,
                                  width: bounds.width - Size.padding10.rawValue * 2,
                                  height: Size.textField.rawValue)
        
        btnTerm.frame = CGRect(x: Size.padding10.rawValue,
                                 y: tfRePassword.frame.maxY + Size.padding10.rawValue * 3 / 2,
                                 width: Size.textField.rawValue + Size.padding10.rawValue,
                                 height: Size.textField.rawValue)
        
        
        webViewPolicy.frame = CGRect(x: btnTerm.frame.maxX - Size.padding10.rawValue * 2,
                                  y: btnTerm.frame.minY ,
                                  width: bounds.width - btnTerm.frame.maxX ,
                                  height: btnTerm.frame.height)
        
        btnRegister.sizeToFit()
        
        btnRegister.frame = CGRect(x: (bounds.width - btnRegister.frame.width - 20) / 2,
                                y: btnTerm.frame.maxY + Size.padding10.rawValue,
                                width: btnRegister.frame.width + 20,
                                height: Size.textField.rawValue)
    }
    
}

//--------------------------
// MARK: - PRIVATE METHOD
//--------------------------

extension UpdateFastAccountContentView {
    
    func animationWithKeyboard(isShow: Bool, y: CGFloat) {
        
        
        let minY: CGFloat = isShow ? max(tfRePassword.frame.maxY + 5, y) : (btnTerm.frame.maxY + Size.padding10.rawValue)
        UIView.animate(withDuration: 0.3) {  self.btnRegister.frame.origin.y = minY }
    }
    
}



//--------------------------
// MARK: - SELECTOR
//--------------------------

extension UpdateFastAccountContentView {
    
    @objc func btnhiddenText(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        if sender.tag == 1 {
             tfPassword.isSecureTextEntry = !sender.isSelected
        } else {
             tfRePassword.isSecureTextEntry = !sender.isSelected
        }
    }
    
    
    
    @objc func btnTermClick(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        btnRegister.isSelected = sender.isSelected
        btnRegister.isUserInteractionEnabled = sender.isSelected
    }
}


extension UpdateFastAccountContentView : WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        
    }
    
    
    
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        


    }

    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
                
        let myURL = navigationAction.request.url
        
        switch navigationAction.navigationType {
        case .linkActivated:
            
            guard webView == webViewPolicy else {
                 
                if let url = myURL, UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
                
                decisionHandler(.cancel)
                return
            }
            
            
            let myString = myURL?.lastPathComponent
            
            let privacyViewController = PrivacyViewController()
            privacyViewController.pageLoad = myString
            delegate?.pushViewController(privacyViewController)

        default:
            break
        }
        
         decisionHandler(.allow)
    }
    

}

extension UpdateFastAccountContentView : WKUIDelegate {
    
    
    
    @available(iOS 10.0, *)
    func webView(_ webView: WKWebView, shouldPreviewElement elementInfo: WKPreviewElementInfo) -> Bool {
        
        return true
    }
    
    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping () -> Void) {
        
    }
    
    func webView(_ webView: WKWebView, commitPreviewingViewController previewingViewController: UIViewController) {
        
    }
    
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        
        
        if let targetFrame = navigationAction.targetFrame {
            
            if !targetFrame.isMainFrame {
                webView.load(navigationAction.request)
            }
        } else {
            webView.load(navigationAction.request)
        }
        return nil
    }
}



//--------------------------
// MARK: - SETUP
//--------------------------

extension UpdateFastAccountContentView {
    
    func setupAllSubviews() {
        
        tfUsername = setupTextField("Tài khoản")

        
        tfPassword = setupTextField("Mật khẩu", isSecureTextEntry: true)
        tfPassword.returnKeyType = .next
        
        tfRePassword = setupTextField("Nhập lại mật khẩu", isSecureTextEntry: true)
        
     
        tfPassword.rightView = setupButtonPass(tag: 1)
        tfPassword.rightViewRect(forBounds: CGRect(x: 0, y: 0, width: 24, height: Size.textField.rawValue))
        
        tfRePassword.rightView = setupButtonPass(tag: 2)
        tfRePassword.rightViewRect(forBounds: CGRect(x: 0, y: 0, width: 24, height: Size.textField.rawValue))
        
        btnTerm = setupButton(image: UtilitieWebService.shareSingleton()?.getImageFromStringBase(dk_bg_un_checkbox),
                              imageSelected: UtilitieWebService.shareSingleton()?.getImageFromStringBase(dk_bg_checkbox))
        
        btnTerm.contentHorizontalAlignment = .left
        btnTerm.contentMode = .center
        btnTerm.imageView?.contentMode = .center
        btnTerm.addTarget(self, action: #selector(self.btnTermClick(_:)), for: .touchUpInside)
        
        webViewPolicy = setupWebView()
        
        btnRegister = setupButton(title: "Liên kết tài khoản".uppercased(), titleColor: .white)
        btnRegister.setBackgroundImage(UtilitieWebService.shareSingleton()?.getImageFromStringBase(dn_bg_bt_li), for: .selected)
        btnRegister.setBackgroundImage(UtilitieWebService.shareSingleton()?.getImageFromStringBase(dn_bg_bt_dis), for: .normal)
    
      
     
        addSubview(tfUsername)
        addSubview(tfPassword)
        addSubview(tfRePassword)
        addSubview(webViewPolicy)
        addSubview(btnTerm)
        addSubview(btnRegister)
      
        btnTerm.isSelected = false
        btnRegister.isSelected = btnTerm.isSelected
    
    }
    
    func setupTextField(_ placeHoler: String, textInsets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0),
                        isSecureTextEntry: Bool = false) -> DLTextField {
        let textField = DLTextField()
        textField.placeholder = placeHoler
        textField.layer.cornerRadius = 3
        textField.layer.borderWidth = 0.5
        textField.layer.borderColor = UIColor.lightGray.cgColor
        textField.layer.masksToBounds = true
        textField.textInsets = textInsets
        textField.rightViewMode = isSecureTextEntry ? .always : .never
        textField.isSecureTextEntry = isSecureTextEntry
        textField.font = UIFont.systemFont(ofSize: 14)
        textField.background = UtilitieWebService.shareSingleton()?.getImageFromStringBase(dn_bg_textfile)
        
        textField.returnKeyType = isSecureTextEntry ? .done : .next
        textField.textColor = UIColor.Text.blackMediumColor()
        return textField
    }
    
    func setupButtonPass(image: UIImage? = nil,
                         imageSelected: UIImage? = nil, tag: Int) -> UIButton {
        let button = UIButton()

        button.setImage(UtilitieWebService.shareSingleton()?.getImageFromStringBase(img_icon_hidden_mk_tk), for: .normal)
        button.setImage(UtilitieWebService.shareSingleton()?.getImageFromStringBase(img_dn_show_mk), for: .selected)
        
        button.clipsToBounds = true
        button.frame = CGRect(x: 0, y: Size.textField.rawValue / 6, width: 24, height: Size.textField.rawValue * 2 / 3 )
        button.addTarget(self, action: #selector(self.btnhiddenText(_:)), for: .touchUpInside)
        button.imageView?.contentMode = .scaleAspectFit
        button.contentMode = .scaleAspectFit
        button.imageEdgeInsets = UIEdgeInsets(top: 12, left: 24, bottom: 12, right: 0)
        button.tag = tag
        return button
    }
    
    func setupButton(title: String? = nil,
                     titleSelected: String? = nil,
                     image: UIImage? = nil,
                     imageSelected: UIImage? = nil,
                     titleColor: UIColor = .white) -> UIButton {
        let button = UIButton()
        button.setTitleColor( titleColor, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 11)
        button.setTitle(title, for: .normal)
        button.setTitle(titleSelected, for: .selected)
        button.setImage(image, for: .normal)
        button.setImage(imageSelected, for: .selected)
        button.clipsToBounds = false

        return button
    }
    
    func setupLabel(_ title: String = "", textColor: UIColor = .black,
                    font: UIFont = UIFont.systemFont(ofSize: 12),
                    bgColor: UIColor = .clear,
                    alignment: NSTextAlignment = .left) -> UILabel {
        
        let label = UILabel()
        label.text = title
        label.textColor = textColor
        label.backgroundColor = bgColor
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = alignment
        label.font = font
        return label
    }
    
    func setupView(_ bgColor: UIColor = .lightGray) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        view.clipsToBounds = true
        return view
    }
    
    func setupWebView() -> WKWebView {
        
        let webConfiguration = WKWebViewConfiguration()
        
        let web = WKWebView(frame: .zero, configuration: webConfiguration)
        
        web.backgroundColor = .clear
        web.uiDelegate = self
        web.navigationDelegate = self
        //        web.delegate = self
        //        web.scalesPageToFit = true
        web.isOpaque = false
        web.scrollView.isScrollEnabled = false
        web.scrollView.maximumZoomScale = 1 // set as you want.
        web.scrollView.minimumZoomScale = 1 // set as you want.
        web.scrollView.showsHorizontalScrollIndicator = false
        // web.enableLongPressingToSaveImage()
        web.allowsBackForwardNavigationGestures = true
        if #available(iOS 9.0, *) {
            web.allowsLinkPreview = true
        } else {
            // Fallback on earlier versions
        }
        
        return web
    }
}


