//
//  ShowInforFastAccView.swift
//  SplayGameSDK
//
//  Created by Nguyen Nang on 7/17/20.
//  Copyright © 2020 splay. All rights reserved.
//

import UIKit
//import STPopup
import STPopup

protocol ShowFaseInfoDelegate: class {
    func chooseShowInfo(_ view: ShowInforFastAccView)
}


class ShowInforFastAccView: UIView {
    
    var btnShowUser: UIButton!
    
    weak var delegate: ShowFaseInfoDelegate?
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupAllSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        setupAllSubviews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        btnShowUser.frame = bounds
    }
    


}

//--------------------------
// MARK: - SELECTOR
//--------------------------

extension ShowInforFastAccView {
    
    @objc func btnShowInfoClick(_ sender: UIGestureRecognizer) {
        
        guard let rootVC = UIApplication.shared.keyWindow?.rootViewController else { return }
        
        let infoAccountVC = STPopupController(rootViewController: InfoFastAccountViewController())
        infoAccountVC.containerView.layer.cornerRadius = 8
        infoAccountVC.present(in: rootVC)
        infoAccountVC.navigationBarHidden = true
        
        delegate?.chooseShowInfo(self)
    }
    
    @objc func orientationChanged(_ sender: Notification) {
        
        self.frame.origin.x = min(max(5, frame.minX), UIScreen.main.bounds.width - frame.width - 5)
        self.frame.origin.y = min(max(5, frame.minY), UIScreen.main.bounds.height - frame.height - 5)
        
    }
}




//--------------------------
// MARK: - SETUP
//--------------------------

extension ShowInforFastAccView {
    
    
    func setupAllSubviews() {
        
        backgroundColor = .clear
        
        btnShowUser = setupButton()
        btnShowUser.contentMode = .scaleToFill
        btnShowUser.setBackgroundImage(UtilitieWebService.shareSingleton()?.getImageFromStringBase(img_icon_user_info),
                                       for: .normal)
        
        
        isUserInteractionEnabled = true
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.btnShowInfoClick(_:))))
        btnShowUser.isUserInteractionEnabled = false

        addSubview(btnShowUser)
        
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.orientationChanged(_:)),
                                               name: UIDevice.orientationDidChangeNotification,
                                               object: UIDevice.current)
    }
    
    
    func setupButton(title: String? = nil,
                     titleSelected: String? = nil,
                     image: UIImage? = nil,
                     imageSelected: UIImage? = nil,
                     titleColor: UIColor = .white) -> UIButton {
        let button = UIButton()
        button.setTitleColor( titleColor, for: .normal)
        button.setTitle(title, for: .normal)
        button.setTitle(titleSelected, for: .selected)
        button.setImage(image, for: .normal)
        button.setImage(imageSelected, for: .selected)
        button.clipsToBounds = true
        return button
    }
}


