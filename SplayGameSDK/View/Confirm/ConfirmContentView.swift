//
//  ConfirmContentView.swift
//  SplayGameSDK
//
//  Created by Nguyen Nang on 7/10/20.
//  Copyright © 2020 splay. All rights reserved.
//


import UIKit


class ConfirmContentView: UIView {
    
    enum Size: CGFloat {
        case padding10 = 10, button = 36, label = 30
    }

    

    var labelTitle: UIButton!
    
    var lbTitleAccount: UILabel!
    var lbTitlePhone: UILabel!
    var lbTitleEmail: UILabel!
    
    var lbAccount: UILabel!
    var lbPhone: UILabel!
    var lbEmail: UILabel!
   
    var btnPlayGame: UIButton!
    
   
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupAllSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        setupAllSubviews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        setupFrame()
    }
    
    
    func setupFrame() {
        
        
        
        labelTitle.frame = CGRect(x: Size.padding10.rawValue,
                                  y: Size.padding10.rawValue,
                                  width: bounds.width - Size.padding10.rawValue * 2,
                                  height: Size.label.rawValue)
        
        btnPlayGame.frame = CGRect(x: bounds.width / 3,
                                     y: bounds.height - Size.button.rawValue,
                                     width: bounds.width / 3,
                                     height: Size.button.rawValue)
        
        lbTitleAccount.frame = CGRect(x: Size.padding10.rawValue,
                                      y: labelTitle.frame.maxY + Size.padding10.rawValue * 2,
                                      width: bounds.width / 2 - Size.padding10.rawValue * 2,
                                      height: Size.label.rawValue)
        
        lbTitlePhone.frame = CGRect(x: Size.padding10.rawValue,
                                      y: lbTitleAccount.frame.maxY,
                                      width: bounds.width / 2 - Size.padding10.rawValue * 2,
                                      height: Size.label.rawValue)
        
        lbTitleEmail.frame = CGRect(x: Size.padding10.rawValue,
                                      y: lbTitlePhone.frame.maxY,
                                      width: bounds.width / 2 - Size.padding10.rawValue * 2,
                                      height: Size.label.rawValue)
        
        lbAccount.frame = CGRect(x: bounds.width / 2,
                                 y: lbTitleAccount.frame.minY,
                                 width: bounds.width / 2 - Size.padding10.rawValue,
                                 height: lbTitleAccount.frame.height)
        
        lbPhone.frame = CGRect(x: bounds.width / 2,
                                 y: lbTitlePhone.frame.minY,
                                 width: bounds.width / 2 - Size.padding10.rawValue,
                                 height: lbTitlePhone.frame.height)
        
        lbEmail.frame = CGRect(x: bounds.width / 2,
                                 y: lbTitleEmail.frame.minY,
                                 width: bounds.width / 2 - Size.padding10.rawValue,
                                 height: lbTitleEmail.frame.height)
        
        
    }
    
}




//--------------------------
// MARK: - SETUP
//--------------------------

extension ConfirmContentView {
    
    func setupAllSubviews() {
        
        labelTitle = setupButton(image: UtilitieWebService.shareSingleton()?.getImageFromStringBase(img_icon_done),
                                 titleColor: UIColor(red: 131/255.0, green: 196/255.0, blue: 88/255.0, alpha: 1.0))
        
        labelTitle.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        labelTitle.isUserInteractionEnabled = false
        labelTitle.contentHorizontalAlignment = .center
        labelTitle.imageView?.contentMode = .scaleAspectFit
        labelTitle.titleLabel?.numberOfLines = 0
        labelTitle.titleEdgeInsets = UIEdgeInsets(top: 0, left: -20, bottom: 0, right: 0)
        labelTitle.titleLabel?.lineBreakMode = .byWordWrapping
        labelTitle.titleLabel?.textAlignment = .left
        
        lbTitleAccount = setupLabel(textColor: .gray,
                               font: UIFont.systemFont(ofSize: 13),
                               alignment: .right)
        
        
        lbTitlePhone = setupLabel(textColor: .gray,
                               font: UIFont.systemFont(ofSize: 13),
                               alignment: .right)
        
        
        lbTitleEmail = setupLabel(textColor: .gray,
                               font: UIFont.systemFont(ofSize: 13),
                               alignment: .right)
        
        
        lbAccount = setupLabel(textColor: .gray,
                               font: UIFont.systemFont(ofSize: 13))
        
        
        lbPhone = setupLabel(textColor: .gray,
                               font: UIFont.italicSystemFont(ofSize: 13))
        
        
        lbEmail = setupLabel(textColor: .gray,
                               font: UIFont.italicSystemFont(ofSize: 13))
        
        
        
        btnPlayGame = setupButton(title: LanguageSetting.get("title_button_playgame", alter: ""),
                                  titleColor: .white)
        
        btnPlayGame.setBackgroundImage(UtilitieWebService.shareSingleton()?.getImageFromStringBase(img_bg_bt_success), for: .normal)

        addSubview(labelTitle)
        addSubview(lbTitleAccount)
        addSubview(lbTitlePhone)
        addSubview(lbTitleEmail)
        addSubview(lbAccount)
        addSubview(lbPhone)
        addSubview(lbEmail)
        addSubview(btnPlayGame)
    }
    
    
    
    func setupIcon() -> UIImageView {
        let imageView = UIImageView()
        imageView.image = UtilitieWebService.shareSingleton()?.getImageFromStringBase(img_icon_done)
        return imageView
    }
    
    func setupButton(title: String? = nil,
                     titleSelected: String? = nil,
                     image: UIImage? = nil,
                     imageSelected: UIImage? = nil,
                     titleColor: UIColor = .white) -> UIButton {
        let button = UIButton()
        button.setTitleColor( titleColor, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 11)
        button.setTitle(title, for: .normal)
        button.setTitle(titleSelected, for: .selected)
        button.setImage(image, for: .normal)
        button.setImage(imageSelected, for: .selected)
        button.clipsToBounds = false

        return button
    }
    
    func setupLabel(_ title: String = "", textColor: UIColor = .black,
                    font: UIFont = UIFont.systemFont(ofSize: 12),
                    bgColor: UIColor = .clear,
                    alignment: NSTextAlignment = .left) -> UILabel {
        
        let label = UILabel()
        label.text = title
        label.textColor = textColor
        label.backgroundColor = bgColor
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = alignment
        label.font = font
        return label
    }
    
    func setupView(_ bgColor: UIColor = .lightGray) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        view.clipsToBounds = true
        return view
    }
}


