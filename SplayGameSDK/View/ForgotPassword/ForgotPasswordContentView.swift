//
//  ForgotPasswordContentView.swift
//  SplayGameSDK
//
//  Created by Nguyen Nang on 7/14/20.
//  Copyright © 2020 splay. All rights reserved.
//


import UIKit


class ForgotPasswordContentView: UIView {
    
    enum Size: CGFloat {
        case padding10 = 10, button = 36, label = 30
    }

    

    var lbTitle: UILabel!
    var btnMobile: RadioButton!
    var tfFourEndNumber: UITextField!
    var lbFourEndNumber: UILabel!
    var lbOr: UILabel!
    var btnEmail: RadioButton!
    var btnBack: UIButton!
    var btnNext: UIButton!
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupAllSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        setupAllSubviews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        setupFrame()
    }
    
    
    func setupFrame() {
        
        
        btnBack.frame = CGRect(x: bounds.width / 2 - Size.padding10.rawValue / 2 - bounds.width / 3,
                               y: bounds.height - Size.button.rawValue,
                               width: bounds.width / 3,
                               height: Size.button.rawValue)
        
        btnNext.frame = CGRect(x: bounds.width / 2 + Size.padding10.rawValue / 2,
                               y: bounds.height - Size.button.rawValue,
                               width: bounds.width / 3,
                               height: Size.button.rawValue)
        
        
        lbTitle.frame = CGRect(x: Size.padding10.rawValue,
                                  y: Size.padding10.rawValue,
                                  width: bounds.width - Size.padding10.rawValue * 2,
                                  height: Size.label.rawValue)
        
        let widthButton = bounds.width * 200 / 336
        
        btnMobile.frame = CGRect(x: (bounds.width - widthButton) / 2,
                                 y: lbTitle.frame.maxY + Size.padding10.rawValue ,
                                 width: widthButton,
                                 height: btnMobile.isHidden ? 0 : Size.label.rawValue)
        
        lbFourEndNumber.sizeToFit()
        
        lbFourEndNumber.frame = CGRect(x: btnMobile.frame.minX + Size.padding10.rawValue * 3,
                                      y: btnMobile.frame.maxY,
                                      width: lbFourEndNumber.frame.width,
                                      height: btnMobile.isHidden ? 0 : Size.label.rawValue)
        
        tfFourEndNumber.frame = CGRect(x: lbFourEndNumber.frame.maxX + Size.padding10.rawValue,
                                      y: lbFourEndNumber.frame.minY,
                                      width: 55,
                                      height: btnMobile.isHidden ? 0 : Size.label.rawValue)
        
        lbOr.frame = CGRect(x: Size.padding10.rawValue,
                            y: lbFourEndNumber.frame.maxY,
                            width: bounds.width  - Size.padding10.rawValue * 2,
                            height: btnMobile.isHidden ? 0 : Size.label.rawValue)

        btnEmail.frame = CGRect(x: (bounds.width - widthButton) / 2,
                                y: lbOr.frame.maxY ,
                                width: widthButton,
                                height: btnEmail.isHidden ? 0 : Size.label.rawValue)
        
        
    }
    
}


//--------------------------
// MARK: - PRIVATE METHOD
//--------------------------

extension ForgotPasswordContentView {
    
    func animationWithKeyboard(isShow: Bool, y: CGFloat) {
        
        
        let minY: CGFloat = isShow ? max(tfFourEndNumber.frame.maxY + 5, y) : (bounds.height - Size.button.rawValue)
        UIView.animate(withDuration: 0.3) {  self.btnNext.frame.origin.y = minY;  self.btnBack.frame.origin.y = minY }
    }
    
    
}



//--------------------------
// MARK: - SETUP
//--------------------------

extension ForgotPasswordContentView {
    
    func setupAllSubviews() {
        

        lbTitle = setupLabel(textColor: UIColor.Text.blackMediumColor(),
                             font: UIFont.systemFont(ofSize: 13),
                             alignment: .center)
        
        btnBack = setupButton(title: LanguageSetting.get("title_button_back", alter: nil), titleColor: .white)
        btnBack.setBackgroundImage(UtilitieWebService.shareSingleton()?.getImageFromStringBase(img_icon_back), for: .normal)
        btnNext = setupButton(title: LanguageSetting.get("TitleButtonNext", alter: nil), titleColor: .white)
        btnNext.setBackgroundImage(UtilitieWebService.shareSingleton()?.getImageFromStringBase(dn_bg_bt_li), for: .normal)

        
        lbOr = setupLabel(LanguageSetting.get("TitleOr", alter: nil),
                          textColor: UIColor.Text.blackMediumColor(),
                          font: UIFont.systemFont(ofSize: 13),
                          alignment: .center)
        
        
        lbFourEndNumber = setupLabel(textColor: UIColor.Text.blackMediumColor(),
                               font: UIFont.systemFont(ofSize: 13))
        
        tfFourEndNumber = setupTextField("")
        
        btnEmail = setupButtonRadio(image: UtilitieWebService.shareSingleton()?.getImageFromStringBase(img_qmk_unchecked),
                               imageSelected: UtilitieWebService.shareSingleton()?.getImageFromStringBase(img_qmk_checked),
                               titleColor: UIColor.Text.blackMediumColor(), tag: 1)
        
     
        btnMobile = setupButtonRadio(image: UtilitieWebService.shareSingleton()?.getImageFromStringBase(img_qmk_unchecked),
                               imageSelected: UtilitieWebService.shareSingleton()?.getImageFromStringBase(img_qmk_checked),
                               titleColor: UIColor.Text.blackMediumColor(), tag: 0)
        
        btnEmail.groupButtons = [btnMobile as Any]

        addSubview(lbTitle)
        addSubview(btnBack)
        addSubview(btnNext)
        addSubview(lbOr)
        addSubview(lbFourEndNumber)
        addSubview(btnEmail)
        addSubview(btnMobile)
        addSubview(tfFourEndNumber)
    }
    
    
    func setupTextField(_ placeHoler: String, textInsets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0),
                        isSecureTextEntry: Bool = false) -> DLTextField {
        let textField = DLTextField()
        textField.textColor = .black
        textField.placeholder = placeHoler
        textField.layer.cornerRadius = 3
        textField.layer.borderWidth = 0.5
        textField.layer.borderColor = UIColor.lightGray.cgColor
        textField.layer.masksToBounds = true
        textField.textInsets = textInsets
        textField.rightViewMode = isSecureTextEntry ? .always : .never
        textField.isSecureTextEntry = isSecureTextEntry
        textField.font = UIFont.systemFont(ofSize: 14)
        textField.background = UtilitieWebService.shareSingleton()?.getImageFromStringBase(dn_bg_textfile)
        textField.keyboardType = .numberPad
        textField.returnKeyType = isSecureTextEntry ? .done : .next
        textField.textColor = UIColor.Text.blackMediumColor()
        return textField
    }
    
    func setupButtonRadio(image: UIImage? = nil,
                          imageSelected: UIImage? = nil,
                          titleColor: UIColor = .white, tag: Int) -> RadioButton {
        let button = RadioButton()
        button.setTitleColor( titleColor, for: .normal)
        button.titleLabel?.font = UIFont.italicSystemFont(ofSize: 14)
        button.setImage(image, for: .normal)
        button.setImage(imageSelected, for: .selected)
        button.clipsToBounds = false
        button.tag = tag
        button.imageView?.contentMode = .center
        button.contentHorizontalAlignment = .left
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 2, bottom: 0, right: 0)
        return button
    }
    
    func setupButton(title: String? = nil,
                     titleSelected: String? = nil,
                     image: UIImage? = nil,
                     imageSelected: UIImage? = nil,
                     titleColor: UIColor = .white) -> UIButton {
        let button = UIButton()
        button.setTitleColor( titleColor, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 11)
        button.setTitle(title, for: .normal)
        button.setTitle(titleSelected, for: .selected)
        button.setImage(image, for: .normal)
        button.setImage(imageSelected, for: .selected)
        button.clipsToBounds = false

        return button
    }
    
    func setupLabel(_ title: String = "", textColor: UIColor = .black,
                    font: UIFont = UIFont.systemFont(ofSize: 12),
                    bgColor: UIColor = .clear,
                    alignment: NSTextAlignment = .left) -> UILabel {
        
        let label = UILabel()
        label.text = title
        label.textColor = textColor
        label.backgroundColor = bgColor
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = alignment
        label.font = font
        return label
    }
    
    func setupView(_ bgColor: UIColor = .lightGray) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        view.clipsToBounds = true
        return view
    }
}



