//
//  ActiveRegisterContentView.swift
//  SplayGameSDK
//
//  Created by Nguyen Nang on 7/15/20.
//  Copyright © 2020 splay. All rights reserved.
//

import UIKit
import OTPFieldView

class ActiveRegisterContentView: UIView {
    
    enum Size: CGFloat {
        case padding10 = 8, button = 36
    }
    
    
    var lbSendMessage: UILabel!
    var btnSendMessage: UIButton!
    var lbMessager: UILabel!
    
//    var otpStackView: OTPStackView!
    var otpStackView: OTPFieldView!

    var btnConfirm: UIButton!
    var btnBack: UIButton!
    
    
//    var btnGetOTP: UIButton!
//    var lbGetOTP: UIButton!

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupAllSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        setupAllSubviews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        setupFrame()
    }

    
    func setupFrame() {
        
        
        if btnBack.isHidden {
            
            btnConfirm.frame = CGRect(x: bounds.width / 3,
                                      y: bounds.height - Size.button.rawValue,
                                      width: bounds.width / 3,
                                      height: Size.button.rawValue)
            
        } else {
            
            
            btnBack.frame = CGRect(x: bounds.width / 2 - bounds.width / 3 - Size.padding10.rawValue,
                                      y: bounds.height - Size.button.rawValue,
                                      width: bounds.width / 3,
                                      height: Size.button.rawValue)
            
            
            btnConfirm.frame = CGRect(x: bounds.width / 2 + Size.padding10.rawValue / 2,
                                      y: bounds.height - Size.button.rawValue,
                                      width: bounds.width / 3,
                                      height: Size.button.rawValue)
            
        }
        

        
        lbSendMessage.frame = CGRect(x:  Size.padding10.rawValue * 3 / 2,
                                     y:  Size.padding10.rawValue * 2,
                                     width: bounds.width - Size.padding10.rawValue * 3,
                                     height: CGFloat.leastNonzeroMagnitude)
        
        lbSendMessage.sizeToFit()
        
        lbSendMessage.frame = CGRect(x:  Size.padding10.rawValue * 3 / 2,
                                     y:  Size.padding10.rawValue * 2,
                                     width: bounds.width - Size.padding10.rawValue * 3,
                                     height: lbSendMessage.frame.height)
        
        
        btnSendMessage.sizeToFit()
        
        let widthButton = min(btnSendMessage.frame.width + 20,
                              bounds.width - Size.padding10.rawValue * 2)
        
        btnSendMessage.frame = CGRect(x: (bounds.width - widthButton) / 2,
                                      y: lbSendMessage.frame.maxY + Size.padding10.rawValue / 2,
                                      width: widthButton,
                                      height: 30)
        
        lbMessager.frame = CGRect(x:  Size.padding10.rawValue * 3 / 2,
                                  y: btnSendMessage.frame.maxY + Size.padding10.rawValue / 2,
                                  width: bounds.width - Size.padding10.rawValue * 3,
                                  height: CGFloat.leastNonzeroMagnitude)
        
        lbMessager.sizeToFit()
        
//        let widthOTP: CGFloat = 40 * 6 + 5 * 5
//
////        otpStackView.frame = CGRect(x: (bounds.width - widthOTP) / 2,
////                                    y: lbMessager.frame.maxY + Size.padding10.rawValue * 2,
////                                    width: widthOTP,
////                                    height: btnConfirm.frame.minY - lbMessager.frame.maxY - Size.padding10.rawValue * 2)
//
//
//        otpStackView.frame = CGRect(x: (bounds.width - widthOTP) / 2,
//                                    y: lbMessager.frame.maxY + Size.padding10.rawValue * 2,
//                                    width: widthOTP,
//                                    height: 45)
        
        let widthOTP: CGFloat = 40 * 6 + 8 * 5
        
        otpStackView.frame = CGRect(x: (bounds.width - widthOTP) / 2,
                                    y: lbMessager.frame.maxY + Size.padding10.rawValue * 2,
                                    width: widthOTP,
                                    height: 45)

        otpStackView.initializeUI()
    }
    
}

//--------------------------
// MARK: - PRIVATE METHOD
//--------------------------

extension ActiveRegisterContentView {
    
    func animationWithKeyboard(isShow: Bool, y: CGFloat) {
        
        
        let minY: CGFloat = isShow ? max(otpStackView.frame.maxY + 5, y) : (self.frame.height - Size.button.rawValue)
        UIView.animate(withDuration: 0.3) {  self.btnConfirm.frame.origin.y = minY;  self.btnBack.frame.origin.y = minY }
    }
    
}


//--------------------------
// MARK: - SETUP
//--------------------------

extension ActiveRegisterContentView {
    
    func setupAllSubviews() {
        
        
        lbSendMessage = setupLabel(textColor: .gray)
        

        btnSendMessage = setupButton(image: UtilitieWebService.shareSingleton()?.getImageFromStringBase(forgot_ic_send_message)?.tint(.white),
                                     titleColor: UIColor.Text.blackMediumColor())
        
        
        
        btnSendMessage.semanticContentAttribute = .forceRightToLeft
        btnSendMessage.layer.cornerRadius = 3
        btnSendMessage.imageView?.contentMode = .scaleAspectFit
        btnSendMessage.imageEdgeInsets = UIEdgeInsets(top: 6, left: 12, bottom: 6, right: -6)
        btnSendMessage.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -12)
//        btnSendMessage.layer.borderWidth = 1 / UIScreen.main.scale
//        btnSendMessage.layer.borderColor = UIColor.lightGray.cgColor
        btnSendMessage.backgroundColor = UIColor.Navigation.mainColor()
        
        lbMessager = setupLabel(LanguageSetting.get("confirm_otp_register", alter: nil),
                                textColor: .gray)
        
        otpStackView = setupOTPView()
        
//        btnGetOTP = setupButton()
//
//        lbGetOTP = setupLabel(LanguageSetting.get("get_verification_code_again", alter: nil),
//                               textColor: .red)
        
        btnConfirm = setupButton(title: LanguageSetting.get("title_button_confirm", alter: nil), titleColor: .white)
        btnConfirm.setBackgroundImage(UtilitieWebService.shareSingleton()?.getImageFromStringBase(dn_bg_bt_li), for: .normal)
        btnConfirm.contentHorizontalAlignment = .center
        
        btnBack = setupButton(title: LanguageSetting.get("title_button_back", alter: nil), titleColor: .white)
        btnBack.setBackgroundImage(UtilitieWebService.shareSingleton()?.getImageFromStringBase(img_icon_back), for: .normal)
        btnBack.contentHorizontalAlignment = .center
  
        
        addSubview(lbSendMessage)
        addSubview(btnSendMessage)
        addSubview(lbMessager)
        addSubview(otpStackView)
//        addSubview(lbGetOTP)
//        addSubview(btnGetOTP)
        addSubview(btnConfirm)
        addSubview(btnBack)
        
        btnBack.isHidden = true

    }
    
//    func setupOTPView() -> OTPStackView {
//        let view = OTPStackView()
//
//
//        return view
//    }
    
    func setupOTPView() -> OTPFieldView {
        let view = OTPFieldView()
        
        view.fieldsCount = 6
        view.fieldBorderWidth = 2
        view.defaultBorderColor = UIColor.lightGray
        view.filledBorderColor = UIColor.Navigation.subColor()
        view.cursorColor = UIColor.Navigation.mainColor()
        view.displayType = .underlinedBottom
        view.fieldSize = 40
        view.separatorSpace = 8
        view.shouldAllowIntermediateEditing = false
        view.fieldFont = UIFont.systemFont(ofSize: 32)
        view.errorBorderColor = UIColor.red.alpha(0.7)
        
        
        return view
    }

    func setupButton(title: String? = nil,
                     titleSelected: String? = nil,
                     image: UIImage? = nil,
                     imageSelected: UIImage? = nil,
                     titleColor: UIColor = .white) -> UIButton {
        let button = UIButton()
        button.setTitleColor( titleColor, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 11)
        button.setTitle(title, for: .normal)
        button.setTitle(titleSelected, for: .selected)
        button.setImage(image, for: .normal)
        button.setImage(imageSelected, for: .selected)
        button.clipsToBounds = false
        button.contentHorizontalAlignment = .right
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        return button
    }
    
    func setupLabel(_ title: String = "", textColor: UIColor = .black,
                    font: UIFont = UIFont.systemFont(ofSize: 12),
                    bgColor: UIColor = .clear,
                    alignment: NSTextAlignment = .center) -> UILabel {
        
        let label = UILabel()
        label.text = title
        label.textColor = textColor
        label.backgroundColor = bgColor
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = alignment
        label.font = font
        return label
    }
    
    func setupView(_ bgColor: UIColor = .lightGray) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        view.clipsToBounds = true
        return view
    }
}

